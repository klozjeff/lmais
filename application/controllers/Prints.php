<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Prints extends CI_Controller {

    public function __construct() {
        parent::__construct();
		 $this->load->library("Pdf");
		 $this->load->model('SellerModel', '', TRUE);
		$this->load->model('BuyerModel', '', TRUE);
		  $this->load->model('ConsentModel', '', TRUE);
		  $this->load->model('CautionModel', '', TRUE);
		  $this->load->model('PropertyModel', '', TRUE);
		  $this->load->model('encumbranceModel', '', TRUE);
		  $this->load->model('proprietorModel', '', TRUE);
    }

 
	function PrintTitle() {
        $this->load->view('Prints/PrintTitle');
    }
	
	function PrintCautionWithdrawal() {
        $this->load->view('Prints/Caution_withdrawal');
    }
	
	function PrintCaution($transfer_id) {
		$data['transfer_id'] = $transfer_id;
        $this->load->view('Prints/caution', $data);
    }
	
	function RLA_Transfer($transfer_id) {
		$data['transfer_id'] = $transfer_id;
        $this->load->view('Prints/RLA_Transfer', $data);
    }
	
	function RLA_Charge($transfer_id) {
		$data['transfer_id'] = $transfer_id;
        $this->load->view('Prints/RLA_Charge', $data);
    }
	
	function Consent_Application_Doc($transfer_id) {
		$data['transfer_id'] = $transfer_id;
        $this->load->view('Prints/Consent_Application-Doc', $data);
    }
	function Correction_Of_Name() {
        $this->load->view('Prints/Correction_Of_Name');
    }

	function Official_Search($DocID) {
	$data['DocID'] = $DocID;
        $this->load->view('Prints/Official_Search', $data);
    }

function convertNumberToWord($num = false)
{
    $num = str_replace(array(',', ' '), '' , trim($num));
    if(! $num) {
        return false;
    }
    $num = (int) $num;
    $words = array();
    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
    );
    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
    );
    $num_length = strlen($num);
    $levels = (int) (($num_length + 2) / 3);
    $max_length = $levels * 3;
    $num = substr('00' . $num, -$max_length);
    $num_levels = str_split($num, 3);
    for ($i = 0; $i < count($num_levels); $i++) {
        $levels--;
        $hundreds = (int) ($num_levels[$i] / 100);
        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ( $hundreds == 1 ? '' : 's' ) . ' ' : '');
        $tens = (int) ($num_levels[$i] % 100);
        $singles = '';
        if ( $tens < 20 ) {
            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
        } else {
            $tens = (int)($tens / 10);
            $tens = ' ' . $list2[$tens] . ' ';
            $singles = (int) ($num_levels[$i] % 10);
            $singles = ' ' . $list1[$singles] . ' ';
        }
        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
    } //end for loop
    $commas = count($words);
    if ($commas > 1) {
        $commas = $commas - 1;
    }
    return implode(' ', $words);
}
}
