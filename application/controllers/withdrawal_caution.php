<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Withdrawal_Caution extends CI_Controller {

    private $service_slug = 'withdrawalofcaution';
	private $parent_service_slug = 'caution';
    private $service_payable = array(
		array(
            'slug' => 'withdrawalofcaution',
            'db_field' => 'CautionFee',
        ),
        array(
            'slug' => 'convenience',
            'db_field' => 'ConvenienceFee',
        ),
		
    );

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('TenureModel', '', TRUE);
        $this->load->model('Countymodel', '', TRUE);
        $this->load->model('Registrymodel', '', TRUE);
        $this->load->model('ParcelsValuationModel', '', TRUE);
        $this->load->model('Countrymodel', '', TRUE);
          $this->load->model('CautionModel', '', TRUE);
          $this->load->model('withdrawal_of_cautionmodel', '', TRUE);
        // $this->load->model('Settings_Model', '', TRUE);
        // $this->load->model('proprietorModel', '', TRUE);
        // $this->load->model('encumbranceModel', '', TRUE);
    }


    public function payment_completed() {
        $data = $this->input->get();
        //Load Ecitizen
        $this->load->library('ecitizen');
        if (array_key_exists('bill_ref', $data)) {
			
			//Get RowID from returened Bill Ref
			$RowID=explode('_',$data['bill_ref']);
			$RowID=$RowID[1];
		    
			//Slug Details eg ServiceName,ServiceAmount
			$withdraw_caution_model = $this->db
                                ->select('*')
                                ->where('Slug', $this->service_slug)
                                ->get('lmais_landservicetypes')->row();
								
			//Caution withdrawn(paid) Details					
            $caution_model = $this->db
                            ->select('*')
                            ->where('RowID', $RowID)
                            ->get('lmais_caution')->row();

           //if caution withdrawn exists in the DB
            if ($caution_model) {
                $ecitizen_response = $this->ecitizen->query_transaction_status($data['bill_ref']);
                if ($ecitizen_response['status']) {
                    $amount_paid = $ecitizen_response['message']['amount'];
					$AccountNumber= $ecitizen_response['message']['account_number'];

                    $now = date('Y-m-d H:i:s');

                    $transaction_model = $this->db
                                    ->select('*')
                                    ->where('ServiceID', $caution_model->RowID)
                                    ->where('ServiceSlug', $this->service_slug)
                                    ->where('Type', 'payment')
                                    ->get('lmais_transactions')->row();

                    if (!$transaction_model) {//Payment does not exist so create one
                        //Rent Due Payment
                        $payment_data = array(
                            'AccountNumber' => $ecitizen_response['message']['account_number'], //CHANGE - Addition
                            'Currency' => 'KES',
                            'Date' => $now,
                            'Type' => 'payment',
                            'Description' => $withdraw_caution_model->ServiceName,
                            'Amount' => $amount_paid,
                            'ServiceSlug' => $this->service_slug,
                            'ServiceID' => $caution_model->RowID,
                            'UserID' => $caution_model->UserID,
                            'TransactionID' => $caution_model->TransactionID,
                            'DocID' => $caution_model->DocID,
                            'Status' => 'paid',
                            'Ip' => 'paid',
                            'Agent' => $_SERVER['HTTP_USER_AGENT'],
                            'Ip' => $_SERVER['REMOTE_ADDR'],
                        );

                        // var_dump($payment_data);
                        // die("SAd");
                        $this->db->insert('lmais_transactions', $payment_data);
                    }
					
					//E# if else statement
                    //Get total payments
                    $total_payment = $this->db
                                    ->select_sum('Amount')
                                    ->where('ServiceID', $caution_model->RowID)
                                    ->where('ServiceSlug', $this->service_slug)
                                    ->where('Type', 'payment')
                                    ->get('lmais_transactions')->row();

                    //Get total invoices
                    $total_invoice = $this->db
                                    ->select_sum('Amount')
                                    ->where('ServiceID', $caution_model->RowID)
                                    ->where('ServiceSlug', $this->service_slug)
                                    ->where('Type', 'invoice')
                                    ->get('lmais_transactions')->row();

                    if (($total_payment->Amount == $total_invoice->Amount) || ($total_payment->Amount > $total_invoice->Amount)) {

                        $caution_data = array(
                            'WithdrawalPaymentStatus' => 'paid',
							'WithdrawalEcitizenBillRef' =>$data['bill_ref'],
							'ApprovalStatus'=>'Withdrawn',
                        );

                        $this->db->where('RowID', $caution_model->RowID);

                        $this->db->update('lmais_caution', $caution_data);

                        $received_request_data = array(
                            'Status' => 'withdrawn',
                        );

                        $this->db->where('TransactionID', $caution_model->TransactionID);

                        $this->db->update('lmais_receivedrequests', $received_request_data);
                    }
					
					
                    //Get user model
                    $user_model = $this->db
                                    ->select('*')
                                    ->where('RowID', $caution_model->UserID)
                                    ->get('lmais_users')->row();

                    if ($user_model) {
                        //Send login email
                        $parameters = array(
                            'name' => ucwords(strtolower($user_model->FirstName . ' ' . $user_model->LastName)),
                            'currency' => 'KES',
                            'service' => $withdraw_caution_model->ServiceName,
                            'amount' => $amount_paid,
                            'time' => date('d/m/Y H:i'),
                            'refnumber' => $caution_model->TransactionID,
                        );

                        $this->load->library('message');

                        if ($user_model->Email) {
                            $recipient['to']['email'] = $user_model->Email;
                            $recipient['to']['name'] = $parameters['name'];

                            //Send email
                            $sent = $this->message->email(1, array('name' => 'LMAIS', 'email' => 'info@ardhi.go.ke'), 1, $recipient, 'payment_received', 'en', $parameters);
                        }//E# statement

                        if ($user_model->Phone) {
                            $this->message->sms(1, 1, 1, $user_model->Phone, 'payment_received', 'en', $parameters);
                        }//E# statement
                    }//E# statement
					
                    //Clear all consent session
                    $this->session->unset_userdata('sessioned_caution');
                    $this->session->unset_userdata('sessioned_buyers');
                    $this->session->unset_userdata('sessioned_rla');

                    $this->session->set_flashdata('paymentmsg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>

						<strong>Success!</strong> Thanks for making the payment</div>');
                } else {

                }//E# if else statement
            }//E# if else statement
        }//E# if else statement

       redirect('withdrawal_caution/cautionlist');
    }

//E# payment_completed() function

    public function test_email() {
        //Send login email
        $this->load->library('message');

        $this->message->send_email();
    }


    public function get_user_by_id() {
        //Load Ecitizen
        $this->load->library('ecitizen');

        $ecitizen_response = $this->ecitizen->get_user_by_id_number($this->input->post('idNumber'));

        //var_dump($ecitizen_response);
        $response['message'] = $ecitizen_response['message'];
        $response['type'] = $ecitizen_response['status'] ? 'success' : 'error';

        echo json_encode($response);
    }

    public function index() {
        if ($this->session->userdata('Logged_in') != '') {

            self::loadindex();
        } else
            $this->load->view('signin/PageResources/loginheader');
        $this->load->view('signin/login');
    }

    private function raise_invoices($sessioned_caution,$transaction_id) {

        $doc_id = $sessioned_caution['DocID'];

        $invoice_data = array();

        $now = date('Y-m-d H:i:s');

        foreach ($this->service_payable as $single_service_payable) {
            //Service model
            $service_model = $this->db
                            ->select('*')
                            ->where('Slug',$single_service_payable['slug'])
                            ->get('lmais_landservicetypes')->row();
							/*var_dump($service_model);
								exit();*/

            if ($service_model){
                //Invoice model
                $invoice_model = $this->db
                                ->select('*')
                                ->where('ServiceID', $sessioned_caution['RowID'])
                                ->where('ServiceTypeID', $service_model->RowID)
								->where('ServiceSlug', $single_service_payable['slug'])
                                ->get('lmais_transactions')->row();

								
              if (!$invoice_model) {
                    //Consent Fee Invoice
                    $invoice_data[] = array(
                        'Agent' => $_SERVER['HTTP_USER_AGENT'],
                        'Ip' => $_SERVER['REMOTE_ADDR'],
                        'Currency' => 'KES',
                        'Date' => $now,
                        'Type' => 'invoice',
                        'Description' => $service_model->ServiceName,
                        'ServiceTypeID' => $service_model->RowID,
                        'DocID' => $doc_id,
                        'Amount' => $service_model->ServiceAmount,
                        'ServiceSlug' => $this->service_slug,
                        'ServiceID' => $sessioned_caution['RowID'],
                        'Status' => 'unpaid',
                        'UserID' => $this->session->userdata('UserID'),
                        'TransactionID' => $transaction_id,
                    );
                }//E# if statement
            }//E# if statement
		}//End Foreach
       

        if ($invoice_data) {
            $this->db->insert_batch('lmais_transactions', $invoice_data);
        }//E# if statement
    }


    public function payment() {
        $sessioned_caution = $this->session->userdata('sessioned_caution');

        $received_request_model = $this->db
                        ->select('*')
                        ->order_by('RowID', 'desc')
                        ->where('ServiceSlug',$this->parent_service_slug)
                        ->where('ServiceID', $sessioned_caution['RowID'])
                        ->get('lmais_receivedrequests')->row();
						

        if (!$received_request_model) {
            $received_request_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->get('lmais_receivedrequests')->row();

            $serial = $received_request_model ? $received_request_model->TransactionID : 'TRA00D0C1A';

            $serial++;
            $generated_serial = $serial++;

            if ($this->session->has_userdata('sessioned_rla')) {
                $doc_id = $this->session->userdata('sessioned_rla')->DocID;
            } else {
                $doc_id = '';
            }//E# if else statement

            $received_request_data = array(
                'BuyerUserID' => $this->session->userdata('UserID'),
                'TransactionID' => $generated_serial,
                'Status' => 'unpaid',
                'CompletionStage' => 0,
                'ServiceSlug' => $this->parent_service_slug,
                'ServiceID' => $sessioned_caution['RowID'],
                'ServiceTypeID' => 1,
                'DocID' => $doc_id,
                'LandUse' => 1,
                'Status' => 'Pending',
                'DateCreated' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('lmais_receivedrequests', $received_request_data);
        } else {
            $generated_serial = $received_request_model->TransactionID;
        }//E# if else statement
        //Update buyers with the transaction id
        $seller_data = array(
            'TransactionID' => $generated_serial,
        );

        $sessioned_caution = $this->session->userdata('sessioned_caution');

        $this->db->where('ServiceID', $sessioned_caution['RowID']);
        $this->db->where('ServiceSlug', $this->parent_service_slug);

        $this->db->update('lmais_sellers', $seller_data);

        //Create invoices
        $this->raise_invoices($sessioned_caution, $generated_serial);
    /*
        $caution_data = array(
            'TransactionID' => $generated_serial
        );

        $this->db->where('RowID', $sessioned_caution['RowID']);

        $this->db->update('lmais_caution', $caution_data);

       // return $serial;

        //Clear all caution session
      $this->session->unset_userdata('sessioned_caution');
      $this->session->unset_userdata('sessioned_rent');
      $this->session->unset_userdata('sessioned_buyers');
      $this->session->unset_userdata('sessioned_rla');

       // redirect('withdrawal_caution/withdraw_caution?step=7&status=done');
	   */
    }

//E# payment() function

//Paid & Registered Caution Lists-for further actions as withdrawal
public function cautionlist()
{
  if ($this->session->userdata('Logged_in') != '') {

      $Department = $this->session->userdata('UserType');
	  $data['ServiceSlug']=$this->service_slug;
      $data['cautions'] = $this->withdrawal_of_cautionmodel->getcautionlists(29);
	  

      $this->load->view($Department . '/PageResources/addparcelsheader');
      $this->load->view($Department . '/Header/header');
      if ($Department == 'user') {

      } else {
          $this->load->view($Department . '/Menu/menu');
      }//E# if else statement


      $this->load->view('caution/caution_withdrawal', $data);

}
}

//Load withdraw of caution page
Public function forminfo($transfer_id, $ServiceID) 
{

	  if ($this->session->userdata('Logged_in') != '') {
            $FormView;
			$DBView;

            $sql = "SELECT * FROM lmais_landservicetypes where Slug = '$ServiceID'";

            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $row) {
					if($row->Slug=='withdrawalofcaution'){
						$DBView='caution';
					}
					 else{
						$DBView=$row->Slug;
					 }
                    $FormView = $row->Slug;
                }
            }
            $transfer_model = $this->db
                            ->select('*')
                            ->where('RowID', $transfer_id)
                            ->get('lmais_' . $DBView)->result_array();

            $data['transfer_model'] = $transfer_model ? $transfer_model[0] : null;
            $data['transfer_id'] = $transfer_id;
			$balance = $this->get_balance($transfer_id);
			$this->session->set_userdata('sessioned_balance', $balance);
            $this->session->set_userdata('sessioned_action', 'updating');
           

            $data['sessioned_action'] = $this->session->userdata('sessioned_action');
            $data['sessioned_balance'] = $this->session->userdata('sessioned_balance');
			$data['bal']=$balance;
			$transfer_id;
			//check if invoice raised
			 $invoice_model = $this->db
                                ->select('*')
                                ->where('ServiceID', $transfer_id)
								->where('ServiceSlug', $ServiceID)
                                ->get('lmais_transactions')->num_rows();
								//if invoice doesnt exists create 
								if($invoice_model<1):
								//Generate invoice on page Load
										$this->session_caution($transfer_id);
										$this->payment();
								endif;
								//EndIf
								
			//End check invoice raised

            $Department = $this->session->userdata('UserType');
            $this->load->view($Department . '/PageResources/dashboardheader');
            $this->load->view($Department . '/Header/header');
            $this->load->view($Department . '/Pages/' . $FormView, $data);
        }
}


	//Withdrawal of caution:Review & Payment 
public function withdraw_caution() {
	
        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');
            $data['input'] = $this->input->get();

            if (!array_key_exists('step', $data['input'])) {
                $data['input']['step'] = 1;
            }//E# if statement

            if ($data['input']['step'] == 1) {
                //Withdrawal_Caution model
                $data['withdraw_caution_model'] = $this->db
                                ->select('*')
                                ->where('Slug', 'withdrawalofcaution')
                                ->get('lmais_landservicetypes')->row();
								
            }//E# if statement
			
			
			
            $balance = 0;
            $action = 'creating';

            if (array_key_exists('reference_number', $data['input'])) {
                $this->session_caution($data['input']['reference_number']);
                $this->session->set_userdata('action', 'updating');
                $balance = $this->get_balance($data['input']['reference_number']);

                $this->session->set_userdata('sessioned_balance', $balance);
                $this->session->set_userdata('sessioned_action', 'updating');
            }//E# if else statement

            $data['sessioned_action'] = $this->session->userdata('sessioned_action');
            $data['sessioned_balance'] = $this->session->userdata('sessioned_balance');
		

            if ($this->session->has_userdata('sessioned_caution')) {
                $data['sessioned_caution'] = $this->session->userdata('sessioned_caution');
            } else {
                $data['sessioned_caution'] = false;
                if ($data['input']['step'] > 1) {
                    redirect('withdrawal_caution/withdraw_caution');
                }//E# if statement
            }//E# if else statement

            
            if ($this->session->userdata()) {
                $data['sessioned_user'] = $this->session->userdata();
            } else {
                $data['sessioned_user'] = false;
            }//E# if else statement
			
            $this->load->view($Department . '/PageResources/addparcelsheader');
            $this->load->view($Department . '/Header/header');
            if ($Department == 'user') {
                
            } else {
                $this->load->view($Department . '/Menu/menu');
            }//E# if else statement


            $this->load->view('caution/withdraw_caution', $data);
        }
    }
	
	
    public function save_transaction_details() {

        $caution_data = array(
            'CautionInterest' => $this->input->post('CautionInterest'),
            'CautionDescription' => $this->input->post('CautionDescription'),
        );

        $sessioned_caution = $this->session->userdata('sessioned_caution');

        $this->db->where('RowID', $sessioned_caution['RowID']);

        $this->db->update('lmais_caution', $caution_data);

        $this->session_caution($sessioned_caution['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>

                        <strong>Success!</strong> Transaction details saved successfully</div>');

        redirect('caution/apply_caution?step=4');
    }

    public function save_applicant() {

        $caution_data = array(
            'ProprietorFirstName' => $this->input->post('ProprietorFirstName'),
            'ProprietorMiddleName' => $this->input->post('ProprietorMiddleName'),
            'ProprietorLastName' => $this->input->post('ProprietorLastName'),
            'ProprietorNationality' => $this->input->post('ProprietorNationality'),
            'ProprietorIdentification' => $this->input->post('ProprietorIdentification'),
            'ProprietorMobileNumber' => $this->input->post('ProprietorMobileNumber'),
            'ProprietorEmailAddress' => $this->input->post('ProprietorEmailAddress'),
            'ProprietorAddress' => $this->input->post('ProprietorAddress'),
        );

        $sessioned_caution = $this->session->userdata('sessioned_caution');

        $this->db->where('RowID', $sessioned_caution['RowID']);

        $this->db->update('lmais_caution', $caution_data);

        $this->session_caution($sessioned_caution['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>

                        <strong>Success!</strong> Applicant\'s details saved successfully</div>');

        redirect('caution/apply_caution?step=3');
    }

    public function rate_clearance() {
        $config['upload_path'] = './files/storage_files';
        $config['allowed_types'] = 'gif|jpg|png|pdf';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('clearance_certificate')) {
            $error = array('error' => $this->upload->display_errors());

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error!</strong> ' . trim($error['error']) . '</div>');

            redirect('caution/apply_caution?step=2');
        } else {

            $file_data = $this->upload->data();

            $now = date('Y-m-d H:i:s');

            $sessioned_caution = $this->session->userdata('sessioned_caution');

            $filestorage_data = array(
                'CreatedBy' => $this->session->userdata('UserID'),
                'DateCreated' => $now,
                'DateModified' => $now,
                'FileName' => $file_data['file_name'],
                'FileType' => $file_data['file_type'],
                'FileSize' => $file_data['file_size'],
                'FileExt' => $file_data['file_ext'],
                'IsImage' => $file_data['is_image'],
                'FileStage' => '',
                'ModifiedBy' => $this->session->userdata('UserID'),
                'OriginalFilename' => $file_data['orig_name'],
                'ServiceID' => 'caution',
                'ServiceTypeID' => $sessioned_caution['RowID']
            );

            $this->db->insert('lmais_filestorage', $filestorage_data);

            $data = array('upload_data' => $file_data);

            $caution_data = array(
                'CautionDocument' => $data['upload_data']['file_name'],
                'CautionInterest' => $this->input->post('CautionInterest'),
                'CautionDescription' => $this->input->post('CautionDescription'),
            );

            $sessioned_caution = $this->session->userdata('sessioned_caution');

            $this->db->where('RowID', $sessioned_caution['RowID']);

            $this->db->update('lmais_caution', $caution_data);

            $this->session_caution($sessioned_caution['RowID']);

            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>

                        <strong>Success!</strong> Thanks you for uploading caution document</div>');

            redirect('caution/apply_caution?step=3');
        }//E# if else statement

        die();
    }

    public function caution_clearance() {

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>

                        <strong>Success!</strong> Thanks you for clearing your Caution Fees</div>');
        redirect('caution/apply_caution?step=7');
    }
	
	
	#Sessioning of Caution 
    private function session_caution($caution_id, $session_sellers = false) {

        $caution_model = $this->db->get_where('lmais_caution', array('RowID' => $caution_id))->result_array();

        $this->session->set_userdata('sessioned_caution', $caution_model[0]);

        if ($session_sellers == 'sellers') {
            //Select all sellers
            $seller_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ServiceID', $caution_model[0]['RowID'])
                            ->where('ServiceSlug', $this->service_slug)
                            ->get('lmais_sellers')->result_array();


            $this->session->set_userdata('sessioned_sellers', $seller_model);
        }

        if ($session_sellers == 'sellers') {
            //Select all sellers
            $seller_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ServiceID', $caution_model[0]['RowID'])
                            ->where('ServiceSlug', $this->service_slug)
                            ->get('lmais_sellers')->result_array();


            $this->session->set_userdata('sessioned_sellers', $seller_model);
        }
    }
	

    public function save_seller_continue() {
        $sessioned_sellers = $this->session->userdata('sessioned_sellers');

        if ($sessioned_sellers) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>

                        <strong>Success!</strong> caution applicant\'s details saved successfully</div>');

            redirect('caution/apply_caution?step=3');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>

                        <strong>Error!</strong> Please add at least one caution applicant</div>');

            redirect('caution/apply_caution?step=2');
        }//E# if else statement
    }

	
    public function save_seller() {

        if ($this->input->post('IsContactPerson') == 'Y') {
            $caution_data = array(
                'IsContactPerson' => 'N',
            );
            $this->db->where('ServiceID', $this->input->post('CautionID'));
            $this->db->where('ServiceSlug', 'caution');

            $this->db->update('lmais_sellers', $caution_data);
        }//E# if statement
        $seller_data = array(
            'IsContactPerson' => $this->input->post('IsContactPerson'),
            'UserID' => $this->session->userdata('UserID'),
            'ServiceID' => $this->input->post('CautionID'),
            'FirstName' => $this->input->post('FirstName'),
            'MiddleName' => $this->input->post('MiddleName'),
            'LastName' => $this->input->post('LastName'),
            'CountryID' => $this->input->post('CountryID'),
            'Identification' => $this->input->post('Identification'),
            'MobileNumber' => $this->input->post('MobileNumber'),
            'EmailAddress' => $this->input->post('EmailAddress'),
            'Address' => $this->input->post('Address'),
            'CreatedBy' => $this->session->userdata('UserID'),
            'DateCreated' => date('Y-m-d H:i:s'),
            'ModifiedBy' => $this->session->userdata('UserID'),
            'DateModified' => date('Y-m-d H:i:s'),
            'Type' => 'individual',
            'ServiceSlug' => 'caution',
        );

        $this->db->insert('lmais_sellers', $seller_data);

        $seller_id = $this->db->insert_id();

        $sessioned_caution = $this->session->userdata('sessioned_caution');

        $this->session_caution($sessioned_caution['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>

                        <strong>Success!</strong> Caution Applicant\'s details saved successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/caution/apply_caution?step=2'
        ));
    }



    /**
     * S# get_balance() function
     *
     * Get balance
     *
     * @param int $caution_id Caution ID
     *
     * @return float Balance
     */
    private function get_balance($caution_id) {
        //Get total invoice
        $total_invoice = $this->db
                        ->select_sum('Amount')
                        ->where('ServiceID', $caution_id)
                        ->where('ServiceSlug', $this->service_slug)
                        ->where('Type', 'invoice')
                        ->get('lmais_transactions')->row();

        //Get total payment
        $total_payment = $this->db
                        ->select_sum('Amount')
                        ->where('ServiceID', $caution_id)
                        ->where('ServiceSlug', $this->service_slug)
                        ->where('Type', 'payment')
                        ->get('lmais_transactions')->row();

        return $total_invoice->Amount - $total_payment->Amount;
    }

//E# get_balance() function

    public function apply_caution() {

        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');
            $data['tenure'] = $this->TenureModel->GetAllTenure();
            $data['input'] = $this->input->get();

            if (!array_key_exists('step', $data['input'])) {
                $data['input']['step'] = 1;
            }//E# if statement

            if ($data['input']['step'] == 1) {
                //Convenience model
                $data['convenience_model'] = $this->db
                                ->select('*')
                                ->where('Slug', 'convenience')
                                ->get('lmais_landservicetypes')->row();
            }//E# if statement

            $balance = 0;
            $action = 'creating';

            if (array_key_exists('reference_number', $data['input'])) {
                $this->session_caution($data['input']['reference_number']);
                $this->session->set_userdata('action', 'updating');
                $balance = $this->get_balance($data['input']['reference_number']);

                $this->session->set_userdata('sessioned_balance', $balance);
                $this->session->set_userdata('sessioned_action', 'updating');
            }//E# if else statement

            $data['sessioned_action'] = $this->session->userdata('sessioned_action');
            $data['sessioned_balance'] = $this->session->userdata('sessioned_balance');

            if ($this->session->has_userdata('sessioned_caution')) {
                $data['sessioned_caution'] = $this->session->userdata('sessioned_caution');
            } else {
                $data['sessioned_caution'] = false;
                if ($data['input']['step'] > 1) {
                    redirect('caution/apply_caution');
                }//E# if statement
            }//E# if else statement

            if ($this->session->has_userdata('sessioned_sellers')) {
                $data['sessioned_sellers'] = $this->session->userdata('sessioned_sellers');
            } else {
                $data['sessioned_sellers'] = false;
            }//E# if else statement
            if ($this->session->userdata()) {
                $data['sessioned_user'] = $this->session->userdata();
            } else {
                $data['sessioned_user'] = false;
            }//E# if else statement
            //Review
            if ($data['input']['step'] == 3) {
                $registry_model = $this->db
                                ->select('*')
                                ->where('RowID', $data['sessioned_caution']['RegistryID'])
                                ->get('lmais_registry')->row();

                $data['registry_model'] = $registry_model ? $registry_model : null;

                $registration_section_model = $this->db
                                ->select('*')
                                ->where('RowID', $data['sessioned_caution']['RegistrationSectionID'])
                                ->get('lmais_registrationsections')->row();

                $data['registration_section_model'] = $registration_section_model ? $registration_section_model : null;

                $tenure_model = $this->db
                                ->select('*')
                                ->where('RowID', $data['sessioned_caution']['TenureID'])
                                ->get('lmais_tenure')->row();

                $data['tenure_model'] = $tenure_model ? $tenure_model : null;
            }//E# if statement

            $this->load->view($Department . '/PageResources/addparcelsheader');
            $this->load->view($Department . '/Header/header');
            if ($Department == 'user') {

            } else {
                $this->load->view($Department . '/Menu/menu');
            }//E# if else statement


            $this->load->view('caution/caution', $data);
        }
    }

    public function caution_app() {
        $CautionID = "CNT" . md5(time() . mt_rand(1, 1000000));
        $Proprietor_First_Name = $this->input->post("ProprietorFirstName");
        $Proprietor_Middle_Name = $this->input->post("ProprietorMiddleName");
        $Proprietor_Last_Name = $this->input->post("ProprietorLastName");
        $Proprietor_Nationality = $this->input->post("ProprietorNationality");
        $Proprietor_Idenification = $this->input->post("ProprietorIdenification");
        $Proprietor_Mobile_Number = $this->input->post("ProprietorMobileNumber");
        $Proprietor_Email_Address = $this->input->post("ProprietorEmailAddress");
        $Proprietor_Address = $this->input->post("ProprietorAddress");
        $Proposed_First_Name = $this->input->post("ProposedFirstName");
        $Proposed_Middle_Name = $this->input->post("ProposedMiddleName");
        $Proposed_Last_Name = $this->input->post("ProposedLastName");
        $Proposed_Nationality = $this->input->post("ProposedNationality");
        $Proposed_Identification = $this->input->post("ProposedIdentification");
        $Proposed_Mobile_Number = $this->input->post("ProposedMobileNumber");
        $Proposed_Email_Address = $this->input->post("ProposedEmailAddress");
        $Proposed_Address = $this->input->post("ProposedAddress");
        $Caution_Interest = $this->input->post("CautionInterest");
        $Caution_Description = $this->input->post("CautionDescription");
        $LRNumber = $this->input->post("LRNumber");
        $AreaSize = $this->input->post("AreaSize");
        $Registry = $this->input->post("Registry");
        $County = $this->input->post("County");
        $CreatedBy = $this->session->userdata('RowID');

        $data = array(
            'Registry' => $Registry,
            'LRNO' => $LRNO,
            'AreaSize' => $AreaSize,
            'AreaUnit' => $AreaUnit,
            'Tenure' => $Tenure,
            'RegisteredProprietor' => $RegisteredProprietor,
            'TypeOfDevelopment' => $TypeOfDevelopment,
            'NearbyInfrastructure' => $NearbyInfrastructure,
            'NearbyValue' => $NearbyValue,
            'TypeOfRoad' => $TypeOfRoad,
            'NameOfRoad' => $NameOfRoad,
            'DistanceFromRoad' => $DistanceFromRoad,
            'TypeOfTown' => $TypeOfTown,
            'NameOfTown' => $NameOfTown,
            'DistanceFromTown' => $DistanceFromTown,
            'CreatedBy' => $CreatedBy,
        );
        $usr_result = $this->CautionModel->apply_caution($data);

        //check if username and password is correct
        if ($usr_result == TRUE) { //active user record is present
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                              <a href="#" class="close" data-dismiss="alert">&times;</a>

                            <strong>Success!</strong> You have successfully Added new Parcel!</div>');

            redirect('start/addvaluationdata');
        } else if ($usr_result == FALSE) {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                             <a href="#" class="close" data-dismiss="alert">&times;</a>
                              <strong>Error!</strong> Data are not saved.. Try again</div>');

            redirect('start/addvaluationdata');
        }
    }

    public function delete_seller() {
        $sellerID = $this->input->post('SellerID');

        $this->db->where('RowID', $sellerID);
        $this->db->delete('lmais_sellers');

        $sessioned_caution = $this->session->userdata('sessioned_caution');

        $this->session_caution($sessioned_caution['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>

                        <strong>Success!</strong>Caution Applicant\'s details deleted successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/caution/apply_caution?step=2'
        ));
    }

    public function review_application() {

        $this->payment();

        redirect('caution/apply_caution?step=4');
    }

//E# delete_seller() function

    function GetRegistriesMatch($ActID, $CountyID) {
        //$ActID = $this->uri->segment(3);
        //$CountyID = $this->uri->segment(4);
        $Registries = $this->Registrymodel->GetMatchedRegistries($ActID, $CountyID);

        return $Registries;
    }

    public function clear_all_cache() {

        $CI = & get_instance();
        $path = $CI->config->item('cache_path');

        $cache_path = ($path == '') ? APPPATH . 'cache/' : $path;

        $handle = opendir($cache_path);
        while (($file = readdir($handle)) !== FALSE) {
            //Leave the directory protection alone
            if ($file != '.htaccess' && $file != 'index.html') {
                @unlink($cache_path . '/' . $file);
            }
        }
        closedir($handle);


        if ($handle = opendir($cache_path)) {
            //echo "Directory handle: $handle <br /><br/>";

            while (false !== ($entry = readdir($handle))) {
                //echo $entry."<br />";
                $n = basename($entry);
                //echo "name = ".$n."<br />";
                //echo "length of name = ".strlen($n)."<br />";
                if (strlen($n) >= 32) {
                    //echo "file name's 32 chars long <br />";
                    $p = APPPATH . 'cache/' . $entry;
                    //echo $p;
                    unlink($p);
                }
                //echo "<br />";
            }
            closedir($handle);
        }
    }

}
