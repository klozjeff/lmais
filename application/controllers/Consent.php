<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Picqer\Barcode\BarcodeGeneratorHTML;

class Consent extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('actsModel', '', TRUE);
        $this->load->model('Countymodel', '', TRUE);
        $this->load->model('Registrymodel', '', TRUE);
        $this->load->model('ParcelsValuationModel', '', TRUE);
        $this->load->model('Countrymodel', '', TRUE);
        $this->load->library("Pdf");
        // $this->load->model('Settings_Model', '', TRUE);
        // $this->load->model('proprietorModel', '', TRUE);
        // $this->load->model('encumbranceModel', '', TRUE);
    }

    /**
     * S# calculateRent() function
     * 
     * Calculate rent
     * 
     * @param str $lr_number LR Number
     * 
     * @return array
     * 
     */
    public function calculateRent($lr_number) {
        $data = array();
        $data['total'] = $data['interest'] = $data['principal'] = $rent = $principal = 0;
        //$lr_number = 'Nairobi/block106/515';

        $this->db->where('LRNumber', $lr_number);
        $rent_model = $this->db->get('lmais_rent_data')->row();

        $now = new \Datetime('now');

        if ($rent_model) {
            $interest = 0;
            $principal = $rent_model->Amount;
            $data['rent_amount'] = $principal;
            //Arrears from
            $arrears_from = ($rent_model->InArrearsSince == '0000-00-00 00:00:00') ? $rent_model->WithEffectFrom : $rent_model->InArrearsSince;

            $date = new \DateTime();
            $date->setDate(substr(($arrears_from + 1), 0, 4), 1, 1);

            $data['in_arrears_from'] = $date;
            //$date = date_create_from_format('Y-m-d', substr($arrears_from, 0, 10));
            //$start = new DateTime('2009-02-01');
            $interval = new DateInterval('P1M');

            echo '<p>';
            var_dump($interval);
            echo '<p>';
            //$end = new DateTime('2011-01-01');
            $period = new DatePeriod($date, $interval, $now);

            var_dump($period);
            echo '<p>Effective from: ' . $date->format('Y-m-d') . '<p>';
            echo 'Now: ' . $now->format('Y-m-d') . '<p>';
            $index = 0;
            foreach ($period as $dt) {
                if ($dt->format('m') == 1) {
                    $principal += $rent_model->Amount;
                    $description = $dt->format('Y') . ' land rent ' . number_format($rent_model->Amount, 2);
                    $amount = $rent_model->Amount;
                } else {
                    $month_interest = $this->calculateInterest($rent_model->Amount, 1);
                    echo $dt->format('F Y') . ': Interest: ' . $month_interest;

                    $interest += $month_interest;
                    echo '<p>';
                    $description = $dt->format('M-Y') . ' 1% penalty on ' . number_format($rent_model->Amount, 2);
                    $amount = $month_interest;
                }//E# if statement

                $data['items'][] = array(
                    'description' => $description,
                    'amount' => $amount,
                );
                $index++;
            }//E# foreach statement

            $principal -= $rent_model->Amount;
            $data['principal'] = $principal;
            $data['interest'] = $interest;

            echo '<p>Months ' . $index;
            echo '<p>Total principal ' . $principal . '<p>';
            echo '<p>Total Interest ' . $interest . '<p>';

            $data['total'] = $principal + $interest;
        } else {
            
        }//E# if else statement

        return $data;
    }

//E# calculateRent() function

    /**
     * S# calculateInterest() function
     * 
     * Calculate Interest
     * 
     * @param float $principal Principal
     * @param float $rate Rate
     * 
     * @return float
     */
    private function calculateInterest($principal, $rate) {
        $interest = ($principal * $rate) / 100;
        return $interest;
    }

//E# calculateInterest() function

    /**
     * S# save_advocate_details() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Save advocates details
     * 
     */
    public function save_advocate_details() {
        $advocate_data = array(
            'AdvocateFirstName' => $this->input->post('AdvocateFirstName'),
            'AdvocateMiddleName' => $this->input->post('AdvocateMiddleName'),
            'AdvocateLastName' => $this->input->post('AdvocateLastName'),
            'AdvocatePinNumber' => $this->input->post('AdvocatePinNumber'),
            'AdvocatePostalAddress' => $this->input->post('AdvocatePostalAddress'),
            'AdvocateEmail' => $this->input->post('AdvocateEmail'),
            'AdvocatePhone' => $this->input->post('AdvocatePhone'),
            'AdvocateLawFirm' => $this->input->post('AdvocateLawFirm'),
            'AdvocatePFNumber' => $this->input->post('AdvocatePFNumber'),
            'AdvocateIDNumber' => $this->input->post('AdvocateIDNumber'),
            'AdvocateCountryID' => $this->input->post('AdvocateCountryID'),
        );

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->db->where('RowID', $sessioned_consent['RowID']);

        $this->db->update('lmais_transfer', $advocate_data);

        $this->session_consent($sessioned_consent['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Advocates details saved successfully</div>');

        redirect('consent/apply_consent?step=5');
    }

//S# save_advocate_details() function

    /**
     * S# delete_buyer() function 
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Delete buyer
     * 
     */
    public function delete_buyer() {
        $buyerID = $this->input->post('BuyerID');

        $this->db->where('RowID', $buyerID);
        $this->db->delete('lmais_buyers');

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->session_consent($sessioned_consent['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Buyer\'s details deleted successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/consent/apply_consent?step=3'
        ));
    }

//E# delete_buyer() function

    /**
     * S# delete_seller() function 
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Delete seller
     * 
     */
    public function delete_seller() {
        $sellerID = $this->input->post('SellerID');

        $this->db->where('RowID', $sellerID);
        $this->db->delete('lmais_sellers');

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->session_consent($sessioned_consent['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Seller\'s details deleted successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/consent/apply_consent?step=2'
        ));
    }

//E# delete_seller() function

    function PrintCaution() {
        $this->load->view('consent/forms/caution');
    }

    function PrintTitle() {
        $this->load->view('PrintTitle');
    }

    function PrintCautionWithdrawal() {
        $this->load->view('Caution_withdrawal');
    }

    function RLA_Transfer() {
        $this->load->view('RLA_Transfer');
    }

    function Correction_Of_Name() {
        $this->load->view('Correction_Of_Name');
    }

    /**
     * S# payment_completed() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Payment completed
     * 
     */
    public function payment_completed() {
        $data = $this->input->get();

        //Load Ecitizen
        $this->load->library('ecitizen');

        if (array_key_exists('bill_ref', $data)) {

            $transfer_model = $this->db
                            ->select('*')
                            ->where('RowID', $data['bill_ref'])
                            ->get('lmais_transfer')->row();

            if ($transfer_model) {

                $ecitizen_response = $this->ecitizen->query_transaction_status($data['bill_ref']);


                if ($ecitizen_response['status']) {

                    $amount_paid = $ecitizen_response['message']['amount'];

                    $now = date('Y-m-d H:i:s');

                    $transaction_model = $this->db
                                    ->select('*')
                                    ->where('ServiceID', $data['bill_ref'])
                                    ->where('ServiceType', 'transfer')
                                    ->get('lmais_transactions')->row();


                    if (!$transaction_model) {//Payment does not exist so create one
                        //Rent Due Payment
                        $payment_data = array(
                            'Currency' => 'KES',
                            'Date' => $now,
                            'Type' => 'payment',
                            'Description' => 'Transfer Payment',
                            'Amount' => $amount_paid,
                            'ServiceType' => 'transfer',
                            'ServiceID' => $transfer_model->RowID,
                            'UserID' => $transfer_model->UserID,
                            'TransactionID' => $transfer_model->TransactionID,
                            'DocID' => $transfer_model->DocID,
                        );

                        $this->db->insert('lmais_transactions', $payment_data);
                    }//E# if else statement
                    //Get total payments
                    $total_payment = $this->db
                                    ->select_sum('Amount')
                                    ->where('ServiceID', $data['bill_ref'])
                                    ->where('ServiceType', 'transfer')
                                    ->where('Type', 'payment')
                                    ->get('lmais_transactions')->row();

                    //Get total invoices
                    $total_invoice = $this->db
                                    ->select_sum('Amount')
                                    ->where('ServiceID', $data['bill_ref'])
                                    ->where('ServiceType', 'transfer')
                                    ->where('Type', 'invoice')
                                    ->get('lmais_transactions')->row();


                    if (($total_payment->Amount == $total_invoice->Amount) || ($total_payment->Amount > $total_payment->Amount)) {

                        $consent_data = array(
                            'PaymentStatus' => 'Paid',
                        );

                        $this->db->where('RowID', $this->input->get('bill_ref'));

                        $this->db->update('lmais_transfer', $consent_data);
                    }//E# statement
                    //Get total invoices
                    $user_model = $this->db
                                    ->select('*')
                                    ->where('RowID', $transfer_model->UserID)
                                    ->get('lmais_users')->row();

                    if ($user_model) {

                        $consent_data = array(
                            'PaymentStatus' => 'Paid',
                        );

                        $this->db->where('RowID', $this->input->get('bill_ref'));

                        $this->db->update('lmais_transfer', $consent_data);


                        //Send login email
                        $parameters = array(
                            'name' => ucwords(strtolower($user_model->FirstName . ' ' . $user_model->LastName)),
                            'currency' => 'KES',
                            'service' => 'Land Transfer',
                            'amount' => $amount_paid,
                            'time' => date('d/m/Y H:i'),
                            'refnumber' => $transfer_model->TransactionID,
                        );

                        $this->load->library('message');

                        if ($user_model->Email) {
                            $recipient['to']['email'] = $user_model->Email;
                            $recipient['to']['name'] = $parameters['name'];

                            //Send email
                            $sent = $this->message->email(1, array('name' => 'LMAIS', 'email' => 'info@ardhi.go.ke'), 1, $recipient, 'payment_received', 'en', $parameters);
                        }//E# statement


                        if ($user_model->Phone) {
                            $this->message->sms(1, 1, 1, $user_model->Phone, 'payment_received', 'en', $parameters);
                        }//E# statement
                    }//E# statement
                    //Clear all consent session
                    $this->session->unset_userdata('sessioned_consent');
                    $this->session->unset_userdata('sessioned_rent');
                    $this->session->unset_userdata('sessioned_buyers');
                    $this->session->unset_userdata('sessioned_sellers');
                    $this->session->unset_userdata('sessioned_rla');

                    $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Thanks making the payment</div>');
                } else {
                    
                }//E# if else statement
            }//E# if else statement
        }//E# if else statement

        redirect('UserView/LoadApplications');
    }

//E# payment_completed() function

    public function test_qrcode() {

        //Send login email
        $this->load->library('barcode');

        echo $this->barcode->bar_code('sapama1', 'html');
    }

    public function test_email() {
        //Send login email
        $this->load->library('message');

        $this->message->send_email();
    }

    public function test_invoice() {
        //Load Ecitizen
        $this->load->library('ecitizen');

        $parameters = array(
            'apiClientID' => 'TYV0FUD0BY',
            'secureHash' => 'TYV0FUD0BY',
            'billDesc' => 'TYV0FUD0BY',
            'billRefNumber' => 'TYV0FUD0BY',
            'currency' => 'TYV0FUD0BY',
            'serviceID' => '29',
            'clientMSISDN' => 'TYV0FUD0BY',
            'clientName' => 'TYV0FUD0BY',
            'clientIDNumber' => 'TYV0FUD0BY',
            'clientEmail' => 'TYV0FUD0BY',
            'callBackURLOnSuccess' => 'TYV0FUD0BY',
            'pictureURL' => 'TYV0FUD0BY',
            'notificationURL' => 'TYV0FUD0BY',
            'amountExpected' => 'TYV0FUD0BY',
        );
        $ecitizen_response = $this->ecitizen->raise_invoice($parameters);

        var_dump($ecitizen_response);
    }

    /**
     * S# get_user_by_id() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * get user by id
     * 
     */
    public function get_user_by_id() {
        //Load Ecitizen
        $this->load->library('ecitizen');

        $ecitizen_response = $this->ecitizen->get_user_by_id_number($this->input->post('idNumber'));

        //var_dump($ecitizen_response);
        $response['message'] = $ecitizen_response['message'];
        $response['type'] = $ecitizen_response['status'] ? 'success' : 'error';

        echo json_encode($response);
    }

//E# get_user_by_id() function

    public function index() {
        if ($this->session->userdata('Logged_in') != '') {

            self::loadindex();
        } else
            $this->load->view('signin/PageResources/loginheader');
        $this->load->view('signin/login');
    }

    /**
     * S# raise_invoices() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Raise invoices
     * 
     */
    private function raise_invoices($sessioned_consent, $transaction_id) {
        //Delete any invoices
        $this->db->where('ServiceID', $sessioned_consent['RowID']);
        $this->db->where('ServiceType', 'transfer');
        $this->db->where('Type', 'invoice');

        $transaction_data = array(
            'Deleted' => 1,
        );

        $this->db->update('lmais_transactions', $transaction_data);

        $doc_id = $this->session->userdata('sessioned_rla')->DocID;

        $invoice_data = array();

        $now = date('Y-m-d H:i:s');

        if ($sessioned_consent['Rent']) {
            //Rent Due Invoice
            $invoice_data[] = array(
                'Agent' => $_SERVER['HTTP_USER_AGENT'],
                'Ip' => $_SERVER['REMOTE_ADDR'],
                'Currency' => 'KES',
                'Date' => $now,
                'Type' => 'invoice',
                'Description' => 'Rent Due',
                'DocID' => $doc_id,
                'Amount' => $sessioned_consent['Rent'],
                'ServiceType' => 'transfer',
                'ServiceID' => $sessioned_consent['RowID'],
                'Status' => 'unpaid',
                'UserID' => $this->session->userdata('UserID'),
                'TransactionID' => $transaction_id,
            );
        }//E# if statement
        //Consent Fee Invoice
        $invoice_data[] = array(
            'Agent' => $_SERVER['HTTP_USER_AGENT'],
            'Ip' => $_SERVER['REMOTE_ADDR'],
            'Currency' => 'KES',
            'Date' => $now,
            'Type' => 'invoice',
            'Description' => 'Consent Fee',
            'DocID' => $doc_id,
            'Amount' => 1000,
            'ServiceType' => 'transfer',
            'ServiceID' => $sessioned_consent['RowID'],
            'Status' => 'unpaid',
            'UserID' => $this->session->userdata('UserID'),
            'TransactionID' => $transaction_id,
        );

        //Convinience Fee
        $invoice_data[] = array(
            'Currency' => 'KES',
            'Date' => $now,
            'Type' => 'invoice',
            'Description' => 'Convinience Fee',
            'DocID' => $doc_id,
            'Amount' => 50,
            'ServiceType' => 'transfer',
            'ServiceID' => $sessioned_consent['RowID'],
            'Status' => 'unpaid',
            'UserID' => $this->session->userdata('UserID'),
            'TransactionID' => $transaction_id,
        );

        $this->db->insert_batch('lmais_transactions', $invoice_data);
    }

//E# raise_invoices() function

    /**
     * S# payment() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * payment
     */
    public function payment() {

        $create_request = false;

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $received_request_model = $this->db
                        ->select('*')
                        ->order_by('RowID', 'desc')
                        ->where('ServiceType', 'transfer')
                        ->where('ServiceID', $sessioned_consent['RowID'])
                        ->get('lmais_receivedrequests')->row();


        if (!$received_request_model) {

            $create_request = true;

            $received_request_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ServiceType', 'transfer')
                            ->limit(1)
                            ->get('lmais_receivedrequests')->row();

            $serial = $received_request_model ? $received_request_model->TransactionID : 'TRA00D0C1A';

            //$serial = 'TRA00D0C1A';
            $serial++;
            $generated_serial = $serial++;

            if ($this->session->has_userdata('sessioned_rla')) {
                $doc_id = $this->session->userdata('sessioned_rla')->DocID;
            } else {
                $doc_id = '';
            }//E# if else statement

            $received_request_data = array(
                'BuyerUserID' => $this->session->userdata('UserID'),
                'TransactionID' => $generated_serial,
                'PaymentStatus' => 0,
                'CompletionStage' => 0,
                'ServiceType' => 'transfer',
                'ServiceID' => $sessioned_consent['RowID'],
                'ServiceTypeID' => 1,
                'DocID' => $doc_id,
                'LandUse' => 1,
                'Status' => 'Pending',
                'DateCreated' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('lmais_receivedrequests', $received_request_data);
        } else {
            $generated_serial = $received_request_model->TransactionID;
        }//E# if else statement
        //Update buyers with the transaction id
        $buyer_data = array(
            'TransactionID' => $generated_serial,
        );

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->db->where('ServiceID', $sessioned_consent['RowID']);
        $this->db->where('ServiceType', 'transfer');

        $this->db->update('lmais_buyers', $buyer_data);

        //Update buyers with the transaction id
        $seller_data = array(
            'TransactionID' => $generated_serial,
        );

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->db->where('ServiceID', $sessioned_consent['RowID']);
        $this->db->where('ServiceType', 'transfer');

        $this->db->update('lmais_sellers', $seller_data);

        //Create invoices
        $this->raise_invoices($sessioned_consent, $generated_serial);

        $consent_data = array(
            'TransactionID' => $generated_serial
        );

        $this->db->where('RowID', $sessioned_consent['RowID']);

        $this->db->update('lmais_transfer', $consent_data);

        return $serial;

        //Clear all consent session
        $this->session->unset_userdata('sessioned_consent');
        $this->session->unset_userdata('sessioned_rent');
        $this->session->unset_userdata('sessioned_buyers');
        $this->session->unset_userdata('sessioned_rla');

        redirect('consent/apply_consent?step=7&status=done');
    }

//E# payment() function

    /**
     * S# save_transaction_details() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Save transaction details
     * 
     */
    public function save_transaction_details() {

        $consent_data = array(
            'TransactionNature' => $this->input->post('TransactionNature'),
            'TransactionDescription' => $this->input->post('TransactionDescription'),
            'TransactionTerm' => $this->input->post('TransactionTerm'),
            'TransactionTermPeriod' => $this->input->post('TransactionTermPeriod'),
            'TypeOfTransaction' => $this->input->post('TypeOfTransaction'),
            'TransactionAmount' => $this->input->post('TransactionAmount'),
        );

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->db->where('RowID', $sessioned_consent['RowID']);

        $this->db->update('lmais_transfer', $consent_data);

        $this->session_consent($sessioned_consent['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Transaction details saved successfully</div>');

        redirect('consent/apply_consent?step=6');
    }

//E# save_transaction_details() function

    /**
     * S# save_applicant() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Save applicant
     * 
     */
    public function save_applicant() {

        $consent_data = array(
            'ProprietorFirstName' => $this->input->post('ProprietorFirstName'),
            'ProprietorMiddleName' => $this->input->post('ProprietorMiddleName'),
            'ProprietorLastName' => $this->input->post('ProprietorLastName'),
            'ProprietorNationality' => $this->input->post('ProprietorNationality'),
            'ProprietorIdentification' => $this->input->post('ProprietorIdentification'),
            'ProprietorMobileNumber' => $this->input->post('ProprietorMobileNumber'),
            'ProprietorEmailAddress' => $this->input->post('ProprietorEmailAddress'),
            'ProprietorAddress' => $this->input->post('ProprietorAddress'),
        );

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->db->where('RowID', $sessioned_consent['RowID']);

        $this->db->update('lmais_transfer', $consent_data);

        $this->session_consent($sessioned_consent['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Applicant\'s details saved successfully</div>');

        redirect('consent/apply_consent?step=3');
    }

//E# save_applicant() function

    /**
     * S# rate_clearance() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Rate clearance
     */
    public function rate_clearance() {
        $config['upload_path'] = './files/storage_files';
        $config['allowed_types'] = 'gif|jpg|png|pdf';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('clearance_certificate')) {
            $error = array('error' => $this->upload->display_errors());

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						<strong>Error!</strong> ' . trim($error['error']) . '</div>');

            redirect('consent/apply_consent?step=7');
        } else {

            $file_data = $this->upload->data();

            $now = date('Y-m-d H:i:s');

            $sessioned_consent = $this->session->userdata('sessioned_consent');

            $filestorage_data = array(
                'CreatedBy' => $this->session->userdata('UserID'),
                'DateCreated' => $now,
                'DateModified' => $now,
                'FileName' => $file_data['file_name'],
                'FileType' => $file_data['file_type'],
                'FileSize' => $file_data['file_size'],
                'FileExt' => $file_data['file_ext'],
                'IsImage' => $file_data['is_image'],
                'FileStage' => '',
                'ModifiedBy' => $this->session->userdata('UserID'),
                'OriginalFilename' => $file_data['orig_name'],
                'ServiceID' => 'consent',
                'ServiceTypeID' => $sessioned_consent['RowID']
            );

            $this->db->insert('lmais_filestorage', $filestorage_data);

            $data = array('upload_data' => $file_data);

            $consent_data = array(
                'RateClearanceCertificateFile' => $data['upload_data']['file_name'],
                'RateClearanceNumber' => $this->input->post('RateClearanceNumber'),
            );

            $sessioned_consent = $this->session->userdata('sessioned_consent');

            $this->db->where('RowID', $sessioned_consent['RowID']);

            $this->db->update('lmais_transfer', $consent_data);

            $this->session_consent($sessioned_consent['RowID']);

            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Thanks you for clearing your rates</div>');

            redirect('consent/apply_consent?step=8');
        }//E# if else statement

        die();
    }

//E# rate_clearance() function

    /**
     * S# rent_clearance() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Rent clearance
     * 
     */
    public function rent_clearance() {

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Thanks you for clearing your rent</div>');
        redirect('consent/apply_consent?step=7');
    }

//E# rent_clearance() functions

    /**
     * S# session_consent() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Session consent
     * 
     */
    private function session_consent($consent_id, $session_buyers_sellers = false) {

        $consent_model = $this->db->get_where('lmais_transfer', array('RowID' => $consent_id))->result_array();

        $this->session->set_userdata('sessioned_consent', $consent_model[0]);

        if ($session_buyers_sellers == 'buyers') {
            //Select all buyers
            $buyer_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ServiceID', $consent_model[0]['RowID'])
                            ->where('ServiceType', 'transfer')
                            ->get('lmais_buyers')->result_array();


            $this->session->set_userdata('sessioned_buyers', $buyer_model);
        }

        if ($session_buyers_sellers == 'sellers') {
            //Select all sellers
            $seller_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ServiceID', $consent_model[0]['RowID'])
                            ->where('ServiceType', 'transfer')
                            ->get('lmais_sellers')->result_array();


            $this->session->set_userdata('sessioned_sellers', $seller_model);
        }
    }

//E# session_consent() function

    /**
     * S# save_seller_continue() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Save seller continue
     * 
     */
    public function save_seller_continue() {
        $sessioned_sellers = $this->session->userdata('sessioned_sellers');

        if ($sessioned_sellers) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Seller\'s details saved successfully</div>');

            redirect('consent/apply_consent?step=3');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Please add at least one seller</div>');

            redirect('consent/apply_consent?step=2');
        }//E# if else statement
    }

//E# save_seller_continue() function

    /**
     * S# save_seller() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Save seller
     * 
     */
    public function save_seller() {

        if ($this->input->post('IsContactPerson') == 'Y') {
            $consent_data = array(
                'IsContactPerson' => 'N',
            );
            $this->db->where('ServiceID', $this->input->post('ConsentID'));
            $this->db->where('ServiceType', 'transfer');

            $this->db->update('lmais_sellers', $consent_data);
        }//E# if statement
        $seller_data = array(
            'IsContactPerson' => $this->input->post('IsContactPerson'),
            'UserID' => $this->session->userdata('UserID'),
            'ServiceID' => $this->input->post('ConsentID'),
            'FirstName' => $this->input->post('FirstName'),
            'MiddleName' => $this->input->post('MiddleName'),
            'LastName' => $this->input->post('LastName'),
            'CountryID' => $this->input->post('CountryID'),
            'Identification' => $this->input->post('Identification'),
            'MobileNumber' => $this->input->post('MobileNumber'),
            'EmailAddress' => $this->input->post('EmailAddress'),
            'Address' => $this->input->post('Address'),
            'CreatedBy' => $this->session->userdata('UserID'),
            'DateCreated' => date('Y-m-d H:i:s'),
            'ModifiedBy' => $this->session->userdata('UserID'),
            'DateModified' => date('Y-m-d H:i:s'),
            'Type' => 'individual',
            'ServiceType' => 'transfer',
        );

        $this->db->insert('lmais_sellers', $seller_data);

        $seller_id = $this->db->insert_id();

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->session_consent($sessioned_consent['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Seller\'s details saved successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/consent/apply_consent?step=2'
        ));
    }

//E# save_seller() function

    /**
     * S# save_seller_company() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Save seller company
     * 
     */
    public function save_seller_company() {

        if ($this->input->post('IsContactPerson') == 'Y') {
            $consent_data = array(
                'IsContactPerson' => 'N',
            );
            $this->db->where('ServiceID', $this->input->post('ConsentID'));
            $this->db->where('ServiceType', 'transfer');

            $this->db->update('lmais_sellers', $consent_data);
        }//E# if statement
        $seller_company_data = array(
            'IsContactPerson' => $this->input->post('IsContactPerson'),
            //Contact Person
            'UserID' => $this->session->userdata('UserID'),
            'ServiceID' => $this->input->post('ConsentID'),
            'FirstName' => $this->input->post('FirstName'),
            'MiddleName' => $this->input->post('MiddleName'),
            'LastName' => $this->input->post('LastName'),
            'CountryID' => $this->input->post('CountryID'),
            'MobileNumber' => $this->input->post('MobileNumber'),
            'EmailAddress' => $this->input->post('EmailAddress'),
            //Company
            'CompanyAddress' => $this->input->post('CompanyAddress'),
            'CompanyPinNumber' => $this->input->post('CompanyPinNumber'),
            'CompanyName' => $this->input->post('CompanyName'),
            'CompanyRegistrationNumber' => $this->input->post('CompanyRegistrationNumber'),
            'CompanyPhone' => $this->input->post('CompanyPhone'),
            'CompanyEmail' => $this->input->post('CompanyEmail'),
            'CreatedBy' => $this->session->userdata('UserID'),
            'DateCreated' => date('Y-m-d H:i:s'),
            'ModifiedBy' => $this->session->userdata('UserID'),
            'DateModified' => date('Y-m-d H:i:s'),
            'Type' => 'company',
            'ServiceType' => 'transfer',
        );

        $this->db->insert('lmais_sellers', $seller_company_data);

        $seller_company_id = $this->db->insert_id();

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->session_consent($sessioned_consent['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Company\'s details saved successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/consent/apply_consent?step=2'
        ));
    }

//E# save_seller_company() function

    /**
     * S# save_buyer_continue() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Save buyer continue
     * 
     */
    public function save_buyer_continue() {

        $sessioned_buyers = $this->session->userdata('sessioned_buyers');

        if ($sessioned_buyers) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Buyer\'s details saved successfully</div>');

            redirect('consent/apply_consent?step=4');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Please add at least one buyer</div>');

            redirect('consent/apply_consent?step=3');
        }//E# if else statement
    }

//E# save_buyer_continue() function

    /**
     * S# save_buyer() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Save buyer
     * 
     */
    public function save_buyer() {

        if ($this->input->post('IsContactPerson') == 'Y') {
            $consent_data = array(
                'IsContactPerson' => 'N',
            );
            $this->db->where('ServiceID', $this->input->post('ConsentID'));
            $this->db->where('ServiceType', 'transfer');

            $this->db->update('lmais_buyers', $consent_data);
        }//E# if statement
        $buyer_data = array(
            'IsContactPerson' => $this->input->post('IsContactPerson'),
            'UserID' => $this->session->userdata('UserID'),
            'Share' => $this->input->post('Share'),
            'ServiceID' => $this->input->post('ConsentID'),
            'FirstName' => $this->input->post('FirstName'),
            'MiddleName' => $this->input->post('MiddleName'),
            'LastName' => $this->input->post('LastName'),
            'CountryID' => $this->input->post('CountryID'),
            'Identification' => $this->input->post('Identification'),
            'MobileNumber' => $this->input->post('MobileNumber'),
            'EmailAddress' => $this->input->post('EmailAddress'),
            'Address' => $this->input->post('Address'),
            'CreatedBy' => $this->session->userdata('UserID'),
            'DateCreated' => date('Y-m-d H:i:s'),
            'ModifiedBy' => $this->session->userdata('UserID'),
            'DateModified' => date('Y-m-d H:i:s'),
            'Type' => 'individual',
            'ServiceType' => 'transfer',
        );

        $this->db->insert('lmais_buyers', $buyer_data);

        $buyer_id = $this->db->insert_id();

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->session_consent($sessioned_consent['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Buyer\'s details saved successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/consent/apply_consent?step=3'
        ));
    }

//E# save_buyer() function

    /**
     * S# save_buyer_company() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Save buyer company
     * 
     */
    public function save_buyer_company() {

        if ($this->input->post('IsContactPerson') == 'Y') {
            $consent_data = array(
                'IsContactPerson' => 'N',
            );
            $this->db->where('ServiceID', $this->input->post('ConsentID'));
            $this->db->where('ServiceType', 'transfer');

            $this->db->update('lmais_buyers', $consent_data);
        }//E# if statement
        $buyer_company_data = array(
            'IsContactPerson' => $this->input->post('IsContactPerson'),
            //Contact Person
            'UserID' => $this->session->userdata('UserID'),
            'ServiceID' => $this->input->post('ConsentID'),
            'FirstName' => $this->input->post('FirstName'),
            'MiddleName' => $this->input->post('MiddleName'),
            'LastName' => $this->input->post('LastName'),
            'CountryID' => $this->input->post('CountryID'),
            'MobileNumber' => $this->input->post('MobileNumber'),
            'EmailAddress' => $this->input->post('EmailAddress'),
            //Company
            'Share' => $this->input->post('Share'),
            'CompanyAddress' => $this->input->post('CompanyAddress'),
            'CompanyPinNumber' => $this->input->post('CompanyPinNumber'),
            'CompanyName' => $this->input->post('CompanyName'),
            'CompanyRegistrationNumber' => $this->input->post('CompanyRegistrationNumber'),
            'CompanyPhone' => $this->input->post('CompanyPhone'),
            'CompanyEmail' => $this->input->post('CompanyEmail'),
            'CreatedBy' => $this->session->userdata('UserID'),
            'DateCreated' => date('Y-m-d H:i:s'),
            'ModifiedBy' => $this->session->userdata('UserID'),
            'DateModified' => date('Y-m-d H:i:s'),
            'Type' => 'company',
            'ServiceType' => 'transfer',
        );

        $this->db->insert('lmais_buyers', $buyer_company_data);

        $buyer_id = $this->db->insert_id();

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->session_consent($sessioned_consent['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Buyer\'s details saved successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/consent/apply_consent?step=3'
        ));
    }

//E# save_buyer_company() function

    /**
     * S# check_property_details() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Check if property details exists
     * 
     */
    public function check_property_details() {
        //Clear all consent session
        $this->session->unset_userdata('sessioned_consent');
        $this->session->unset_userdata('sessioned_rent');
        $this->session->unset_userdata('sessioned_buyers');
        $this->session->unset_userdata('sessioned_sellers');
        $this->session->unset_userdata('sessioned_rla');

        $rla_model = $this->db
                        ->select('*')
                        ->where('RegistryID', $this->input->post('RegistryID'))
                        ->where('RegistrationSectionID', $this->input->post('RegistrationSectionID'))
                        ->where('ParcelNo', $this->input->post('ParcelNo'))
                        ->limit(1)
                        ->get('lmais_rla')->row();

        $property_availability = array(
            'Agent' => $_SERVER['HTTP_USER_AGENT'],
            'DateCreated' => date('Y-m-d H:i:s'),
            'Ip' => $_SERVER['REMOTE_ADDR'],
            'ParcelNo' => $this->input->post('ParcelNo'),
            'RegistryID' => $this->input->post('RegistryID'),
            'RegistrationSectionID' => $this->input->post('RegistrationSectionID'),
            'NatureOfTitle' => $this->input->post('NatureOfTitle'),
            'Status' => 0,
            'ServiceSlug' => $this->service_slug
        );

        if ($rla_model) {

            $property_availability['Status'] = 1;

            $this->session->set_userdata('sessioned_rla', $rla_model);

            $consent_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->limit(1)
                            ->get('lmais_transfer')->row();

            $serial = 'G4AX2A';
            if ($consent_model) {
                $serial = $consent_model->ConsentID;
            }//E# if statement

            $serial++;
            $generated_serial = $serial++;

            $rent_data = $this->calculateRent($rla_model->Title);

            $consent_data = array(
                'UserID' => $this->session->userdata('UserID'),
                'ConsentID' => $generated_serial,
                'RegistryID' => $this->input->post('RegistryID'),
                'RegistrationSectionID' => $this->input->post('RegistrationSectionID'),
                'ParcelNumber' => $this->input->post('ParcelNo'),
                'DocID' => $rla_model->DocID,
                'LRNumber' => $rla_model->Title,
                'Act' => $this->input->post('NatureOfTitle'),
                'Description' => 'Transfer Consent',
                'PaymentStatus' => 'Unpaid',
                'Rate' => '0',
                'Rent' => $rent_data['total'],
                'RentInterest' => $rent_data['interest'],
                'RentPrincipal' => $rent_data['principal'],
                'RentData' => json_encode($rent_data),
            );

            $this->db->insert('lmais_transfer', $consent_data);

            $consent_id = $this->db->insert_id();

            //Select all rent
            $rent_model = $this->db
                            ->select('*')
                            ->where('LRNumber', $rla_model->Title)
                            ->where('Status', 'N')
                            ->get('lmais_rent')->result_array();

            if ($rent_model) {
                $unpaid_rent = 0;
                foreach ($rent_model as $single_rent) {
                    $unpaid_rent +=$single_rent['Amount'];
                }//E# foreach statement

                $consent_data = array(
                    'Rent' => $unpaid_rent,
                );

                //var_dump($single_rent)
                //dd("sa");
                $this->db->where('RowID', $consent_id);

                $this->db->update('lmais_transfer', $consent_data);
            }//E# if else statement

            $this->session->set_userdata('sessioned_rent', $rent_model);

            $this->session_consent($consent_id);

            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> We have found your land</div>');
            $redirect_to = 'consent/apply_consent?step=2';
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						<strong>Error!</strong> This parcel of land could not be found. Kindly confirm your details</div>');

            $redirect_to = 'consent/apply_consent';
        }//E# if statement

        $this->db->insert('lmais_property_availability', $property_availability);

        redirect($redirect_to);
    }

//E# check_property_details() functions

    /**
     * S# review_application() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Review application
     * 
     */
    public function review_application() {
        //Raise invoices
        $this->payment();

        redirect('consent/apply_consent?step=9');
    }

//E# review_application() function

    /**
     * S# () function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Apply consent
     * 
     * @return view Consent view
     */
    public function apply_consent() {
        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');
            $data['acts'] = $this->actsModel->GetAllActs();
            $data['input'] = $this->input->get();

            if (!array_key_exists('step', $data['input'])) {
                $data['input']['step'] = 1;
            }//E# if else statement

            if ($this->session->has_userdata('sessioned_consent')) {
                $data['sessioned_consent'] = $this->session->userdata('sessioned_consent');
            } else {
                $data['sessioned_consent'] = false;
                if ($data['input']['step'] > 1) {
                    redirect('consent/apply_consent');
                }//E# if statement
            }//E# if else statement

            if ($this->session->has_userdata('sessioned_rent')) {
                $data['sessioned_rent'] = $this->session->userdata('sessioned_rent');
            } else {
                $data['sessioned_rent'] = false;
            }//E# if else statement

            if ($this->session->has_userdata('sessioned_buyers')) {
                $data['sessioned_buyers'] = $this->session->userdata('sessioned_buyers');
            } else {
                $data['sessioned_buyers'] = false;
            }//E# if else statement

            if ($this->session->has_userdata('sessioned_sellers')) {
                $data['sessioned_sellers'] = $this->session->userdata('sessioned_sellers');
            } else {
                $data['sessioned_sellers'] = false;
            }//E# if else statement
            if ($this->session->userdata()) {
                $data['sessioned_user'] = $this->session->userdata();
            } else {
                $data['sessioned_user'] = false;
            }//E# if else statement

            $this->load->view($Department . '/PageResources/addparcelsheader');
            $this->load->view($Department . '/Header/header');
            if ($Department == 'user') {
                
            } else {
                $this->load->view($Department . '/Menu/menu');
            }//E# if else statement
            $this->load->view('consent/consent_application', $data);
        }
    }

//E# apply_consent() function

    public function consent_app() {
        $ConsentID = "CNT" . md5(time() . mt_rand(1, 1000000));

        $Proprietor_First_Name = $this->input->post("ProprietorFirstName");
        $Proprietor_Middle_Name = $this->input->post("ProprietorMiddleName");
        $Proprietor_Last_Name = $this->input->post("ProprietorLastName");
        $Proprietor_Nationality = $this->input->post("ProprietorNationality");
        $Proprietor_Idenification = $this->input->post("ProprietorIdenification");
        $Proprietor_Mobile_Number = $this->input->post("ProprietorMobileNumber");
        $Proprietor_Email_Address = $this->input->post("ProprietorEmailAddress");
        $Proprietor_Address = $this->input->post("ProprietorAddress");
        $Proposed_First_Name = $this->input->post("ProposedFirstName");
        $Proposed_Middle_Name = $this->input->post("ProposedMiddleName");
        $Proposed_Last_Name = $this->input->post("ProposedLastName");
        $Proposed_Nationality = $this->input->post("ProposedNationality");
        $Proposed_Identification = $this->input->post("ProposedIdentification");
        $Proposed_Mobile_Number = $this->input->post("ProposedMobileNumber");
        $Proposed_Email_Address = $this->input->post("ProposedEmailAddress");
        $Proposed_Address = $this->input->post("ProposedAddress");
        $Nature_Of_Transaction = $this->input->post("TransactionNature");
        $Transaction_Description = $this->input->post("TransactionDescription");
        $Transaction_Term = $this->input->post("TransactionTerm");
        $Transaction_Term_Period = $this->input->post("TransactionTermPeriod");
        $LRNumber = $this->input->post("LRNumber");
        $AreaSize = $this->input->post("AreaSize");
        $Registry = $this->input->post("Registry");
        $County = $this->input->post("County");
        $CreatedBy = $this->session->userdata('RowID');

        $data = array(
            'Registry' => $Registry,
            'LRNO' => $LRNO,
            'AreaSize' => $AreaSize,
            'AreaUnit' => $AreaUnit,
            'NatureOfTitle' => $NatureOfTitle,
            'RegisteredProprietor' => $RegisteredProprietor,
            'TypeOfDevelopment' => $TypeOfDevelopment,
            'NearbyInfrastructure' => $NearbyInfrastructure,
            'NearbyValue' => $NearbyValue,
            'TypeOfRoad' => $TypeOfRoad,
            'NameOfRoad' => $NameOfRoad,
            'DistanceFromRoad' => $DistanceFromRoad,
            'TypeOfTown' => $TypeOfTown,
            'NameOfTown' => $NameOfTown,
            'DistanceFromTown' => $DistanceFromTown,
            'CreatedBy' => $CreatedBy,
        );
        $usr_result = $this->ConsentModel->apply_consent($data);

        //check if username and password is correct
        if ($usr_result == TRUE) { //active user record is present
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> You have successfully Added new Parcel!</div>');

            redirect('start/addvaluationdata');
        } else if ($usr_result == FALSE) {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						 <a href="#" class="close" data-dismiss="alert">&times;</a>
						  <strong>Error!</strong> Data are not saved.. Try again</div>');

            redirect('start/addvaluationdata');
        }
    }

    public function send_reset_password_mail() {

        $email = $this->input->post("Email");
        $existing_user = $this->usermodel->is_email_exists($email);

        //send reset password email if found account with this email
        if ($existing_user) {
            $email_template = $this->Email_templates_model->get_final_template("reset_password");

            $parser_data["ACCOUNT_HOLDER_NAME"] = $existing_user->FirstName . " " . $existing_user->LastName;
            $parser_data["SIGNATURE"] = $email_template->signature;
            $parser_data["SITE_URL"] = base_url();
            $key = encode_id($this->encrypt->encode($existing_user->Email . '|' . (time() + (24 * 60 * 60))), "reset_password");
            $parser_data['RESET_PASSWORD_URL'] = base_url("signin/new_password/" . $key);

            $message = $this->parser->parse_string($email_template->message, $parser_data, TRUE);
            if ($this->send_app_mails($email, $email_template->subject, $message)) {
                echo json_encode(array('success' => true, 'message' => lang("reset_info_send")));
            } else {
                echo json_encode(array('success' => false, 'message' => lang('error_occurred')));
            }
        } else {
            echo json_encode(array("success" => false, 'message' => lang("no_acount_found_with_this_email")));
            return false;
        }
    }

    public function EditValuation($RowID) {
        if (!$RowID) {
            
        } Else
            $data['Results'] = $this->SearchResultsModel->SearchDetails($RowID);

        $this->load->view('Valuation/PageResources/RegisterUserheader');
        $this->load->view('Valuation/Header/header');
        $this->load->view('Valuation/Menu/menu');
        $this->load->view('Valuation/Pages/EditValuation', $data);
    }

    public function UnassignedValuations() {

        $data['Results'] = $this->ParcelsValuationModel->GetUnassignedValuations();

        $this->load->view('Valuation/PageResources/UnassignedValuationsheader');
        $this->load->view('Valuation/Header/header');
        $this->load->view('Valuation/Menu/menu');
        $this->load->view('Valuation/Pages/UnassignedValuations', $data);
    }

    public function EditParcelValuation() {
        $Registry = $this->input->post("Registry");
        $LRNO = $this->input->post("LRNO");
        $AreaSize = $this->input->post("AreaSize");
        $AreaUnit = $this->input->post("AreaUnit");
        $NatureOfTitle = $this->input->post("NatureOfTitle");
        $RegisteredProprietor = $this->input->post("RegisteredProprietor");
        $TypeOfDevelopment = $this->input->post("TypeOfDevelopment");
        $NearbyInfrastructure = $this->input->post("NearbyInfrastructure");
        $NearbyValue = $this->input->post("NearbyValue");
        $LandValue = $this->input->post("LandValue");
        $TypeOfRoad = $this->input->post("TypeOfRoad");
        $NameOfRoad = $this->input->post("NameOfRoad");
        $DistanceFromRoad = $this->input->post("DistanceFromRoad");
        $TypeOfTown = $this->input->post("TypeOfTown");
        $NameOfTown = $this->input->post("NameOfTown");
        $DistanceFromTown = $this->input->post("DistanceFromTown");
        $ModifiedBy = $this->session->userdata('RowID');
        $Email = $this->input->post("Email");
        $Telephone = $this->input->post("Telephone");
        $RowID = $this->input->post("RowID");
        $data = array(
            'Registry' => $Registry,
            'LRNO' => $LRNO,
            'AreaSize' => $AreaSize,
            'AreaUnit' => $AreaUnit,
            'NatureOfTitle' => $NatureOfTitle,
            'RegisteredProprietor' => $RegisteredProprietor,
            'TypeOfDevelopment' => $TypeOfDevelopment,
            'NearbyInfrastructure' => $NearbyInfrastructure,
            'NearbyValue' => $NearbyValue,
            'LandValue' => $LandValue,
            'TypeOfRoad' => $TypeOfRoad,
            'NameOfRoad' => $NameOfRoad,
            'DistanceFromRoad' => $DistanceFromRoad,
            'TypeOfTown' => $TypeOfTown,
            'NameOfTown' => $NameOfTown,
            'DistanceFromTown' => $DistanceFromTown,
            'ModifiedBy' => $ModifiedBy,
            'Telephone' => $Telephone,
            'Email' => $Email
        );
        $usr_result = $this->ParcelsValuationModel->editparcel($RowID, $data);

        //check if username and password is correct
        if ($usr_result == TRUE) { //active user record is present
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> You have successfully edited the parcel!</div>');

            redirect('start/searchdetails/' . $RowID);
        } else if ($usr_result == FALSE) {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						 <a href="#" class="close" data-dismiss="alert">&times;</a>
						  <strong>Error!</strong> Data are not saved.No Changes were made</div>');

            redirect('start/searchdetails/' . $RowID);
        }
    }

    function GetRegistriesMatch($ActID, $CountyID) {
        //$ActID = $this->uri->segment(3);
        //$CountyID = $this->uri->segment(4);
        $Registries = $this->Registrymodel->GetMatchedRegistries($ActID, $CountyID);

        return $Registries;
    }

}
