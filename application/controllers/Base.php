<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Base extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * S# generateUniqueField () function
     * Generate unique field
     * 
     * @param string $field Model field
     * @param int $length Length
     * @param string $case Upper or lower case
     * @return string Field value
     */
    public function generateUniqueField($table, $field, $length, $case = 'lower') {
        //Start with not unique
        $notUnique = true;
        while ($notUnique) {//While till you get the field value is unique
            //Generate value
            $value = \Str::$case(\Str::random($length));

            //Get model by field
            $model = $this->getModelByField($field, $value);

            if (!$model) {//Field Unique
                break;
            }//E# if statement        
        }//E# while statement

        return $value;
    }

}
