<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Caution extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('actsModel', '', TRUE);
        $this->load->model('Countymodel', '', TRUE);
        $this->load->model('Registrymodel', '', TRUE);
        $this->load->model('ParcelsValuationModel', '', TRUE);
        $this->load->model('Countrymodel', '', TRUE);
        // $this->load->model('Settings_Model', '', TRUE);
        // $this->load->model('proprietorModel', '', TRUE);
        // $this->load->model('encumbranceModel', '', TRUE);
    }

    public function payment_completed() {
        $data = $this->input->get();

        if (array_key_exists('bill_ref', $data)) {
            $doc_id = $this->session->userdata('sessioned_rla')->DocID;

            $consent_data = array(
                'PaymentStatus' => 'Paid',
            );

            $sessioned_consent = $this->session->userdata('sessioned_consent');

            $this->db->where('RowID', $this->input->get('bill_ref'));

            $this->db->update('lmais_transfer', $consent_data);

            //Rent Due Payment
            $payment_data = array(
                'Description' => 'Rent Due',
                'DocID' => $doc_id,
                'PaymentDate' => date('Y-m-d H:i:s'),
                'Amount' => $sessioned_consent['Rent'],
                'Note' => 'Paid',
                'Gateway' => 'Ecitizen',
                'ServiceType' => 'Transfer',
                'ServiceID' => $sessioned_consent['RowID'],
                'Status' => 1,
            );

            $this->db->insert('lmais_payment', $payment_data);

            //Consent Fee Payment
            $payment_data = array(
                'Description' => 'Consent Fee',
                'DocID' => $doc_id,
                'PaymentDate' => date('Y-m-d H:i:s'),
                'Amount' => 1000,
                'Note' => 'Paid',
                'Gateway' => 'Ecitizen',
                'ServiceType' => 'Transfer',
                'ServiceID' => $sessioned_consent['RowID'],
                'Status' => 1,
            );

            $this->db->insert('lmais_payment', $payment_data);

            //Conveyance Fee Payment
            $payment_data = array(
                'Description' => 'Conveyance Fee',
                'DocID' => $doc_id,
                'PaymentDate' => date('Y-m-d H:i:s'),
                'Amount' => 500,
                'Note' => 'Paid',
                'Gateway' => 'Ecitizen',
                'ServiceType' => 'Transfer',
                'ServiceID' => $sessioned_consent['RowID'],
                'Status' => 1,
            );

            $this->db->insert('lmais_payment', $payment_data);

            //Convinience Fee Payment
            $payment_data = array(
                'Description' => 'Convinience Fee',
                'DocID' => $doc_id,
                'PaymentDate' => date('Y-m-d H:i:s'),
                'Amount' => 50,
                'Note' => 'Paid',
                'Gateway' => 'Ecitizen',
                'ServiceType' => 'Transfer',
                'ServiceID' => $sessioned_consent['RowID'],
                'Status' => 1,
            );

            $this->db->insert('lmais_payment', $payment_data);
        }//E# if else statement

        $sessioned_user = $this->session->userdata();

        //Send login email

        $parameters = array(
            'name' => $sessioned_user['FirstName'],
            'currency' => 'KES',
            'service' => 'Transfer of Land',
            'amount' => $sessioned_consent['Rent'],
            'time' => date('d/m/Y H:i')
        );

        $this->load->library('message');

        $sent = $this->message->sms(1, 1, 1, $sessioned_user['Phone'], 'payment_received', 'en', $parameters);

        //Clear all consent session
        $this->session->unset_userdata('sessioned_consent');
        $this->session->unset_userdata('sessioned_rent');
        $this->session->unset_userdata('sessioned_buyers');
        $this->session->unset_userdata('sessioned_sellers');
        $this->session->unset_userdata('sessioned_rla');

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Thanks ' . $sessioned_user['FirstName'] . ' ' . $sessioned_user['LastName'] . ' for making the payment</div>');

        redirect('UserView/LoadApplications');
    }

    public function test_email() {
        //Send login email
        $this->load->library('message');

        $this->message->send_email();
    }

    public function test_invoice() {
        //Load Ecitizen
        $this->load->library('ecitizen');

        $parameters = array(
            'apiClientID' => 'TYV0FUD0BY',
            'secureHash' => 'TYV0FUD0BY',
            'billDesc' => 'TYV0FUD0BY',
            'billRefNumber' => 'TYV0FUD0BY',
            'currency' => 'TYV0FUD0BY',
            'serviceID' => '29',
            'clientMSISDN' => 'TYV0FUD0BY',
            'clientName' => 'TYV0FUD0BY',
            'clientIDNumber' => 'TYV0FUD0BY',
            'clientEmail' => 'TYV0FUD0BY',
            'callBackURLOnSuccess' => 'TYV0FUD0BY',
            'pictureURL' => 'TYV0FUD0BY',
            'notificationURL' => 'TYV0FUD0BY',
            'amountExpected' => 'TYV0FUD0BY',
        );
        $ecitizen_response = $this->ecitizen->raise_invoice($parameters);

        var_dump($ecitizen_response);
    }

    public function get_user_by_id() {
        //Load Ecitizen
        $this->load->library('ecitizen');

        $ecitizen_response = $this->ecitizen->get_user_by_id_number($this->input->post('idNumber'));

        //var_dump($ecitizen_response);
        $response['message'] = $ecitizen_response['message'];
        $response['type'] = $ecitizen_response['status'] ? 'success' : 'error';

        echo json_encode($response);
    }

    public function index() {
        if ($this->session->userdata('Logged_in') != '') {

            self::loadindex();
        } else
            $this->load->view('signin/PageResources/loginheader');
        $this->load->view('signin/login');
    }

    //20642
    private function raise_invoices($sessioned_consent, $transactionID) {
        $doc_id = $this->session->userdata('sessioned_rla')->DocID;

        if ($sessioned_consent['Rent']) {
            //Rent Due Invoice
            $invoice_data = array(
                'Description' => 'Rent Due',
                'DocID' => $doc_id,
                'BillDate' => date('Y-m-d'),
                'InvoiceValue' => $sessioned_consent['Rent'],
                'Note' => '',
                'ServiceType' => 'Transfer',
                'ServiceID' => $sessioned_consent['RowID'],
                'Status' => '0',
            );

            $this->db->insert('lmais_invoice', $invoice_data);
        }//E# if statement
        //Consent Fee Invoice
        $invoice_data = array(
            'Description' => 'Consent Fee',
            'DocID' => $doc_id,
            'BillDate' => date('Y-m-d H:i:s'),
            'InvoiceValue' => 1000,
            'Note' => '',
            'ServiceType' => 'Transfer',
            'ServiceID' => $sessioned_consent['RowID'],
            'Status' => '0',
        );

        $this->db->insert('lmais_invoice', $invoice_data);

        //Conveyance Fee
        $invoice_data = array(
            'Description' => 'Conveyance Fee',
            'DocID' => $doc_id,
            'BillDate' => date('Y-m-d H:i:s'),
            'InvoiceValue' => 500,
            'Note' => '',
            'ServiceType' => 'Transfer',
            'ServiceID' => $sessioned_consent['RowID'],
            'Status' => '0',
        );

        $this->db->insert('lmais_invoice', $invoice_data);

        //Convinience Fee
        $invoice_data = array(
            'Description' => 'Convinience Fee',
            'DocID' => $doc_id,
            'BillDate' => date('Y-m-d H:i:s'),
            'InvoiceValue' => 50,
            'Note' => '',
            'ServiceType' => 'Transfer',
            'ServiceID' => $sessioned_consent['RowID'],
            'Status' => '0',
        );

        $this->db->insert('lmais_invoice', $invoice_data);
    }

    public function payment() {

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $received_request_model = $this->db
                        ->select('*')
                        ->order_by('RowID', 'desc')
                        ->limit(1)
                        ->get('lmais_receivedrequests')->row();

        $serial = 'FRX00D0C1A';

        if ($received_request_model) {
            $serial = $received_request_model->TransactionID;
        }//E# if statement

        $serial++;
        $generated_serial = $serial++;

        if ($this->session->has_userdata('sessioned_rla')) {
            $doc_id = $this->session->userdata('sessioned_rla')->DocID;
        } else {
            $doc_id = '';
        }//E# if else statement

        $received_request_data = array(
            'UserID' => $this->session->userdata('UserID'),
            'TransactionID' => $generated_serial,
            'PaymentStatus' => 0,
            'CompletionStage' => 0,
            'ServiceID' => 1,
            'DocID' => $doc_id,
            'LandUse' => 1,
            'Status' => 'Pending',
            'DateCreated' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('lmais_receivedrequests', $received_request_data);

        //Update buyers with the transaction id
        $buyer_data = array(
            'TransactionID' => $generated_serial,
        );

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->db->where('ConsentID', $sessioned_consent['ConsentID']);

        $this->db->update('lmais_buyers', $buyer_data);

        //Update buyers with the transaction id
        $seller_data = array(
            'TransactionID' => $generated_serial,
        );

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->db->where('ConsentID', $sessioned_consent['ConsentID']);

        $this->db->update('lmais_sellers', $seller_data);

        //Create invoices
        $this->raise_invoices($sessioned_consent, $generated_serial);

        $consent_data = array(
            'TransactionID' => $generated_serial
        );

        $this->db->where('RowID', $sessioned_consent['RowID']);

        $this->db->update('lmais_transfer', $consent_data);

        return $serial;

        //Clear all consent session
        $this->session->unset_userdata('sessioned_consent');
        $this->session->unset_userdata('sessioned_rent');
        $this->session->unset_userdata('sessioned_buyers');
        $this->session->unset_userdata('sessioned_rla');

        redirect('caution/apply_caution?step=7&status=done');
    }

    public function save_transaction_details() {

        $consent_data = array(
            'TransactionNature' => $this->input->post('TransactionNature'),
            'TransactionDescription' => $this->input->post('TransactionDescription'),
            'TransactionTerm' => $this->input->post('TransactionTerm'),
            'TransactionTermPeriod' => $this->input->post('TransactionTermPeriod'),
            'TypeOfTransaction' => $this->input->post('TypeOfTransaction'),
            'TransactionAmount' => $this->input->post('TransactionAmount'),
        );

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->db->where('RowID', $sessioned_consent['RowID']);

        $this->db->update('lmais_transfer', $consent_data);

        $this->session_consent($sessioned_consent['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Transaction details saved successfully</div>');

        redirect('caution/apply_caution?step=5');
    }

    public function save_applicant() {

        $consent_data = array(
            'ProprietorFirstName' => $this->input->post('ProprietorFirstName'),
            'ProprietorMiddleName' => $this->input->post('ProprietorMiddleName'),
            'ProprietorLastName' => $this->input->post('ProprietorLastName'),
            'ProprietorNationality' => $this->input->post('ProprietorNationality'),
            'ProprietorIdentification' => $this->input->post('ProprietorIdentification'),
            'ProprietorMobileNumber' => $this->input->post('ProprietorMobileNumber'),
            'ProprietorEmailAddress' => $this->input->post('ProprietorEmailAddress'),
            'ProprietorAddress' => $this->input->post('ProprietorAddress'),
        );

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->db->where('RowID', $sessioned_consent['RowID']);

        $this->db->update('lmais_transfer', $consent_data);

        $this->session_consent($sessioned_consent['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Applicant\'s details saved successfully</div>');

        redirect('caution/apply_caution?step=3');
    }

    public function rate_clearance() {
        $config['upload_path'] = './files/storage_files';
        $config['allowed_types'] = 'gif|jpg|png|pdf';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('clearance_certificate')) {
            $error = array('error' => $this->upload->display_errors());

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						<strong>Error!</strong> ' . trim($error['error']) . '</div>');

            redirect('caution/apply_caution?step=6');
        } else {

            $file_data = $this->upload->data();

            $now = date('Y-m-d H:i:s');

            $sessioned_consent = $this->session->userdata('sessioned_consent');

            $filestorage_data = array(
                'CreatedBy' => $this->session->userdata('UserID'),
                'DateCreated' => $now,
                'DateModified' => $now,
                'FileName' => $file_data['file_name'],
                'FileType' => $file_data['file_type'],
                'FileSize' => $file_data['file_size'],
                'FileExt' => $file_data['file_ext'],
                'IsImage' => $file_data['is_image'],
                'FileStage' => '',
                'ModifiedBy' => $this->session->userdata('UserID'),
                'OriginalFilename' => $file_data['orig_name'],
                'ServiceID' => 'consent',
                'ServiceTypeID' => $sessioned_consent['RowID']
            );

            $this->db->insert('lmais_filestorage', $filestorage_data);

            $data = array('upload_data' => $file_data);

            $consent_data = array(
                'RateClearanceCertificateFile' => $data['upload_data']['file_name'],
                'RateClearanceNumber' => $this->input->post('RateClearanceNumber'),
            );

            $sessioned_consent = $this->session->userdata('sessioned_consent');

            $this->db->where('RowID', $sessioned_consent['RowID']);

            $this->db->update('lmais_transfer', $consent_data);

            $this->session_consent($sessioned_consent['RowID']);

            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Thanks you for clearing your rates</div>');

            redirect('caution/apply_caution?step=5');
        }//E# if else statement

        die();
    }

    public function rent_clearance() {

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Thanks you for clearing your rent</div>');
        redirect('caution/apply_caution?step=6');
    }

    private function session_consent($consent_id, $session_buyers_sellers = false) {
        $consent_model = $this->db->get_where('lmais_transfer', array('RowID' => $consent_id))->result_array();

        $this->session->set_userdata('sessioned_consent', $consent_model[0]);

        if ($session_buyers_sellers == 'buyers') {
            //Select all buyers
            $buyer_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ConsentID', $consent_model[0]['ConsentID'])
                            ->get('lmais_buyers')->result_array();


            $this->session->set_userdata('sessioned_buyers', $buyer_model);
        }

        if ($session_buyers_sellers == 'sellers') {
            //Select all sellers
            $seller_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ConsentID', $consent_model[0]['ConsentID'])
                            ->get('lmais_sellers')->result_array();


            $this->session->set_userdata('sessioned_sellers', $seller_model);
        }
    }

    public function save_seller_continue() {

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Seller\'s details saved successfully</div>');

        redirect('caution/apply_caution?step=3');
    }

    public function save_seller() {

        if ($this->input->post('IsContactPerson') == 'Y') {
            $consent_data = array(
                'IsContactPerson' => 'N',
            );
            $this->db->where('ConsentID', $this->input->post('ConsentID'));

            $this->db->update('lmais_sellers', $consent_data);
        }//E# if statement
        $seller_data = array(
            'IsContactPerson' => $this->input->post('IsContactPerson'),
            'UserID' => $this->session->userdata('UserID'),
            'ConsentID' => $this->input->post('ConsentID'),
            'FirstName' => $this->input->post('FirstName'),
            'MiddleName' => $this->input->post('MiddleName'),
            'LastName' => $this->input->post('LastName'),
            'CountryID' => $this->input->post('CountryID'),
            'Identification' => $this->input->post('Identification'),
            'MobileNumber' => $this->input->post('MobileNumber'),
            'EmailAddress' => $this->input->post('EmailAddress'),
            'Address' => $this->input->post('Address'),
            'CreatedBy' => $this->session->userdata('UserID'),
            'DateCreated' => date('Y-m-d H:i:s'),
            'ModifiedBy' => $this->session->userdata('UserID'),
            'DateModified' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('lmais_sellers', $seller_data);

        $seller_id = $this->db->insert_id();

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->session_consent($sessioned_consent['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Seller\'s details saved successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/caution/apply_caution?step=2'
        ));
    }

    public function save_buyer_continue() {

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Buyer\'s details saved successfully</div>');

        redirect('caution/apply_caution?step=4');
    }

    public function save_buyer() {

        if ($this->input->post('IsContactPerson') == 'Y') {
            $consent_data = array(
                'IsContactPerson' => 'N',
            );
            $this->db->where('ConsentID', $this->input->post('ConsentID'));

            $this->db->update('lmais_buyers', $consent_data);
        }//E# if statement
        $buyer_data = array(
            'IsContactPerson' => $this->input->post('IsContactPerson'),
            'UserID' => $this->session->userdata('UserID'),
            'ConsentID' => $this->input->post('ConsentID'),
            'FirstName' => $this->input->post('FirstName'),
            'MiddleName' => $this->input->post('MiddleName'),
            'LastName' => $this->input->post('LastName'),
            'CountryID' => $this->input->post('CountryID'),
            'Identification' => $this->input->post('Identification'),
            'MobileNumber' => $this->input->post('MobileNumber'),
            'EmailAddress' => $this->input->post('EmailAddress'),
            'Address' => $this->input->post('Address'),
            'CreatedBy' => $this->session->userdata('UserID'),
            'DateCreated' => date('Y-m-d H:i:s'),
            'ModifiedBy' => $this->session->userdata('UserID'),
            'DateModified' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('lmais_buyers', $buyer_data);

        $buyer_id = $this->db->insert_id();

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->session_consent($sessioned_consent['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Buyer\'s details saved successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/caution/apply_caution?step=3'
        ));
    }

    public function check_property_details() {
        //Clear all consent session
        $this->session->unset_userdata('sessioned_consent');
        $this->session->unset_userdata('sessioned_rent');
        $this->session->unset_userdata('sessioned_buyers');
        $this->session->unset_userdata('sessioned_sellers');
        $this->session->unset_userdata('sessioned_rla');



        $rla_model = $this->db
                        ->select('*')
                        ->where('RegistryID', $this->input->post('RegistryID'))
                        ->where('RegistrationSectionID', $this->input->post('RegistrationSectionID'))
                        ->where('ParcelNo', $this->input->post('ParcelNo'))
                        ->limit(1)
                        ->get('lmais_rla')->row();

        $property_availability = array(
            'Agent' => $_SERVER['HTTP_USER_AGENT'],
            'DateCreated' => date('Y-m-d H:i:s'),
            'Ip' => $_SERVER['REMOTE_ADDR'],
            'ParcelNo' => $this->input->post('ParcelNo'),
            'RegistryID' => $this->input->post('RegistryID'),
            'RegistrationSectionID' => $this->input->post('RegistrationSectionID'),
            'NatureOfTitle' => $this->input->post('NatureOfTitle'),
            'Status' => 0,
        );

        if ($rla_model) {

            $property_availability['Status'] = 1;

            $this->session->set_userdata('sessioned_rla', $rla_model);

            $consent_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->limit(1)
                            ->get('lmais_transfer')->row();

            $serial = 'G4AX2A';
            if ($consent_model) {
                $serial = $consent_model->ConsentID;
            }//E# if statement

            $serial++;
            $generated_serial = $serial++;

            $consent_data = array(
                'ConsentID' => $generated_serial,
                'RegistryID' => $this->input->post('RegistryID'),
                'RegistrationSectionID' => $this->input->post('RegistrationSectionID'),
                'ParcelNumber' => $this->input->post('ParcelNo'),
                'LRNumber' => $rla_model->Title,
                'Act' => $this->input->post('NatureOfTitle'),
                'Description' => 'Transfer Consent',
                'PaymentStatus' => 'Unpaid',
                'Rate' => '0',
                'Rent' => '0',
            );

            $this->db->insert('lmais_transfer', $consent_data);

            $consent_id = $this->db->insert_id();

            //Select all rent
            $rent_model = $this->db
                            ->select('*')
                            ->where('LRNumber', $rla_model->Title)
                            ->where('Status', 'N')
                            ->get('lmais_rent')->result_array();

            if ($rent_model) {
                $unpaid_rent = 0;
                foreach ($rent_model as $single_rent) {
                    $unpaid_rent +=$single_rent['Amount'];
                }//E# foreach statement

                $consent_data = array(
                    'Rent' => $unpaid_rent,
                );

                //var_dump($single_rent)
                //dd("sa");
                $this->db->where('RowID', $consent_id);

                $this->db->update('lmais_transfer', $consent_data);
            }//E# if else statement

            $this->session->set_userdata('sessioned_rent', $rent_model);

            $this->session_consent($consent_id);

            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> We have found your land</div>');
            $redirect_to = 'caution/apply_caution?step=2';
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						<strong>Error!</strong> This parcel of land could not be found. Kindly confirm your details</div>');

            $redirect_to = 'caution/apply_caution';
        }//E# if statement

        $this->db->insert('lmais_property_availability', $property_availability);

        redirect($redirect_to);
    }

    public function apply_caution() {
        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');
            $data['acts'] = $this->actsModel->GetAllActs();
            $data['input'] = $this->input->get();

            if ($this->session->has_userdata('sessioned_consent')) {
                $data['sessioned_consent'] = $this->session->userdata('sessioned_consent');
            } else {
                $data['sessioned_consent'] = false;
            }//E# if else statement

            if ($this->session->has_userdata('sessioned_rent')) {
                $data['sessioned_rent'] = $this->session->userdata('sessioned_rent');
            } else {
                $data['sessioned_rent'] = false;
            }//E# if else statement

            if ($this->session->has_userdata('sessioned_buyers')) {
                $data['sessioned_buyers'] = $this->session->userdata('sessioned_buyers');
            } else {
                $data['sessioned_buyers'] = false;
            }//E# if else statement

            if ($this->session->has_userdata('sessioned_sellers')) {
                $data['sessioned_sellers'] = $this->session->userdata('sessioned_sellers');
            } else {
                $data['sessioned_sellers'] = false;
            }//E# if else statement
            if ($this->session->userdata()) {
                $data['sessioned_user'] = $this->session->userdata();
            } else {
                $data['sessioned_user'] = false;
            }//E# if else statement

            $this->load->view($Department . '/PageResources/addparcelsheader');
            $this->load->view($Department . '/Header/header');
            if ($Department == 'user') {
                
            } else {
                $this->load->view($Department . '/Menu/menu');
            }//E# if else statement
            $this->load->view('consent/caution', $data);
        }
    }

    public function consent_app() {
        $ConsentID = "CNT" . md5(time() . mt_rand(1, 1000000));

        $Proprietor_First_Name = $this->input->post("ProprietorFirstName");
        $Proprietor_Middle_Name = $this->input->post("ProprietorMiddleName");
        $Proprietor_Last_Name = $this->input->post("ProprietorLastName");
        $Proprietor_Nationality = $this->input->post("ProprietorNationality");
        $Proprietor_Idenification = $this->input->post("ProprietorIdenification");
        $Proprietor_Mobile_Number = $this->input->post("ProprietorMobileNumber");
        $Proprietor_Email_Address = $this->input->post("ProprietorEmailAddress");
        $Proprietor_Address = $this->input->post("ProprietorAddress");
        $Proposed_First_Name = $this->input->post("ProposedFirstName");
        $Proposed_Middle_Name = $this->input->post("ProposedMiddleName");
        $Proposed_Last_Name = $this->input->post("ProposedLastName");
        $Proposed_Nationality = $this->input->post("ProposedNationality");
        $Proposed_Identification = $this->input->post("ProposedIdentification");
        $Proposed_Mobile_Number = $this->input->post("ProposedMobileNumber");
        $Proposed_Email_Address = $this->input->post("ProposedEmailAddress");
        $Proposed_Address = $this->input->post("ProposedAddress");
        $Nature_Of_Transaction = $this->input->post("TransactionNature");
        $Transaction_Description = $this->input->post("TransactionDescription");
        $Transaction_Term = $this->input->post("TransactionTerm");
        $Transaction_Term_Period = $this->input->post("TransactionTermPeriod");
        $LRNumber = $this->input->post("LRNumber");
        $AreaSize = $this->input->post("AreaSize");
        $Registry = $this->input->post("Registry");
        $County = $this->input->post("County");
        $CreatedBy = $this->session->userdata('RowID');

        $data = array(
            'Registry' => $Registry,
            'LRNO' => $LRNO,
            'AreaSize' => $AreaSize,
            'AreaUnit' => $AreaUnit,
            'NatureOfTitle' => $NatureOfTitle,
            'RegisteredProprietor' => $RegisteredProprietor,
            'TypeOfDevelopment' => $TypeOfDevelopment,
            'NearbyInfrastructure' => $NearbyInfrastructure,
            'NearbyValue' => $NearbyValue,
            'TypeOfRoad' => $TypeOfRoad,
            'NameOfRoad' => $NameOfRoad,
            'DistanceFromRoad' => $DistanceFromRoad,
            'TypeOfTown' => $TypeOfTown,
            'NameOfTown' => $NameOfTown,
            'DistanceFromTown' => $DistanceFromTown,
            'CreatedBy' => $CreatedBy,
        );
        $usr_result = $this->ConsentModel->apply_consent($data);

        //check if username and password is correct
        if ($usr_result == TRUE) { //active user record is present
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> You have successfully Added new Parcel!</div>');

            redirect('start/addvaluationdata');
        } else if ($usr_result == FALSE) {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						 <a href="#" class="close" data-dismiss="alert">&times;</a>
						  <strong>Error!</strong> Data are not saved.. Try again</div>');

            redirect('start/addvaluationdata');
        }
    }

    public function send_reset_password_mail() {

        $email = $this->input->post("Email");
        $existing_user = $this->usermodel->is_email_exists($email);

        //send reset password email if found account with this email
        if ($existing_user) {
            $email_template = $this->Email_templates_model->get_final_template("reset_password");

            $parser_data["ACCOUNT_HOLDER_NAME"] = $existing_user->FirstName . " " . $existing_user->LastName;
            $parser_data["SIGNATURE"] = $email_template->signature;
            $parser_data["SITE_URL"] = base_url();
            $key = encode_id($this->encrypt->encode($existing_user->Email . '|' . (time() + (24 * 60 * 60))), "reset_password");
            $parser_data['RESET_PASSWORD_URL'] = base_url("signin/new_password/" . $key);

            $message = $this->parser->parse_string($email_template->message, $parser_data, TRUE);
            if ($this->send_app_mails($email, $email_template->subject, $message)) {
                echo json_encode(array('success' => true, 'message' => lang("reset_info_send")));
            } else {
                echo json_encode(array('success' => false, 'message' => lang('error_occurred')));
            }
        } else {
            echo json_encode(array("success" => false, 'message' => lang("no_acount_found_with_this_email")));
            return false;
        }
    }

    public function EditValuation($RowID) {
        if (!$RowID) {
            
        } Else
            $data['Results'] = $this->SearchResultsModel->SearchDetails($RowID);

        $this->load->view('Valuation/PageResources/RegisterUserheader');
        $this->load->view('Valuation/Header/header');
        $this->load->view('Valuation/Menu/menu');
        $this->load->view('Valuation/Pages/EditValuation', $data);
    }

    public function UnassignedValuations() {

        $data['Results'] = $this->ParcelsValuationModel->GetUnassignedValuations();

        $this->load->view('Valuation/PageResources/UnassignedValuationsheader');
        $this->load->view('Valuation/Header/header');
        $this->load->view('Valuation/Menu/menu');
        $this->load->view('Valuation/Pages/UnassignedValuations', $data);
    }

    public function EditParcelValuation() {
        $Registry = $this->input->post("Registry");
        $LRNO = $this->input->post("LRNO");
        $AreaSize = $this->input->post("AreaSize");
        $AreaUnit = $this->input->post("AreaUnit");
        $NatureOfTitle = $this->input->post("NatureOfTitle");
        $RegisteredProprietor = $this->input->post("RegisteredProprietor");
        $TypeOfDevelopment = $this->input->post("TypeOfDevelopment");
        $NearbyInfrastructure = $this->input->post("NearbyInfrastructure");
        $NearbyValue = $this->input->post("NearbyValue");
        $LandValue = $this->input->post("LandValue");
        $TypeOfRoad = $this->input->post("TypeOfRoad");
        $NameOfRoad = $this->input->post("NameOfRoad");
        $DistanceFromRoad = $this->input->post("DistanceFromRoad");
        $TypeOfTown = $this->input->post("TypeOfTown");
        $NameOfTown = $this->input->post("NameOfTown");
        $DistanceFromTown = $this->input->post("DistanceFromTown");
        $ModifiedBy = $this->session->userdata('RowID');
        $Email = $this->input->post("Email");
        $Telephone = $this->input->post("Telephone");
        $RowID = $this->input->post("RowID");
        $data = array(
            'Registry' => $Registry,
            'LRNO' => $LRNO,
            'AreaSize' => $AreaSize,
            'AreaUnit' => $AreaUnit,
            'NatureOfTitle' => $NatureOfTitle,
            'RegisteredProprietor' => $RegisteredProprietor,
            'TypeOfDevelopment' => $TypeOfDevelopment,
            'NearbyInfrastructure' => $NearbyInfrastructure,
            'NearbyValue' => $NearbyValue,
            'LandValue' => $LandValue,
            'TypeOfRoad' => $TypeOfRoad,
            'NameOfRoad' => $NameOfRoad,
            'DistanceFromRoad' => $DistanceFromRoad,
            'TypeOfTown' => $TypeOfTown,
            'NameOfTown' => $NameOfTown,
            'DistanceFromTown' => $DistanceFromTown,
            'ModifiedBy' => $ModifiedBy,
            'Telephone' => $Telephone,
            'Email' => $Email
        );
        $usr_result = $this->ParcelsValuationModel->editparcel($RowID, $data);

        //check if username and password is correct
        if ($usr_result == TRUE) { //active user record is present
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> You have successfully edited the parcel!</div>');

            redirect('start/searchdetails/' . $RowID);
        } else if ($usr_result == FALSE) {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						 <a href="#" class="close" data-dismiss="alert">&times;</a>
						  <strong>Error!</strong> Data are not saved.No Changes were made</div>');

            redirect('start/searchdetails/' . $RowID);
        }
    }

    function GetRegistriesMatch($ActID, $CountyID) {
        //$ActID = $this->uri->segment(3);
        //$CountyID = $this->uri->segment(4);
        $Registries = $this->Registrymodel->GetMatchedRegistries($ActID, $CountyID);

        return $Registries;
    }

}
