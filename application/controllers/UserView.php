<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserView extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('ServiceModel', '', TRUE);
        $this->load->model('ParcelsValuationModel', '', TRUE);
        $this->load->model('usermodel', '', TRUE);
        $this->load->model('SearchResultsModel', '', TRUE);
        $this->load->model('Email_templates_model', '', TRUE);
        $this->load->model('Settings_Model', '', TRUE);
        $this->load->model('proprietorModel', '', TRUE);
        $this->load->model('encumbranceModel', '', TRUE);
        $this->load->model('SellerModel', '', TRUE);
        $this->load->model('BuyerModel', '', TRUE);
    }

    public function ConsentInfo($transfer_id) {
        if ($this->session->userdata('Logged_in') != '') {
            $data['input'] = $this->input->get();
            $transfer_model = $this->db
                            ->select('*')
                            ->where('TransactionID', $transfer_id)
                            ->get('lmais_transfer')->result_array();

            $data['transfer_model'] = $transfer_model ? $transfer_model[0] : null;

            $Department = $this->session->userdata('UserType');
            $Department = ($Department == 'bank') ? 'user' : $Department;

            $this->load->view($Department . '/PageResources/dashboardheader');
            $this->load->view($Department . '/Header/header');
            $this->load->view($Department . '/Pages/AppliedConsent', $data);
        }
    }

    public function index() {
        if ($this->session->userdata('Logged_in') != '') {

            self::loadindex();
        } else
            $this->load->view('signin/PageResources/loginheader');
        $this->load->view('signin/login');
    }

    public function loadindex() {
        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');
            $data['Results'] = get_setting("app_title");
            $this->load->view($Department . '/PageResources/dashboardheader');
            $this->load->view($Department . '/Header/header');
            if ($Department == 'user') {
                
            } else {
                $this->load->view($Department . '/Menu/menu');
            }//E# if else statement

            $this->load->view($Department . '/Pages/index2');
        } else {
            redirect(base_url() . 'start/start/index');
        }
    }

    public function LoadInfo($slug) {
        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');

            $Department = ($Department == 'bank') ? 'user' : $Department;

            $this->load->view($Department . '/PageResources/dashboardheader');
            $this->load->view($Department . '/Header/header');

            $this->load->view($Department . '/Pages/Info/' . $slug);
        }
    }

    //loads the consent forms information on AppliedInfo View

    public function forminfo($transfer_id, $ServiceID) {
        if ($this->session->userdata('Logged_in') != '') {
            $FormView;
			$DBView;

            $sql = "SELECT * FROM lmais_landservicetypes where Slug = '$ServiceID'";

            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $row) {
					if($row->Slug=='withdrawalofcaution'){
						$DBView='caution';
					}
					 else{
						$DBView=$row->Slug;
					 }
                    $FormView = $row->Slug;
                }
            }
            $transfer_model = $this->db
                            ->select('*')
                            ->where('TransactionID', $transfer_id)
                            ->get('lmais_' . $DBView)->result_array();

            $data['transfer_model'] = $transfer_model ? $transfer_model[0] : null;
            $data['transfer_id'] = $transfer_id;      
            $Department = $this->session->userdata('UserType');
            $this->load->view($Department . '/PageResources/dashboardheader');
            $this->load->view($Department . '/Header/header');
            $this->load->view($Department . '/Pages/' . $FormView, $data);
        }
    }

    public function LoadApplications() {
        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');

            $Department = ($Department == 'bank') ? 'user' : $Department;
            $this->load->view($Department . '/PageResources/dashboardheader');
            $this->load->view($Department . '/Header/header');
            if ($Department == 'user') {
                
            } else {
                $this->load->view($Department . '/Menu/menu');
            }//E# if else statement
            $this->load->view($Department . '/Pages/ApplicationList');
        }
    }

    public function AddParcelValuation() {
        $Registry = $this->input->post("Registry");
        $LRNO = $this->input->post("LRNO");
        $AreaSize = $this->input->post("AreaSize");
        $AreaUnit = $this->input->post("AreaUnit");
        $NatureOfTitle = $this->input->post("NatureOfTitle");
        $RegisteredProprietor = $this->input->post("RegisteredProprietor");
        $TypeOfDevelopment = $this->input->post("TypeOfDevelopment");
        $NearbyInfrastructure = $this->input->post("NearbyInfrastructure");
        $NearbyValue = $this->input->post("NearbyValue");
        $TypeOfRoad = $this->input->post("TypeOfRoad");
        $NameOfRoad = $this->input->post("NameOfRoad");
        $DistanceFromRoad = $this->input->post("DistanceFromRoad");
        $TypeOfTown = $this->input->post("TypeOfTown");
        $NameOfTown = $this->input->post("NameOfTown");
        $DistanceFromTown = $this->input->post("DistanceFromTown");
        $CreatedBy = $this->session->userdata('RowID');
        ;

        $data = array(
            'Registry' => $Registry,
            'LRNO' => $LRNO,
            'AreaSize' => $AreaSize,
            'AreaUnit' => $AreaUnit,
            'NatureOfTitle' => $NatureOfTitle,
            'RegisteredProprietor' => $RegisteredProprietor,
            'TypeOfDevelopment' => $TypeOfDevelopment,
            'NearbyInfrastructure' => $NearbyInfrastructure,
            'NearbyValue' => $NearbyValue,
            'TypeOfRoad' => $TypeOfRoad,
            'NameOfRoad' => $NameOfRoad,
            'DistanceFromRoad' => $DistanceFromRoad,
            'TypeOfTown' => $TypeOfTown,
            'NameOfTown' => $NameOfTown,
            'DistanceFromTown' => $DistanceFromTown,
            'CreatedBy' => $CreatedBy,
        );
        $usr_result = $this->ParcelsValuationModel->addnewparcel($data);

        //check if username and password is correct
        if ($usr_result == TRUE) { //active user record is present
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> You have successfully Added new Parcel!</div>');

            redirect('start/addvaluationdata');
        } else if ($usr_result == FALSE) {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						 <a href="#" class="close" data-dismiss="alert">&times;</a>
						  <strong>Error!</strong> Data are not saved.. Try again</div>');

            redirect('start/addvaluationdata');
        }
    }

    public function logout() {

        $this->session->unset_userdata('logged_in');
        session_destroy();
        $this->load->view('Valuation/PageResources/loginheader');
        $this->load->view('Valuation/Pages/login');
    }

    public function reset_password_form() {
        $this->load->view('Valuation/PageResources/loginheader');
        $this->load->view('signin/reset_password_form');
    }

    public function send_reset_password_mail() {

        $email = $this->input->post("Email");
        $existing_user = $this->usermodel->is_email_exists($email);

        //send reset password email if found account with this email
        if ($existing_user) {
            $email_template = $this->Email_templates_model->get_final_template("reset_password");

            $parser_data["ACCOUNT_HOLDER_NAME"] = $existing_user->FirstName . " " . $existing_user->LastName;
            $parser_data["SIGNATURE"] = $email_template->signature;
            $parser_data["SITE_URL"] = base_url();
            $key = encode_id($this->encrypt->encode($existing_user->Email . '|' . (time() + (24 * 60 * 60))), "reset_password");
            $parser_data['RESET_PASSWORD_URL'] = base_url("signin/new_password/" . $key);

            $message = $this->parser->parse_string($email_template->message, $parser_data, TRUE);
            if ($this->send_app_mails($email, $email_template->subject, $message)) {
                echo json_encode(array('success' => true, 'message' => lang("reset_info_send")));
            } else {
                echo json_encode(array('success' => false, 'message' => lang('error_occurred')));
            }
        } else {
            echo json_encode(array("success" => false, 'message' => lang("no_acount_found_with_this_email")));
            return false;
        }
    }

    public function EditValuation($RowID) {
        if (!$RowID) {
            
        } Else
            $data['Results'] = $this->SearchResultsModel->SearchDetails($RowID);

        $this->load->view('Valuation/PageResources/RegisterUserheader');
        $this->load->view('Valuation/Header/header');
        $this->load->view('Valuation/Menu/menu');
        $this->load->view('Valuation/Pages/EditValuation', $data);
    }

    public function UnassignedValuations() {

        $data['Results'] = $this->ParcelsValuationModel->GetUnassignedValuations();

        $this->load->view('Valuation/PageResources/UnassignedValuationsheader');
        $this->load->view('Valuation/Header/header');
        $this->load->view('Valuation/Menu/menu');
        $this->load->view('Valuation/Pages/UnassignedValuations', $data);
    }

    public function EditParcelValuation() {
        $Registry = $this->input->post("Registry");
        $LRNO = $this->input->post("LRNO");
        $AreaSize = $this->input->post("AreaSize");
        $AreaUnit = $this->input->post("AreaUnit");
        $NatureOfTitle = $this->input->post("NatureOfTitle");
        $RegisteredProprietor = $this->input->post("RegisteredProprietor");
        $TypeOfDevelopment = $this->input->post("TypeOfDevelopment");
        $NearbyInfrastructure = $this->input->post("NearbyInfrastructure");
        $NearbyValue = $this->input->post("NearbyValue");
        $LandValue = $this->input->post("LandValue");
        $TypeOfRoad = $this->input->post("TypeOfRoad");
        $NameOfRoad = $this->input->post("NameOfRoad");
        $DistanceFromRoad = $this->input->post("DistanceFromRoad");
        $TypeOfTown = $this->input->post("TypeOfTown");
        $NameOfTown = $this->input->post("NameOfTown");
        $DistanceFromTown = $this->input->post("DistanceFromTown");
        $ModifiedBy = $this->session->userdata('RowID');
        $Email = $this->input->post("Email");
        $Telephone = $this->input->post("Telephone");
        $RowID = $this->input->post("RowID");
        $data = array(
            'Registry' => $Registry,
            'LRNO' => $LRNO,
            'AreaSize' => $AreaSize,
            'AreaUnit' => $AreaUnit,
            'NatureOfTitle' => $NatureOfTitle,
            'RegisteredProprietor' => $RegisteredProprietor,
            'TypeOfDevelopment' => $TypeOfDevelopment,
            'NearbyInfrastructure' => $NearbyInfrastructure,
            'NearbyValue' => $NearbyValue,
            'LandValue' => $LandValue,
            'TypeOfRoad' => $TypeOfRoad,
            'NameOfRoad' => $NameOfRoad,
            'DistanceFromRoad' => $DistanceFromRoad,
            'TypeOfTown' => $TypeOfTown,
            'NameOfTown' => $NameOfTown,
            'DistanceFromTown' => $DistanceFromTown,
            'ModifiedBy' => $ModifiedBy,
            'Telephone' => $Telephone,
            'Email' => $Email
        );
        $usr_result = $this->ParcelsValuationModel->editparcel($RowID, $data);

        //check if username and password is correct
        if ($usr_result == TRUE) { //active user record is present
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> You have successfully edited the parcel!</div>');

            redirect('start/searchdetails/' . $RowID);
        } else if ($usr_result == FALSE) {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						 <a href="#" class="close" data-dismiss="alert">&times;</a>
						  <strong>Error!</strong> Data are not saved.No Changes were made</div>');

            redirect('start/searchdetails/' . $RowID);
        }
    }

    function GetRoadNameMatchRoadType() {
        $roadTypeID = $this->uri->segment(3);
        $roadNames = $this->ParcelsValuationModel->GetRoadNameMatchRoadType($roadTypeID);
        echo $roadNames;
        return $roadNames;
    }

    function GetTownNamesMatchTownType() {
        $townTypeID = $this->uri->segment(3);
        $townNames = $this->ParcelsValuationModel->GetTownNamesMatchTownType($townTypeID);
        echo $townNames;
        return $townNames;
    }

}
