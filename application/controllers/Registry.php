<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Registry extends CI_Controller {

    function __construct() {
		
		
        parent::__construct();
		$this->load->model('Settings_Model','',TRUE);
		$this->load->model('Registrymodel','',TRUE);
		$this->load->model('Countymodel','',TRUE);
        //$this->access_only_team_members();
    }

    //only admin can change other user's info
    //none admin users can only change his/her own info
    function only_admin_or_own($user_id) {
        if ($user_id && ($this->login_user->is_admin || $this->login_user->id === $user_id)) {
            return true;
        } else {
            redirect("forbidden");
        }
    }

	
    public function index() {
		
			redirect('Registry/RegistryManagement');
    }
	
	public function RegistryManagement() {
		
									$data['Results'] = $this->Registrymodel->GetAllRegistries();
									$Department = $this->session->userdata('UserType');
									$Department = ($Department == 'bank') ? 'user' : $Department;
									$this->load->view($Department . '/PageResources/dashboardheader');
									$this->load->view($Department . '/Header/header');
									$this->load->view('Registries/index', $data);
    }
	
	
	public function RegistrySections($para1)
	{
		$data['Results']=$this->Registrymodel->GetRegistrationSections($para1);
		$data['RegistryID']=$para1;
		$Department = $this->session->userdata('UserType');
		$Department = ($Department == 'bank') ? 'user' : $Department;
		$this->load->view($Department . '/PageResources/dashboardheader');
		$this->load->view($Department . '/Header/header');
		$this->load->view('Registries/Sections/index', $data);
		
	}
	
	


   
    function add_team_member() {
        $this->access_only_admin();

        //check duplicate email address, if found then show an error message
        if ($this->Users_model->is_email_exists($this->input->post('email'))) {
            echo json_encode(array("success" => false, 'message' => lang('duplicate_email')));
            exit();
        }

        validate_submitted_data(array(
            "email" => "required|valid_email",
            "first_name" => "required",
            "last_name" => "required",
            "job_title" => "required",
            "role" => "numeric"
        ));

        $user_data = array(
            "email" => $this->input->post('email'),
            "password" => md5($this->input->post('password')),
            "first_name" => $this->input->post('first_name'),
            "last_name" => $this->input->post('last_name'),
            "is_admin" => $this->input->post('is_admin'),
            "address" => $this->input->post('address'),
            "phone" => $this->input->post('phone'),
            "gender" => $this->input->post('gender'),
            "job_title" => $this->input->post('job_title'),
            "phone" => $this->input->post('phone'),
            "gender" => $this->input->post('gender'),
            "user_type" => "staff",
            "created_at" => get_current_utc_time()
        );

        //make role id or admin permission 
        $role = $this->input->post('role');
        $role_id = $role;

        if ($role === "admin") {
            $user_data["is_admin"] = 1;
            $user_data["role_id"] = 0;
        } else {
            $user_data["is_admin"] = 0;
            $user_data["role_id"] = $role_id;
        }


        //add a new team member
        $user_id = $this->Users_model->save($user_data);
        if ($user_id) {
            //user added, now add the job info for the user
            $job_data = array(
                "user_id" => $user_id,
                "salary" => $this->input->post('salary'),
                "salary_term" => $this->input->post('salary_term'),
                "date_of_hire" => $this->input->post('date_of_hire')
            );
            $this->Users_model->save_job_info($job_data);

            //send login details to user
            if ($this->input->post('email_login_details')) {

                //get the login details template
                $email_template = $this->Email_templates_model->get_final_template("login_info");

                $parser_data["SIGNATURE"] = $email_template->signature;
                $parser_data["USER_FIRST_NAME"] = $user_data["first_name"];
                $parser_data["USER_LAST_NAME"] = $user_data["last_name"];
                $parser_data["USER_LOGIN_EMAIL"] = $user_data["email"];
                $parser_data["USER_LOGIN_PASSWORD"] = $this->input->post('password');
                $parser_data["DASHBOARD_URL"] = base_url();

                $message = $this->parser->parse_string($email_template->message, $parser_data, TRUE);
                send_app_mail($this->input->post('email'), $email_template->subject, $message);
            }
        }

        if ($user_id) {
            echo json_encode(array("success" => true, "data" => $this->_row_data($user_id), 'id' => $user_id, 'message' => lang('record_saved')));
        } else {
            echo json_encode(array("success" => false, 'message' => lang('error_occurred')));
        }
    }

    /* open invitation modal */

    function invitation_modal() {
        $this->access_only_admin();
        $this->load->view('team_members/invitation_modal');
    }

    //send a team member invitation to an email address
    function send_invitation() {
        $this->access_only_admin();

        validate_submitted_data(array(
            "email" => "required|valid_email"
        ));

        $email = $this->input->post('email');

        //get the send invitation template 
        $email_template = $this->Email_templates_model->get_final_template("team_member_invitation");

        $parser_data["INVITATION_SENT_BY"] = $this->login_user->first_name . " " . $this->login_user->last_name;
        $parser_data["SIGNATURE"] = $email_template->signature;
        $parser_data["SITE_URL"] = get_uri();

        //make the invitation url with 24hrs validity
        $key = encode_id($this->encrypt->encode('staff|' . $email . '|' . (time() + (24 * 60 * 60))), "signup");
        $parser_data['INVITATION_URL'] = get_uri("signup/accept_invitation/" . $key);

        //send invitation email
        $message = $this->parser->parse_string($email_template->message, $parser_data, TRUE);
        if (send_app_mail($email, $email_template->subject, $message)) {
            echo json_encode(array('success' => true, 'message' => lang("invitation_sent")));
        } else {
            echo json_encode(array('success' => false, 'message' => lang('error_occurred')));
        }
    }

    
    

    public function AddRegistry()
				{
		if($this->session->userdata('Logged_in')!='')
							{

						$CreatedBy = $this->session->userdata('RowID');

							}
							else
		$CreatedBy = "0";
       	$Name = $this->input->post("Name");
		$County = $this->input->post("County");
		$RegistryID = $this->input->post("RegistryID");
		$DigitizationStatus = $this->input->post("DigitizationStatus");
					$data=array(
								'Name'=>$Name,
								'County'=>$County,
								'CreatedBy'=>$CreatedBy,
								'RegistryID'=>$RegistryID,
								'DigitizationStatus'=>$DigitizationStatus,
								
								);
								$usr_result = $this->Registrymodel->SaveData('lmais_registry',$data);
						
				if ($usr_result ==TRUE) //active user record is present
				{
	$this->session->set_flashdata('registrymsg', '<div class="alert alert-info fade in">
<a href="#" class="close" data-dismiss="alert">&times;</a>
<strong>Success!</strong>
<br>Registry was added successfully. </strong>
</div>');

				 	redirect('Registry/RegistryManagement');
				   }
				   			
				
				else
				{  
				 
					$this->session->set_flashdata('registrymsg', '<div class="alert alert-info fade in">
<a href="#" class="close" data-dismiss="alert">&times;</a>
			  	<strong>Error!</strong> Registry not created. That Registry Name already exist ! </div>');
					
						redirect('Registry/RegistryManagement');
				}
				
				
				}
				
				
				 public function AddRegistrySection()
				{
		if($this->session->userdata('Logged_in')!='')
							{

						$CreatedBy = $this->session->userdata('RowID');

							}
							else
		$CreatedBy = "0";
       	$Name = $this->input->post("SectionName");
		$RegistryName = $this->input->post("RegistryName");
		$RegistryID = $this->input->post("RegistryID");
					$data=array(
								'Name'=>$Name,
								'CreatedBy'=>$CreatedBy,
								'RegistryID'=>$RegistryID,
								);
								$usr_result = $this->Registrymodel->SaveData('lmais_registrationsections',$data);
						
				if ($usr_result ==TRUE) //active user record is present
				{
	$this->session->set_flashdata('registrymsg', '<div class="alert alert-info fade in">
<a href="#" class="close" data-dismiss="alert">&times;</a>
<strong>Success!</strong>
<br>Registry Section was added successfully. </strong>
</div>');

				 	redirect('Registry/RegistryManagement');
				   }
				   			
				
				else
				{  
				 
					$this->session->set_flashdata('registrymsg', '<div class="alert alert-info fade in">
<a href="#" class="close" data-dismiss="alert">&times;</a>
			  	<strong>Error!</strong> Registry not created. That Registry Name already exist ! </div>');
					
						redirect('Registry/RegistryManagement');
				}
				
				
				}



}

/* End of file team_member.php */
/* Location: ./application/controllers/team_member.php */