<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MY_Controller {

    private $service_slug = 'search';
    private $service_payable = array(
        array(
            'slug' => 'search',
            'db_field' => 'SearchFee',
        ),
        array(
            'slug' => 'convenience',
            'db_field' => 'ConvenienceFee',
        ),
    );

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('TenureModel', '', TRUE);
        $this->load->model('Countymodel', '', TRUE);
        $this->load->model('Registrymodel', '', TRUE);
        $this->load->model('ParcelsValuationModel', '', TRUE);
        $this->load->model('Countrymodel', '', TRUE);
        // $this->load->model('Settings_Model', '', TRUE);
        // $this->load->model('proprietorModel', '', TRUE);
        // $this->load->model('encumbranceModel', '', TRUE);
    }

    /**
     * S# payment_completed() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Payment completed
     * 
     */
    public function payment_completed() {
        $data = $this->input->get();

        //Load Ecitizen
        $this->load->library('ecitizen');


        if (array_key_exists('bill_ref', $data)) {
            $search_model = $this->db
                            ->select('*')
                            ->where('EcitizenBillRef', $data['bill_ref'])
                            ->get('lmais_search')->row();


            if ($search_model) {
                $ecitizen_response = $this->ecitizen->query_transaction_status($data['bill_ref']);

                if ($ecitizen_response['status']) {
                    $amount_paid = $ecitizen_response['message']['amount'];

                    $now = date('Y-m-d H:i:s');

                    $transaction_model = $this->db
                                    ->select('*')
                                    ->where('ServiceID', $search_model->RowID)
                                    ->where('ServiceSlug', $this->service_slug)
                                    ->where('Type', 'payment')
                                    ->get('lmais_transactions')->row();

                    if (!$transaction_model) {//Payment does not exist so create one
                        //Rent Due Payment
                        $payment_data = array(
                            'AccountNumber' => $ecitizen_response['message']['account_number'], //CHANGE - Addition
                            'Currency' => 'KES',
                            'Date' => $now,
                            'Type' => 'payment',
                            'Description' => $search_model->Description,
                            'Amount' => $amount_paid,
                            'ServiceSlug' => $this->service_slug,
                            'ServiceID' => $search_model->RowID,
                            'UserID' => $search_model->UserID,
                            'TransactionID' => $search_model->TransactionID,
                            'DocID' => $search_model->DocID,
                            'Status' => 'paid',
                            'Ip' => 'paid',
                            'Agent' => $_SERVER['HTTP_USER_AGENT'],
                            'Ip' => $_SERVER['REMOTE_ADDR'],
                        );

                        // var_dump($payment_data);
                        // die("SAd");
                        $this->db->insert('lmais_transactions', $payment_data);
                    }//E# if else statement
                    //Get total payments
                    $total_payment = $this->db
                                    ->select_sum('Amount')
                                    ->where('ServiceID', $search_model->RowID)
                                    ->where('ServiceSlug', $this->service_slug)
                                    ->where('Type', 'payment')
                                    ->get('lmais_transactions')->row();

                    //Get total invoices
                    $total_invoice = $this->db
                                    ->select_sum('Amount')
                                    ->where('ServiceID', $search_model->RowID)
                                    ->where('ServiceSlug', $this->service_slug)
                                    ->where('Type', 'invoice')
                                    ->get('lmais_transactions')->row();

                    if (($total_payment->Amount == $total_invoice->Amount) || ($total_payment->Amount > $total_payment->Amount)) {

                        $search_data = array(
                            'Status' => 'paid',
                        );

                        $this->db->where('RowID', $search_model->RowID);

                        $this->db->update('lmais_search', $search_data);

                        $received_request_data = array(
                            'Status' => 'paid',
                        );

                        $this->db->where('TransactionID', $search_model->TransactionID);

                        $this->db->update('lmais_receivedrequests', $received_request_data);
                    }//E# statement

                    /*
                      if ($this->config->item('environment') == 'local') {
                      $consent_data = array(
                      'Status' => 'paid',
                      );

                      $this->db->where('RowID', $transfer_model->RowID);

                      $this->db->update('lmais_search', $consent_data);
                      } */
                    //Get user model
                    $user_model = $this->db
                                    ->select('*')
                                    ->where('RowID', $search_model->UserID)
                                    ->get('lmais_users')->row();

                    if ($user_model) {
                        //Send login email
                        $parameters = array(
                            'name' => ucwords(strtolower($user_model->FirstName . ' ' . $user_model->LastName)),
                            'currency' => 'KES',
                            'service' => $search_model->Description,
                            'amount' => $amount_paid,
                            'time' => date('d/m/Y H:i'),
                            'refnumber' => $search_model->TransactionID,
                        );

                        //Generate PDF invoice and save to file
                        $pdf_attachment_path = $this->invoice($search_model->TransactionID, $this->service_slug, $search_model->UserID, 'file');

                        //Generate PDF invoice and save to file
                        $document_attachment_path = $this->search($search_model->TransactionID, 'file');

                        if ($document_attachment_path) {//Attachment was generated successfully
                            $recipient['attachments'] = array($pdf_attachment_path, $document_attachment_path);
                        }//E# if statement

                        $this->load->library('message');

                        if ($user_model->Email) {
                            $recipient['to']['email'] = $user_model->Email;
                            $recipient['to']['name'] = $parameters['name'];

                            //Send email
                            $sent = $this->message->email(1, array('name' => 'LMAIS', 'email' => 'info@ardhi.go.ke'), 1, $recipient, 'document', 'en', $parameters);
                        }//E# statement
                        //Delete the genearated pdf to save on space
                        if ($pdf_attachment_path) {
                            unlink($pdf_attachment_path);
                        }//E# if statement
                        //Delete the genearated pdf to save on space
                        if ($document_attachment_path) {
                            unlink($document_attachment_path);
                        }//E# if statement

                        if ($user_model->Phone) {
                            $this->message->sms(1, 1, 1, $user_model->Phone, 'document', 'en', $parameters);
                        }//E# statement
                    }//E# statement
                    //Clear all consent session
                    $this->session->unset_userdata('sessioned_search');
                    $this->session->unset_userdata('sessioned_buyers');
                    $this->session->unset_userdata('sessioned_rla');

                    $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Thanks making the payment</div>');
                } else {
                    
                }//E# if else statement
            }//E# if else statement
        }//E# if else statement

        redirect('UserView/LoadApplications');
    }

//E# payment_completed() function

    public function test_email() {
        //Send login email
        $this->load->library('message');

        $this->message->send_email();
    }

    public function test_invoice() {
        //Load Ecitizen
        $this->load->library('ecitizen');

        $parameters = array(
            'apiClientID' => 'TYV0FUD0BY',
            'secureHash' => 'TYV0FUD0BY',
            'billDesc' => 'TYV0FUD0BY',
            'billRefNumber' => 'TYV0FUD0BY',
            'currency' => 'TYV0FUD0BY',
            'serviceID' => '29',
            'clientMSISDN' => 'TYV0FUD0BY',
            'clientName' => 'TYV0FUD0BY',
            'clientIDNumber' => 'TYV0FUD0BY',
            'clientEmail' => 'TYV0FUD0BY',
            'callBackURLOnSuccess' => 'TYV0FUD0BY',
            'pictureURL' => 'TYV0FUD0BY',
            'notificationURL' => 'TYV0FUD0BY',
            'amountExpected' => 'TYV0FUD0BY',
        );
        $ecitizen_response = $this->ecitizen->raise_invoice($parameters);

        var_dump($ecitizen_response);
    }

    public function get_user_by_id() {
        //Load Ecitizen
        $this->load->library('ecitizen');

        $ecitizen_response = $this->ecitizen->get_user_by_id_number($this->input->post('idNumber'));

        //var_dump($ecitizen_response);
        $response['message'] = $ecitizen_response['message'];
        $response['type'] = $ecitizen_response['status'] ? 'success' : 'error';

        echo json_encode($response);
    }

    public function index() {
        if ($this->session->userdata('Logged_in') != '') {

            self::loadindex();
        } else
            $this->load->view('signin/PageResources/loginheader');
        $this->load->view('signin/login');
    }

    /**
     * S# raise_invoices() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Raise invoices
     * 
     */
    private function raise_invoices($sessioned_search, $transaction_id) {

        $doc_id = $this->session->userdata('sessioned_rla')->DocID;

        $invoice_data = array();

        $now = date('Y-m-d H:i:s');

        foreach ($this->service_payable as $single_service_payable) {
            //Service model
            $service_model = $this->db
                            ->select('*')
                            ->where('Slug', $single_service_payable['slug'])
                            ->get('lmais_landservicetypes')->row();

            if ($service_model) {
                //Invoice model
                $invoice_model = $this->db
                                ->select('*')
                                ->where('ServiceID', $sessioned_search['RowID'])
                                ->where('ServiceTypeID', $service_model->RowID)
                                ->get('lmais_transactions')->row();

                if (!$invoice_model) {
                    //Consent Fee Invoice
                    $invoice_data[] = array(
                        'Agent' => $_SERVER['HTTP_USER_AGENT'],
                        'Ip' => $_SERVER['REMOTE_ADDR'],
                        'Currency' => 'KES',
                        'Date' => $now,
                        'Type' => 'invoice',
                        'Description' => $service_model->ServiceName,
                        'ServiceTypeID' => $service_model->RowID,
                        'DocID' => $doc_id,
                        'Amount' => $service_model->ServiceAmount,
                        'ServiceSlug' => $this->service_slug,
                        'ServiceID' => $sessioned_search['RowID'],
                        'Status' => 'unpaid',
                        'UserID' => $this->session->userdata('UserID'),
                        'TransactionID' => $transaction_id,
                    );
                }//E# if statement
            }//E# if statement
        }//E# foreach statement

        if ($invoice_data) {
            $this->db->insert_batch('lmais_transactions', $invoice_data);
        }//E# if statement
    }

//E# raise_invoices() function

    /**
     * S# payment() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * payment
     */
    public function payment() {
        //Service model
        $service_model = $this->db
                        ->select('*')
                        ->where('Slug', $this->service_slug)
                        ->get('lmais_landservicetypes')->row();

        $sessioned_search = $this->session->userdata('sessioned_search');

        $received_request_model = $this->db
                        ->select('*')
                        ->order_by('RowID', 'desc')
                        ->where('ServiceSlug', $this->service_slug)
                        ->where('ServiceID', $sessioned_search['RowID'])
                        ->get('lmais_receivedrequests')->row();

        if (!$received_request_model) {
            $received_request_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->get('lmais_receivedrequests')->row();

            $serial = $received_request_model ? $received_request_model->TransactionID : 'TRA00D0C1A';

            $serial++;
            $generated_serial = $serial++;

            if ($this->session->has_userdata('sessioned_rla')) {
                $doc_id = $this->session->userdata('sessioned_rla')->DocID;
            } else {
                $doc_id = '';
            }//E# if else statement

            $received_request_data = array(
                'BuyerUserID' => $this->session->userdata('UserID'),
                'TransactionID' => $generated_serial,
                'Status' => 'unpaid',
                'CompletionStage' => 0,
                'ServiceSlug' => $this->service_slug,
                'ServiceID' => $sessioned_search['RowID'],
                'ServiceTypeID' => 1,
                'DocID' => $doc_id,
                'LandUse' => 1,
                'Status' => 'Pending',
                'DateCreated' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('lmais_receivedrequests', $received_request_data);
        } else {
            $generated_serial = $received_request_model->TransactionID;
        }//E# if else statement
        //Update buyers with the transaction id
        $seller_data = array(
            'TransactionID' => $generated_serial,
        );

        $sessioned_search = $this->session->userdata('sessioned_search');

        $this->db->where('ServiceID', $sessioned_search['RowID']);
        $this->db->where('ServiceSlug', $this->service_slug);

        $this->db->update('lmais_sellers', $seller_data);

        //Create invoices
        $this->raise_invoices($sessioned_search, $generated_serial);

        $search_data = array(
            'TransactionID' => $generated_serial
        );

        $this->db->where('RowID', $sessioned_search['RowID']);

        $this->db->update('lmais_search', $search_data);

        //Get user model
        $user_model = $this->db
                        ->select('*')
                        ->where('RowID', $sessioned_search['UserID'])
                        ->get('lmais_users')->row();

        if ($user_model) {
            //Send login email
            $parameters = array(
                'name' => ucwords(strtolower($user_model->FirstName . ' ' . $user_model->LastName)),
                'currency' => 'KES',
                'service' => ucfirst($this->service_slug),
                'amount' => $sessioned_search['AmountPayable'],
                'time' => date('d/m/Y H:i'),
                'refnumber' => $generated_serial,
            );

            //Generate PDF invoice and save to file
            $pdf_attachment_path = $this->invoice($generated_serial, $this->service_slug, $sessioned_search['UserID'], 'file');

            if ($pdf_attachment_path) {//Attachment was generated successfully
                $recipient['attachments'] = array($pdf_attachment_path);
            }//E# if statement

            $this->load->library('message');

            if ($user_model->Email) {
                $recipient['to']['email'] = $user_model->Email;
                $recipient['to']['name'] = $parameters['name'];

                //Send email
                $sent = $this->message->email(1, array('name' => 'LMAIS', 'email' => 'info@ardhi.go.ke'), 1, $recipient, 'invoice', 'en', $parameters);
            }//E# statement
            //Delete the genearated pdf to save on space
            if ($pdf_attachment_path) {
                unlink($pdf_attachment_path);
            }//E# if statement

            if ($user_model->Phone) {
                $this->message->sms(1, 1, 1, $user_model->Phone, 'invoice', 'en', $parameters);
            }//E# statement
        }//E# statement

        return $serial;
    }

//E# payment() function


    private function session_search($search_id, $session_sellers = false) {

        $search_model = $this->db->get_where('lmais_search', array('RowID' => $search_id))->result_array();

        $this->session->set_userdata('sessioned_search', $search_model[0]);

        if ($session_sellers == 'sellers') {
            //Select all sellers
            $seller_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ServiceID', $search_model[0]['RowID'])
                            ->where('ServiceSlug', $this->service_slug)
                            ->get('lmais_sellers')->result_array();


            $this->session->set_userdata('sessioned_sellers', $seller_model);
        }

        if ($session_sellers == 'sellers') {
            //Select all sellers
            $seller_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ServiceID', $search_model[0]['RowID'])
                            ->where('ServiceSlug', $this->service_slug)
                            ->get('lmais_sellers')->result_array();


            $this->session->set_userdata('sessioned_sellers', $seller_model);
        }
    }

    public function check_property_details() {
        //Clear all search session
        $this->session->unset_userdata('sessioned_search');
        $this->session->unset_userdata('sessioned_rent');
        $this->session->unset_userdata('sessioned_sellers');
        $this->session->unset_userdata('sessioned_rla');
        $this->session->unset_userdata('sessioned_action');
        $this->session->unset_userdata('sessioned_balance');

        $this->clear_all_cache();

        $rla_model = $this->db
                        ->select('*')
                        ->where('RegistryID', $this->input->post('RegistryID'))
                        ->where('RegistrationSectionID', $this->input->post('RegistrationSectionID'))
                        ->where('ParcelNo', $this->input->post('ParcelNo'))
                        ->limit(1)
                        ->get('lmais_rla')->row();

        $property_availability = array(
            'Agent' => $_SERVER['HTTP_USER_AGENT'],
            'DateCreated' => date('Y-m-d H:i:s'),
            'Ip' => $_SERVER['REMOTE_ADDR'],
            'ParcelNo' => $this->input->post('ParcelNo'),
            'RegistryID' => $this->input->post('RegistryID'),
            'RegistrationSectionID' => $this->input->post('RegistrationSectionID'),
            'TenureID' => $this->input->post('TenureID'),
            'Status' => 0,
            'ServiceSlug' => $this->service_slug
        );

        if ($rla_model) {

            $property_availability['Status'] = 1;

            $this->session->set_userdata('sessioned_rla', $rla_model);

            //Service model
            $service_model = $this->db
                            ->select('*')
                            ->where('Slug', $this->service_slug)
                            ->get('lmais_landservicetypes')->row();

            $search_data = array(
                'Agent' => $_SERVER['HTTP_USER_AGENT'],
                'DateCreated' => date('Y-m-d H:i:s'),
                'Ip' => $_SERVER['REMOTE_ADDR'],
                'UserID' => $this->session->userdata('UserID'),
                'RegistryID' => $this->input->post('RegistryID'),
                'RegistrationSectionID' => $this->input->post('RegistrationSectionID'),
                'ParcelNumber' => $this->input->post('ParcelNo'),
                'TenureID' => $this->input->post('TenureID'),
                'LRNumber' => $rla_model->Title,
                'DocID' => $rla_model->DocID,
                'Description' => $service_model->ServiceName,
                'Status' => 'unpaid',
            );
            $search_data['AmountPayable'] = 0;
            foreach ($this->service_payable as $single_service_payable) {
                //Service model
                $service_model = $this->db
                                ->select('*')
                                ->where('Slug', $single_service_payable['slug'])
                                ->get('lmais_landservicetypes')->row();
                if ($service_model) {
                    $search_data[$single_service_payable['db_field']] = $service_model->ServiceAmount;
                }//E# if statement

                $search_data['AmountPayable'] +=$service_model->ServiceAmount;
            }//E# foreach statement

            $this->db->insert('lmais_search', $search_data);

            $search_id = $this->db->insert_id();

            $search_data = array(
                'EcitizenBillRef' => $this->service_slug . '_' . $search_id
            );

            $this->db->where('RowID', $search_id);

            $this->db->update('lmais_search', $search_data);

            $this->session_search($search_id);

            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> We have found your land</div>');
            $redirect_to = 'search/apply_search?step=2';
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error!</strong> This parcel of land could not be found. Kindly confirm your details</div>');

            $redirect_to = 'search/apply_search';
        }//E# if statement

        $this->db->insert('lmais_property_availability', $property_availability);

        redirect($redirect_to);
    }

    /**
     * S# get_balance() function
     * 
     * Get balance
     * 
     * @param int $search_id Search ID
     * 
     * @return float Balance
     */
    private function get_balance($search_id) {
        //Get total invoice
        $total_invoice = $this->db
                        ->select_sum('Amount')
                        ->where('ServiceID', $search_id)
                        ->where('ServiceSlug', $this->service_slug)
                        ->where('Type', 'invoice')
                        ->get('lmais_transactions')->row();

        //Get total payment
        $total_payment = $this->db
                        ->select_sum('Amount')
                        ->where('ServiceID', $search_id)
                        ->where('ServiceSlug', $this->service_slug)
                        ->where('Type', 'payment')
                        ->get('lmais_transactions')->row();

        return $total_invoice->Amount - $total_payment->Amount;
    }

//E# get_balance() function 

    public function apply_search() {

        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');
            $data['tenure'] = $this->TenureModel->GetAllTenure();
            $data['input'] = $this->input->get();

            if (!array_key_exists('step', $data['input'])) {
                $data['input']['step'] = 1;
            }//E# if statement

            if ($data['input']['step'] == 1) {
                //Convenience model
                $data['convenience_model'] = $this->db
                                ->select('*')
                                ->where('Slug', 'convenience')
                                ->get('lmais_landservicetypes')->row();
            }//E# if statement

            $balance = 0;
            $action = 'creating';

            if (array_key_exists('reference_number', $data['input'])) {
                $this->session_search($data['input']['reference_number']);
                $this->session->set_userdata('action', 'updating');
                $balance = $this->get_balance($data['input']['reference_number']);

                $this->session->set_userdata('sessioned_balance', $balance);
                $this->session->set_userdata('sessioned_action', 'updating');
            }//E# if else statement

            $data['sessioned_action'] = $this->session->userdata('sessioned_action');
            $data['sessioned_balance'] = $this->session->userdata('sessioned_balance');

            if ($this->session->has_userdata('sessioned_search')) {
                $data['sessioned_search'] = $this->session->userdata('sessioned_search');
            } else {
                $data['sessioned_search'] = false;
                if ($data['input']['step'] > 1) {
                    redirect('search/apply_search');
                }//E# if statement
            }//E# if else statement

            if ($this->session->has_userdata('sessioned_sellers')) {
                $data['sessioned_sellers'] = $this->session->userdata('sessioned_sellers');
            } else {
                $data['sessioned_sellers'] = false;
            }//E# if else statement
            if ($this->session->userdata()) {
                $data['sessioned_user'] = $this->session->userdata();
            } else {
                $data['sessioned_user'] = false;
            }//E# if else statement
            //Review
            if ($data['input']['step'] == 2) {
                $registry_model = $this->db
                                ->select('*')
                                ->where('RowID', $data['sessioned_search']['RegistryID'])
                                ->get('lmais_registry')->row();

                $data['registry_model'] = $registry_model ? $registry_model : null;

                $registration_section_model = $this->db
                                ->select('*')
                                ->where('RowID', $data['sessioned_search']['RegistrationSectionID'])
                                ->get('lmais_registrationsections')->row();

                $data['registration_section_model'] = $registration_section_model ? $registration_section_model : null;

                $tenure_model = $this->db
                                ->select('*')
                                ->where('RowID', $data['sessioned_search']['TenureID'])
                                ->get('lmais_tenure')->row();

                $data['tenure_model'] = $tenure_model ? $tenure_model : null;
            }//E# if statement

            $this->load->view($Department . '/PageResources/addparcelsheader');
            $this->load->view($Department . '/Header/header');
            if ($Department == 'user') {
                
            } else {
                $this->load->view($Department . '/Menu/menu');
            }//E# if else statement


            $this->load->view('search/search', $data);
        }
    }

    public function search_app() {
        $SearchID = "CNT" . md5(time() . mt_rand(1, 1000000));
        $Proprietor_First_Name = $this->input->post("ProprietorFirstName");
        $Proprietor_Middle_Name = $this->input->post("ProprietorMiddleName");
        $Proprietor_Last_Name = $this->input->post("ProprietorLastName");
        $Proprietor_Nationality = $this->input->post("ProprietorNationality");
        $Proprietor_Idenification = $this->input->post("ProprietorIdenification");
        $Proprietor_Mobile_Number = $this->input->post("ProprietorMobileNumber");
        $Proprietor_Email_Address = $this->input->post("ProprietorEmailAddress");
        $Proprietor_Address = $this->input->post("ProprietorAddress");
        $Proposed_First_Name = $this->input->post("ProposedFirstName");
        $Proposed_Middle_Name = $this->input->post("ProposedMiddleName");
        $Proposed_Last_Name = $this->input->post("ProposedLastName");
        $Proposed_Nationality = $this->input->post("ProposedNationality");
        $Proposed_Identification = $this->input->post("ProposedIdentification");
        $Proposed_Mobile_Number = $this->input->post("ProposedMobileNumber");
        $Proposed_Email_Address = $this->input->post("ProposedEmailAddress");
        $Proposed_Address = $this->input->post("ProposedAddress");
        $Search_Interest = $this->input->post("SearchInterest");
        $Search_Description = $this->input->post("SearchDescription");
        $LRNumber = $this->input->post("LRNumber");
        $AreaSize = $this->input->post("AreaSize");
        $Registry = $this->input->post("Registry");
        $County = $this->input->post("County");
        $CreatedBy = $this->session->userdata('RowID');

        $data = array(
            'Registry' => $Registry,
            'LRNO' => $LRNO,
            'AreaSize' => $AreaSize,
            'AreaUnit' => $AreaUnit,
            'Tenure' => $Tenure,
            'RegisteredProprietor' => $RegisteredProprietor,
            'TypeOfDevelopment' => $TypeOfDevelopment,
            'NearbyInfrastructure' => $NearbyInfrastructure,
            'NearbyValue' => $NearbyValue,
            'TypeOfRoad' => $TypeOfRoad,
            'NameOfRoad' => $NameOfRoad,
            'DistanceFromRoad' => $DistanceFromRoad,
            'TypeOfTown' => $TypeOfTown,
            'NameOfTown' => $NameOfTown,
            'DistanceFromTown' => $DistanceFromTown,
            'CreatedBy' => $CreatedBy,
        );
        $usr_result = $this->SearchModel->apply_search($data);

        //check if username and password is correct
        if ($usr_result == TRUE) { //active user record is present
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                              <a href="#" class="close" data-dismiss="alert">&times;</a>
                             
                            <strong>Success!</strong> You have successfully Added new Parcel!</div>');

            redirect('start/addvaluationdata');
        } else if ($usr_result == FALSE) {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                             <a href="#" class="close" data-dismiss="alert">&times;</a>
                              <strong>Error!</strong> Data are not saved.. Try again</div>');

            redirect('start/addvaluationdata');
        }
    }

    public function review_application() {

        $this->payment();

        redirect('search/apply_search?step=3');
    }

    function GetRegistriesMatch($ActID, $CountyID) {
        //$ActID = $this->uri->segment(3);
        //$CountyID = $this->uri->segment(4);
        $Registries = $this->Registrymodel->GetMatchedRegistries($ActID, $CountyID);

        return $Registries;
    }

    public function clear_all_cache() {

        $CI = & get_instance();
        $path = $CI->config->item('cache_path');

        $cache_path = ($path == '') ? APPPATH . 'cache/' : $path;

        $handle = opendir($cache_path);
        while (($file = readdir($handle)) !== FALSE) {
            //Leave the directory protection alone
            if ($file != '.htaccess' && $file != 'index.html') {
                @unlink($cache_path . '/' . $file);
            }
        }
        closedir($handle);


        if ($handle = opendir($cache_path)) {
            //echo "Directory handle: $handle <br /><br/>";

            while (false !== ($entry = readdir($handle))) {
                //echo $entry."<br />";
                $n = basename($entry);
                //echo "name = ".$n."<br />";  
                //echo "length of name = ".strlen($n)."<br />";
                if (strlen($n) >= 32) {
                    //echo "file name's 32 chars long <br />";
                    $p = APPPATH . 'cache/' . $entry;
                    //echo $p;                  
                    unlink($p);
                }
                //echo "<br />";
            }
            closedir($handle);
        }
    }

}
