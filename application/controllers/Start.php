<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class start extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('ParcelsValuationModel', '', TRUE);
        $this->load->model('usermodel', '', TRUE);
        $this->load->model('SearchResultsModel', '', TRUE);
        $this->load->model('Email_templates_model', '', TRUE);
        $this->load->model('Settings_Model', '', TRUE);
        $this->load->model('proprietorModel', '', TRUE);
        $this->load->model('encumbranceModel', '', TRUE);
    }

    public function internal() {

        $data = array();
        $data['input'] = $this->input->get();

        if ($this->session->userdata('Logged_in') != '') {

            self::loadindex();
        } else {
            $this->load->view('signin/PageResources/loginheader');
            $this->load->view('signin/login', $data);
        }
    }

    public function backend() {

        $data = array();
        $data['input'] = $this->input->get();

        if ($this->session->userdata('Logged_in') != '') {

            self::loadindex();
        } else {
            $this->load->view('signin/PageResources/loginheader');
            $this->load->view('signin/login', $data);
        }
    }

    /**
     * S# complete_registration() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Complete registration from Ecitizen
     * 
     * */
    public function complete_registration() {
        //Load Ecitizen
        $this->load->library('ecitizen');

        $ecitizen_response = $this->ecitizen->get_user_by_id_number($this->input->post('IDNumber'));

        $response['message'] = $ecitizen_response['message'];

        if ($ecitizen_response['status']) {
            $user_data = array(
                'Pin' => $this->input->post('Pin'),
                'IDNumber' => $ecitizen_response['message']['id_number'],
                'Phone' => $ecitizen_response['message']['mobile_number'],
                'Email' => $ecitizen_response['message']['email'],
                'FirstName' => $ecitizen_response['message']['first_name'],
                'MiddleName' => $ecitizen_response['message']['last_name'],
                'LastName' => $ecitizen_response['message']['surname'],
                'Position' => 'user',
                'DateCreated' => date('Y-m-d H:i:s'),
                'UserStatus' => 'Active',
            );


            $this->db->insert('lmais_users', $user_data);

            $user_id = $this->db->insert_id();

            $parameters = array(
                'name' => ucwords(strtolower($ecitizen_response['message']['first_name'] . ' ' . $ecitizen_response['message']['surname'])),
            );

            $this->load->library('message');

            $recipient['to']['email'] = $ecitizen_response['message']['email'];
            $recipient['to']['name'] = $parameters['name'];
            if ($ecitizen_response['message']['email']) {
                //Send email
                $sent = $this->message->email(1, array('name' => 'LMAIS', 'email' => 'info@ardhi.go.ke'), 1, $recipient, 'ecitizen_registration', 'en', $parameters);
            }

            if ($ecitizen_response['message']['mobile_number']) {
                $this->message->sms(1, 1, 1, $ecitizen_response['message']['mobile_number'], 'ecitizen_registration', 'en', $parameters);
            }//E

            $sessiondata = array(
                'UserID' => $user_id,
                'IDNumber' => $this->input->post('IDNumber'),
                'FirstName' => $this->input->post('FirstName'),
                'LastName' => $this->input->post('LastName'),
                'UserType' => 'user',
                'Phone' => $this->input->post('Phone'),
                'Email' => $this->input->post('Email'),
                'Logged_in' => true
            );
            $this->session->set_userdata($sessiondata);

            $this->session->set_flashdata('login_success ', '<div class="alert alert-danger');
            //Send registration email
            redirect('start/loadindex');
        } else {
            redirect('http://197.248.4.237:666/oauth/authorize?client_id=aeb38ab4f46811e6bff70050560101de&redirect_uri=http://197.248.119.218:8086/lmais/&response_type=code');
        }//E# if else statement
    }

//E# complete_registration() function

    /**
     * ecitizen_login() function 
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Ecitizen login for citizens
     */
    public function index() {
        //Get returned code from ecitizen
        $code = $this->input->get('code');

        //Check if ID Number Exists
        if (isset($code) && $code != '') {
            $this->load->library('ecitizen');
            $ecitizen_response = $this->ecitizen->ecitizen_login($code);

            if ($ecitizen_response['status']) {
                $id_number = $ecitizen_response['message']['id_number'];
                $usr_result = $this->usermodel->getusers($id_number);
                //Select user model
                $user_model = $this->db
                                ->select('*')
                                ->where('IDNumber', $id_number)
                                ->where('Position', 'user')
                                ->get('lmais_users')->result_array();

                //if ($rent_model) {
                //if user already exists
                if ($user_model) {

                    $sessiondata = array(
                        'UserID' => $usr_result[0]->RowID,
                        'IDNumber' => $id_number,
                        'FirstName' => $usr_result[0]->FirstName,
                        'LastName' => $usr_result[0]->LastName,
                        'UserType' => $usr_result[0]->Position,
                        'Phone' => $usr_result[0]->Phone,
                        'Email' => $usr_result[0]->Email,
                        'PartnerID' => $usr_result[0]->PartnerID,
                        'Logged_in' => true
                    );
                    $this->session->set_userdata($sessiondata);

                    $this->session->set_flashdata('login_success ', '<div class="alert alert-danger');

                    redirect('start/loadindex');
                } else {
                    $data['user'] = $ecitizen_response['message'];
                    $this->load->view('signin/PageResources/registerheader');
                    //$this->load->view('signin/PageResources/Header/header');
                    //Register View for first time user
                    $this->load->view('signin/register', $data);
                }//E# if else statement
            } else {
                redirect('http://197.248.4.237:666/oauth/authorize?client_id=aeb38ab4f46811e6bff70050560101de&redirect_uri=http://197.248.119.218:8086/lmais/&response_type=code');
            }//E# if else statement    
        } else {
            redirect('http://197.248.4.237:666/oauth/authorize?client_id=aeb38ab4f46811e6bff70050560101de&redirect_uri=http://197.248.119.218:8086/lmais/&response_type=code');
        }//E# if else statement
    }

    public function resend_otp() {

        $user_model = $this->usermodel->getUserByField('RowID', $this->input->post('user_id'));

        if ($user_model) {
            $otp = $this->usermodel->generateSmsOtp('numeric', 4);

            $this->usermodel->updateSmsOtp($user_model->RowID, $otp);

            $parameters = array(
                'name' => $user_model->FirstName,
                'otp' => $otp,
                'product' => 'LMAIS',
                'time' => date('d-m-Y H:i:s')
            );

            //Send login email
            $this->load->library('message');

            $this->message->sms(1, 1, 1, $user_model->Phone, 'login_otp', 'en', $parameters);

            $this->session->set_flashdata('loginmsg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						 Dear ' . $user_model->FirstName . ' as SMS code has been re-sent to you, use  it to complete login</div>');
        } else {
            $this->session->set_flashdata('loginmsg', '<div class="alert alert-danger fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						 Oops! User not found. Kindly restart login process.</div>');
        }//E# if statement

        redirect("?otp_view=otp&user_id=" . $this->input->post('user_id'));
    }

//E# resend_otp() function

    /**
     * S# confirm_otp() function 
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Confirm otp
     * 
     * @return redirectS
     */
    public function confirm_otp() {

        $user_model = $this->usermodel->otpCheck($this->input->post('SmsOtp'));

        if ($user_model) {
            $now = date('Y-m-d H:i:s');

            $db_time = date('Y-m-d H:i:s', strtotime($user_model->SmsOtpTime . "+5 minutes"));

            if ($now > $db_time) {

                $this->session->set_flashdata('loginmsg', '<div class="alert alert-danger fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						 Sorry ' . $user_model->FirstName . ', your login code has expired. Kindly request another one</div>');

                redirect("?otp_view=otp&user_id=" . $user_model->RowID);
            } else {

                $sessiondata = array(
                    'UserID' => $user_model->RowID,
                    'IDNumber' => $user_model->IDNumber,
                    'FirstName' => $user_model->FirstName,
                    'LastName' => $user_model->LastName,
                    'UserType' => $user_model->Position,
                    'Phone' => $user_model->Phone,
                    'Email' => $user_model->Email,
                    'PartnerID' => $user_model->PartnerID,
                    'Logged_in' => true
                );
                $this->session->set_userdata($sessiondata);

                //Clear otp
                $this->usermodel->updateSmsOtp($user_model->RowID, '');

                redirect("start/loadindex");
            }//E# if else statement
        } else {
            $this->session->set_flashdata('loginmsg', '<div class="alert alert-danger fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						 Incorrect login code. Kindly try again or request the code</div>');

            redirect("?otp_view=otp");
        }//
    }

    public function LoginUsers() {

        $IDNumber = trim($this->input->post("IDNumber"));
        $Password = trim($this->input->post("Password"));

        $usr_result = $this->usermodel->getusers($IDNumber);

        //check if username and password is correct
        if ($usr_result > 0) { //active user record is present
            //$dbpassword =$usr_result->Password;
            $dbpassword = $usr_result[0]->Password;

            if (password_verify($Password, $dbpassword)) {

                if ($usr_result[0]->Position !== 'user') {

                    $otp = $this->usermodel->generateSmsOtp('numeric', 4);

                    $this->usermodel->updateSmsOtp($usr_result[0]->RowID, $otp);

                    $parameters = array(
                        'name' => $usr_result[0]->FirstName,
                        'otp' => $otp,
                        'product' => 'LMAIS',
                        'time' => date('d-m-Y H:i:s')
                    );

                    //Send login email
                    $this->load->library('message');

                    $this->message->sms(1, 1, 1, $usr_result[0]->Phone, 'login_otp', 'en', $parameters);

                    $this->session->set_flashdata('loginmsg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						 Dear ' . $usr_result[0]->FirstName . ' as SMS code has been sent to you, use it to complete login</div>');
                    
                    redirect("start/internal?otp_view=otp&user_id=" . $usr_result[0]->RowID);
                } else {
                    $sessiondata = array(
                        'UserID' => $usr_result[0]->RowID,
                        'IDNumber' => $IDNumber,
                        'FirstName' => $usr_result[0]->FirstName,
                        'LastName' => $usr_result[0]->LastName,
                        'UserType' => $usr_result[0]->Position,
                        'Phone' => $usr_result[0]->Phone,
                        'Email' => $usr_result[0]->Email,
                        'PartnerID' => $usr_result[0]->PartnerID,
                        'Logged_in' => true
                    );
                    $this->session->set_userdata($sessiondata);

                    $this->session->set_flashdata('login_success ', '<div class="alert alert-danger');

                    redirect("start/loadindex");
                }//E# if statement
            } else {
                //set the session variables
                $this->session->set_flashdata('loginmsg', '<div class="alert alert-danger text-center">password... Try again or recover your password online by clicking on Recover Password button below. .</div>');

                redirect('start/index');
            }
        } else if ($usr_result == FALSE) {
            $this->session->set_flashdata('loginmsg', '<div class="alert alert-danger text-center">IIncorrect ID Number or password.</div>');

            redirect('start/index');
        }
    }

    public function loadindex() {
        if ($this->session->userdata('Logged_in') != '') {
            //
            $received_model = $this->db
                            ->select('*')
                            ->where('BuyerUserID', $this->session->userdata('UserID'))
                            ->or_where('SellerUserID', $this->session->userdata('UserID'))
                            ->get('lmais_receivedrequests')->result_array();

            $data['received_model'] = $received_model ? $received_model : null;

            if ($data['received_model']) {
                $index = 0;

                foreach ($data['received_model'] as $single_model) {

                    //Get total payments
                    $total_payment = $this->db
                                    ->select_sum('Amount')
                                    ->where('ServiceID', $single_model['ServiceID'])
                                    ->where('ServiceSlug', $single_model['ServiceSlug'])
                                    ->where('Type', 'payment')
                                    ->where('Type', 'payment')
                                    ->get('lmais_transactions')->row();

                    //Get total invoices
                    $total_invoice = $this->db
                                    ->select_sum('Amount')
                                    ->where('ServiceID', $single_model['ServiceID'])
                                    ->where('ServiceSlug', $single_model['ServiceSlug'])
                                    ->where('Type', 'invoice')
                                    ->get('lmais_transactions')->row();

                    $data['received_model'][$index]['Balance'] = $total_invoice->Amount - $total_payment->Amount;
                    $index++;
                }//E# foreach statement
            }//E# if statement

            $Department = $this->session->userdata('UserType');
            $Department =($Department == 'bank')?'user' :$Department;
            
            $data['Results'] = get_setting('app_title');
            $this->load->view($Department . '/PageResources/dashboardheader');
            $this->load->view($Department . '/Header/header');

            if ($Department == '9999') {

                // } else {
                $this->load->view($Department . '/Menu/menu');
            }//E# if else statement
            $this->load->view($Department . '/Pages/index2', $data);
        } else {
            redirect(base_url() . 'start/start/index');
        }
    }

    public function addvaluationdata() {
        $data['RoadNames'] = $this->GetRoadNameMatchRoadType();
        $this->load->view('Valuation/PageResources/addparcelsheader');
        $this->load->view('Valuation/Header/header');
        $this->load->view('Valuation/Menu/menu');
        $this->load->view('Valuation/Pages/addvaluationdata', $data);
    }

    public function RegisterUser() {
        $this->load->view('Valuation/PageResources/RegisterUserheader');
        $this->load->view('Valuation/Header/header');
        $this->load->view('Valuation/Menu/menu');

        $this->load->view('Valuation/Pages/RegisterUser');
    }

    public function RegisterNewUser() {
        $Identification = $this->input->post("Identification");
        $FirstName = $this->input->post("FirstName");
        $MiddleName = $this->input->post("MiddleName");
        $LastName = $this->input->post("LastName");
        $MobileNumber = $this->input->post("MobileNumber");
        $EmailAddress = $this->input->post("EmailAddress");
        $Gender = $this->input->post("Gender");
        $Nationality = $this->input->post("Nationality");
        $PinNo = $this->input->post("PinNo");
        $BoxNo = $this->input->post("BoxNo");
        $PhysicalAddress = $this->input->post("PhysicalAddress");
        $Town = $this->input->post("Town");
        $Usertype = "user";

        $data = array(
            'IDNumber' => $Identification,
            'FirstName' => $FirstName,
            'MiddleName' => $MiddleName,
            'LastName' => $LastName,
            'Phone' => $MobileNumber,
            'Email' => $EmailAddress,
            'Gender' => $Gender,
            'Nationality' => $Nationality,
            'PinNo' => $PinNo,
            'BoxNo' => $BoxNo,
            'PhysicalAddress' => $PhysicalAddress,
            'City' => $Town,
            'Position' => $Usertype,
        );

        $add_user = $this->usermodel->registerusers($data);
        //check if username and password is correct
        if ($add_user == TRUE) {
            //active user record is present
            $sessiondata = array(
                'UserID' => $add_user[0]->RowID,
                'IDNumber' => $Identification,
                'FirstName' => $FirstName,
                'LastName' => $LastName,
                'UserType' => $Usertype,
                'Logged_in' => true
            );

            $this->session->set_userdata($sessiondata);

            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> You have successfully completed Registration!</div>');


            redirect("start/loadindex");
        } else if ($add_user == FALSE) {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						 <a href="#" class="close" data-dismiss="alert">&times;</a>
						  <strong>Error!</strong> Data are not saved.. Try again</div>');
        }
    }

    public function AddParcelValuation() {
        $Registry = $this->input->post("Registry");
        $LRNO = $this->input->post("LRNO");
        $AreaSize = $this->input->post("AreaSize");
        $AreaUnit = $this->input->post("AreaUnit");
        $NatureOfTitle = $this->input->post("NatureOfTitle");
        $RegisteredProprietor = $this->input->post("RegisteredProprietor");
        $TypeOfDevelopment = $this->input->post("TypeOfDevelopment");
        $NearbyInfrastructure = $this->input->post("NearbyInfrastructure");
        $NearbyValue = $this->input->post("NearbyValue");
        $TypeOfRoad = $this->input->post("TypeOfRoad");
        $NameOfRoad = $this->input->post("NameOfRoad");
        $DistanceFromRoad = $this->input->post("DistanceFromRoad");
        $TypeOfTown = $this->input->post("TypeOfTown");
        $NameOfTown = $this->input->post("NameOfTown");
        $DistanceFromTown = $this->input->post("DistanceFromTown");
        $CreatedBy = $this->session->userdata('RowID');
        ;

        $data = array(
            'Registry' => $Registry,
            'LRNO' => $LRNO,
            'AreaSize' => $AreaSize,
            'AreaUnit' => $AreaUnit,
            'NatureOfTitle' => $NatureOfTitle,
            'RegisteredProprietor' => $RegisteredProprietor,
            'TypeOfDevelopment' => $TypeOfDevelopment,
            'NearbyInfrastructure' => $NearbyInfrastructure,
            'NearbyValue' => $NearbyValue,
            'TypeOfRoad' => $TypeOfRoad,
            'NameOfRoad' => $NameOfRoad,
            'DistanceFromRoad' => $DistanceFromRoad,
            'TypeOfTown' => $TypeOfTown,
            'NameOfTown' => $NameOfTown,
            'DistanceFromTown' => $DistanceFromTown,
            'CreatedBy' => $CreatedBy,
        );
        $usr_result = $this->ParcelsValuationModel->addnewparcel($data);

        //check if username and password is correct
        if ($usr_result == TRUE) { //active user record is present
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> You have successfully Added new Parcel!</div>');

            redirect('start/addvaluationdata');
        } else if ($usr_result == FALSE) {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						 <a href="#" class="close" data-dismiss="alert">&times;</a>
						  <strong>Error!</strong> Data are not saved.. Try again</div>');

            redirect('start/addvaluationdata');
        }
    }

    public function logout() {

        $this->session->unset_userdata('logged_in');

        session_destroy();
        self::index();
    }

    public function reset_password_form() {
        $this->load->view('Valuation/PageResources/loginheader');
        $this->load->view('signin/reset_password_form');
    }

    public function send_reset_password_mail() {

        $email = $this->input->post("Email");
        $existing_user = $this->usermodel->is_email_exists($email);

        //send reset password email if found account with this email
        if ($existing_user) {
            $email_template = $this->Email_templates_model->get_final_template("reset_password");

            $parser_data["ACCOUNT_HOLDER_NAME"] = $existing_user->FirstName . " " . $existing_user->LastName;
            $parser_data["SIGNATURE"] = $email_template->signature;
            $parser_data["SITE_URL"] = base_url();
            $key = encode_id($this->encrypt->encode($existing_user->Email . '|' . (time() + (24 * 60 * 60))), "reset_password");
            $parser_data['RESET_PASSWORD_URL'] = base_url("signin/new_password/" . $key);

            $message = $this->parser->parse_string($email_template->message, $parser_data, TRUE);
            if ($this->send_app_mails($email, $email_template->subject, $message)) {
                echo json_encode(array('success' => true, 'message' => lang("reset_info_send")));
            } else {
                echo json_encode(array('success' => false, 'message' => lang('error_occurred')));
            }
        } else {
            echo json_encode(array("success" => false, 'message' => lang("no_acount_found_with_this_email")));
            return false;
        }
    }

    public function send_app_mails($to, $subject, $message, $optoins = array()) {
        $email_config = Array(
            'charset' => 'utf-8',
            'mailtype' => 'html'
        );

        //check mail sending method from settings
        if (get_setting("email_protocol") === "smtp") {
            $email_config["protocol"] = "smtp";
            $email_config["smtp_host"] = $this->Settings_model->get_setting("email_smtp_host");
            $email_config["smtp_port"] = $this->Settings_model->get_setting("email_smtp_port");
            $email_config["smtp_user"] = $this->Settings_model->get_setting("email_smtp_user");
            $email_config["smtp_pass"] = $this->Settings_model->get_setting("email_smtp_pass");
        }

        $ci = get_instance();
        $ci->load->library('email', $email_config);
        $ci->email->clear();
        $ci->email->set_newline("\r\n");
        $ci->email->from(get_setting("email_sent_from_address"), get_setting("email_sent_from_name"));
        $ci->email->to($to);
        $ci->email->subject($subject);
        $ci->email->message($message);

        //add attachment
        $attachments = get_array_value($optoins, "attachments");
        if (is_array($attachments)) {
            foreach ($attachments as $value) {
                $ci->email->attach(trim($value));
            }
        }

        //check cc
        $cc = get_array_value($optoins, "cc");
        if ($cc) {
            $ci->email->cc($cc);
        }

        //send email
        if ($ci->email->send()) {
            return true;
        } else {
            //show error message in none production version
            if (ENVIRONMENT !== 'production') {
                show_error($ci->email->print_debugger());
            }
            return false;
        }
    }

    public function SearchLR() {

        $LRNO = trim($this->input->post("LRNO"));
        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');

            $data['Results'] = $this->SearchResultsModel->SearchResults($LRNO);
            $data['ResultsCount'] = $this->SearchResultsModel->ReturnResultsCount($LRNO);
            $this->load->view($Department . '/PageResources/searchresultsheader');
            $this->load->view($Department . '/Header/header');
            $this->load->view($Department . '/Menu/menu');
            $this->load->view($Department . '/Pages/searchresults', $data);
        }
    }

    public function SearchLRd() {

        $LRNO = trim($this->input->post("LRNO"));
        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');

            $data['Results'] = $this->SearchResultsModel->SearchResultsd($LRNO);

            $this->load->view($Department . '/PageResources/searchresultsheader');
            $this->load->view($Department . '/Header/header');
            $this->load->view($Department . '/Menu/menu');
            $this->load->view($Department . '/Pages/searchresultsd', $data);
        }
    }

    public function searchdetails($RowID) {
        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');
            if (!$RowID) {
                
            } Else
                $data['Results'] = $this->SearchResultsModel->SearchDetails($RowID);

            $this->load->view($Department . '/PageResources/searchdetailsheader');
            $this->load->view($Department . '/Header/header');
            $this->load->view($Department . '/Menu/menu');
            $this->load->view($Department . '/Pages/searchdetails', $data);
        }
    }

    public function searchdetailsd($RowID) {
        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');

            if (!$RowID) {
                
            } Else
                $data['Results'] = $this->SearchResultsModel->SearchDetailsd($RowID);
            $this->load->view($Department . '/PageResources/usersheader');
            $this->load->view($Department . '/Header/header');
            $this->load->view($Department . '/Menu/menu');
            $this->load->view($Department . '/Pages/searchdetailsd', $data);
        }
    }

    public function UnassignedView($TransactionID) {
        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');

            if (!$TransactionID) {
                
            } else
                $result = $this->SearchResultsModel->UnassignedView($TransactionID);

            $data['Results'] = $result;
            $this->load->view($Department . '/PageResources/searchdetailsheader');
            $this->load->view($Department . '/Header/header');
            $this->load->view($Department . '/Menu/menu');
            $this->load->view($Department . '/Pages/UnassignedView', $data);
        }
    }

    public function EditValuation($RowID) {
        if (!$RowID) {
            
        } Else
            $data['Results'] = $this->SearchResultsModel->SearchDetails($RowID);

        $this->load->view('Valuation/PageResources/RegisterUserheader');
        $this->load->view('Valuation/Header/header');
        $this->load->view('Valuation/Menu/menu');
        $this->load->view('Valuation/Pages/EditValuation', $data);
    }

    public function UnassignedValuations() {

        $data['Results'] = $this->ParcelsValuationModel->GetUnassignedValuations();

        $this->load->view('Valuation/PageResources/UnassignedValuationsheader');
        $this->load->view('Valuation/Header/header');
        $this->load->view('Valuation/Menu/menu');
        $this->load->view('Valuation/Pages/UnassignedValuations', $data);
    }

    public function EditParcelValuation() {
        $Registry = $this->input->post("Registry");
        $LRNO = $this->input->post("LRNO");
        $AreaSize = $this->input->post("AreaSize");
        $AreaUnit = $this->input->post("AreaUnit");
        $NatureOfTitle = $this->input->post("NatureOfTitle");
        $RegisteredProprietor = $this->input->post("RegisteredProprietor");
        $TypeOfDevelopment = $this->input->post("TypeOfDevelopment");
        $NearbyInfrastructure = $this->input->post("NearbyInfrastructure");
        $NearbyValue = $this->input->post("NearbyValue");
        $LandValue = $this->input->post("LandValue");
        $TypeOfRoad = $this->input->post("TypeOfRoad");
        $NameOfRoad = $this->input->post("NameOfRoad");
        $DistanceFromRoad = $this->input->post("DistanceFromRoad");
        $TypeOfTown = $this->input->post("TypeOfTown");
        $NameOfTown = $this->input->post("NameOfTown");
        $DistanceFromTown = $this->input->post("DistanceFromTown");
        $ModifiedBy = $this->session->userdata('RowID');
        $Email = $this->input->post("Email");
        $Telephone = $this->input->post("Telephone");
        $RowID = $this->input->post("RowID");
        $data = array(
            'Registry' => $Registry,
            'LRNO' => $LRNO,
            'AreaSize' => $AreaSize,
            'AreaUnit' => $AreaUnit,
            'NatureOfTitle' => $NatureOfTitle,
            'RegisteredProprietor' => $RegisteredProprietor,
            'TypeOfDevelopment' => $TypeOfDevelopment,
            'NearbyInfrastructure' => $NearbyInfrastructure,
            'NearbyValue' => $NearbyValue,
            'LandValue' => $LandValue,
            'TypeOfRoad' => $TypeOfRoad,
            'NameOfRoad' => $NameOfRoad,
            'DistanceFromRoad' => $DistanceFromRoad,
            'TypeOfTown' => $TypeOfTown,
            'NameOfTown' => $NameOfTown,
            'DistanceFromTown' => $DistanceFromTown,
            'ModifiedBy' => $ModifiedBy,
            'Telephone' => $Telephone,
            'Email' => $Email
        );
        $usr_result = $this->ParcelsValuationModel->editparcel($RowID, $data);

        //check if username and password is correct
        if ($usr_result == TRUE) { //active user record is present
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> You have successfully edited the parcel!</div>');

            redirect('start/searchdetails/' . $RowID);
        } else if ($usr_result == FALSE) {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						 <a href="#" class="close" data-dismiss="alert">&times;</a>
						  <strong>Error!</strong> Data are not saved.No Changes were made</div>');

            redirect('start/searchdetails/' . $RowID);
        }
    }

    /*     * Added functions, original lmais above
     * the code below represents functionality added by
     * The Royal King Kabs
     */

    function GetRoadNameMatchRoadType() {
        $roadTypeID = $this->uri->segment(3);
        $roadNames = $this->ParcelsValuationModel->GetRoadNameMatchRoadType($roadTypeID);
        echo $roadNames;
        return $roadNames;
    }

    function GetTownNamesMatchTownType() {
        $townTypeID = $this->uri->segment(3);
        $townNames = $this->ParcelsValuationModel->GetTownNamesMatchTownType($townTypeID);
        echo $townNames;
        return $townNames;
    }

}
