<?php

use Dompdf\Dompdf;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Title extends CI_Controller {

    public function index() {

        $this->load->library('pdf');

        $this->pdf->setPaper('A3', 'landscape');
        $pages = array();

        $pages['page_1'] = $this->load->view('title/page_1', '$data',true);
        $pages['page_2'] = $this->load->view('title/page_2', '$data',true);
        $pages['page_3'] = $this->load->view('title/page_3', '$data',true);
        $pages['page_4'] = $this->load->view('title/page_4', '$data',true);

        $this->pdf->load_view('title/title', $pages);
        $this->pdf->render();
        $this->pdf->stream("welcome.pdf");
        /*
          // instantiate and use the dompdf class
          $dompdf = new Dompdf();

          $pages['page_1'] = $this->load->view('title/page_1', '$data', true);

          $html = $this->load->view('title/title', $pages, true);

          $dompdf->loadHtml($html);

          // (Optional) Setup the paper size and orientation
          $dompdf->setPaper('A4', 'landscape');

          // Render the HTML as PDF
          $dompdf->render();

          // Get the generated PDF file contents
          $pdf = $dompdf->output();

          // Output the generated PDF to Browser
          $dompdf->stream();
          /*
          // die('sad');
          //Load the library
          $this->load->library('html2pdf');

          //Set folder to save PDF to
          $this->html2pdf->folder('./assets/pdfs/');

          //Set the filename to save/download as
          $this->html2pdf->filename('test.pdf');

          //Set the paper defaults
          $this->html2pdf->paper('a4', 'landscape');

          $data = array(
          'page1' => 'PDF Created',
          'message' => 'Hello World!'
          );

          $pages['page_1'] = $this->load->view('title/page_1', '$data', true);
          // echo $pages['page_1'];
          //return $pages['page_1'];
          //$pages['page_2'] = $this->load->view('title/page_2', $data, true);
          //$pages['page_3'] = $this->load->view('title/page_3', $data, true);
          //$pages['page_4'] = $this->load->view('title/page_4', $data, true);
          //Load html view
          //echo $this->load->view('title/title', $pages, true);

          echo $this->html2pdf->html($this->load->view('title/title', $pages, true));

          //dd(;);
          if ($this->html2pdf->create('save')) {
          //PDF was successfully saved or downloaded
          echo 'PDF saved';
          }
         * 
         */
    }

    public function mail_pdf() {
        //Load the library
        $this->load->library('html2pdf');

        $this->html2pdf->folder('./assets/pdfs/');
        $this->html2pdf->filename('email_test.pdf');
        $this->html2pdf->paper('a4', 'portrait');

        $data = array(
            'title' => 'PDF Created',
            'message' => 'Hello World!'
        );
        //Load html view
        $this->html2pdf->html($this->load->view('pdf', $data, true));

        //Check that the PDF was created before we send it
        if ($path = $this->html2pdf->create('save')) {

            $this->load->library('email');

            $this->email->from('your@example.com', 'Your Name');
            $this->email->to('someone@example.com');

            $this->email->subject('Email PDF Test');
            $this->email->message('Testing the email a freshly created PDF');

            $this->email->attach($path);

            $this->email->send();

            echo $this->email->print_debugger();
        }
    }

}

/* End of file example.php */
/* Location: ./application/controllers/example.php */