<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Changeofuser extends MY_Controller {

    private $service_slug = 'changeofuser';
    private $service_payable = array(
        array(
            'slug' => 'changeofuser',
            'db_field' => 'ChangeofUserFee',
        ),
        array(
            'slug' => 'convenience',
            'db_field' => 'ConvenienceFee',
        ),
    );

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('TenureModel', '', TRUE);
        $this->load->model('ChangeuserModel', '', TRUE);
        $this->load->model('Countymodel', '', TRUE);
        $this->load->model('Registrymodel', '', TRUE);
        $this->load->model('ParcelsValuationModel', '', TRUE);
        $this->load->model('Countrymodel', '', TRUE);
        // $this->load->model('Settings_Model', '', TRUE);
        // $this->load->model('proprietorModel', '', TRUE);
        // $this->load->model('encumbranceModel', '', TRUE);
    }

    /**
     * S# payment_completed() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Payment completed
     * 
     */
    public function payment_completed() {
        $data = $this->input->get();

        //Load Ecitizen
        $this->load->library('ecitizen');


        if (array_key_exists('bill_ref', $data)) {
            $changeofuser_model = $this->db
                            ->select('*')
                            ->where('EcitizenBillRef', $data['bill_ref'])
                            ->get('lmais_changeofuser')->row();

            if ($changeofuser_model) {
                $ecitizen_response = $this->ecitizen->query_transaction_status($data['bill_ref']);

                if ($ecitizen_response['status']) {
                    $amount_paid = $ecitizen_response['message']['amount'];

                    $now = date('Y-m-d H:i:s');

                    $transaction_model = $this->db
                                    ->select('*')
                                    ->where('ServiceID', $changeofuser_model->RowID)
                                    ->where('ServiceSlug', $this->service_slug)
                                    ->where('Type', 'payment')
                                    ->get('lmais_transactions')->row();

                    if (!$transaction_model) {//Payment does not exist so create one
                        //Rent Due Payment
                        $payment_data = array(
                            'AccountNumber' => $ecitizen_response['message']['account_number'], //CHANGE - Addition
                            'Currency' => 'KES',
                            'Date' => $now,
                            'Type' => 'payment',
                            'Description' => $changeofuser_model->Description,
                            'Amount' => $amount_paid,
                            'ServiceSlug' => $this->service_slug,
                            'ServiceID' => $changeofuser_model->RowID,
                            'UserID' => $changeofuser_model->UserID,
                            'TransactionID' => $changeofuser_model->TransactionID,
                            'DocID' => $changeofuser_model->DocID,
                            'Status' => 'paid',
                            'Ip' => 'paid',
                            'Agent' => $_SERVER['HTTP_USER_AGENT'],
                            'Ip' => $_SERVER['REMOTE_ADDR'],
                        );

                        // var_dump($payment_data);
                        // die("SAd");
                        $this->db->insert('lmais_transactions', $payment_data);
                    }//E# if else statement
                    //Get total payments
                    $total_payment = $this->db
                                    ->select_sum('Amount')
                                    ->where('ServiceID', $changeofuser_model->RowID)
                                    ->where('ServiceSlug', $this->service_slug)
                                    ->where('Type', 'payment')
                                    ->get('lmais_transactions')->row();

                    //Get total invoices
                    $total_invoice = $this->db
                                    ->select_sum('Amount')
                                    ->where('ServiceID', $changeofuser_model->RowID)
                                    ->where('ServiceSlug', $this->service_slug)
                                    ->where('Type', 'invoice')
                                    ->get('lmais_transactions')->row();

                    if (($total_payment->Amount == $total_invoice->Amount) || ($total_payment->Amount > $total_payment->Amount)) {

                        $changeofuser_data = array(
                            'Status' => 'paid',
                        );

                        $this->db->where('RowID', $changeofuser_model->RowID);

                        $this->db->update('lmais_changeofuser', $changeofuser_data);

                        $received_request_data = array(
                            'Status' => 'paid',
                        );

                        $this->db->where('TransactionID', $changeofuser_model->TransactionID);

                        $this->db->update('lmais_receivedrequests', $received_request_data);
                    }//E# statement

                    /*
                      if ($this->config->item('environment') == 'local') {
                      $consent_data = array(
                      'Status' => 'paid',
                      );

                      $this->db->where('RowID', $transfer_model->RowID);

                      $this->db->update('lmais_changeofuser', $consent_data);
                      } */
                    //Get user model
                    $user_model = $this->db
                                    ->select('*')
                                    ->where('RowID', $changeofuser_model->UserID)
                                    ->get('lmais_users')->row();

                    if ($user_model) {
                        //Send login email
                        $parameters = array(
                            'name' => ucwords(strtolower($user_model->FirstName . ' ' . $user_model->LastName)),
                            'currency' => 'KES',
                            'service' => $changeofuser_model->Description,
                            'amount' => $amount_paid,
                            'time' => date('d/m/Y H:i'),
                            'refnumber' => $changeofuser_model->TransactionID,
                        );

                        //Generate PDF invoice and save to file
                        $pdf_attachment_path = $this->invoice($changeofuser_model->TransactionID, $this->service_slug, $changeofuser_model->UserID, 'file');

                        if ($pdf_attachment_path) {//Attachment was generated successfully
                            $recipient['attachments'] = array($pdf_attachment_path);
                        }//E# if statement

                        $this->load->library('message');

                        if ($user_model->Email) {
                            $recipient['to']['email'] = $user_model->Email;
                            $recipient['to']['name'] = $parameters['name'];

                            //Send email
                            $sent = $this->message->email(1, array('name' => 'LMAIS', 'email' => 'info@ardhi.go.ke'), 1, $recipient, 'invoice', 'en', $parameters);
                        }//E# statement
                        //Delete the genearated pdf to save on space
                        if ($pdf_attachment_path) {
                            unlink($pdf_attachment_path);
                        }//E# if statement

                        if ($user_model->Phone) {
                            $this->message->sms(1, 1, 1, $user_model->Phone, 'invoice', 'en', $parameters);
                        }//E# statement
                    }//E# statement
                    //Clear all consent session
                    $this->session->unset_userdata('sessioned_changeofuser');
                    $this->session->unset_userdata('sessioned_rla');

                    $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Thanks making the payment</div>');
                } else {
                    
                }//E# if else statement
            }//E# if else statement
        }//E# if else statement

        redirect('UserView/LoadApplications');
    }

//E# payment_completed() function

    public function test_email() {
        //Send login email
        $this->load->library('message');

        $this->message->send_email();
    }

    public function test_invoice() {
        //Load Ecitizen
        $this->load->library('ecitizen');

        $parameters = array(
            'apiClientID' => 'TYV0FUD0BY',
            'secureHash' => 'TYV0FUD0BY',
            'billDesc' => 'TYV0FUD0BY',
            'billRefNumber' => 'TYV0FUD0BY',
            'currency' => 'TYV0FUD0BY',
            'serviceID' => '29',
            'clientMSISDN' => 'TYV0FUD0BY',
            'clientName' => 'TYV0FUD0BY',
            'clientIDNumber' => 'TYV0FUD0BY',
            'clientEmail' => 'TYV0FUD0BY',
            'callBackURLOnSuccess' => 'TYV0FUD0BY',
            'pictureURL' => 'TYV0FUD0BY',
            'notificationURL' => 'TYV0FUD0BY',
            'amountExpected' => 'TYV0FUD0BY',
        );
        $ecitizen_response = $this->ecitizen->raise_invoice($parameters);

        var_dump($ecitizen_response);
    }

    public function get_user_by_id() {
        //Load Ecitizen
        $this->load->library('ecitizen');

        $ecitizen_response = $this->ecitizen->get_user_by_id_number($this->input->post('idNumber'));

        //var_dump($ecitizen_response);
        $response['message'] = $ecitizen_response['message'];
        $response['type'] = $ecitizen_response['status'] ? 'success' : 'error';

        echo json_encode($response);
    }

    public function index() {
        if ($this->session->userdata('Logged_in') != '') {

            self::loadindex();
        } else
            $this->load->view('signin/PageResources/loginheader');
        $this->load->view('signin/login');
    }

    /**
     * S# raise_invoices() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Raise invoices
     * 
     */
    private function raise_invoices($sessioned_changeofuser, $transaction_id) {

        $doc_id = $this->session->userdata('sessioned_rla')->DocID;

        $invoice_data = array();

        $now = date('Y-m-d H:i:s');

        foreach ($this->service_payable as $single_service_payable) {
            //Service model
            $service_model = $this->db
                            ->select('*')
                            ->where('Slug', $single_service_payable['slug'])
                            ->get('lmais_landservicetypes')->row();

            if ($service_model) {
                $changeofuser_data[$single_service_payable['db_field']] = $service_model->ServiceAmount;

                //Consent Fee Invoice
                $invoice_data[] = array(
                    'Agent' => $_SERVER['HTTP_USER_AGENT'],
                    'Ip' => $_SERVER['REMOTE_ADDR'],
                    'Currency' => 'KES',
                    'Date' => $now,
                    'Type' => 'invoice',
                    'Description' => $service_model->ServiceName,
                    'DocID' => $doc_id,
                    'Amount' => $service_model->ServiceAmount,
                    'ServiceCode' => ($this->config->item('environment') == 'live') ? $service_model->LiveServiceCode : $service_model->TestServiceCode,
                    'ServiceTypeID' => $service_model->RowID,
                    'ServiceSlug' => $this->service_slug,
                    'ServiceID' => $sessioned_changeofuser['RowID'],
                    'Status' => 'unpaid',
                    'UserID' => $this->session->userdata('UserID'),
                    'TransactionID' => $transaction_id,
                );
            }//E# if statement
        }//E# foreach statement

        if ($invoice_data) {
            $this->db->insert_batch('lmais_transactions', $invoice_data);
        }//E# if statement
    }

//E# raise_invoices() function

    /**
     * S# payment() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * payment
     */
    public function payment() {
        $sessioned_changeofuser = $this->session->userdata('sessioned_changeofuser');

        $received_request_model = $this->db
                        ->select('*')
                        ->order_by('RowID', 'desc')
                        ->where('ServiceSlug', $this->service_slug)
                        ->where('ServiceID', $sessioned_changeofuser['RowID'])
                        ->get('lmais_receivedrequests')->row();

        if (!$received_request_model) {
            $received_request_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->get('lmais_receivedrequests')->row();

            $serial = $received_request_model ? $received_request_model->TransactionID : 'TRA00D0C1A';

            $serial++;
            $generated_serial = $serial++;

            if ($this->session->has_userdata('sessioned_rla')) {
                $doc_id = $this->session->userdata('sessioned_rla')->DocID;
            } else {
                $doc_id = '';
            }//E# if else statement

            $received_request_data = array(
                'BuyerUserID' => $this->session->userdata('UserID'),
                'TransactionID' => $generated_serial,
                'Status' => 'unpaid',
                'CompletionStage' => 0,
                'ServiceSlug' => $this->service_slug,
                'ServiceID' => $sessioned_changeofuser['RowID'],
                'ServiceTypeID' => 1,
                'DocID' => $doc_id,
                'LandUse' => 1,
                'Status' => 'Pending',
                'DateCreated' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('lmais_receivedrequests', $received_request_data);
        } else {
            $generated_serial = $received_request_model->TransactionID;
        }//E# if else statement
        //Update buyers with the transaction id
        $seller_data = array(
            'TransactionID' => $generated_serial,
        );

        $sessioned_changeofuser = $this->session->userdata('sessioned_changeofuser');

        $this->db->where('ServiceID', $sessioned_changeofuser['RowID']);
        $this->db->where('ServiceSlug', $this->service_slug);

        $this->db->update('lmais_sellers', $seller_data);

        //Create invoices
        $this->raise_invoices($sessioned_changeofuser, $generated_serial);

        $changeofuser_data = array(
            'TransactionID' => $generated_serial
        );

        $this->db->where('RowID', $sessioned_changeofuser['RowID']);

        $this->db->update('lmais_changeofuser', $changeofuser_data);

        //Get user model
        $user_model = $this->db
                        ->select('*')
                        ->where('RowID', $sessioned_rent['UserID'])
                        ->get('lmais_users')->row();

        if ($user_model) {
            //Send login email
            $parameters = array(
                'name' => ucwords(strtolower($user_model->FirstName . ' ' . $user_model->LastName)),
                'currency' => 'KES',
                'service' => ucfirst($this->service_slug),
                'amount' => $sessioned_rent['AmountPayable'],
                'time' => date('d/m/Y H:i'),
                'refnumber' => $generated_serial,
            );

            //Generate PDF invoice and save to file
            $pdf_attachment_path = $this->invoice($generated_serial, $this->service_slug, $sessioned_rent['UserID'], 'file');

            if ($pdf_attachment_path) {//Attachment was generated successfully
                $recipient['attachments'] = array($pdf_attachment_path);
            }//E# if statement

            $this->load->library('message');

            if ($user_model->Email) {
                $recipient['to']['email'] = $user_model->Email;
                $recipient['to']['name'] = $parameters['name'];

                //Send email
                $sent = $this->message->email(1, array('name' => 'LMAIS', 'email' => 'info@ardhi.go.ke'), 1, $recipient, 'invoice', 'en', $parameters);
            }//E# statement
            var_dump($sent);
            die();
            //Delete the genearated pdf to save on space
            if ($pdf_attachment_path) {
                unlink($pdf_attachment_path);
            }//E# if statement

            if ($user_model->Phone) {
                $this->message->sms(1, 1, 1, $user_model->Phone, 'invoice', 'en', $parameters);
            }//E# statement
        }//E# statement

        return $serial;
    }

//E# payment() function


    public function save_transaction_details() {

        $changeofuser_data = array(
            'CurrentUse' => $this->input->post('currentuse'),
            'ProposedUse' => $this->input->post('proposeduse'),
            'Reason' => $this->input->post('reason'),
        );

        $sessioned_changeofuser = $this->session->userdata('sessioned_changeofuser');

        $this->db->where('RowID', $sessioned_changeofuser['RowID']);

        $this->db->update('lmais_changeofuser', $changeofuser_data);

        $this->session_changeofuser($sessioned_changeofuser['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Change of use  details saved successfully</div>');

        redirect('changeofuser/apply_changeofuser?step=3');
    }

    public function save_applicant() {

        $changeofuser_data = array(
            'ProprietorFirstName' => $this->input->post('ProprietorFirstName'),
            'ProprietorMiddleName' => $this->input->post('ProprietorMiddleName'),
            'ProprietorLastName' => $this->input->post('ProprietorLastName'),
            'ProprietorNationality' => $this->input->post('ProprietorNationality'),
            'ProprietorIdentification' => $this->input->post('ProprietorIdentification'),
            'ProprietorMobileNumber' => $this->input->post('ProprietorMobileNumber'),
            'ProprietorEmailAddress' => $this->input->post('ProprietorEmailAddress'),
            'ProprietorAddress' => $this->input->post('ProprietorAddress'),
        );

        $sessioned_changeofuser = $this->session->userdata('sessioned_changeofuser');

        $this->db->where('RowID', $sessioned_changeofuser['RowID']);

        $this->db->update('lmais_changeofuser', $changeofuser_data);

        $this->session_changeofuser($sessioned_changeofuser['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Applicant\'s details saved successfully</div>');

        redirect('changeofuser/apply_changeofuser?step=3');
    }

    public function rate_clearance() {
        $config['upload_path'] = './files/storage_files';
        $config['allowed_types'] = 'gif|jpg|png|pdf';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('clearance_certificate')) {
            $error = array('error' => $this->upload->display_errors());

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error!</strong> ' . trim($error['error']) . '</div>');

            redirect('changeofuser/apply_changeofuser?step=5');
        } else {

            $file_data = $this->upload->data();

            $now = date('Y-m-d H:i:s');

            $sessioned_changeofuser = $this->session->userdata('sessioned_changeofuser');

            $filestorage_data = array(
                'CreatedBy' => $this->session->userdata('UserID'),
                'DateCreated' => $now,
                'DateModified' => $now,
                'FileName' => $file_data['file_name'],
                'FileType' => $file_data['file_type'],
                'FileSize' => $file_data['file_size'],
                'FileExt' => $file_data['file_ext'],
                'IsImage' => $file_data['is_image'],
                'FileStage' => '',
                'ModifiedBy' => $this->session->userdata('UserID'),
                'OriginalFilename' => $file_data['orig_name'],
                'ServiceID' => 'changeofuse',
                'ServiceTypeID' => $sessioned_changeofuser['RowID']
            );

            $this->db->insert('lmais_filestorage', $filestorage_data);

            $data = array('upload_data' => $file_data);

            $changeofuser_data = array(
                'RateClearanceCertificateFile' => $data['upload_data']['file_name'],
                'RateClearanceNumber' => $this->input->post('RateClearanceNumber'),
            );

            $sessioned_changeofuser = $this->session->userdata('sessioned_changeofuser');

            $this->db->where('RowID', $sessioned_changeofuser['RowID']);

            $this->db->update('lmais_changeofuser', $changeofuser_data);

            $this->session_changeofuser($sessioned_changeofuser['RowID']);

            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Thanks you for clearing your rates</div>');

            redirect('changeofuser/apply_changeofuser?step=6');
        }//E# if else statement

        die();
    }

    public function changeofuser_clearance() {

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Thanks you for clearing your Change of User Fees</div>');
        redirect('changeofuser/apply_changeofuser?step=7');
    }

    private function session_changeofuser($changeofuser_id, $session_sellers = false) {

        $changeofuser_model = $this->db->get_where('lmais_changeofuser', array('RowID' => $changeofuser_id))->result_array();

        $this->session->set_userdata('sessioned_changeofuser', $changeofuser_model[0]);

        if ($session_sellers == 'sellers') {
            //Select all sellers
            $seller_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ServiceID', $changeofuser_model[0]['RowID'])
                            ->where('ServiceSlug', $this->service_slug)
                            ->get('lmais_sellers')->result_array();


            $this->session->set_userdata('sessioned_sellers', $seller_model);
        }

        if ($session_sellers == 'sellers') {
            //Select all sellers
            $seller_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ServiceID', $changeofuser_model[0]['RowID'])
                            ->where('ServiceSlug', $this->service_slug)
                            ->get('lmais_sellers')->result_array();


            $this->session->set_userdata('sessioned_sellers', $seller_model);
        }
    }

    public function save_seller_continue() {
        $sessioned_sellers = $this->session->userdata('sessioned_sellers');

        if ($sessioned_sellers) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> caution applicant\'s details saved successfully</div>');

            redirect('changeofuser/apply_changeofuser?step=3');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Error!</strong> Please add at least one caution applicant</div>');

            redirect('changeofuser/apply_changeofuser?step=2');
        }//E# if else statement
    }

    public function save_seller() {

        if ($this->input->post('IsContactPerson') == 'Y') {
            $changeofuser_data = array(
                'IsContactPerson' => 'N',
            );
            $this->db->where('ServiceID', $this->input->post('ChangeofuserID'));
            $this->db->where('ServiceSlug', 'changeofuser');

            $this->db->update('lmais_sellers', $changeofuser_data);
        }//E# if statement
        $seller_data = array(
            'IsContactPerson' => $this->input->post('IsContactPerson'),
            'UserID' => $this->session->userdata('UserID'),
            'ServiceID' => $this->input->post('ChangeofuserID'),
            'FirstName' => $this->input->post('FirstName'),
            'MiddleName' => $this->input->post('MiddleName'),
            'LastName' => $this->input->post('LastName'),
            'CountryID' => $this->input->post('CountryID'),
            'Identification' => $this->input->post('Identification'),
            'MobileNumber' => $this->input->post('MobileNumber'),
            'EmailAddress' => $this->input->post('EmailAddress'),
            'Address' => $this->input->post('Address'),
            'CreatedBy' => $this->session->userdata('UserID'),
            'DateCreated' => date('Y-m-d H:i:s'),
            'ModifiedBy' => $this->session->userdata('UserID'),
            'DateModified' => date('Y-m-d H:i:s'),
            'Type' => 'individual',
            'ServiceSlug' => 'changeofuser',
        );

        $this->db->insert('lmais_sellers', $seller_data);

        $seller_id = $this->db->insert_id();

        $sessioned_changeofuser = $this->session->userdata('sessioned_changeofuser');

        $this->session_changeofuser($sessioned_changeofuser['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Change of User Applicant\'s details saved successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/changeofuser/apply_changeofuser?step=2'
        ));
    }

    public function check_property_details() {
        //Clear all caution session
        $this->session->unset_userdata('sessioned_changeofuser');
        $this->session->unset_userdata('sessioned_rent');
        $this->session->unset_userdata('sessioned_sellers');
        $this->session->unset_userdata('sessioned_rla');
        $this->session->unset_userdata('sessioned_action');
        $this->session->unset_userdata('sessioned_balance');

        $this->clear_all_cache();

        $rla_model = $this->db
                        ->select('*')
                        ->where('RegistryID', $this->input->post('RegistryID'))
                        ->where('RegistrationSectionID', $this->input->post('RegistrationSectionID'))
                        ->where('ParcelNo', $this->input->post('ParcelNo'))
                        ->limit(1)
                        ->get('lmais_rla')->row();

        $property_availability = array(
            'Agent' => $_SERVER['HTTP_USER_AGENT'],
            'DateCreated' => date('Y-m-d H:i:s'),
            'Ip' => $_SERVER['REMOTE_ADDR'],
            'ParcelNo' => $this->input->post('ParcelNo'),
            'RegistryID' => $this->input->post('RegistryID'),
            'RegistrationSectionID' => $this->input->post('RegistrationSectionID'),
            'TenureID' => $this->input->post('TenureID'),
            'Status' => 0,
        );

        if ($rla_model) {

            $property_availability['Status'] = 1;

            $this->session->set_userdata('sessioned_rla', $rla_model);

            //Service model
            $service_model = $this->db
                            ->select('*')
                            ->where('Slug', $this->service_slug)
                            ->get('lmais_landservicetypes')->row();

            $changeofuser_data = array(
                'Agent' => $_SERVER['HTTP_USER_AGENT'],
                'DateCreated' => date('Y-m-d H:i:s'),
                'Ip' => $_SERVER['REMOTE_ADDR'],
                'UserID' => $this->session->userdata('UserID'),
                'RegistryID' => $this->input->post('RegistryID'),
                'RegistrationSectionID' => $this->input->post('RegistrationSectionID'),
                'ParcelNumber' => $this->input->post('ParcelNo'),
                'TenureID' => $this->input->post('TenureID'),
                'LRNumber' => $rla_model->Title,
                'DocID' => $rla_model->DocID,
                'Description' => $service_model->ServiceName,
                'Prefix' => $service_model->Prefix,
                'Status' => 'unpaid',
            );
            $changeofuser_data['AmountPayable'] = 0;
            foreach ($this->service_payable as $single_service_payable) {
                //Service model
                $service_model = $this->db
                                ->select('*')
                                ->where('Slug', $single_service_payable['slug'])
                                ->get('lmais_landservicetypes')->row();
                if ($service_model) {
                    $changeofuser_data[$single_service_payable['db_field']] = $service_model->ServiceAmount;
                }//E# if statement

                $changeofuser_data['AmountPayable'] +=$service_model->ServiceAmount;
            }//E# foreach statement

            $this->db->insert('lmais_changeofuser', $changeofuser_data);

            $changeofuser_id = $this->db->insert_id();

            $changeofuser_data = array(
                'EcitizenBillRef' => $this->service_slug . '_' . $changeofuser_id
            );

            $this->db->where('RowID', $changeofuser_id);

            $this->db->update('lmais_changeofuser', $changeofuser_data);

            $this->session_changeofuser($changeofuser_id);

            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> We have found your land</div>');
            $redirect_to = 'changeofuser/apply_changeofuser?step=2';
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error!</strong> This parcel of land could not be found. Kindly confirm your details</div>');

            $redirect_to = 'changeofuser/apply_changeofuser';
        }//E# if statement

        $this->db->insert('lmais_property_availability', $property_availability);

        redirect($redirect_to);
    }

    /**
     * S# get_balance() function
     * 
     * Get balance
     * 
     * @param int $changeofuser_id Caution ID
     * 
     * @return float Balance
     */
    private function get_balance($changeofuser_id) {
        //Get total invoice
        $total_invoice = $this->db
                        ->select_sum('Amount')
                        ->where('ServiceID', $changeofuser_id)
                        ->where('ServiceSlug', $this->service_slug)
                        ->where('Type', 'invoice')
                        ->get('lmais_transactions')->row();

        //Get total payment
        $total_payment = $this->db
                        ->select_sum('Amount')
                        ->where('ServiceID', $changeofuser_id)
                        ->where('ServiceSlug', $this->service_slug)
                        ->where('Type', 'payment')
                        ->get('lmais_transactions')->row();

        return $total_invoice->Amount - $total_payment->Amount;
    }

//E# get_balance() function 

    public function apply_changeofuser() {

        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');
            $data['tenure'] = $this->TenureModel->GetAllTenure();
            $data['input'] = $this->input->get();

            if (!array_key_exists('step', $data['input'])) {
                $data['input']['step'] = 1;
            }

            $balance = 0;
            $action = 'creating';

            if (array_key_exists('reference_number', $data['input'])) {
                $this->session_changeofuser($data['input']['reference_number']);
                $this->session->set_userdata('action', 'updating');
                $balance = $this->get_balance($data['input']['reference_number']);

                $this->session->set_userdata('sessioned_balance', $balance);
                $this->session->set_userdata('sessioned_action', 'updating');
            }//E# if else statement

            $data['sessioned_action'] = $this->session->userdata('sessioned_action');
            $data['sessioned_balance'] = $this->session->userdata('sessioned_balance');

            if ($this->session->has_userdata('sessioned_changeofuser')) {
                $data['sessioned_changeofuser'] = $this->session->userdata('sessioned_changeofuser');
            } else {
                $data['sessioned_changeofuser'] = false;
                if ($data['input']['step'] > 1) {
                    redirect('changeofuser/apply_changeofuser');
                }//E# if statement
            }//E# if else statement

            if ($this->session->has_userdata('sessioned_sellers')) {
                $data['sessioned_sellers'] = $this->session->userdata('sessioned_sellers');
            } else {
                $data['sessioned_sellers'] = false;
            }//E# if else statement
            if ($this->session->userdata()) {
                $data['sessioned_user'] = $this->session->userdata();
            } else {
                $data['sessioned_user'] = false;
            }//E# if else statement
            //Review
            if ($data['input']['step'] == 3) {
                $registry_model = $this->db
                                ->select('*')
                                ->where('RowID', $data['sessioned_changeofuser']['RegistryID'])
                                ->get('lmais_registry')->row();

                $data['registry_model'] = $registry_model ? $registry_model : null;

                $registration_section_model = $this->db
                                ->select('*')
                                ->where('RowID', $data['sessioned_changeofuser']['RegistrationSectionID'])
                                ->get('lmais_registrationsections')->row();

                $data['registration_section_model'] = $registration_section_model ? $registration_section_model : null;

                $tenure_model = $this->db
                                ->select('*')
                                ->where('RowID', $data['sessioned_changeofuser']['TenureID'])
                                ->get('lmais_tenure')->row();

                $data['tenure_model'] = $tenure_model ? $tenure_model : null;

                $currentuser_model = $this->db
                                ->select('*')
                                ->where('RowID', $data['sessioned_changeofuser']['CurrentUse'])
                                ->get('lmais_listitems')->row();

                $data['currentuser_model'] = $currentuser_model ? $currentuser_model : null;

                $proposeduser_model = $this->db
                                ->select('*')
                                ->where('RowID', $data['sessioned_changeofuser']['ProposedUse'])
                                ->get('lmais_listitems')->row();

                $data['proposeduser_model'] = $currentuser_model ? $currentuser_model : null;
            }//E# if statement

            $this->load->view($Department . '/PageResources/addparcelsheader');
            $this->load->view($Department . '/Header/header');
            if ($Department == 'user') {
                
            } else {
                $this->load->view($Department . '/Menu/menu');
            }//E# if else statement


            $this->load->view('changeofuser/changeofuser', $data);
        }
    }

    public function changeofuser_app() {
        $ChangeofuserID = "CNT" . md5(time() . mt_rand(1, 1000000));
        $Proprietor_First_Name = $this->input->post("ProprietorFirstName");
        $Proprietor_Middle_Name = $this->input->post("ProprietorMiddleName");
        $Proprietor_Last_Name = $this->input->post("ProprietorLastName");
        $Proprietor_Nationality = $this->input->post("ProprietorNationality");
        $Proprietor_Idenification = $this->input->post("ProprietorIdenification");
        $Proprietor_Mobile_Number = $this->input->post("ProprietorMobileNumber");
        $Proprietor_Email_Address = $this->input->post("ProprietorEmailAddress");
        $Proprietor_Address = $this->input->post("ProprietorAddress");
        $Proposed_First_Name = $this->input->post("ProposedFirstName");
        $Proposed_Middle_Name = $this->input->post("ProposedMiddleName");
        $Proposed_Last_Name = $this->input->post("ProposedLastName");
        $Proposed_Nationality = $this->input->post("ProposedNationality");
        $Proposed_Identification = $this->input->post("ProposedIdentification");
        $Proposed_Mobile_Number = $this->input->post("ProposedMobileNumber");
        $Proposed_Email_Address = $this->input->post("ProposedEmailAddress");
        $Proposed_Address = $this->input->post("ProposedAddress");
        $Current_Use = $this->input->post("Ccurrentuse");
        $Proposed_Use = $this->input->post("proposeduse");
        $LRNumber = $this->input->post("LRNumber");
        $AreaSize = $this->input->post("AreaSize");
        $Registry = $this->input->post("Registry");
        $County = $this->input->post("County");
        $CreatedBy = $this->session->userdata('RowID');

        $data = array(
            'Registry' => $Registry,
            'LRNO' => $LRNO,
            'AreaSize' => $AreaSize,
            'AreaUnit' => $AreaUnit,
            'Tenure' => $Tenure,
            'RegisteredProprietor' => $RegisteredProprietor,
            'TypeOfDevelopment' => $TypeOfDevelopment,
            'NearbyInfrastructure' => $NearbyInfrastructure,
            'NearbyValue' => $NearbyValue,
            'TypeOfRoad' => $TypeOfRoad,
            'NameOfRoad' => $NameOfRoad,
            'DistanceFromRoad' => $DistanceFromRoad,
            'TypeOfTown' => $TypeOfTown,
            'NameOfTown' => $NameOfTown,
            'DistanceFromTown' => $DistanceFromTown,
            'CreatedBy' => $CreatedBy,
        );
        $usr_result = $this->ChangeofuserModel->apply_caution($data);

        //check if username and password is correct
        if ($usr_result == TRUE) { //active user record is present
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                              <a href="#" class="close" data-dismiss="alert">&times;</a>
                             
                            <strong>Success!</strong> You have successfully Added new Parcel!</div>');

            redirect('start/addvaluationdata');
        } else if ($usr_result == FALSE) {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                             <a href="#" class="close" data-dismiss="alert">&times;</a>
                              <strong>Error!</strong> Data are not saved.. Try again</div>');

            redirect('start/addvaluationdata');
        }
    }

    public function delete_seller() {
        $sellerID = $this->input->post('SellerID');

        $this->db->where('RowID', $sellerID);
        $this->db->delete('lmais_sellers');

        $sessioned_changeofuser = $this->session->userdata('sessioned_changeofuser');

        $this->session_changeofuser($sessioned_changeofuser['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong>Change of User Applicant\'s details deleted successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/changeofuser/apply_changeofuser?step=2'
        ));
    }

    public function review_application() {

        $this->payment();

        redirect('changeofuser/apply_changeofuser?step=4');
    }

//E# delete_seller() function

    function GetRegistriesMatch($ActID, $CountyID) {
        //$ActID = $this->uri->segment(3);
        //$CountyID = $this->uri->segment(4);
        $Registries = $this->Registrymodel->GetMatchedRegistries($ActID, $CountyID);

        return $Registries;
    }

    public function clear_all_cache() {

        $CI = & get_instance();
        $path = $CI->config->item('cache_path');

        $cache_path = ($path == '') ? APPPATH . 'cache/' : $path;

        $handle = opendir($cache_path);
        while (($file = readdir($handle)) !== FALSE) {
            //Leave the directory protection alone
            if ($file != '.htaccess' && $file != 'index.html') {
                @unlink($cache_path . '/' . $file);
            }
        }
        closedir($handle);


        if ($handle = opendir($cache_path)) {
            //echo "Directory handle: $handle <br /><br/>";

            while (false !== ($entry = readdir($handle))) {
                //echo $entry."<br />";
                $n = basename($entry);
                //echo "name = ".$n."<br />";  
                //echo "length of name = ".strlen($n)."<br />";
                if (strlen($n) >= 32) {
                    //echo "file name's 32 chars long <br />";
                    $p = APPPATH . 'cache/' . $entry;
                    //echo $p;                  
                    unlink($p);
                }
                //echo "<br />";
            }
            closedir($handle);
        }
    }

}
