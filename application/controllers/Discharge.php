<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Discharge extends MY_Controller {
    private $service_slug = 'dischargeofcharge';
	private $parent_service_slug='charge';
    private $service_payable = array(
        array(
            'slug' => 'dischargeofcharge',
            'db_field' => 'DisChargeFee',
        ),
        array(
            'slug' => 'convenience',
            'db_field' => 'ConvenienceFee',
        ),
    );

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('TenureModel', '', TRUE);
        $this->load->model('Countymodel', '', TRUE);
        $this->load->model('Registrymodel', '', TRUE);
        $this->load->model('ParcelsValuationModel', '', TRUE);
        $this->load->model('Countrymodel', '', TRUE);
        $this->load->model('DischargeModel', '', TRUE);
        // $this->load->model('proprietorModel', '', TRUE);
        // $this->load->model('encumbranceModel', '', TRUE);
    }

    /**
     * S# owner_approve() function
     * 
     * Owner approve
     * 
     */
    public function owner_approve() {

        $TransactionID = $this->input->post("TransactionID");
        $Status = $this->input->post("Status");

        // var_dump($this->input->post());
        //die();
        //  $ChargeID = 55;
        // $Status = 'Approved';
        //Get charge model
        $charge_model = $this->db
                        ->select('*')
                        ->where('TransactionID', $TransactionID)
                        ->get('lmais_charge')->row();

        if ($charge_model) {

            $charge_data = array(
                'Status' => $Status,
            );

            $this->db->where('TransactionID', $TransactionID)
                    ->update('lmais_charge', $charge_data);

            $this->db
                    ->where('UserID', $this->session->userdata('UserID'))
                    ->where('TransactionID', $TransactionID)
                    ->update('lmais_users_actions', $charge_data);

            //Get user model
            $user_model = $this->db
                            ->select('*')
                            ->where('RowID', $charge_model->UserID)
                            ->get('lmais_users')->row();




            if ($user_model) {
                //Service model
                $service_model = $this->db
                                ->select('*')
                                ->where('Slug', $this->service_slug)
                                ->get('lmais_landservicetypes')->row();
                //Send login email
                $parameters = array(
                    'name' => ucwords(strtolower($user_model->FirstName . ' ' . $user_model->LastName)),
                    'service' => $this->service_slug,
                    'refnumber' => $charge_model->TransactionID,
                    'status' => $Status,
                );

                $user_ids = array($user_model->RowID);

                $recipient = array();
                if ($user_model->Email) {
                    $recipient['email'][] = array(
                        'name' => $parameters['name'],
                        'email' => $user_model->Email
                    );
                }//E# statement
                if ($user_model->Phone) {
                    $recipient['sms'][] = array(
                        'name' => $parameters['name'],
                        'phone' => $user_model->Phone
                    );
                }//E# statement
                var_dump($recipient);
                $this->status_changed(strtolower($Status), $charge_model->RowID, $charge_model->DocID, null, $charge_model->TransactionID, $user_ids, $service_model, $recipient, $parameters);
            }//E# statement
            //check if username and password is correct
            if ($Status) { //active user record is present
                $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Status changed to : ' . $Status . '</div>');

                redirect('start/actions');
            } else {

                $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
						 <a href="#" class="close" data-dismiss="alert">&times;</a>
						  <strong>Error!</strong> Data are not saved.No Changes were made</div>');

                redirect('start/actions');
            }//E# if else statement
        }
    }

//E# owner_approve() function

    public function payment_completed() {

        $data = $this->input->get();
//Load Ecitizen
        $this->load->library('ecitizen');
//var_dump($this->config->item('environment'));

        if (array_key_exists('bill_ref', $data)) {
			$RowID=explode('_',$data['bill_ref']);
			$RowID=$RowID[1];
		    
            $charge_model = $this->db
                            ->select('*')
                            ->where('RowID', $RowID)
                            ->get('lmais_charge')->row();


            if ($charge_model) {
                $ecitizen_response = $this->ecitizen->query_transaction_status($data['bill_ref']);

                if ($ecitizen_response['status']) {
                    $amount_paid = $ecitizen_response['message']['amount'];

                    $now = date('Y-m-d H:i:s');

                    $transaction_model = $this->db
                                    ->select('*')
                                    ->where('ServiceID', $charge_model->RowID)
                                    ->where('ServiceSlug', $this->service_slug)
                                    ->where('Type', 'payment')
                                    ->get('lmais_transactions')->row();


                    if (!$transaction_model) {//Payment does not exist so create one
//Rent Due Payment
                        $payment_data = array(
                            'Currency' => 'KES',
                            'Date' => $now,
                            'Type' => 'payment',
                            'Description' => $charge_model->Description,
                            'Amount' => $amount_paid,
                            'ServiceSlug' => $this->service_slug,
                            'ServiceID' => $charge_model->RowID,
                            'UserID' => $charge_model->UserID,
                            'TransactionID' => $charge_model->TransactionID,
                            'DocID' => $charge_model->DocID,
                            'Status' => 'paid',
                            'Ip' => 'paid',
                            'Agent' => $_SERVER['HTTP_USER_AGENT'],
                            'Ip' => $_SERVER['REMOTE_ADDR'],
                        );

                        $this->db->insert('lmais_transactions', $payment_data);
                    }//E# if else statement
//Get total payments
                    $total_payment = $this->db
                                    ->select_sum('Amount')
                                    ->where('ServiceID', $charge_model->RowID)
                                    ->where('ServiceSlug', $this->service_slug)
                                    ->where('Type', 'payment')
                                    ->get('lmais_transactions')->row();

//Get total invoices
                    $total_invoice = $this->db
                                    ->select_sum('Amount')
                                    ->where('ServiceID', $charge_model->RowID)
                                    ->where('ServiceSlug', $this->service_slug)
                                    ->where('Type', 'invoice')
                                    ->get('lmais_transactions')->row();


                    if (($total_payment->Amount == $total_invoice->Amount) || ($total_payment->Amount > $total_invoice->Amount)) {

                        $charge_data = array(
                            'DischargePaymentStatus' => 'paid',
							'DischargeEcitizenBillRef' =>$data['bill_ref'],
							'ApprovalStatus'=>'Discharged',
                        );

                        $this->db->where('RowID', $charge_model->RowID);

                        $this->db->update('lmais_charge', $charge_data);
                    }//E# statement
// if ($this->config->item('environment') == 'local') {
//$charge_data = array(
//    'Status' => 'paid',
//);
//$this->db->where('RowID', $transfer_model->RowID);
// $this->db->update('lmais_charge', $charge_data);
// }
//Get user model
                    $user_model = $this->db
                                    ->select('*')
                                    ->where('RowID', $charge_model->UserID)
                                    ->get('lmais_users')->row();

                    if ($user_model) {
//Send login email
                        $parameters = array(
                            'name' => ucwords(strtolower($user_model->FirstName . ' ' . $user_model->LastName)),
                            'currency' => 'KES',
                            'service' => $charge_model->Description,
                            'amount' => $amount_paid,
                            'time' => date('d/m/Y H:i'),
                            'refnumber' => $charge_model->TransactionID,
                        );

                        $this->load->library('message');

                        if ($user_model->Email) {
                            $recipient['to']['email'] = $user_model->Email;
                            $recipient['to']['name'] = $parameters['name'];

                            //Send email
                            $sent = $this->message->email(1, array('name' => 'LMAIS', 'email' => 'info@ardhi.go.ke'), 1, $recipient, 'payment_received', 'en', $parameters);
                        }//E# statement

                        if ($user_model->Phone) {
                            $this->message->sms(1, 1, 1, $user_model->Phone, 'payment_received', 'en', $parameters);
                        }//E# statement
                    }//E# statement
                    //Request Authorization
                    $this->request_authorization($charge_model, $charge_model->TransactionID);

                    //Clear all consent session
                    $this->session->unset_userdata('sessioned_charge');
                    $this->session->unset_userdata('sessioned_buyers');
                    $this->session->unset_userdata('sessioned_rla');

                    $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Thanks .' . ucfirst(strtolower($this->session->userdata('FirstName') . ' ' . $this->session->userdata('LastName'))) . ' making the payment. We\'ve requested the owners to approve the consent.</div>');
                } else {
                    
                }//E# if else statement
            }//E# if else statement
        }//E# if else statement

        redirect('charge/chargelist');
    }

//E# payment_completed() function

    public function test_email() {
//Send login email
        $this->load->library('message');

        $this->message->send_email();
    }

    public function test_invoice() {
//Load Ecitizen
        $this->load->library('ecitizen');

        $parameters = array(
            'apiClientID' => 'TYV0FUD0BY',
            'secureHash' => 'TYV0FUD0BY',
            'billDesc' => 'TYV0FUD0BY',
            'billRefNumber' => 'TYV0FUD0BY',
            'currency' => 'TYV0FUD0BY',
            'serviceID' => '29',
            'clientMSISDN' => 'TYV0FUD0BY',
            'clientName' => 'TYV0FUD0BY',
            'clientIDNumber' => 'TYV0FUD0BY',
            'clientEmail' => 'TYV0FUD0BY',
            'callBackURLOnSuccess' => 'TYV0FUD0BY',
            'pictureURL' => 'TYV0FUD0BY',
            'notificationURL' => 'TYV0FUD0BY',
            'amountExpected' => 'TYV0FUD0BY',
        );
        $ecitizen_response = $this->ecitizen->raise_invoice($parameters);

        var_dump($ecitizen_response);
    }

    public function get_user_by_id() {
//Load Ecitizen
        $this->load->library('ecitizen');

        $ecitizen_response = $this->ecitizen->get_user_by_id_number($this->input->post('idNumber'));

//var_dump($ecitizen_response);
        $response['message'] = $ecitizen_response['message'];
        $response['type'] = $ecitizen_response['status'] ? 'success' : 'error';

        echo json_encode($response);
    }

    public function index() {
        if ($this->session->userdata('Logged_in') != '') {

            self::loadindex();
        } else
            $this->load->view('signin/PageResources/loginheader');
        $this->load->view('signin/login');
    }

    /**
     * S# request_authorization() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Request authorization
     * 
     * @param array $sessioned_charge Charge
     * @param str $transaction_id Transaction ID
     * 
     */
    private function request_authorization($sessioned_charge, $transaction_id) {

        $proprietorship_model = $this->db
                        ->select('*')
                        ->where('DocID', $sessioned_charge->DocID)
                        ->where('Status', 1)
                        ->get('lmais_proprietorship')->result();

        if ($proprietorship_model) {
            //Get Service Model
            $service_model = $this->db
                            ->select('*')
                            ->where('Slug', $this->service_slug)
                            ->get('lmais_landservicetypes')->row();

            $partner_model = $this->db
                    ->select('*')
                    ->where('RowID', $this->session->userdata('PartnerID'))
                    ->where('Status', 1)
                    ->get('lmais_partners')
                    ->row();

            //Set partner name
            $partner_name = $partner_model ? $partner_model->Name : '';

            //Service model
            $service_model = $this->db
                    ->select('*')
                    ->where('Slug', $this->service_slug)
                    ->get('lmais_landservicetypes')
                    ->row();


//Dear :name, :partner wants to charge your land :title for :currency :principal at :interest per annum repayable before :date. The charge reference number is :refnumber. Kindly log into ecitizen and pay registration fee of :currency :fee to complete the charge process. 
//Set parameters
            $parameters = array(
                'partner' => $partner_name,
                'title' => $sessioned_charge->LRNumber,
                'principal' => number_format($sessioned_charge->PrincipalAmount, 2),
                'interest' => number_format($sessioned_charge->InterestRate, 2),
                'date' => date_format(date_create($sessioned_charge->RepaidByDate), 'd/m/Y'),
                'refnumber' => $transaction_id,
                'fee' => $service_model->ServiceAmount,
                'currency' => 'KES',
                'service' => $service_model->ServiceName,
            );


            $this->load->library('message');

            $now = date('Y-m-d H:i:s');

            foreach ($proprietorship_model as $single_model) {

                //Users Charges Model
                $users_charges_model = $this->db
                                ->select('*')
                                ->where('TransactionID', $transaction_id)
                                ->where('Identification', $single_model->RegisteredID)
                                ->where('ServiceTypeID', $service_model->RowID)
                                ->get('lmais_users_actions')->row();

                //Users Charges Model
                $user_model = $this->db
                                ->select('*')
                                ->where('IDNumber', $single_model->RegisteredID)
                                ->get('lmais_users')->row();

                if (!$user_model) {
                    //Users
                    $users_data = array(
                        'Agent' => $_SERVER['HTTP_USER_AGENT'],
                        'Ip' => $_SERVER['REMOTE_ADDR'],
                        'FirstName' => $single_model->RegisteredProprietor,
                        'IDNumber' => $single_model->RegisteredID,
                        'Phone' => $single_model->RegisteredTel1,
                        'Email' => $single_model->RegisteredEmail,
                        'CreatedBy' => $this->session->userdata('UserID'),
                        'ModifiedBy' => $this->session->userdata('UserID'),
                        'Position' => 'user',
                        'UserStatus' => 'Active',
                        'DateCreated' => $now,
                        'DateModified' => $now,
                    );

                    $this->db->insert('lmais_users', $users_data);

                    //Users Charges Model
                    $user_model = $this->db
                                    ->select('*')
                                    ->where('IDNumber', $single_model->RegisteredID)
                                    ->get('lmais_users')->row();
                }//E# if statement
                //Users Charges Model
                $users_charges_model = $this->db
                                ->select('*')
                                ->where('UserID', $single_model->RegisteredID)
                                ->where('TransactionID', $transaction_id)
                                ->get('lmais_users_actions')->row();

                if (!$users_charges_model) {
                    //Users Charge 
                    $users_charges_data = array(
                        'Agent' => $_SERVER['HTTP_USER_AGENT'],
                        'Ip' => $_SERVER['REMOTE_ADDR'],
                        'ChargeID' => 'KES',
                        'CreatedBy' => $this->session->userdata('UserID'),
                        'ModifiedBy' => $this->session->userdata('UserID'),
                        'DateCreated' => $now,
                        'DateModified' => $now,
                        'DocID' => $sessioned_charge->DocID,
                        'Identification' => $single_model->RegisteredID,
                        'Status' => 'Pending',
                        'UserID' => $user_model ? $user_model->RowID : 0,
                        'TransactionID' => $transaction_id,
                        'ServiceSlug' => $this->service_slug,
                        'ServiceTypeID' => $service_model->RowID,
                    );

                    $this->db->insert('lmais_users_actions', $users_charges_data);
                }//E# if statement


                $parameters['name'] = ucwords(strtolower($single_model->RegisteredProprietor));

                if ($single_model->RegisteredEmail) {
                    $recipient['to']['email'] = $single_model->RegisteredEmail;
                    $recipient['to']['name'] = $parameters['name'];

                    //Send email
                    $sent = $this->message->email(1, array('name' => 'LMAIS', 'email' => 'info@ardhi.go.ke'), 1, $recipient, 'seek_authorization', 'en', $parameters);
                }//E# statement

                if ($single_model->RegisteredTel1) {
                    $this->message->sms(1, 1, 1, $single_model->RegisteredTel1, 'seek_authorization', 'en', $parameters);
                }//E# statement
                if ($single_model->RegisteredTel2) {
                    $this->message->sms(1, 1, 1, $single_model->RegisteredTel2, 'seek_authorization', 'en', $parameters);
                }//E# statement
            }//E# foreach statement
        }//E# if statement
    }

//E# request_authorization() function

    /**
     * S# raise_invoices() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Raise invoices
     * 
     * @param array $sessioned_charge Charge
     * @param str $transaction_id Transaction ID
     * 
     * 
     */
    private function raise_invoices($sessioned_charge, $transaction_id) {

        $doc_id = $sessioned_charge['DocID'];

        $invoice_data = array();

        $now = date('Y-m-d H:i:s');

        foreach ($this->service_payable as $single_service_payable) {
//Service model
            $service_model = $this->db
                            ->select('*')
                            ->where('Slug', $single_service_payable['slug'])
                            ->get('lmais_landservicetypes')->row();

            if ($service_model) {
//Invoice model
                $invoice_model = $this->db
                                ->select('*')
                                ->where('ServiceID', $sessioned_charge['RowID'])
                                ->where('ServiceTypeID', $service_model->RowID)
								->where('ServiceSlug', $single_service_payable['slug'])
                                ->get('lmais_transactions')->row();

                if (!$invoice_model) {
//Consent Fee Invoice
                    $invoice_data[] = array(
                        'Agent' => $_SERVER['HTTP_USER_AGENT'],
                        'Ip' => $_SERVER['REMOTE_ADDR'],
                        'Currency' => 'KES',
                        'Date' => $now,
                        'Type' => 'invoice',
                        'Description' => $service_model->ServiceName,
                        'ServiceTypeID' => $service_model->RowID,
                        'DocID' => $doc_id,
                        'Amount' => $service_model->ServiceAmount,
                        'ServiceSlug' => $this->service_slug,
                        'ServiceID' => $sessioned_charge['RowID'],
                        'Status' => 'unpaid',
                        'UserID' => $this->session->userdata('UserID'),
                        'TransactionID' => $transaction_id,
                    );
                }//E# if statement
            }//E# if statement
        }//E# foreach statement

        if ($invoice_data) {
            $this->db->insert_batch('lmais_transactions', $invoice_data);
        }//E# if statement
    }

//E# raise_invoices() function

    public function payment() {
        $sessioned_charge = $this->session->userdata('sessioned_charge');

        $received_request_model = $this->db
                        ->select('*')
                        ->order_by('RowID', 'desc')
                        ->where('ServiceSlug', $this->parent_service_slug)
                        ->where('ServiceID', $sessioned_charge['RowID'])
                        ->get('lmais_receivedrequests')->row();

        if (!$received_request_model) {
            $received_request_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->get('lmais_receivedrequests')->row();

            $serial = $received_request_model ? $received_request_model->TransactionID : 'TRA00D0C1A';

            $serial++;
            $generated_serial = $serial++;

            if ($this->session->has_userdata('sessioned_rla')) {
                $doc_id = $this->session->userdata('sessioned_rla')->DocID;
            } else {
                $doc_id = '';
            }//E# if else statement

            $received_request_data = array(
                'BuyerUserID' => $this->session->userdata('UserID'),
                'TransactionID' => $generated_serial,
                'Status' => 'unpaid',
                'CompletionStage' => 0,
                'ServiceSlug' => $this->parent_service_slug,
                'ServiceID' => $sessioned_charge['RowID'],
                'ServiceTypeID' => 1,
                'DocID' => $doc_id,
                'LandUse' => 1,
                'Status' => 'Pending',
                'DateCreated' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('lmais_receivedrequests', $received_request_data);
        } else {
            $generated_serial = $received_request_model->TransactionID;
        }//E# if else statement
//Create invoices
        $this->raise_invoices($sessioned_charge, $generated_serial);
/*

        $charge_data = array(
            'TransactionID' => $generated_serial
        );

        $this->db->where('RowID', $sessioned_charge['RowID']);

        $this->db->update('lmais_charge', $charge_data);

        return $generated_serial;
		redirect('charge/apply_charge?step=7&status=done');
		*/

//Clear all caution session
        $this->session->unset_userdata('sessioned_charge');
        $this->session->unset_userdata('sessioned_rent');
        $this->session->unset_userdata('sessioned_buyers');
        $this->session->unset_userdata('sessioned_rla');

        
    }
	
	public function dischargelist()
{
  if ($this->session->userdata('Logged_in') != '') {

      $Department = $this->session->userdata('UserType');
	  $data['ServiceSlug']=$this->service_slug;
      $data['charges'] = $this->DischargeModel->getdischarglists(40);
	  

      $this->load->view($Department . '/PageResources/addparcelsheader');
      $this->load->view($Department . '/Header/header');
      if ($Department == 'user') {

      } else {
          $this->load->view($Department . '/Menu/menu');
      }//E# if else statement


      $this->load->view('charge/chargelist', $data);

}
}

//Load withdraw of caution page
Public function forminfo($transfer_id, $ServiceID) 
{

	  if ($this->session->userdata('Logged_in') != '') {
            $FormView;
			$DBView;

            $sql = "SELECT * FROM lmais_landservicetypes where Slug = '$ServiceID'";

            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $row) {
					if($row->Slug=='dischargeofcharge'){
						$DBView='charge';
					}
					 else{
						$DBView=$row->Slug;
					 }
                    $FormView = $row->Slug;
                }
            }
            $transfer_model = $this->db
                            ->select('*')
                            ->where('RowID', $transfer_id)
                            ->get('lmais_' . $DBView)->result_array();

            $data['transfer_model'] = $transfer_model ? $transfer_model[0] : null;
            $data['transfer_id'] = $transfer_id;
			$balance = $this->get_balance($transfer_id);
			$this->session->set_userdata('sessioned_balance', $balance);
            $this->session->set_userdata('sessioned_action', 'updating');
           
            $data['sessioned_action'] = $this->session->userdata('sessioned_action');
            $data['sessioned_balance'] = $this->session->userdata('sessioned_balance');
			$data['bal']=$balance;
			$transfer_id;
			//check if invoice raised
			 $invoice_model = $this->db
                                ->select('*')
                                ->where('ServiceID', $transfer_id)
								->where('ServiceSlug', $ServiceID)
                                ->get('lmais_transactions')->num_rows();
								//if invoice doesnt exists create 
								if($invoice_model<1):
								//Generate invoice on page Load
										$this->session_charge($transfer_id);
										$this->payment();
								endif;
								//EndIf
								
			//End check invoice raised

            $Department = $this->session->userdata('UserType');
            $this->load->view($Department . '/PageResources/dashboardheader');
            $this->load->view($Department . '/Header/header');
            $this->load->view($Department . '/Pages/' . $FormView, $data);
        }
}


public function discharge()
{
 if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');
            $data['input'] = $this->input->get();
            if (!array_key_exists('step', $data['input'])) {
                $data['input']['step'] = 1;
            }//E# if statement
            if ($data['input']['step'] == 1) {
                //Withdrawal_Caution model
                $data['discharge_model'] = $this->db
                                ->select('*')
                                ->where('Slug', 'dischargeofcharge')
                                ->get('lmais_landservicetypes')->row();
								
            }//E# if statement
		
            $balance = 0;
            $action = 'creating';

            if (array_key_exists('reference_number', $data['input'])) {
                $this->session_charge($data['input']['reference_number']);
                $this->session->set_userdata('action', 'updating');
                $balance = $this->get_balance($data['input']['reference_number']);

                $this->session->set_userdata('sessioned_balance', $balance);
                $this->session->set_userdata('sessioned_action', 'updating');
            }//E# if else statement

            $data['sessioned_action'] = $this->session->userdata('sessioned_action');
            $data['sessioned_balance'] = $this->session->userdata('sessioned_balance');
		

            if ($this->session->has_userdata('sessioned_charge')) {
                $data['sessioned_charge'] = $this->session->userdata('sessioned_charge');
            } else {
                $data['sessioned_charge'] = false;
                if ($data['input']['step'] > 1) {
                    redirect('discharge/dischargelist');
                }//E# if statement
            }//E# if else statement
            
            if ($this->session->userdata()) {
                $data['sessioned_user'] = $this->session->userdata();
            } else {
                $data['sessioned_user'] = false;
            }//E# if else statement
			
            $this->load->view($Department . '/PageResources/addparcelsheader');
            $this->load->view($Department . '/Header/header');
            if ($Department == 'user') {
                
            } else {
                $this->load->view($Department . '/Menu/menu');
            }//E# if else statement

 
            $this->load->view('charge/discharge', $data);
        }
}

    public function save_transaction_details() {

        $charge_data = array(
            'ChargeTypeID' => $this->input->post('ChargeTypeID'),
            'PrincipalAmount' => $this->input->post('PrincipalAmount'),
            'InterestRate' => $this->input->post('InterestRate'),
            'RepaidByDate' => $this->input->post('RepaidByDate'),
        );

        $sessioned_charge = $this->session->userdata('sessioned_charge');

        $this->db->where('RowID', $sessioned_charge['RowID']);

        $this->db->update('lmais_charge', $charge_data);

        $this->session_charge($sessioned_charge['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Transaction details saved successfully</div>');

        redirect('charge/apply_charge?step=4');
    }

    public function save_applicant() {

        $charge_data = array(
            'ProprietorFirstName' => $this->input->post('ProprietorFirstName'),
            'ProprietorMiddleName' => $this->input->post('ProprietorMiddleName'),
            'ProprietorLastName' => $this->input->post('ProprietorLastName'),
            'ProprietorNationality' => $this->input->post('ProprietorNationality'),
            'ProprietorIdentification' => $this->input->post('ProprietorIdentification'),
            'ProprietorMobileNumber' => $this->input->post('ProprietorMobileNumber'),
            'ProprietorEmailAddress' => $this->input->post('ProprietorEmailAddress'),
            'ProprietorAddress' => $this->input->post('ProprietorAddress'),
        );

        $sessioned_charge = $this->session->userdata('sessioned_charge');

        $this->db->where('RowID', $sessioned_charge['RowID']);

        $this->db->update('lmais_charge', $charge_data);

        $this->session_charge($sessioned_charge['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Applicant\'s details saved successfully</div>');

        redirect('charge/apply_charge?step=3');
    }

    public function rate_clearance() {
        $config['upload_path'] = './files/storage_files';
        $config['allowed_types'] = 'gif|jpg|png|pdf';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('clearance_certificate')) {
            $error = array('error' => $this->upload->display_errors());

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error!</strong> ' . trim($error['error']) . '</div>');

            redirect('charge/apply_charge?step=5');
        } else {

            $file_data = $this->upload->data();

            $now = date('Y-m-d H:i:s');

            $sessioned_charge = $this->session->userdata('sessioned_charge');

            $filestorage_data = array(
                'CreatedBy' => $this->session->userdata('UserID'),
                'DateCreated' => $now,
                'DateModified' => $now,
                'FileName' => $file_data['file_name'],
                'FileType' => $file_data['file_type'],
                'FileSize' => $file_data['file_size'],
                'FileExt' => $file_data['file_ext'],
                'IsImage' => $file_data['is_image'],
                'FileStage' => '',
                'ModifiedBy' => $this->session->userdata('UserID'),
                'OriginalFilename' => $file_data['orig_name'],
                'ServiceID' => 'charge',
                'ServiceTypeID' => $sessioned_charge['RowID']
            );

            $this->db->insert('lmais_filestorage', $filestorage_data);

            $data = array('upload_data' => $file_data);

            $charge_data = array(
                'ChargeFile' => $data['upload_data']['file_name'],
                'ChargeReference' => $this->input->post('ChargeReference'),
            );

            $sessioned_charge = $this->session->userdata('sessioned_charge');

            $this->db->where('RowID', $sessioned_charge['RowID']);

            $this->db->update('lmais_charge', $charge_data);

            $this->session_charge($sessioned_charge['RowID']);

            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Thanks you for uploading your Charge Documents</div>');

            redirect('charge/apply_charge?step=6');
        }//E# if else statement

        die();
    }

    /**
     * S# review_property() function
     * 
     * Review property
     * 
     */
    public function review_property() {

        $charge_data = array(
            'Step' => 2,
        );

        $sessioned_charge = $this->session->userdata('sessioned_charge');

        $this->db->where('RowID', $sessioned_charge['RowID']);

        $this->db->update('lmais_charge', $charge_data);

        $this->session_charge($sessioned_charge['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Documents uploaded successfully</div>');
        redirect('charge/apply_charge?step=3');
    }

//E# review_property() function

    private function session_charge($charge_id, $session_buyers = false, $session_sellers = false) {

        $charge_model = $this->db->get_where('lmais_charge', array('RowID' => $charge_id))->result_array();

        $this->session->set_userdata('sessioned_charge', $charge_model[0]);

        if ($session_buyers) {
//Select all buyers
            $buyer_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ServiceID', $charge_model[0]['RowID'])
                            ->where('ServiceSlug', 'charge')
                            ->get('lmais_buyers')->result_array();


            $this->session->set_userdata('sessioned_buyers', $buyer_model);
        }

        if ($session_sellers) {
//Select all sellers
            $seller_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ServiceID', $charge_model[0]['RowID'])
                            ->where('ServiceSlug', 'charge')
                            ->get('lmais_sellers')->result_array();


            $this->session->set_userdata('sessioned_sellers', $seller_model);
        }
    }

    /**
     * S# check_consent_validity() function
     * 
     * Check consent validity
     * 
     */
    public function check_consent_validity() {
//Clear all caution session
        $this->session->unset_userdata('sessioned_charge');
        $this->session->unset_userdata('sessioned_rent');
        $this->session->unset_userdata('sessioned_sellers');
        $this->session->unset_userdata('sessioned_buyers');
        $this->session->unset_userdata('sessioned_rla');
        $this->session->unset_userdata('sessioned_action');
        $this->session->unset_userdata('sessioned_balance');

        $TransactionID = $this->input->post('TransactionID');

        $consent_model = $this->db
                        ->select('*')
                        ->where('TransactionID', $TransactionID)
                        ->where('ConsentTypeID', 4)
                        ->get('lmais_consentrequest')->row();

        if ($consent_model && strtolower($consent_model->Status) == 'approved') {

            //Sesssion consent
            $this->session->set_userdata('sessioned_consent', $consent_model);

            //Get RLA
            $rla_model = $this->db
                            ->select('*')
                            ->where('RegistryID', $this->input->post('RegistryID'))
                            ->where('RegistrationSectionID', $this->input->post('RegistrationSectionID'))
                            ->where('ParcelNo', $this->input->post('ParcelNo'))
                            ->limit(1)
                            ->get('lmais_rla')->row();

            if ($rla_model) {
                $this->session->set_userdata('sessioned_rla', $rla_model);
            }//E# if statement
            //Service model
            $service_model = $this->db
                            ->select('*')
                            ->where('Slug', $this->service_slug)
                            ->get('lmais_landservicetypes')->row();

            $charge_data = array(
                'Agent' => $_SERVER['HTTP_USER_AGENT'],
                'DateCreated' => date('Y-m-d H:i:s'),
                'Ip' => $_SERVER['REMOTE_ADDR'],
                'UserID' => $consent_model->UserID,
                'RegistryID' => $consent_model->RegistryID,
                'RegistrationSectionID' => $consent_model->RegistrationSectionID,
                'ParcelNumber' => $consent_model->ParcelNumber,
                'TenureID' => $consent_model->TenureID,
                'LRNumber' => $consent_model->LRNumber,
                'DocID' => $consent_model->DocID,
                'Description' => $service_model->ServiceName,
                'Status' => 'Incomplete',
                'Step' => 1,
            );
            $charge_data['AmountPayable'] = 0;
            foreach ($this->service_payable as $single_service_payable) {
//Service model
                $service_model = $this->db
                                ->select('*')
                                ->where('Slug', $single_service_payable['slug'])
                                ->get('lmais_landservicetypes')->row();
                if ($service_model) {
                    $charge_data[$single_service_payable['db_field']] = $service_model->ServiceAmount;
                }//E# if statement

                $charge_data['AmountPayable'] +=$service_model->ServiceAmount;
            }//E# foreach statement

            $this->db->insert('lmais_charge', $charge_data);

            $charge_id = $this->db->insert_id();

            $charge_data = array(
                'EcitizenBillRef' => $this->service_slug . '_' . $charge_id
            );

            $this->db->where('RowID', $charge_id);

            $this->db->update('lmais_charge', $charge_data);

            $this->session_charge($charge_id);

            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Success!</strong> This consent is valid.</div>');
            $redirect_to = 'charge/apply_charge?step=2';
        } else {
            if (!$consent_model) {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error!</strong> This consent was not found. Kindly confirm with the land owner that this reference number ' . $TransactionID . ' is correct. </div>');
            } else {
//Get user model
                $user_model = $this->db
                                ->select('*')
                                ->where('RowID', $consent_model->UserID)
                                ->get('lmais_users')->row();

                $name = $user_model ? $user_model->FirstName . ' ' . $user_model->LastName : '';

                $name = $name ? $name : 'the land owner';

                var_dump($consent_model->Status);

                if (strtolower($consent_model->Status) == 'paid') {
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error!</strong> This consent had been paid for but not yet approved. Kindly contact the registrar to approve it. </div>');
                } elseif (strtolower($consent_model->Status) == 'unpaid') {
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error!</strong> This consent has not been paid for. Kindly request ' . $name . '</div>');
                } else if (strtolower($consent_model->Status) == 'used') {
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error!</strong> This consent has already been used. Kindly request ' . $name . ' to apply for another consent.</div>');
                }//E# if else statement
            }//E# if else statement
            $redirect_to = 'charge/apply_charge';
        }//E# if statement

        redirect($redirect_to);
    }

//E# check_consent_validity() function

    /**
     * S# get_balance() function
     * 
     * Get balance
     * 
     * @param int $charge_id Caution ID
     * 
     * @return float Balance
     */
    private function get_balance($charge_id) {
//Get total invoice
        $total_invoice = $this->db
                        ->select_sum('Amount')
                        ->where('ServiceID', $charge_id)
                        ->where('ServiceSlug', $this->service_slug)
                        ->where('Type', 'invoice')
                        ->get('lmais_transactions')->row();

//Get total payment
        $total_payment = $this->db
                        ->select_sum('Amount')
                        ->where('ServiceID', $charge_id)
                        ->where('ServiceSlug', $this->service_slug)
                        ->where('Type', 'payment')
                        ->get('lmais_transactions')->row();

        return $total_invoice->Amount - $total_payment->Amount;
    }

//E# get_balance() function 

    public function apply_charge() {

        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');
            $Department = ($Department == 'bank') ? 'user' : $Department;
            $data['tenure'] = $this->TenureModel->GetAllTenure();
            $data['input'] = $this->input->get();

            if (!array_key_exists('step', $data['input'])) {
                $data['input']['step'] = 1;
            }

            if ($data['input']['step'] == 1) {
//Convenience model
                $data['convenience_model'] = $this->db
                                ->select('*')
                                ->where('Slug', 'convenience')
                                ->get('lmais_landservicetypes')->row();
            }

            $balance = 0;
            $action = 'creating';

            if (array_key_exists('reference_number', $data['input'])) {
                $this->session_charge($data['input']['reference_number']);
                $this->session->set_userdata('action', 'updating');
                $balance = $this->get_balance($data['input']['reference_number']);

                $this->session->set_userdata('sessioned_balance', $balance);
                $this->session->set_userdata('sessioned_action', 'updating');
            }//E# if else statement

            $data['sessioned_action'] = $this->session->userdata('sessioned_action');
            $data['sessioned_balance'] = $this->session->userdata('sessioned_balance');

            if ($this->session->has_userdata('sessioned_charge')) {
                $data['sessioned_charge'] = $this->session->userdata('sessioned_charge');
            } else {
                $data['sessioned_charge'] = false;
                if ($data['input']['step'] > 1) {
                    redirect('charge/apply_charge');
                }//E# if statement
            }//E# if else statement
            if ($this->session->has_userdata('sessioned_buyers')) {
                $data['sessioned_buyers'] = $this->session->userdata('sessioned_buyers');
            } else {
                $data['sessioned_buyers'] = false;
            }//E# if else statement
//var_dump($data['sessioned_buyers']);
// die();
            if ($this->session->has_userdata('sessioned_sellers')) {
                $data['sessioned_sellers'] = $this->session->userdata('sessioned_sellers');
            } else {
                $data['sessioned_sellers'] = false;
            }//E# if else statement
            if ($this->session->userdata()) {
                $data['sessioned_user'] = $this->session->userdata();
            } else {
                $data['sessioned_user'] = false;
            }//E# if else statement

            $data['charge_types'] = $this->db
                            ->select('*')
                            ->where('ItemType', 'Charge')
                            ->get('lmais_listitems')->result();

            if (($data['input']['step'] == 2) || ($data['input']['step'] == 4)) {

                $registry_model = $this->db
                                ->select('*')
                                ->where('RowID', $data['sessioned_charge']['RegistryID'])
                                ->get('lmais_registry')->row();

                $data['registry_model'] = $registry_model ? $registry_model : null;

                $registration_section_model = $this->db
                                ->select('*')
                                ->where('RowID', $data['sessioned_charge']['RegistrationSectionID'])
                                ->get('lmais_registrationsections')->row();

                $data['registration_section_model'] = $registration_section_model ? $registration_section_model : null;

                $tenure_model = $this->db
                                ->select('*')
                                ->where('RowID', $data['sessioned_charge']['TenureID'])
                                ->get('lmais_tenure')->row();

                $data['tenure_model'] = $tenure_model ? $tenure_model : null;


                $data['charge_type_model'] = $this->db
                                ->select('*')
                                ->where('RowID', $data['sessioned_charge']['ChargeTypeID'])
                                ->get('lmais_listitems')->row();
            }//E# if statement

            $this->load->view($Department . '/PageResources/addparcelsheader');
            $this->load->view($Department . '/Header/header');
            if ($Department == 'user') {
                
            } else {
                $this->load->view($Department . '/Menu/menu');
            }//E# if else statement


            $this->load->view('charge/charge', $data);
        }
    }

    public function save_advocate_details() {
        $advocate_data = array(
            'advoFName' => $this->input->post('advoFName'),
            'advoMName' => $this->input->post('advoMName'),
            'advoLName' => $this->input->post('advoLName'),
            'advoPinNumber' => $this->input->post('advoPinNumber'),
            'advoPostalAddress' => $this->input->post('advoPostalAddress'),
            'advoEmail' => $this->input->post('advoEmail'),
            'advoTelNumber' => $this->input->post('advoTelNumber'),
            'advoLawFirm' => $this->input->post('advoLawFirm'),
            'advoPFNumber' => $this->input->post('advoPFNumber'),
            'advoIDNumber' => $this->input->post('advoIDNumber'),
            'advoNationality' => $this->input->post('advoNationality'),
        );

        $sessioned_charge = $this->session->userdata('sessioned_charge');

        $this->db->where('RowID', $sessioned_charge['RowID']);

        $this->db->update('lmais_charge', $advocate_data);

        $this->session_charge($sessioned_charge['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Advocates details saved successfully</div>');

        redirect('charge/apply_charge?step=5');
    }

    public function charge_app() {
        $ChargeID = "CNT" . md5(time() . mt_rand(1, 1000000));
        $Proprietor_First_Name = $this->input->post("ProprietorFirstName");
        $Proprietor_Middle_Name = $this->input->post("ProprietorMiddleName");
        $Proprietor_Last_Name = $this->input->post("ProprietorLastName");
        $Proprietor_Nationality = $this->input->post("ProprietorNationality");
        $Proprietor_Idenification = $this->input->post("ProprietorIdenification");
        $Proprietor_Mobile_Number = $this->input->post("ProprietorMobileNumber");
        $Proprietor_Email_Address = $this->input->post("ProprietorEmailAddress");
        $Proprietor_Address = $this->input->post("ProprietorAddress");
        $Proposed_First_Name = $this->input->post("ProposedFirstName");
        $Proposed_Middle_Name = $this->input->post("ProposedMiddleName");
        $Proposed_Last_Name = $this->input->post("ProposedLastName");
        $Proposed_Nationality = $this->input->post("ProposedNationality");
        $Proposed_Identification = $this->input->post("ProposedIdentification");
        $Proposed_Mobile_Number = $this->input->post("ProposedMobileNumber");
        $Proposed_Email_Address = $this->input->post("ProposedEmailAddress");
        $Proposed_Address = $this->input->post("ProposedAddress");
        $InterestRatePA = $this->input->post("InterestRatePA");
        $PrincipalAmount = $this->input->post("PrincipalAmount");
        $PrepaymentByDate = $this->input->post("PrepaymentByDate");
        $EntryAmount = $this->input->post("EntryAmount");
        $LRNumber = $this->input->post("LRNumber");
        $AreaSize = $this->input->post("AreaSize");
        $Registry = $this->input->post("Registry");
        $County = $this->input->post("County");
        $CreatedBy = $this->session->userdata('RowID');

        $data = array(
            'Registry' => $Registry,
            'LRNO' => $LRNO,
            'AreaSize' => $AreaSize,
            'AreaUnit' => $AreaUnit,
            'Tenure' => $Tenure,
            'RegisteredProprietor' => $RegisteredProprietor,
            'TypeOfDevelopment' => $TypeOfDevelopment,
            'NearbyInfrastructure' => $NearbyInfrastructure,
            'NearbyValue' => $NearbyValue,
            'TypeOfRoad' => $TypeOfRoad,
            'NameOfRoad' => $NameOfRoad,
            'DistanceFromRoad' => $DistanceFromRoad,
            'TypeOfTown' => $TypeOfTown,
            'NameOfTown' => $NameOfTown,
            'DistanceFromTown' => $DistanceFromTown,
            'CreatedBy' => $CreatedBy,
        );
        $usr_result = $this->ChargeModel->apply_charge($data);

//check if username and password is correct
        if ($usr_result == TRUE) { //active user record is present
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                              <a href="#" class="close" data-dismiss="alert">&times;</a>
                             
                            <strong>Success!</strong> You have successfully Added new Parcel!</div>');

            redirect('start/addvaluationdata');
        } else if ($usr_result == FALSE) {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                             <a href="#" class="close" data-dismiss="alert">&times;</a>
                              <strong>Error!</strong> Data are not saved.. Try again</div>');

            redirect('start/addvaluationdata');
        }
    }

    public function delete_seller() {
        $sellerID = $this->input->post('SellerID');

        $this->db->where('RowID', $sellerID);
        $this->db->delete('lmais_sellers');

        $sessioned_charge = $this->session->userdata('sessioned_charge');

        $this->session_charge($sessioned_charge['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong>Change of User Applicant\'s details deleted successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/charge/apply_charge?step=2'
        ));
    }

    public function review_application() {
        if ($this->session->userdata('UserType') == 'bank') {
            $transaction_id = $this->payment();

            $sessioned_charge = $this->session->userdata('sessioned_charge');
            $service_id = $sessioned_charge['RowID'];
            $doc_id = $sessioned_charge['DocID'];
            $user_ids = array($this->session->userdata('UserID'));

//Service model
            $service_model = $this->db
                            ->select('*')
                            ->where('Slug', $this->service_slug)
                            ->get('lmais_landservicetypes')->row();

//Save application trail
            $this->save_application_trail('Incomplete', $service_id, $doc_id, null, $transaction_id, $user_ids, $service_model);


            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> </div>');

            redirect('charge/apply_charge?step=5');
        }//E# if statement
    }

//E# delete_seller() function

    function GetRegistriesMatch($ActID, $CountyID) {
//$ActID = $this->uri->segment(3);
//$CountyID = $this->uri->segment(4);
        $Registries = $this->Registrymodel->GetMatchedRegistries($ActID, $CountyID);

        return $Registries;
    }

    public function clear_all_cache() {

        $CI = & get_instance();
        $path = $CI->config->item('cache_path');

        $cache_path = ($path == '') ? APPPATH . 'cache/' : $path;

        $handle = opendir($cache_path);
        while (($file = readdir($handle)) !== FALSE) {
//Leave the directory protection alone
            if ($file != '.htaccess' && $file != 'index.html') {
                @unlink($cache_path . '/' . $file);
            }
        }
        closedir($handle);


        if ($handle = opendir($cache_path)) {
//echo "Directory handle: $handle <br /><br/>";

            while (false !== ($entry = readdir($handle))) {
//echo $entry."<br />";
                $n = basename($entry);
//echo "name = ".$n."<br />";  
//echo "length of name = ".strlen($n)."<br />";
                if (strlen($n) >= 32) {
//echo "file name's 32 chars long <br />";
                    $p = APPPATH . 'cache/' . $entry;
//echo $p;                  
                    unlink($p);
                }
//echo "<br />";
            }
            closedir($handle);
        }
    }

}
