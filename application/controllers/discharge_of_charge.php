<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Discharge_Of_Charge extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('actsModel', '', TRUE);
        $this->load->model('Countymodel', '', TRUE);
        $this->load->model('Registrymodel', '', TRUE);
        $this->load->model('ParcelsValuationModel', '', TRUE);
        $this->load->model('Countrymodel', '', TRUE);
        // $this->load->model('Settings_Model', '', TRUE);
        // $this->load->model('proprietorModel', '', TRUE);
        // $this->load->model('encumbranceModel', '', TRUE);
    }

    public function payment_completed() {
        $data = $this->input->get();

        if (array_key_exists('bill_ref', $data)) {
            $doc_id = $this->session->userdata('sessioned_rla')->DocID;

            $Discharge_Of_Charge_data = array(
                'PaymentStatus' => 'Paid',
            );

            $sessioned_Discharge_Of_Charge = $this->session->userdata('sessioned_Discharge_Of_Charge');

            $this->db->where('RowID', $this->input->get('bill_ref'));

            $this->db->update('lmais_dischargeofcharge', $Discharge_Of_Charge_data);

            $payment_data = array(
                'Description' => 'Registration Fee',
                'DocID' => $doc_id,
                'PaymentDate' => date('Y-m-d H:i:s'),
                'Amount' => 500,
                'Note' => 'Paid',
                'Gateway' => 'Ecitizen',
                'ServiceType' => 'Discharge_Of_Charge',
                'ServiceID' => $sessioned_Discharge_Of_Charge['RowID'],
                'Status' => 1,
            );

            $this->db->insert('lmais_payment', $payment_data);
        }//E# if else statement


        $sessioned_user = $this->session->userdata();

        //Send login email

        $parameters = array(
            'name' => $sessioned_user['FirstName'],
            'currency' => 'KES',
            'service' => 'Discharge_Of_Charge',
            'amount' => $sessioned_Discharge_Of_Charge['Discharge_Of_Charge_Amount'],
            'time' => date('d/m/Y H:i')
        );

        $this->load->library('message');

        $sent = $this->message->sms(1, 1, 1, $sessioned_user['Phone'], 'payment_received', 'en', $parameters);

        //Clear all Discharge_Of_Charge session
        $this->session->unset_userdata('sessioned_Discharge_Of_Charge');
        $this->session->unset_userdata('sessioned_sellers');
        $this->session->unset_userdata('sessioned_rla');

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
        <strong>Success!</strong> Thanks ' . $sessioned_user['FirstName'] . ' ' . $sessioned_user['LastName'] . ' for making the payment</div>');

        redirect('UserView/LoadApplications');
    }

    public function test_email() {
        //Send login email
        $this->load->library('message');

        $this->message->send_email();
    }

    public function test_invoice() {
        //Load Ecitizen
        $this->load->library('ecitizen');

        $parameters = array(
            'apiClientID' => 'TYV0FUD0BY',
            'secureHash' => 'TYV0FUD0BY',
            'billDesc' => 'TYV0FUD0BY',
            'billRefNumber' => 'TYV0FUD0BY',
            'currency' => 'TYV0FUD0BY',
            'serviceID' => '29',
            'clientMSISDN' => 'TYV0FUD0BY',
            'clientName' => 'TYV0FUD0BY',
            'clientIDNumber' => 'TYV0FUD0BY',
            'clientEmail' => 'TYV0FUD0BY',
            'callBackURLOnSuccess' => 'TYV0FUD0BY',
            'pictureURL' => 'TYV0FUD0BY',
            'notificationURL' => 'TYV0FUD0BY',
            'amountExpected' => 'TYV0FUD0BY',
        );
        $ecitizen_response = $this->ecitizen->raise_invoice($parameters);

        var_dump($ecitizen_response);
    }

    public function get_user_by_id() {
        //Load Ecitizen
        $this->load->library('ecitizen');

        $ecitizen_response = $this->ecitizen->get_user_by_id_number($this->input->post('idNumber'));

        //var_dump($ecitizen_response);
        $response['message'] = $ecitizen_response['message'];
        $response['type'] = $ecitizen_response['status'] ? 'success' : 'error';

        echo json_encode($response);
    }

    public function index() {
        if ($this->session->userdata('Logged_in') != '') {

            self::loadindex();
        } else
            $this->load->view('signin/PageResources/loginheader');
        $this->load->view('signin/login');
    }

    //20642
    private function raise_invoices($sessioned_Discharge_Of_Charge, $transactionID) {
        $doc_id = $this->session->userdata('sessioned_rla')->DocID;

        if ($sessioned_Discharge_Of_Charge['Discharge_Of_ChargeAmount']) {
            //Rent Due Invoice
            $invoice_data = array(
                'Description' => 'Discharge_Of_Charge',
                'DocID' => $doc_id,
                'BillDate' => date('Y-m-d'),
                'InvoiceValue' => $sessioned_Discharge_Of_Charge['Discharge_Of_ChargeAmount'],
                'Note' => '',
                'ServiceType' => 'Discharge_Of_Charge',
                'ServiceID' => $sessioned_Discharge_Of_Charge['RowID'],
                'Status' => '0',
            );

            $this->db->insert('lmais_invoice', $invoice_data);
        }//E# if statement
       
}
   public function payment() {

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $received_request_model = $this->db
                        ->select('*')
                        ->order_by('RowID', 'desc')
                        ->limit(1)
                        ->get('lmais_receivedrequests')->row();

        $serial = 'FRX00D0C1A';

        if ($received_request_model) {
            $serial = $received_request_model->TransactionID;
        }//E# if statement

        $serial++;
        $generated_serial = $serial++;

        if ($this->session->has_userdata('sessioned_rla')) {
            $doc_id = $this->session->userdata('sessioned_rla')->DocID;
        } else {
            $doc_id = '';
        }//E# if else statement

        $received_request_data = array(
            'UserID' => $this->session->userdata('UserID'),
            'TransactionID' => $generated_serial,
            'PaymentStatus' => 0,
            'CompletionStage' => 0,
            'ServiceID' => 1,
            'DocID' => $doc_id,
            'LandUse' => 1,
            'Status' => 'Pending',
            'DateCreated' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('lmais_receivedrequests', $received_request_data);

        //Update buyers with the transaction id
        $buyer_data = array(
            'TransactionID' => $generated_serial,
        );

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->db->where('ConsentID', $sessioned_consent['ConsentID']);

        $this->db->update('lmais_buyers', $buyer_data);

        //Update buyers with the transaction id
        $seller_data = array(
            'TransactionID' => $generated_serial,
        );

        $sessioned_consent = $this->session->userdata('sessioned_consent');

        $this->db->where('ConsentID', $sessioned_consent['ConsentID']);

        $this->db->update('lmais_sellers', $seller_data);

        //Create invoices
        $this->raise_invoices($sessioned_consent, $generated_serial);

        $consent_data = array(
            'TransactionID' => $generated_serial
        );

        $this->db->where('RowID', $sessioned_consent['RowID']);

        $this->db->update('lmais_transfer', $consent_data);

        return $serial;

    }


	public function save_transaction_details() {

        $Discharge_Of_Charge_data = array(
            'Bank' => $this->input->post('Bank'),
            'Amount' => $this->input->post('Amount'),
            'DateOfDischarge' => $this->input->post('DateOfDischarge'),
            
            );

        $sessioned_Discharge_Of_Charge = $this->session->userdata('sessioned_Discharge_Of_Charge');

        $this->db->where('RowID', $sessioned_Discharge_Of_Charge['RowID']);

        $this->db->update('lmais_dischargeofcharge', $Discharge_Of_Charge_data);

        $this->session_Discharge_Of_Charge($sessioned_Discharge_Of_Charge['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Transaction details saved successfully</div>');

        redirect('Discharge_Of_Charge/apply_Discharge_Of_Charge?step=4');
    }

    public function save_applicant() {

        $Discharge_Of_Charge_data = array(
            'ProprietorFirstName' => $this->input->post('ProprietorFirstName'),
            'ProprietorMiddleName' => $this->input->post('ProprietorMiddleName'),
            'ProprietorLastName' => $this->input->post('ProprietorLastName'),
            'ProprietorNationality' => $this->input->post('ProprietorNationality'),
            'ProprietorIdentification' => $this->input->post('ProprietorIdentification'),
            'ProprietorMobileNumber' => $this->input->post('ProprietorMobileNumber'),
            'ProprietorEmailAddress' => $this->input->post('ProprietorEmailAddress'),
            'ProprietorAddress' => $this->input->post('ProprietorAddress'),
        );

        $sessioned_Discharge_Of_Charge = $this->session->userdata('sessioned_Discharge_Of_Charge');

        $this->db->where('RowID', $sessioned_Discharge_Of_Charge['RowID']);

        $this->db->update('lmais_dischargeofcharge', $Discharge_Of_Charge_data);

        $this->session_Discharge_Of_Charge($sessioned_Discharge_Of_Charge['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Applicant\'s details saved successfully</div>');

        redirect('Discharge_Of_Charge/apply_Discharge_Of_Charge?step=3');
    }

    public function rate_clearance() {
        $config['upload_path'] = './files/storage_files';
        $config['allowed_types'] = 'gif|jpg|png|pdf';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('clearance_certificate')) {
            $error = array('error' => $this->upload->display_errors());

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error!</strong> ' . trim($error['error']) . '</div>');

            redirect('Discharge_Of_Charge/apply_Discharge_Of_Charge?step=5');
        } else {

            $file_data = $this->upload->data();

            $now = date('Y-m-d H:i:s');

            $sessioned_Discharge_Of_Charge = $this->session->userdata('sessioned_Discharge_Of_Charge');

            $filestorage_data = array(
                'CreatedBy' => $this->session->userdata('UserID'),
                'DateCreated' => $now,
                'DateModified' => $now,
                'FileName' => $file_data['file_name'],
                'FileType' => $file_data['file_type'],
                'FileSize' => $file_data['file_size'],
                'FileExt' => $file_data['file_ext'],
                'IsImage' => $file_data['is_image'],
                'FileStage' => '',
                'ModifiedBy' => $this->session->userdata('UserID'),
                'OriginalFilename' => $file_data['orig_name'],
                'ServiceID' => 'Discharge of Charge',
                'ServiceTypeID' => $sessioned_Discharge_Of_Charge['RowID']
            );

            $this->db->insert('lmais_filestorage', $filestorage_data);

            $data = array('upload_data' => $file_data);

            $Discharge_Of_Charge_data = array(
                'RateClearanceCertificateFile' => $data['upload_data']['file_name'],
                // 'RateClearanceNumber' => $this->input->post('RateClearanceNumber'),
                'DischargeDocument' => $data['upload_data']['file_name'],
            );

            $sessioned_Discharge_Of_Charge = $this->session->userdata('sessioned_Discharge_Of_Charge');
        // $this->payment();

            $this->db->where('RowID', $sessioned_Discharge_Of_Charge['RowID']);

            $this->db->update('lmais_dischargeofcharge', $Discharge_Of_Charge_data);

            $this->session_Discharge_Of_Charge($sessioned_Discharge_Of_Charge['RowID']);

            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Thanks you for clearing your rates</div>');

            redirect('Discharge_Of_Charge/apply_Discharge_Of_Charge?step=6');
        }//E# if else statement

        die();
    }

    public function Discharge_Of_Charge_clearance() {

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Thanks you for clearing your Discharge_Of_Charge Fees</div>');
        redirect('Discharge_Of_Charge/apply_Discharge_Of_Charge?step=7');
    }

    private function session_Discharge_Of_Charge($Discharge_Of_Charge_id, $session_sellers = false) {
        $Discharge_Of_Charge_model = $this->db->get_where('lmais_dischargeofcharge', array('RowID' => $Discharge_Of_Charge_id))->result_array();

        $this->session->set_userdata('sessioned_Discharge_Of_Charge', $Discharge_Of_Charge_model[0]);

        if ($session_sellers == 'sellers') {
            //Select all sellers
            $seller_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ServiceID', $Discharge_Of_Charge_model[0]['RowID'])
                            ->where('ServiceType', 'Discharge_Of_Charge')
                            ->get('lmais_sellers')->result_array();


            $this->session->set_userdata('sessioned_sellers', $seller_model);
        }
    }

     public function save_seller_continue() {
        $sessioned_sellers = $this->session->userdata('sessioned_sellers');

        if ($sessioned_sellers) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Discharge_Of_Charge applicant\'s details saved successfully</div>');

            redirect('Discharge_Of_Charge/apply_Discharge_Of_Charge?step=3');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Error!</strong> Please add at least one Discharge_Of_Charge applicant</div>');

            redirect('Discharge_Of_Charge/apply_Discharge_Of_Charge?step=2');
        }//E# if else statement
    }

    public function save_seller() {

        if ($this->input->post('IsContactPerson') == 'Y') {
            $Discharge_Of_Charge_data = array(
                'IsContactPerson' => 'N',
            );
            $this->db->where('ServiceID', $this->input->post('Discharge_Of_ChargeID'));
             $this->db->where('ServiceType', 'Discharge_Of_Charge');

            $this->db->update('lmais_sellers', $Discharge_Of_Charge_data);
        }//E# if statement
        $seller_data = array(
            'IsContactPerson' => $this->input->post('IsContactPerson'),
            'UserID' => $this->session->userdata('UserID'),
            'ServiceID' => $this->input->post('Discharge_Of_ChargeID'),
            'FirstName' => $this->input->post('FirstName'),
            'MiddleName' => $this->input->post('MiddleName'),
            'LastName' => $this->input->post('LastName'),
            'PinNumber' => $this->input->post('PinNumber'),
            'CountryID' => $this->input->post('CountryID'),
            'Identification' => $this->input->post('Identification'),
            'MobileNumber' => $this->input->post('MobileNumber'),
            'EmailAddress' => $this->input->post('EmailAddress'),
            'Address' => $this->input->post('Address'),
            'CreatedBy' => $this->session->userdata('UserID'),
            'DateCreated' => date('Y-m-d H:i:s'),
            'ModifiedBy' => $this->session->userdata('UserID'),
            'DateModified' => date('Y-m-d H:i:s'),
            'Type' => 'individual',
            'ServiceType' => 'Discharge_Of_Charge',
        );
        
        $this->db->insert('lmais_sellers', $seller_data);

        $seller_id = $this->db->insert_id();

        $sessioned_Discharge_Of_Charge = $this->session->userdata('sessioned_Discharge_Of_Charge');

        $this->session_Discharge_Of_Charge($sessioned_Discharge_Of_Charge['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Discharge_Of_Charge Applicant\'s details saved successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/Discharge_Of_Charge/apply_Discharge_Of_Charge?step=2'
        ));
    }

    public function check_property_details() {
        //Clear all consent session
        $this->session->unset_userdata('sessioned_Discharge_Of_Charge');
        $this->session->unset_userdata('sessioned_sellers');
        $this->session->unset_userdata('sessioned_rla');

        $rla_model = $this->db
                        ->select('*')
                        ->where('RegistryID', $this->input->post('RegistryID'))
                        ->where('RegistrationSectionID', $this->input->post('RegistrationSectionID'))
                        ->where('ParcelNo', $this->input->post('ParcelNo'))
                        ->limit(1)
                        ->get('lmais_rla')->row();

        $property_availability = array(
            'Agent' => $_SERVER['HTTP_USER_AGENT'],
            'DateCreated' => date('Y-m-d H:i:s'),
            'Ip' => $_SERVER['REMOTE_ADDR'],
            'ParcelNo' => $this->input->post('ParcelNo'),
            'RegistryID' => $this->input->post('RegistryID'),
            'RegistrationSectionID' => $this->input->post('RegistrationSectionID'),
            'NatureOfTitle' => $this->input->post('NatureOfTitle'),
            'Status' => 0,
        );

        if ($rla_model) {

            $property_availability['Status'] = 1;

            $this->session->set_userdata('sessioned_rla', $rla_model);

            $Discharge_Of_Charge_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->limit(1)
                            ->get('lmais_dischargeofcharge')->row();

            $serial = 'G4AX2A';
            if ($Discharge_Of_Charge_model) {
                $serial = $Discharge_Of_Charge_model->Discharge_Of_ChargeID;
            }//E# if statement

            $serial++;
            $generated_serial = $serial++;

            $Discharge_Of_Charge_data = array(
                'Discharge_Of_ChargeID' => $generated_serial,
                'RegistryID' => $this->input->post('RegistryID'),
                'RegistrationSectionID' => $this->input->post('RegistrationSectionID'),
                'ParcelNumber' => $this->input->post('ParcelNo'),
                'LRNumber' => $rla_model->Title,
				 'DocID' => $rla_model->DocID,
                'Act' => $this->input->post('NatureOfTitle'),
                'Description' => 'Discharge_Of_Charge Application',
                'PaymentStatus' => 'Unpaid',
                'AmountPayable' => '500',
                'Rent' => '0',
            );

            $this->db->insert('lmais_dischargeofcharge', $Discharge_Of_Charge_data);

            $Discharge_Of_Charge_id = $this->db->insert_id();

            //Select all rent
            $rent_model = $this->db
                            ->select('*')
                            ->where('LRNumber', $rla_model->Title)
                            ->where('Status', 'N')
                            ->get('lmais_rent')->result_array();

            if ($rent_model) {
                $unpaid_rent = 0;
                foreach ($rent_model as $single_rent) {
                    $unpaid_rent +=$single_rent['Amount'];
                }//E# foreach statement

                $Discharge_Of_Charge_data = array(
                    'Rent' => $unpaid_rent,
                );

                //var_dump($single_rent)
                //dd("sa");
                $this->db->where('RowID', $Discharge_Of_Charge_id);

                $this->db->update('lmais_dischargeofcharge', $Discharge_Of_Charge_data);
            }//E# if else statement

            $this->session->set_userdata('sessioned_rent', $rent_model);

            $this->session_Discharge_Of_Charge($Discharge_Of_Charge_id);

            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> We have found your land</div>');
            $redirect_to = 'Discharge_Of_Charge/apply_Discharge_Of_Charge?step=2';
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error!</strong> This parcel of land could not be found. Kindly confirm your details</div>');

            $redirect_to = 'Discharge_Of_Charge/apply_Discharge_Of_Charge';
        }//E# if statement

        $this->db->insert('lmais_property_availability', $property_availability);
      
       redirect($redirect_to);
    }

    public function apply_Discharge_Of_Charge() {
        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');
            $data['acts'] = $this->actsModel->GetAllActs();
            $data['input'] = $this->input->get();

            if (!array_key_exists('step', $data['input'])) {
                $data['input']['step'] = 1;
            }
            

            if ($this->session->has_userdata('sessioned_Discharge_Of_Charge')) {
                $data['sessioned_Discharge_Of_Charge'] = $this->session->userdata('sessioned_Discharge_Of_Charge');
            } else {
                $data['sessioned_Discharge_Of_Charge'] = false;
                if ($data['input']['step'] > 1) {
                   redirect('Discharge_Of_Charge/apply_Discharge_Of_Charge');
                }//E# if statement
            }//E# if else statement

            if ($this->session->has_userdata('sessioned_sellers')) {
                $data['sessioned_sellers'] = $this->session->userdata('sessioned_sellers');
            } else {
                $data['sessioned_sellers'] = false;
            }//E# if else statement
            if ($this->session->userdata()) {
                $data['sessioned_user'] = $this->session->userdata();
            } else {
                $data['sessioned_user'] = false;
            }//E# if else statement

            $this->load->view($Department . '/PageResources/addparcelsheader');
            $this->load->view($Department . '/Header/header');
            if ($Department == 'user') {
                
            } else {
                $this->load->view($Department . '/Menu/menu');
            }//E# if else statement
            $this->load->view('consent/Discharge_Of_Charge', $data);
        }
    }

           public function Discharge_Of_Charge_app() {
            $Discharge_Of_ChargeID = "CNT" . md5(time() . mt_rand(1, 1000000));
            $Proprietor_First_Name = $this->input->post("ProprietorFirstName");
            $Proprietor_Middle_Name = $this->input->post("ProprietorMiddleName");
            $Proprietor_Last_Name = $this->input->post("ProprietorLastName");
            $Proprietor_Nationality = $this->input->post("ProprietorNationality");
            $Proprietor_Idenification = $this->input->post("ProprietorIdenification");
            $Proprietor_Mobile_Number = $this->input->post("ProprietorMobileNumber");
            $Proprietor_Email_Address = $this->input->post("ProprietorEmailAddress");
            $Proprietor_Address = $this->input->post("ProprietorAddress");
            $Proposed_First_Name = $this->input->post("ProposedFirstName");
            $Proposed_Middle_Name = $this->input->post("ProposedMiddleName");
            $Proposed_Last_Name = $this->input->post("ProposedLastName");
            $Proposed_Nationality = $this->input->post("ProposedNationality");
            $Proposed_Identification = $this->input->post("ProposedIdentification");
            $Proposed_Mobile_Number = $this->input->post("ProposedMobileNumber");
            $Proposed_Email_Address = $this->input->post("ProposedEmailAddress");
            $Proposed_Address = $this->input->post("ProposedAddress");
            $Discharge_Of_Charge_Interest = $this->input->post("Discharge_Of_Chargeinterest");
            $Discharge_Of_Charge_Description = $this->input->post("Discharge_Of_ChargeDescription");
            $LRNumber = $this->input->post("LRNumber");
            $AreaSize = $this->input->post("AreaSize");
            $Registry = $this->input->post("Registry");
            $County = $this->input->post("County");
            $CreatedBy = $this->session->userdata('RowID');

            $data = array(
                'Registry' => $Registry,
                'LRNO' => $LRNO,
                'AreaSize' => $AreaSize,
                'AreaUnit' => $AreaUnit,
                'NatureOfTitle' => $NatureOfTitle,
                'RegisteredProprietor' => $RegisteredProprietor,
                'TypeOfDevelopment' => $TypeOfDevelopment,
                'NearbyInfrastructure' => $NearbyInfrastructure,
                'NearbyValue' => $NearbyValue,
                'TypeOfRoad' => $TypeOfRoad,
                'NameOfRoad' => $NameOfRoad,
                'DistanceFromRoad' => $DistanceFromRoad,
                'TypeOfTown' => $TypeOfTown,
                'NameOfTown' => $NameOfTown,
                'DistanceFromTown' => $DistanceFromTown,
                'CreatedBy' => $CreatedBy,
            );
            $usr_result = $this->Discharge_Of_ChargeModel->apply_Discharge_Of_Charge($data);

            //check if username and password is correct
            if ($usr_result == TRUE) { //active user record is present
                $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                              <a href="#" class="close" data-dismiss="alert">&times;</a>
                             
                            <strong>Success!</strong> You have successfully Added new Parcel!</div>');

                redirect('start/addvaluationdata');
            } else if ($usr_result == FALSE) {

                $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                             <a href="#" class="close" data-dismiss="alert">&times;</a>
                              <strong>Error!</strong> Data are not saved.. Try again</div>');

                redirect('start/addvaluationdata');
            }
        }

    public function save_advocate_details() {
        $advocate_data = array(
            'advoFName' => $this->input->post('advoFName'),
            'advoMName' => $this->input->post('advoMName'),
            'advoLName' => $this->input->post('advoLName'),
            'advoPinNumber' => $this->input->post('advoPinNumber'),
            'advoPostalAddress' => $this->input->post('advoPostalAddress'),
            'advoEmail' => $this->input->post('advoEmail'),
            'advoTelNumber' => $this->input->post('advoTelNumber'),
            'advoLawFirm' => $this->input->post('advoLawFirm'),
            'advoPFNumber' => $this->input->post('advoPFNumber'),
            'advoIDNumber' => $this->input->post('advoIDNumber'),
            'advoNationality' => $this->input->post('advoNationality'),
        );

        $sessioned_Discharge_Of_Charge = $this->session->userdata('sessioned_Discharge_Of_Charge');

        $this->db->where('RowID', $sessioned_Discharge_Of_Charge['RowID']);

        $this->db->update('lmais_dischargeofcharge', $advocate_data);

        $this->session_Discharge_Of_Charge($sessioned_Discharge_Of_Charge['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Advocates details saved successfully</div>');

        redirect('Discharge_Of_Charge/apply_Discharge_Of_Charge?step=5');
    }

    public function delete_seller() {
        $sellerID = $this->input->post('SellerID');

        $this->db->where('RowID', $sellerID);
        $this->db->delete('lmais_sellers');

        $sessioned_Discharge_Of_Charge = $this->session->userdata('sessioned_Discharge_Of_Charge');

        $this->session_Discharge_Of_Charge($sessioned_Discharge_Of_Charge['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong>Discharge_Of_Charge Applicant\'s details deleted successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/Discharge_Of_Charge/apply_Discharge_Of_Charge?step=2'
        ));
    }


     public function review_application() {
		 $this->payment();
        redirect('Discharge_Of_Charge/apply_Discharge_Of_Charge?step=7');
    }

//E# delete_seller() function

    function GetRegistriesMatch($ActID, $CountyID) {
        //$ActID = $this->uri->segment(3);
        //$CountyID = $this->uri->segment(4);
        $Registries = $this->Registrymodel->GetMatchedRegistries($ActID, $CountyID);

        return $Registries;
    }

}
