<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends CI_Controller {

    function __construct() {

        parent::__construct();
        $this->load->model('Settings_Model', '', TRUE);
        $this->load->model('usermodel', '', TRUE);
        //$this->access_only_team_members();
    }

    //only admin can change other user's info
    //none admin users can only change his/her own info
    function only_admin_or_own($user_id) {
        if ($user_id && ($this->login_user->is_admin || $this->login_user->id === $user_id)) {
            return true;
        } else {
            redirect("forbidden");
        }
    }

    public function index() {
        redirect('users/UserManagement');
    }

    public function UserManagement() {
        $data['Results'] = $this->Usermodel->GetAllUsers();

        $this->load->view('Valuation/PageResources/usersheader');
        $this->load->view('Valuation/Header/header');
        $this->load->view('Valuation/Menu/menu');
        $this->load->view('users/index', $data);
    }

    /* open new member modal */

    function modal_form() {
        $this->access_only_admin();

        validate_submitted_data(array(
            "id" => "numeric"
        ));

        $view_data['role_dropdown'] = $this->_get_roles_dropdown();

        $id = $this->input->post('id');
        $options = array(
            "id" => $id,
        );

        $view_data['model_info'] = $this->Users_model->get_details($options)->row();
        $this->load->view('team_members/modal_form', $view_data);
    }

    /* save new member */

    function add_team_member() {
        $this->access_only_admin();

        //check duplicate email address, if found then show an error message
        if ($this->Users_model->is_email_exists($this->input->post('email'))) {
            echo json_encode(array("success" => false, 'message' => lang('duplicate_email')));
            exit();
        }

        validate_submitted_data(array(
            "email" => "required|valid_email",
            "first_name" => "required",
            "last_name" => "required",
            "job_title" => "required",
            "role" => "numeric"
        ));

        $user_data = array(
            "email" => $this->input->post('email'),
            "password" => md5($this->input->post('password')),
            "first_name" => $this->input->post('first_name'),
            "last_name" => $this->input->post('last_name'),
            "is_admin" => $this->input->post('is_admin'),
            "address" => $this->input->post('address'),
            "phone" => $this->input->post('phone'),
            "gender" => $this->input->post('gender'),
            "job_title" => $this->input->post('job_title'),
            "phone" => $this->input->post('phone'),
            "gender" => $this->input->post('gender'),
            "user_type" => "staff",
            "created_at" => get_current_utc_time()
        );

        //make role id or admin permission 
        $role = $this->input->post('role');
        $role_id = $role;

        if ($role === "admin") {
            $user_data["is_admin"] = 1;
            $user_data["role_id"] = 0;
        } else {
            $user_data["is_admin"] = 0;
            $user_data["role_id"] = $role_id;
        }


        //add a new team member
        $user_id = $this->Users_model->save($user_data);
        if ($user_id) {
            //user added, now add the job info for the user
            $job_data = array(
                "user_id" => $user_id,
                "salary" => $this->input->post('salary'),
                "salary_term" => $this->input->post('salary_term'),
                "date_of_hire" => $this->input->post('date_of_hire')
            );
            $this->Users_model->save_job_info($job_data);

            //send login details to user
            if ($this->input->post('email_login_details')) {

                //get the login details template
                $email_template = $this->Email_templates_model->get_final_template("login_info");

                $parser_data["SIGNATURE"] = $email_template->signature;
                $parser_data["USER_FIRST_NAME"] = $user_data["first_name"];
                $parser_data["USER_LAST_NAME"] = $user_data["last_name"];
                $parser_data["USER_LOGIN_EMAIL"] = $user_data["email"];
                $parser_data["USER_LOGIN_PASSWORD"] = $this->input->post('password');
                $parser_data["DASHBOARD_URL"] = base_url();

                $message = $this->parser->parse_string($email_template->message, $parser_data, TRUE);
                send_app_mail($this->input->post('email'), $email_template->subject, $message);
            }
        }

        if ($user_id) {
            echo json_encode(array("success" => true, "data" => $this->_row_data($user_id), 'id' => $user_id, 'message' => lang('record_saved')));
        } else {
            echo json_encode(array("success" => false, 'message' => lang('error_occurred')));
        }
    }

    /* open invitation modal */

    function invitation_modal() {
        $this->access_only_admin();
        $this->load->view('team_members/invitation_modal');
    }

    //send a team member invitation to an email address
    function send_invitation() {
        $this->access_only_admin();

        validate_submitted_data(array(
            "email" => "required|valid_email"
        ));

        $email = $this->input->post('email');

        //get the send invitation template 
        $email_template = $this->Email_templates_model->get_final_template("team_member_invitation");

        $parser_data["INVITATION_SENT_BY"] = $this->login_user->first_name . " " . $this->login_user->last_name;
        $parser_data["SIGNATURE"] = $email_template->signature;
        $parser_data["SITE_URL"] = get_uri();

        //make the invitation url with 24hrs validity
        $key = encode_id($this->encrypt->encode('staff|' . $email . '|' . (time() + (24 * 60 * 60))), "signup");
        $parser_data['INVITATION_URL'] = get_uri("signup/accept_invitation/" . $key);

        //send invitation email
        $message = $this->parser->parse_string($email_template->message, $parser_data, TRUE);
        if (send_app_mail($email, $email_template->subject, $message)) {
            echo json_encode(array('success' => true, 'message' => lang("invitation_sent")));
        } else {
            echo json_encode(array('success' => false, 'message' => lang('error_occurred')));
        }
    }

    //prepere the data for members list
    function list_data() {
        $options = array(
            "status" => $this->input->post("status"),
            "user_type" => "staff",
        );

        $list_data = $this->Users_model->get_details($options)->result();
        $result = array();
        foreach ($list_data as $data) {
            $result[] = $this->_make_row($data);
        }
        echo json_encode(array("data" => $result));
    }

    //get a row data for member list
    function _row_data($id) {
        $options = array("id" => $id);
        $data = $this->Users_model->get_details($options)->row();
        return $this->_make_row($data);
    }

    //prepare team member list row
    private function _make_row($data) {
        $image_url = get_avatar($data->image);
        $user_avatar = "<span class='avatar avatar-xs'><img src='$image_url' alt='...'></span>";
        $full_name = $data->first_name . " " . $data->last_name . " ";

        return array(
            $user_avatar,
            get_team_member_profile_link($data->id, $full_name),
            $data->job_title,
            $data->email,
            $data->phone ? $data->phone : "-"
        );
    }

    //show team member's details view
    function view($id = 0, $tab = "") {
        if ($id * 1) {
            //we have an id. view the team_member's profie
            $options = array("id" => $id, "user_type" => "staff");
            $user_info = $this->Users_model->get_details($options)->row();
            if ($user_info) {

                //check which tabs are viewable for current logged in user
                $view_data['show_attendance'] = false;
                $view_data['show_leave'] = false;
                $view_data['show_general_info'] = false;
                $view_data['show_job_info'] = false;
                $view_data['show_account_settings'] = false;

                //admin can access all members attendance and leave
                //none admin users can only access to his/her own information 

                if ($this->login_user->is_admin || $user_info->id === $this->login_user->id) {
                    $view_data['show_attendance'] = true;
                    $view_data['show_leave'] = true;
                    $view_data['show_general_info'] = true;
                    $view_data['show_job_info'] = true;
                    $view_data['show_account_settings'] = true;
                } else {
                    //none admin users but who has access to this team member's attendance and leave can access this info
                    $access_timecard = $this->get_access_info("attendance");
                    if ($access_timecard->access_type === "all" || array_search($user_info->id, $access_timecard->allowed_members)) {
                        $view_data['show_attendance'] = true;
                    }

                    $access_leave = $this->get_access_info("leave");
                    if ($access_leave->access_type === "all" || array_search($user_info->id, $access_leave->allowed_members)) {
                        $view_data['show_leave'] = true;
                    }
                }

                $view_data['tab'] = $tab; //selected tab
                $view_data['user_info'] = $user_info;
                $view_data['social_link'] = $this->Social_links_model->get_one($id);
                $this->template->rander("team_members/view", $view_data);
            } else {
                show_404();
            }
        } else {
            //we don't have any specific id to view. show the list of team_member
            $view_data['team_members'] = $this->Users_model->get_details(array("user_type" => "staff", "status" => "active"))->result();
            $this->template->rander("team_members/profile_card", $view_data);
        }
    }

    //show the job information of a team member
    function job_info($user_id) {
        $this->only_admin_or_own($user_id);

        $options = array("id" => $user_id);
        $user_info = $this->Users_model->get_details($options)->row();

        $view_data['user_id'] = $user_id;
        $view_data['job_info'] = $this->Users_model->get_job_info($user_id);
        $view_data['job_info']->job_title = $user_info->job_title;
        $this->load->view("team_members/job_info", $view_data);
    }

    //save job information of a team member
    function save_job_info() {
        $this->access_only_admin();

        validate_submitted_data(array(
            "user_id" => "required|numeric"
        ));

        $user_id = $this->input->post('user_id');

        $job_data = array(
            "user_id" => $user_id,
            "salary" => unformat_currency($this->input->post('salary')),
            "salary_term" => $this->input->post('salary_term'),
            "date_of_hire" => $this->input->post('date_of_hire')
        );

        //we'll save the job title in users table
        $user_data = array(
            "job_title" => $this->input->post('job_title')
        );

        $this->Users_model->save($user_data, $user_id);
        if ($this->Users_model->save_job_info($job_data)) {
            echo json_encode(array("success" => true, 'message' => lang('record_updated')));
        } else {
            echo json_encode(array("success" => false, 'message' => lang('error_occurred')));
        }
    }

    //show general information of a team member
    function general_info($user_id) {
        $this->only_admin_or_own($user_id);

        $view_data['user_info'] = $this->Users_model->get_one($user_id);
        $this->load->view("team_members/general_info", $view_data);
    }

    //save general information of a team member
    function save_general_info($user_id) {
        $this->only_admin_or_own($user_id);

        validate_submitted_data(array(
            "first_name" => "required",
            "last_name" => "required"
        ));

        $user_data = array(
            "first_name" => $this->input->post('first_name'),
            "last_name" => $this->input->post('last_name'),
            "address" => $this->input->post('address'),
            "phone" => $this->input->post('phone'),
            "skype" => $this->input->post('skype'),
            "gender" => $this->input->post('gender'),
            "alternative_address" => $this->input->post('alternative_address'),
            "alternative_phone" => $this->input->post('alternative_phone'),
            "dob" => $this->input->post('dob'),
            "ssn" => $this->input->post('ssn')
        );

        $user_info_updated = $this->Users_model->save($user_data, $user_id);
        if ($user_info_updated) {
            echo json_encode(array("success" => true, 'message' => lang('record_updated')));
        } else {
            echo json_encode(array("success" => false, 'message' => lang('error_occurred')));
        }
    }

    //show social links of a team member
    function social_links($user_id) {
        //important! here id=user_id
        $this->only_admin_or_own($user_id);

        $view_data['user_id'] = $user_id;
        $view_data['model_info'] = $this->Social_links_model->get_one($user_id);
        $this->load->view("users/social_links", $view_data);
    }

    //save social links of a team member
    function save_social_links($user_id) {
        $this->only_admin_or_own($user_id);

        $id = 0;
        $has_social_links = $this->Social_links_model->get_one($user_id);
        if (isset($has_social_links->id)) {
            $id = $has_social_links->id;
        }

        $social_link_data = array(
            "facebook" => $this->input->post('facebook'),
            "twitter" => $this->input->post('twitter'),
            "linkedin" => $this->input->post('linkedin'),
            "googleplus" => $this->input->post('googleplus'),
            "digg" => $this->input->post('digg'),
            "youtube" => $this->input->post('youtube'),
            "pinterest" => $this->input->post('pinterest'),
            "instagram" => $this->input->post('instagram'),
            "github" => $this->input->post('github'),
            "tumblr" => $this->input->post('tumblr'),
            "vine" => $this->input->post('vine'),
            "user_id" => $user_id,
            "id" => $id ? $id : $user_id
        );

        $this->Social_links_model->save($social_link_data, $id);
        echo json_encode(array("success" => true, 'message' => lang('record_updated')));
    }

    //show account settings of a team member
    function account_settings($user_id) {
        $this->only_admin_or_own($user_id);

        $view_data['user_info'] = $this->Users_model->get_one($user_id);
        if ($view_data['user_info']->is_admin) {
            $view_data['user_info']->role_id = "admin";
        }
        $view_data['role_dropdown'] = $this->_get_roles_dropdown();
        $this->load->view("users/account_settings", $view_data);
    }

    //prepare the dropdown list of roles
    private function _get_roles_dropdown() {
        $role_dropdown = array(
            "0" => lang('team_member'),
            "admin" => lang('admin') //static role
        );
        $roles = $this->Roles_model->get_all(array("deleted" => 0))->result();
        foreach ($roles as $role) {
            $role_dropdown[$role->id] = $role->title;
        }
        return $role_dropdown;
    }

    //save account settings of a team member
    function save_account_settings($user_id) {
        $this->only_admin_or_own($user_id);

        if ($this->Users_model->is_email_exists($this->input->post('email'), $user_id)) {
            echo json_encode(array("success" => false, 'message' => lang('duplicate_email')));
            exit();
        }
        $account_data = array(
            "email" => $this->input->post('email')
        );

        if ($this->login_user->is_admin && $this->login_user->id != $user_id) {
            //only admin user has permission to update team member's role
            //but admin user can't update his/her own role 
            $role = $this->input->post('role');
            $role_id = $role;

            if ($role === "admin") {
                $account_data["is_admin"] = 1;
                $account_data["role_id"] = 0;
            } else {
                $account_data["is_admin"] = 0;
                $account_data["role_id"] = $role_id;
            }

            $account_data['disable_login'] = $this->input->post('disable_login');
            $account_data['status'] = $this->input->post('status') === "inactive" ? "inactive" : "active";
        }

        //don't reset password if user doesn't entered any password
        if ($this->input->post('password')) {
            $account_data['password'] = md5($this->input->post('password'));
        }

        if ($this->Users_model->save($account_data, $user_id)) {
            echo json_encode(array("success" => true, 'message' => lang('record_updated')));
        } else {
            echo json_encode(array("success" => false, 'message' => lang('error_occurred')));
        }
    }

    //save profile image of a team member
    function save_profile_image($user_id = 0) {
        $this->only_admin_or_own($user_id);

        //process the the file which has uploaded by dropzone
        $profile_image = $this->input->post("profile_image");
        if ($profile_image) {
            $profile_image = move_temp_file("avatar.png", get_setting("profile_image_path"), $profile_image);
            $this->Users_model->save(array("image" => $profile_image), $user_id);
            echo json_encode(array("success" => true, 'message' => lang('profile_image_changed')));
        }

        //process the the file which has uploaded using manual file submit
        if ($_FILES) {
            $profile_image_file = get_array_value($_FILES, "profile_image_file");
            $image_file_name = get_array_value($profile_image_file, "tmp_name");
            if ($image_file_name) {
                $profile_image = move_temp_file("avatar.png", get_setting("profile_image_path"), $image_file_name);
                $this->Users_model->save(array("image" => $profile_image), $user_id);
                echo json_encode(array("success" => true, 'message' => lang('profile_image_changed')));
            }
        }
    }

    public function RegisterUsertoDB() {

        $FirstName = $this->input->post("FirstName");
        $LastName = $this->input->post("LastName");
        $Email = $this->input->post("Email");
        $Phone = $this->input->post("Phone");
        $Password = $this->input->post("IDNumber");
        $IDNumber = $this->input->post("IDNumber");
        $City = $this->input->post("City");
        $ZipCode = $this->input->post("ZipCode");

        $options = ['cost' => 12];
        $encripted_pass = password_hash($Password, PASSWORD_BCRYPT, $options);


        $data = array(
            'FirstName' => $FirstName,
            'LastName' => $LastName,
            'Email' => $Email,
            'Phone' => $Phone,
            'Password' => $encripted_pass,
            'IDNumber' => $IDNumber,
            'City' => $City
        );

        $email_smtp_host = $this->Settings_Model->get_setting("email_smtp_host");
        $email_smtp_port = $this->Settings_Model->get_setting("email_smtp_port");
        $email_smtp_user = $this->Settings_Model->get_setting("email_smtp_user");
        $email_smtp_pass = $this->Settings_Model->get_setting("email_smtp_pass");
        $usr_result = $this->usermodel->registerusers($data);

        //check if username and password is correct
        if ($usr_result == TRUE) { //active user record is present
            $weburl = 'http://localhost/lmais';
            $url = 'https://localhost/lmais';
            $subject = 'Land Management System';
            $message = "Hi " . $LastName . ",<br>
<br> We are excited you signed up for Jkuat online Admission.<br> All communications will be done online through this account.<br> Your login credentials are:<br>
						<strong>
						Username : " . $Email . " <br>
						Password : " . $IDNumber . "<br> 
						</strong>
						Kindly click <a href='" . $url . "'>here to login</a>";
            if ($this->sendEmail($Email, $subject, $message, $IDNumber)) {
                $this->session->set_flashdata('signupmsg', '<div class="alert alert-info fade in">
<a href="#" class="close" data-dismiss="alert">&times;</a>
<strong>Success!</strong>
<br>Account was created successfully. Your login credentials are: <strong>Username : ' . $email_smtp_host . ' and Password : ' . $IDNumber . ' .Kindly check your email </strong>
</div>');

                redirect('users/UserManagement');
            } else {
                $this->session->set_flashdata('signupmsg', '<div class="alert alert-warning fade in">
<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Success!</strong> You have successfully created an account. Unfortunately there was an error sending you an email!<br>
</div>');

                redirect('users/UserManagement');
            }
        } else {
            $this->session->set_flashdata('signupmsg', '<div class="alert alert-danger fade in">
<a href="#" class="close" data-dismiss="alert">&times;</a>
			  	<strong>Error!</strong> Account not created... That ID number already exist ! </div>');

            redirect('users/UserManagement');
        }
    }

    function sendEmail($emailto, $subject, $message, $IDNumber) {




        $htmlContent = '
   <div style="font-family:HelveticaNeue-Light,Arial,sans-serif;background-color:#eeeeee">
	<table align="center" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
    <tbody>
        <tr>
        	<td>
                <table align="center" width="750px" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="width:750px!important">
                <tbody>
                	<tr>
                    	<td>
                			<table width="690" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
                            <tbody>
                            	
                                <tr>
                                    <td colspan="3" align="center">
                                        <table width="630" align="center" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        	<tr>
                                            	<td colspan="3" height="60"/>
</tr>
<tr>
<td width="25"/>
                                                <td align="center">
                                                    <h1 style="font-family:HelveticaNeue-Light,arial,sans-serif;font-size:48px;color:#404040;line-height:48px;font-weight:bold;margin:0;padding:0">Welcome to LMAIS</h1>
                                                </td>
                                                <td width="25"/>
                                            </tr>
                                            <tr>
                                            	<td colspan="3" height="40"/>
</tr>
<tr>
<td colspan="5" align="center">
                                                    <p style="color:#404040;font-size:16px;line-height:24px;font-weight:lighter;padding:0;margin:0">
													<bold> Land Management and Administration System (LMAIS) </bold> is a robust, real-time, GIS integrated land administration system that allows for the management of surface tract data, ownership, permits, lease records, right of ways and valuation information. 
													<p>
													<strong>
						Username : ' . $emailto . ' <br>
						Password : ' . $IDNumber . '<br> 
						</strong>
													</p>
                                                </td>
                                            </tr>
                                            <tr>
                                            <td colspan="4">
                                                <div style="width:100%;text-align:center;margin:30px 0">
                                                    <table align="center" cellpadding="0" cellspacing="0" style="font-family:HelveticaNeue-Light,Arial,sans-serif;margin:0 auto;padding:0">
                                                    <tbody>
                                                    	<tr>
                                                            <td align="center" style="margin:0;text-align:center">
<a href="http://localhost/lmais/" style="font-size:21px;line-height:22px;text-decoration:none;color:#ffffff;font-weight:bold;border-radius:2px;background-color:#0096d3;padding:14px 40px;display:block;letter-spacing:1.2px" target="_blank">Click here to Login</a>
</td>
                                                      	</tr>
                                                   	</tbody>
                                                    </table>
                                               	</div>
                                           	</td>
                                       	</tr>
                                        <tr>
<td colspan="3" height="30"/>
</tr>
                                 	</tbody>
                                    </table>
                             	</td>
                   			</tr>
                            
                            
                          	</tbody>
                            </table>
                  			
                  		</td>
                	</tr>
              	</tbody>
                </table>
            </td>
		</tr>
 	</tbody>
    </table>
</div>';
        $email_smtp_host = $this->Settings_Model->get_setting("email_smtp_host");
        $email_smtp_port = $this->Settings_Model->get_setting("email_smtp_port");
        $email_smtp_user = $this->Settings_Model->get_setting("email_smtp_user");
        $email_smtp_pass = $this->Settings_Model->get_setting("email_smtp_pass");

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => '444625sls@gmail.com',
            'smtp_pass' => 'wsxzaq12',
            'smtp_timeout' => '4',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $url = 'http://localhost/lmais';
        //$email = $this->input->post("email");
        //$surname = $this->input->post("surname");
        //$indexno = $this->input->post("indexno");
        $this->email->from('support@lmais.go.ke', 'User Creation - Land Management System');
        $this->email->to($emailto);
        //$this->email->cc('chriskithu@gmail.com'); 
        $this->email->subject($subject);
        $this->email->message($htmlContent);
        //$this->email->message("Hi!");
        // $this->email->attach('/path/to/file1.png'); // attach file
        //$this->email->attach('/path/to/file2.pdf');
        if ($this->email->send())
            return "Mail Sent!";
        else
            return $this->email->print_debugger();
    }

    public function logout() {

        $this->session->unset_userdata('logged_in');
        session_destroy();
 redirect(base_url(),'refresh');
       
    }

}

/* End of file team_member.php */
/* Location: ./application/controllers/team_member.php */