<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rent extends MY_Controller {

    private $service_slug = 'rent';
    private $service_payable = array(
        array(
            'slug' => 'convenience',
            'db_field' => 'ConvenienceFee',
        ),
        array(
            'slug' => 'rent',
            'db_field' => 'Rent',
        ),
    );

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('TenureModel', '', TRUE);
        $this->load->model('Countymodel', '', TRUE);
        $this->load->model('Registrymodel', '', TRUE);
        $this->load->model('ParcelsValuationModel', '', TRUE);
        $this->load->model('Countrymodel', '', TRUE);
    }

    /**
     * S# calculateRent() function
     * 
     * Calculate rent
     * 
     * @param str $lr_number LR Number
     * 
     * @return array
     * 
     */
    public function calculateRent($lr_number) {
        $data = array();
        $data['total'] = $data['interest'] = $data['principal'] = $rent = $principal = 0;
        //$lr_number = 'Nairobi/block106/515';

        $this->db->where('LRNumber', $lr_number);
        $rent_model = $this->db->get('lmais_rent_data')->row();

        $now = new \Datetime('now');

        if ($rent_model) {
            $interest = 0;
            $principal = $rent_model->RentAmount;
            $data['rent_amount'] = $principal;
            //Arrears from
            $arrears_from = ($rent_model->InArrearsSince == '0000-00-00 00:00:00') ? $rent_model->WithEffectFrom : $rent_model->InArrearsSince;

            $date = new \DateTime();
            $date->setDate(substr(($arrears_from + 1), 0, 4), 1, 1);

            $data['in_arrears_from'] = $date;
            //$date = date_create_from_format('Y-m-d', substr($arrears_from, 0, 10));
            //$start = new DateTime('2009-02-01');
            $interval = new DateInterval('P1M');

            echo '<p>';
            var_dump($interval);
            echo '<p>';
            //$end = new DateTime('2011-01-01');
            $period = new DatePeriod($date, $interval, $now);

            // var_dump($period);
            echo '<p>Effective from: ' . $date->format('Y-m-d') . '<p>';
            echo 'Now: ' . $now->format('Y-m-d') . '<p>';
            $index = 0;
            foreach ($period as $dt) {
                if ($dt->format('m') == 1) {
                    $principal += $rent_model->RentAmount;
                    $description = $dt->format('Y') . ' land rent ' . number_format($rent_model->RentAmount, 2);
                    $amount = $rent_model->RentAmount;
                } else {
                    $month_interest = $this->calculateInterest($rent_model->RentAmount, 1);
                    echo $dt->format('F Y') . ': Interest: ' . $month_interest;

                    $interest += $month_interest;
                    echo '<p>';
                    $description = $dt->format('M-Y') . ' 1% penalty on ' . number_format($rent_model->RentAmount, 2);
                    $amount = $month_interest;
                }//E# if statement

                $data['items'][] = array(
                    'description' => $description,
                    'amount' => $amount,
                );
                $index++;
            }//E# foreach statement

            $principal -= $rent_model->RentAmount;
            $data['principal'] = $principal;
            $data['interest'] = $interest;

            echo '<p>Months ' . $index;
            echo '<p>Total principal ' . $principal . '<p>';
            echo '<p>Total Interest ' . $interest . '<p>';

            $data['total'] = $principal + $interest;
        } else {
            
        }//E# if else statement

        return $data;
    }

//E# calculateRent() function

    /**
     * S# calculateInterest() function
     * 
     * Calculate Interest
     * 
     * @param float $principal Principal
     * @param float $rate Rate
     * 
     * @return float
     */
    private function calculateInterest($principal, $rate) {
        $interest = ($principal * $rate) / 100;
        return $interest;
    }

//E# calculateInterest() function

    /**
     * S# payment_completed() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Payment completed
     * 
     */
    public function payment_completed() {

        $data = $this->input->get();

        //Load Ecitizen
        $this->load->library('ecitizen');

        if (array_key_exists('bill_ref', $data)) {
            $rent_model = $this->db
                            ->select('*')
                            ->where('EcitizenBillRef', $data['bill_ref'])
                            ->get('lmais_rent')->row();


            if ($rent_model) {
                $ecitizen_response = $this->ecitizen->query_transaction_status($data['bill_ref']);

                if ($ecitizen_response['status']) {
                    $amount_paid = $ecitizen_response['message']['amount'];

                    $now = date('Y-m-d H:i:s');

                    $transaction_model = $this->db
                                    ->select('*')
                                    ->where('ServiceID', $rent_model->RowID)
                                    ->where('ServiceSlug', $this->service_slug)
                                    ->where('Type', 'payment')
                                    ->get('lmais_transactions')->row();

                    if (!$transaction_model) {//Payment does not exist so create one
                        //Rent Due Payment
                        $payment_data = array(
                            'AccountNumber' => $ecitizen_response['message']['account_number'], //CHANGE - Addition
                            'Currency' => 'KES',
                            'Date' => $now,
                            'Type' => 'payment',
                            'Description' => $rent_model->Description,
                            'Amount' => $amount_paid,
                            'ServiceSlug' => $this->service_slug,
                            'ServiceID' => $rent_model->RowID,
                            'UserID' => $rent_model->UserID,
                            'TransactionID' => $rent_model->TransactionID,
                            'LRNumber' => $rent_model->LRNumber,
                            'Status' => 'paid',
                            'Ip' => 'paid',
                            'Agent' => $_SERVER['HTTP_USER_AGENT'],
                            'Ip' => $_SERVER['REMOTE_ADDR'],
                        );

                        // var_dump($payment_data);
                        // die("SAd");
                        $this->db->insert('lmais_transactions', $payment_data);
                    }//E# if else statement
                    //Get total payments
                    $total_payment = $this->db
                                    ->select_sum('Amount')
                                    ->where('ServiceID', $rent_model->RowID)
                                    ->where('ServiceSlug', $this->service_slug)
                                    ->where('Type', 'payment')
                                    ->get('lmais_transactions')->row();

                    //Get total invoices
                    $total_invoice = $this->db
                                    ->select_sum('Amount')
                                    ->where('ServiceID', $rent_model->RowID)
                                    ->where('ServiceSlug', $this->service_slug)
                                    ->where('Type', 'invoice')
                                    ->get('lmais_transactions')->row();


                    if (($total_payment->Amount == $total_invoice->Amount) || ($total_payment->Amount > $total_payment->Amount)) {

                        $rent_data = array(
                            'Status' => 'paid',
                        );

                        $this->db->where('RowID', $rent_model->RowID);

                        $this->db->update('lmais_rent', $rent_data);

                        $received_request_data = array(
                            'Status' => 'paid',
                        );

                        $this->db->where('TransactionID', $rent_model->TransactionID);

                        $this->db->update('lmais_receivedrequests', $received_request_data);

                        //Update the rent data
                        $rent_data = array(
                            'InArrearsSince' => $now,
                            'WithEffectFrom' => $now
                        );

                        $this->db->where('LRNumber', $rent_model->LRNumber);

                        $this->db->update('lmais_rent_data', $rent_data);
                    }//E# statement

                    /*
                      if ($this->config->item('environment') == 'local') {
                      $consent_data = array(
                      'Status' => 'paid',
                      );

                      $this->db->where('RowID', $transfer_model->RowID);

                      $this->db->update('lmais_rent', $consent_data);
                      } */
                    //Get user model
                    $user_model = $this->db
                                    ->select('*')
                                    ->where('RowID', $rent_model->UserID)
                                    ->get('lmais_users')->row();

                    if ($user_model) {
                        //Send login email
                        $parameters = array(
                            'name' => ucwords(strtolower($user_model->FirstName . ' ' . $user_model->LastName)),
                            'currency' => 'KES',
                            'service' => $rent_model->Description,
                            'amount' => $amount_paid,
                            'time' => date('d/m/Y H:i'),
                            'refnumber' => $rent_model->TransactionID,
                        );

                        //Generate PDF invoice and save to file
                        $pdf_attachment_path = $this->invoice($rent_model->TransactionID, $this->service_slug, $rent_model->UserID, 'file');

                        if ($pdf_attachment_path) {//Attachment was generated successfully
                            $recipient['attachments'] = array($pdf_attachment_path);
                        }//E# if statement
                        //Generate PDF invoice and save to file
                        $document_attachment_path = $this->land_rent($rent_model->TransactionID, 'file');

                        if ($document_attachment_path) {//Attachment was generated successfully
                            $recipient['attachments'] = array($pdf_attachment_path, $document_attachment_path);
                        }//E# if statement
                        $this->load->library('message');

                        if ($user_model->Email) {
                            $recipient['to']['email'] = $user_model->Email;
                            $recipient['to']['name'] = $parameters['name'];

                            //Send email
                            $sent = $this->message->email(1, array('name' => 'LMAIS', 'email' => 'info@ardhi.go.ke'), 1, $recipient, 'document', 'en', $parameters);
                        }//E# statement
                        //Delete the genearated pdf to save on space
                        if ($pdf_attachment_path) {
                            unlink($pdf_attachment_path);
                        }//E# if statement
                        //Delete the genearated pdf to save on space
                        if ($document_attachment_path) {
                            unlink($document_attachment_path);
                        }//E# if statement
                        if ($user_model->Phone) {
                            $this->message->sms(1, 1, 1, $user_model->Phone, 'document', 'en', $parameters);
                        }//E# statement
                    }//E# statement
                    //Clear all consent session
                    $this->session->unset_userdata('sessioned_rent');
                    $this->session->unset_userdata('sessioned_buyers');
                    $this->session->unset_userdata('sessioned_rla');

                    $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
						  <a href="#" class="close" data-dismiss="alert">&times;</a>
						 
						<strong>Success!</strong> Thanks making the payment</div>');
                } else {
                    
                }//E# if else statement
            }//E# if else statement
        }//E# if else statement

        redirect('UserView/LoadApplications');
    }

//E# payment_completed() function

    public function index() {
        if ($this->session->userdata('Logged_in') != '') {

            self::loadindex();
        } else
            $this->load->view('signin/PageResources/loginheader');
        $this->load->view('signin/login');
    }

    /**
     * S# raise_invoices() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Raise invoices
     * 
     */
    private function raise_invoices($sessioned_rent, $transaction_id) {

        //$doc_id = $this->session->userdata('sessioned_rla')->DocID;

        $invoice_data = array();

        $now = date('Y-m-d H:i:s');

        foreach ($this->service_payable as $single_service_payable) {
            //Service model
            $service_model = $this->db
                            ->select('*')
                            ->where('Slug', $single_service_payable['slug'])
                            ->get('lmais_landservicetypes')->row();
            if ($service_model) {
                //Invoice model
                $invoice_model = $this->db
                                ->select('*')
                                ->where('ServiceID', $sessioned_rent['RowID'])
                                ->where('ServiceTypeID', $service_model->RowID)
                                ->get('lmais_transactions')->row();

                if (!$invoice_model) {
                    //Consent Fee Invoice
                    $invoice_data[] = array(
                        'LRNumber' => $sessioned_rent['LRNumber'],
                        'Agent' => $_SERVER['HTTP_USER_AGENT'],
                        'Ip' => $_SERVER['REMOTE_ADDR'],
                        'Currency' => 'KES',
                        'Date' => $now,
                        'Type' => 'invoice',
                        'Description' => $service_model->ServiceName,
                        'ServiceTypeID' => $service_model->RowID,
                        'ServiceCode' => ($this->config->item('environment') == 'live') ? $service_model->LiveServiceCode : $service_model->TestServiceCode,
                        'Amount' => $service_model->Slug == 'rent' ? $sessioned_rent['Rent'] : $service_model->ServiceAmount,
                        'ServiceSlug' => $this->service_slug,
                        'ServiceID' => $sessioned_rent['RowID'],
                        'Status' => 'unpaid',
                        'UserID' => $this->session->userdata('UserID'),
                        'TransactionID' => $transaction_id,
                    );
                }//E# if statement
            }//E# if statement
        }//E# foreach statement

        if ($invoice_data) {
            $this->db->insert_batch('lmais_transactions', $invoice_data);
        }//E# if statement
    }

//E# raise_invoices() function

    /**
     * S# payment() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * payment
     */
    public function payment() {
        $sessioned_rent = $this->session->userdata('sessioned_rent');

        $received_request_model = $this->db
                        ->select('*')
                        ->order_by('RowID', 'desc')
                        ->where('ServiceSlug', $this->service_slug)
                        ->where('ServiceID', $sessioned_rent['RowID'])
                        ->get('lmais_receivedrequests')->row();

        if (!$received_request_model) {
            $received_request_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->get('lmais_receivedrequests')->row();

            $serial = $received_request_model ? $received_request_model->TransactionID : 'TRA00D0C1A';

            $serial++;
            $generated_serial = $serial++;

            if ($this->session->has_userdata('sessioned_rla')) {
                $doc_id = $this->session->userdata('sessioned_rla')->DocID;
            } else {
                $doc_id = '';
            }//E# if else statement

            $received_request_data = array(
                'BuyerUserID' => $this->session->userdata('UserID'),
                'TransactionID' => $generated_serial,
                'Status' => 'unpaid',
                'CompletionStage' => 0,
                'ServiceSlug' => $this->service_slug,
                'ServiceID' => $sessioned_rent['RowID'],
                'ServiceTypeID' => 1,
                'DocID' => $doc_id,
                'LandUse' => 1,
                'Status' => 'Pending',
                'DateCreated' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('lmais_receivedrequests', $received_request_data);
        } else {
            $generated_serial = $received_request_model->TransactionID;
        }//E# if else statement
        //Update buyers with the transaction id
        $seller_data = array(
            'TransactionID' => $generated_serial,
        );

        $sessioned_rent = $this->session->userdata('sessioned_rent');

        $this->db->where('ServiceID', $sessioned_rent['RowID']);
        $this->db->where('ServiceSlug', $this->service_slug);

        $this->db->update('lmais_sellers', $seller_data);

        //Create invoices
        $this->raise_invoices($sessioned_rent, $generated_serial);

        $rent_data = array(
            'TransactionID' => $generated_serial
        );

        $this->db->where('RowID', $sessioned_rent['RowID']);

        $this->db->update('lmais_rent', $rent_data);

        //Get user model
        $user_model = $this->db
                        ->select('*')
                        ->where('RowID', $sessioned_rent['UserID'])
                        ->get('lmais_users')->row();

        if ($user_model) {
            //Send login email
            $parameters = array(
                'name' => ucwords(strtolower($user_model->FirstName . ' ' . $user_model->LastName)),
                'currency' => 'KES',
                'service' => ucfirst($this->service_slug),
                'amount' => $sessioned_rent['AmountPayable'],
                'time' => date('d/m/Y H:i'),
                'refnumber' => $generated_serial,
            );

            //Generate PDF invoice and save to file
            $pdf_attachment_path = $this->invoice($generated_serial, $this->service_slug, $sessioned_rent['UserID'], 'file');

            if ($pdf_attachment_path) {//Attachment was generated successfully
                $recipient['attachments'] = array($pdf_attachment_path);
            }//E# if statement

            $this->load->library('message');

            if ($user_model->Email) {
                $recipient['to']['email'] = $user_model->Email;
                $recipient['to']['name'] = $parameters['name'];

                //Send email
                $sent = $this->message->email(1, array('name' => 'LMAIS', 'email' => 'info@ardhi.go.ke'), 1, $recipient, 'invoice', 'en', $parameters);
            }//E# statement
            var_dump($sent);
            die();
            //Delete the genearated pdf to save on space
            if ($pdf_attachment_path) {
                unlink($pdf_attachment_path);
            }//E# if statement

            if ($user_model->Phone) {
                $this->message->sms(1, 1, 1, $user_model->Phone, 'invoice', 'en', $parameters);
            }//E# statement
        }//E# statement

        return $serial;
    }

//E# payment() function


    public function rent_clearance() {

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Thanks you for clearing your Rent Fees</div>');
        redirect('rent/apply_rent?step=3');
    }

    private function session_rent($rent_id, $session_sellers = false) {

        $rent_model = $this->db->get_where('lmais_rent', array('RowID' => $rent_id))->result_array();

        $this->session->set_userdata('sessioned_rent', $rent_model[0]);

        if ($session_sellers == 'sellers') {
            //Select all sellers
            $seller_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ServiceID', $rent_model[0]['RowID'])
                            ->where('ServiceSlug', $this->service_slug)
                            ->get('lmais_sellers')->result_array();


            $this->session->set_userdata('sessioned_sellers', $seller_model);
        }

        if ($session_sellers == 'sellers') {
            //Select all sellers
            $seller_model = $this->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('ServiceID', $rent_model[0]['RowID'])
                            ->where('ServiceSlug', $this->service_slug)
                            ->get('lmais_sellers')->result_array();


            $this->session->set_userdata('sessioned_sellers', $seller_model);
        }
    }

    public function check_property_details() {
        //Clear all rent session
        $this->session->unset_userdata('sessioned_rent');
        $this->session->unset_userdata('sessioned_rent');
        $this->session->unset_userdata('sessioned_sellers');
        $this->session->unset_userdata('sessioned_rla');
        $this->session->unset_userdata('sessioned_action');
        $this->session->unset_userdata('sessioned_balance');

        $rla_model = $this->db
                        ->select('*')
                        ->where('LRNumber', $this->input->post('Title'))
                        ->get('lmais_rent_data')->row();

        $property_availability = array(
            'Agent' => $_SERVER['HTTP_USER_AGENT'],
            'DateCreated' => date('Y-m-d H:i:s'),
            'Ip' => $_SERVER['REMOTE_ADDR'],
            'Status' => 0,
            'ServiceSlug' => $this->service_slug
        );

        if ($rla_model) {

            $property_availability['Status'] = 1;

            $this->session->set_userdata('sessioned_rla', $rla_model);

            //Service model
            $service_model = $this->db
                            ->select('*')
                            ->where('Slug', $this->service_slug)
                            ->get('lmais_landservicetypes')->row();

            $rent_array = $this->calculateRent($rla_model->LRNumber);

            $rent_data = array(
                'Agent' => $_SERVER['HTTP_USER_AGENT'],
                'DateCreated' => date('Y-m-d H:i:s'),
                'Ip' => $_SERVER['REMOTE_ADDR'],
                'UserID' => $this->session->userdata('UserID'),
                /*
                  'RegistryID' => $this->input->post('RegistryID'),
                  'RegistrationSectionID' => $this->input->post('RegistrationSectionID'),
                  'ParcelNumber' => $this->input->post('ParcelNo'),
                  'TenureID' => $this->input->post('TenureID'),
                 * 
                 */
                'LRNumber' => $rla_model->LRNumber,
                //'DocID' => $rla_model->DocID,
                'Description' => $service_model->ServiceName,
                'Prefix' => $service_model->Prefix,
                'Status' => 'unpaid',
                'Rent' => $rent_array['total'],
                'RentInterest' => $rent_array['interest'],
                'RentPrincipal' => $rent_array['principal'],
                'RentData' => json_encode($rent_array),
                'AmountPayable' => $rent_array['total'],
            );

            foreach ($this->service_payable as $single_service_payable) {
                //Service model
                $service_model = $this->db
                                ->select('*')
                                ->where('Slug', $single_service_payable['slug'])
                                ->get('lmais_landservicetypes')->row();
                if ($service_model) {
                    if ($single_service_payable['slug'] != 'rent') {
                        $rent_data[$single_service_payable['db_field']] = $service_model->ServiceAmount;
                    }//E# if statement
                }//E# if statement

                $rent_data['AmountPayable'] +=$service_model->ServiceAmount;
            }//E# foreach statement

            $this->db->insert('lmais_rent', $rent_data);

            $rent_id = $this->db->insert_id();

            $rent_data = array(
                'EcitizenBillRef' => $this->service_slug . '_' . $rent_id
            );

            $this->db->where('RowID', $rent_id);

            $this->db->update('lmais_rent', $rent_data);

            $this->session_rent($rent_id);

            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> We have found your land</div>');
            $redirect_to = 'rent/apply_rent?step=2';
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error!</strong> This parcel of land could not be found. Kindly confirm your details</div>');

            $redirect_to = 'rent/apply_rent';
        }//E# if statement

        $this->db->insert('lmais_property_availability', $property_availability);

        //var_dump($redirect_to);
        redirect($redirect_to);
    }

    /**
     * S# get_balance() function
     * 
     * Get balance
     * 
     * @param int $rent_id Rent ID
     * 
     * @return float Balance
     */
    private function get_balance($rent_id) {
        //Get total invoice
        $total_invoice = $this->db
                        ->select_sum('Amount')
                        ->where('ServiceID', $rent_id)
                        ->where('ServiceSlug', $this->service_slug)
                        ->where('Type', 'invoice')
                        ->get('lmais_transactions')->row();

        //Get total payment
        $total_payment = $this->db
                        ->select_sum('Amount')
                        ->where('ServiceID', $rent_id)
                        ->where('ServiceSlug', $this->service_slug)
                        ->where('Type', 'payment')
                        ->get('lmais_transactions')->row();

        return $total_invoice->Amount - $total_payment->Amount;
    }

//E# get_balance() function 

    public function apply_rent() {

        if ($this->session->userdata('Logged_in') != '') {

            $Department = $this->session->userdata('UserType');
            $data['tenure'] = $this->TenureModel->GetAllTenure();
            $data['input'] = $this->input->get();

            if (!array_key_exists('step', $data['input'])) {
                $data['input']['step'] = 1;
            }//E# if statement

            if ($data['input']['step'] == 1) {
                //Convenience model
                $data['convenience_model'] = $this->db
                                ->select('*')
                                ->where('Slug', 'convenience')
                                ->get('lmais_landservicetypes')->row();
            }//E# if statement

            $balance = 0;
            $action = 'creating';

            if (array_key_exists('reference_number', $data['input'])) {
                $this->session_rent($data['input']['reference_number']);
                $this->session->set_userdata('action', 'updating');
                $balance = $this->get_balance($data['input']['reference_number']);

                $this->session->set_userdata('sessioned_balance', $balance);
                $this->session->set_userdata('sessioned_action', 'updating');
            }//E# if else statement

            $data['sessioned_action'] = $this->session->userdata('sessioned_action');
            $data['sessioned_balance'] = $this->session->userdata('sessioned_balance');

            if ($this->session->has_userdata('sessioned_rent')) {
                $data['sessioned_rent'] = $this->session->userdata('sessioned_rent');
            } else {
                $data['sessioned_rent'] = false;
                if ($data['input']['step'] > 1) {
                    redirect('rent/apply_rent');
                }//E# if statement
            }//E# if else statement

            if ($this->session->has_userdata('sessioned_sellers')) {
                $data['sessioned_sellers'] = $this->session->userdata('sessioned_sellers');
            } else {
                $data['sessioned_sellers'] = false;
            }//E# if else statement
            if ($this->session->userdata()) {
                $data['sessioned_user'] = $this->session->userdata();
            } else {
                $data['sessioned_user'] = false;
            }//E# if else statement
            //Payment
            if ($data['input']['step'] == 4) {

                $service_model = $this->db
                                ->select('*')
                                ->where('Slug', $this->service_slug)
                                ->get('lmais_landservicetypes')->row();

                $data['service_model'] = $service_model;
            }//E# if statement

            $this->load->view($Department . '/PageResources/addparcelsheader');
            $this->load->view($Department . '/Header/header');
            if ($Department == 'user') {
                
            } else {
                $this->load->view($Department . '/Menu/menu');
            }//E# if else statement


            $this->load->view('rent/rent', $data);
        }
    }

    public function rent_app() {
        $RentID = "CNT" . md5(time() . mt_rand(1, 1000000));
        $Proprietor_First_Name = $this->input->post("ProprietorFirstName");
        $Proprietor_Middle_Name = $this->input->post("ProprietorMiddleName");
        $Proprietor_Last_Name = $this->input->post("ProprietorLastName");
        $Proprietor_Nationality = $this->input->post("ProprietorNationality");
        $Proprietor_Idenification = $this->input->post("ProprietorIdenification");
        $Proprietor_Mobile_Number = $this->input->post("ProprietorMobileNumber");
        $Proprietor_Email_Address = $this->input->post("ProprietorEmailAddress");
        $Proprietor_Address = $this->input->post("ProprietorAddress");
        $Proposed_First_Name = $this->input->post("ProposedFirstName");
        $Proposed_Middle_Name = $this->input->post("ProposedMiddleName");
        $Proposed_Last_Name = $this->input->post("ProposedLastName");
        $Proposed_Nationality = $this->input->post("ProposedNationality");
        $Proposed_Identification = $this->input->post("ProposedIdentification");
        $Proposed_Mobile_Number = $this->input->post("ProposedMobileNumber");
        $Proposed_Email_Address = $this->input->post("ProposedEmailAddress");
        $Proposed_Address = $this->input->post("ProposedAddress");
        $Rent_Interest = $this->input->post("RentInterest");
        $Rent_Description = $this->input->post("RentDescription");
        $LRNumber = $this->input->post("LRNumber");
        $AreaSize = $this->input->post("AreaSize");
        $Registry = $this->input->post("Registry");
        $County = $this->input->post("County");
        $CreatedBy = $this->session->userdata('RowID');

        $data = array(
            'Registry' => $Registry,
            'LRNO' => $LRNO,
            'AreaSize' => $AreaSize,
            'AreaUnit' => $AreaUnit,
            'Tenure' => $Tenure,
            'RegisteredProprietor' => $RegisteredProprietor,
            'TypeOfDevelopment' => $TypeOfDevelopment,
            'NearbyInfrastructure' => $NearbyInfrastructure,
            'NearbyValue' => $NearbyValue,
            'TypeOfRoad' => $TypeOfRoad,
            'NameOfRoad' => $NameOfRoad,
            'DistanceFromRoad' => $DistanceFromRoad,
            'TypeOfTown' => $TypeOfTown,
            'NameOfTown' => $NameOfTown,
            'DistanceFromTown' => $DistanceFromTown,
            'CreatedBy' => $CreatedBy,
        );
        $usr_result = $this->RentModel->apply_rent($data);

        //check if username and password is correct
        if ($usr_result == TRUE) { //active user record is present
            $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                              <a href="#" class="close" data-dismiss="alert">&times;</a>
                             
                            <strong>Success!</strong> You have successfully Added new Parcel!</div>');

            redirect('start/addvaluationdata');
        } else if ($usr_result == FALSE) {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger fade in">
                             <a href="#" class="close" data-dismiss="alert">&times;</a>
                              <strong>Error!</strong> Data are not saved.. Try again</div>');

            redirect('start/addvaluationdata');
        }
    }

    public function save_advocate_details() {
        $advocate_data = array(
            'advoFName' => $this->input->post('advoFName'),
            'advoMName' => $this->input->post('advoMName'),
            'advoLName' => $this->input->post('advoLName'),
            'advoPinNumber' => $this->input->post('advoPinNumber'),
            'advoPostalAddress' => $this->input->post('advoPostalAddress'),
            'advoEmail' => $this->input->post('advoEmail'),
            'advoTelNumber' => $this->input->post('advoTelNumber'),
            'advoLawFirm' => $this->input->post('advoLawFirm'),
            'advoPFNumber' => $this->input->post('advoPFNumber'),
            'advoIDNumber' => $this->input->post('advoIDNumber'),
            'advoNationality' => $this->input->post('advoNationality'),
        );

        $sessioned_rent = $this->session->userdata('sessioned_rent');

        $this->db->where('RowID', $sessioned_rent['RowID']);

        $this->db->update('lmais_rent', $advocate_data);

        $this->session_rent($sessioned_rent['RowID']);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong> Advocates details saved successfully</div>');

        redirect('rent/apply_rent?step=5');
    }

    public function delete_seller() {
        $sellerID = $this->input->post('SellerID');

        $this->db->where('RowID', $sellerID);
        $this->db->delete('lmais_sellers');

        $sessioned_rent = $this->session->userdata('sessioned_rent');

        $this->session_rent($sessioned_rent['RowID'], true);

        $this->session->set_flashdata('msg', '<div class="alert alert-success fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                         
                        <strong>Success!</strong>Rent Applicant\'s details deleted successfully</div>');

        echo json_encode(array(
            'type' => 'success',
            'link' => site_url() . '/rent/apply_rent?step=2'
        ));
    }

    public function review_application() {

        $this->payment();

        redirect('rent/apply_rent?step=4');
    }

//E# delete_seller() function

    function GetRegistriesMatch($ActID, $CountyID) {
        //$ActID = $this->uri->segment(3);
        //$CountyID = $this->uri->segment(4);
        $Registries = $this->Registrymodel->GetMatchedRegistries($ActID, $CountyID);

        return $Registries;
    }

    public function clear_all_cache() {

        $CI = & get_instance();
        $path = $CI->config->item('cache_path');

        $cache_path = ($path == '') ? APPPATH . 'cache/' : $path;

        $handle = opendir($cache_path);
        while (($file = readdir($handle)) !== FALSE) {
            //Leave the directory protection alone
            if ($file != '.htaccess' && $file != 'index.html') {
                @unlink($cache_path . '/' . $file);
            }
        }
        closedir($handle);


        if ($handle = opendir($cache_path)) {
            //echo "Directory handle: $handle <br /><br/>";

            while (false !== ($entry = readdir($handle))) {
                //echo $entry."<br />";
                $n = basename($entry);
                //echo "name = ".$n."<br />";  
                //echo "length of name = ".strlen($n)."<br />";
                if (strlen($n) >= 32) {
                    //echo "file name's 32 chars long <br />";
                    $p = APPPATH . 'cache/' . $entry;
                    //echo $p;                  
                    unlink($p);
                }
                //echo "<br />";
            }
            closedir($handle);
        }
    }

}
