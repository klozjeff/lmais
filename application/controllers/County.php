<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class County extends CI_Controller {

    function __construct() {
		
		
        parent::__construct();
		$this->load->model('Settings_Model','',TRUE);
		$this->load->model('Registrymodel','',TRUE);
		$this->load->model('Countymodel','',TRUE);
        //$this->access_only_team_members();
    }

    //only admin can change other user's info
    //none admin users can only change his/her own info
    function only_admin_or_own($user_id) {
        if ($user_id && ($this->login_user->is_admin || $this->login_user->id === $user_id)) {
            return true;
        } else {
            redirect("forbidden");
        }
    }

	
    public function index() {
			redirect('Registry/County');
    }
	
	public function County() {
									$data['Results'] = $this->Registrymodel->GetAllRegistries();
								
									$this->load->view('Valuation/PageResources/usersheader');
									$this->load->view('Valuation/Header/header');
									$this->load->view('Valuation/Menu/menu');
									 $this->load->view('Registries/index', $data);
    }

   

    public function AddCounty()
				{
		
		
		if($this->session->userdata('Logged_in')!='')
							{

						$CreatedBy = $this->session->userdata('RowID');

							}
							else
		$CreatedBy = "0";
       	$Name = $this->input->post("Name");
		$County = $this->input->post("County");
		
		$RegistryID = $this->input->post("RegistryID");
		$DigitizationStatus = $this->input->post("DigitizationStatus");
		
					$data=array(
								'Name'=>$Name,
								'County'=>$County,
								'CreatedBy'=>$CreatedBy,
								'RegistryID'=>$RegistryID,
								'DigitizationStatus'=>$DigitizationStatus,
								
								);
								$usr_result = $this->Registrymodel->SaveRegistry($data);
						
				if ($usr_result ==TRUE) //active user record is present
				{
	$this->session->set_flashdata('registrymsg', '<div class="alert alert-info fade in">
<a href="#" class="close" data-dismiss="alert">&times;</a>
<strong>Success!</strong>
<br>Registry was added successfully. </strong>
</div>');

				 	redirect('Registry/RegistryManagement');
				   }
				   			
				
				else
				{  
				 
					$this->session->set_flashdata('registrymsg', '<div class="alert alert-info fade in">
<a href="#" class="close" data-dismiss="alert">&times;</a>
			  	<strong>Error!</strong> Registry not created. That Registry Name already exist ! </div>');
					
						redirect('Registry/RegistryManagement');
				}
				
				
				}



}

/* End of file team_member.php */
/* Location: ./application/controllers/team_member.php */