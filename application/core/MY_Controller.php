<?php

/* @property mpdf_model $mpdf_model */

class My_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('InvoiceModel');
        $this->load->model('proprietorModel');
        $this->load->model('encumbranceModel');
        $this->load->model('PropertyModel');
    }

    function invoice($transaction_id = null, $slug = null, $user_id = null, $output = null) {
        if ($transaction_id == null || $slug == null || $user_id == null) {
            return;
        } else
            $data['UserData'] = $this->InvoiceModel->get_user($user_id);
        $data['InvoiceData'] = $this->InvoiceModel->get_invoices($transaction_id, $slug);
        $data['PaymentData'] = $this->InvoiceModel->get_payment($transaction_id, $slug);
        $data['TransactionData'] = $this->InvoiceModel->get_transaction($transaction_id);
        $data['ServiceData'] = $this->InvoiceModel->get_servicedata($transaction_id, $slug);
        ini_set('memory_limit', '256M');
        // load library
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        // retrieve data from model
        // $data['news'] = $this->mpdf_model->get_news();
        // $data['title'] = "items";
        // boost the memory limit if it's low ;)
        $stylesheet = file_get_contents(FCPATH . '/assets/bootstrap.min.css');
        $html = $this->load->view('printing/invoice', $data, true);


        $pdf->AddPage('P', // L - landscape, P - portrait
                '', '', '', '', 10, // margin_left
                10, // margin right
                10, // margin top
                10, // margin bottom
                5, // margin header
                10); // margin footer


        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html, 2);
        $output = 'INV-' . $slug . '-' . $transaction_id . '.pdf';

        if ($output) {
            $path = FCPATH . '/assets/generated_pdfs/' . $output;
            // die($path);
            $pdf->Output($path, 'F');
        } else {
            $pdf->Output("$output", 'I');
        }//E# if else statement
        var_dump($path);
        return $path;
    }

    function land_rent($transaction_id, $output = null) {

        // load library
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        // retrieve data from model
        // $data['news'] = $this->mpdf_model->get_news();
        // $data['title'] = "items";
        // boost the memory limit if it's low ;)
        $rent_model = $this->db
                        ->select('*')
                        ->where('TransactionID', $transaction_id)
                        ->get('lmais_rent')->row();

        $data['rent_model'] = $rent_model;

        $stylesheet = file_get_contents(FCPATH . '/assets/bootstrap.min.css');
        $html = $this->load->view('printing/landrent', $data, true);
        $pdf->AddPage('P', // L - landscape, P - portrait
                '', '', '', '', 20, // margin_left
                20, // margin right
                20, // margin top
                10, // margin bottom
                5, // margin header
                10); // margin footer
        // render the view into HTML
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html, 2);
        // write the HTML into the PDF
        $output = 'LRT-' . $transaction_id . '.pdf';

        if ($output) {
            $path = FCPATH . '/assets/generated_pdfs/' . $output;
            // die($path);
            $pdf->Output($path, 'F');
        } else {
            $pdf->Output("$output", 'I');
        }//E# if else statement

        return $path;
    }

    function caution($transaction_id, $output = null) {

        // load library
        $this->load->library('pdf');
        $pdf = $this->pdf->load();

        if ($transaction_id == null) {
            return;
        } else
            $sql = "SELECT * FROM lmais_sellers where TransactionID = '$transaction_id'";
        $query = $this->db->query($sql);
        $applicants_model = $query->result();

        //var_dump($applicants_model);
        //return;
        $data['ApplicantsData'] = $applicants_model;

        $sql = "SELECT * FROM lmais_caution where TransactionID = '$transaction_id'";
        $query = $this->db->query($sql);
        $transaction_data = $query->result();

        $data['TransactionData'] = $transaction_data;


        $stylesheet = file_get_contents(FCPATH . '/assets/bootstrap.min.css');
        $html = $this->load->view('printing/caution', $data, true);


        $pdf->AddPage('P', // L - landscape, P - portrait
                '', '', '', '', 20, // margin_left
                20, // margin right
                10, // margin top
                10, // margin bottom
                5, // margin header
                10); // margin footer
        // render the view into HTML
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html, 2);
        // write the HTML into the PDF
        $output = 'CTN-' . $transaction_id . '.pdf';
        if ($output) {
            $path = FCPATH . '/assets/generated_pdfs/' . $output;
            // die($path);
            $pdf->Output($path, 'F');
        } else {
            $pdf->Output("$output", 'I');
        }//E# if else statement

        return $path;
    }

    function search($transaction_id, $output = null) {

        // load library
        $this->load->library('pdf');
        $pdf = $this->pdf->load();

        if ($transaction_id == null) {
            return;
        } else
            $sql = "SELECT * FROM lmais_search where TransactionID = '$transaction_id'";
        $query = $this->db->query($sql);
        $transaction_data = $query->result();

        $data['TransactionData'] = $transaction_data;



        $stylesheet = file_get_contents(FCPATH . '/assets/bootstrap.min.css');
        $html = $this->load->view('printing/search', $data, true);
        $pdf->AddPage('P', // L - landscape, P - portrait
                '', '', '', '', 20, // margin_left
                20, // margin right
                10, // margin top
                10, // margin bottom
                5, // margin header
                10); // margin footer
        // render the view into HTML
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html, 2);
        // write the HTML into the PDF
        $output = 'AOS-' . $transaction_id . '.pdf';

        if ($output) {
            $path = FCPATH . '/assets/generated_pdfs/' . $output;
            // die($path);
            $pdf->Output($path, 'F');
        } else {
            $pdf->Output("$output", 'I');
        }//E# if else statement

        return $path;
    }

    function charge($transaction_id, $output = null) {

        // load library
        $this->load->library('pdf');
        $pdf = $this->pdf->load();

        if ($transaction_id == null) {
            return;
        } else
            $sql = "SELECT * FROM lmais_sellers where TransactionID = '$transaction_id'";
        $query = $this->db->query($sql);
        $applicants_model = $query->result();

        //var_dump($applicants_model);
        //return;
        $data['ApplicantsData'] = $applicants_model;

        $sql = "SELECT * FROM lmais_charge where TransactionID = '$transaction_id'";
        $query = $this->db->query($sql);
        $transaction_data = $query->result();

        $data['TransactionData'] = $transaction_data;


        $stylesheet = file_get_contents(FCPATH . '/assets/bootstrap.min.css');
        $html = $this->load->view('printing/charge', $data, true);


        $pdf->AddPage('P', // L - landscape, P - portrait
                '', '', '', '', 20, // margin_left
                20, // margin right
                10, // margin top
                10, // margin bottom
                5, // margin header
                10); // margin footer
        // render the view into HTML
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html, 2);
        // write the HTML into the PDF
        $output = 'CGR-' . $transaction_id . '.pdf';

        if ($output) {
            $path = FCPATH . '/assets/generated_pdfs/' . $output;
            // die($path);
            $pdf->Output($path, 'F');
        } else {
            $pdf->Output("$output", 'I');
        }//E# if else statement

        return $path;
    }

    function withdrawal_of_caution($transaction_id, $output = null) {

        // load library
        $this->load->library('pdf');
        $pdf = $this->pdf->load();

        if ($transaction_id == null) {
            return;
        } else
            $sql = "SELECT * FROM lmais_sellers where TransactionID = '$transaction_id'";
        $query = $this->db->query($sql);
        $applicants_model = $query->result();

        //var_dump($applicants_model);
        //return;
        $data['ApplicantsData'] = $applicants_model;

        $sql = "SELECT * FROM lmais_withdrawalofcaution where TransactionID = '$transaction_id'";
        $query = $this->db->query($sql);
        $transaction_data = $query->result();

        $data['TransactionData'] = $transaction_data;


        $stylesheet = file_get_contents(FCPATH . '/assets/bootstrap.min.css');
        $html = $this->load->view('printing/withdrawal_of_caution', $data, true);


        $pdf->AddPage('P', // L - landscape, P - portrait
                '', '', '', '', 20, // margin_left
                20, // margin right
                10, // margin top
                10, // margin bottom
                5, // margin header
                10); // margin footer
        // render the view into HTML
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html, 2);
        // write the HTML into the PDF
        $output = 'WCTN-' . $transaction_id . '.pdf';

        if ($output) {
            $path = FCPATH . '/assets/generated_pdfs/' . $output;
            // die($path);
            $pdf->Output($path, 'F');
        } else {
            $pdf->Output("$output", 'I');
        }//E# if else statement

        return $path;
    }

}
