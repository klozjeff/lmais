<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class encumbranceModel extends CI_Model {

    

    function ReturnResultsCount($DocID) {
        if ($DocID) {


            $this->db->where('DOCID', $DocID);
            $q = $this->db->get('lmais_encumbrance');
            return $q->num_rows();
        } 
    }

    function Currentencumbrance($DocID) {
        if ($DocID) {
			
			$this -> db -> select('NatureOfEncumbrance');
   $this -> db -> from('lmais_encumbrance');
   $this -> db -> where('DocID', $DocID);
   $this -> db -> order_by("entryno", "desc");
   $this -> db -> limit(1);
   
       $query = $this -> db -> get();
	   
            return $query->row()->NatureOfEncumbrance;
        } 
          
    }
	
	function CurrentFurtherParticulars($DocID) {
        if ($DocID) {
			
			$this -> db -> select('FurtherParticulars');
   $this -> db -> from('lmais_encumbrance');
   $this -> db -> where('DocID', $DocID);
   $this -> db -> order_by("entryno", "desc");
   $this -> db -> limit(1);
   
       $query = $this -> db -> get();
	   
            return $query->row()->FurtherParticulars;
        } 
          
    }

	
	
	function GetencumbranceData($DocID) {
        if ($DocID) {
            $query = $this->db->get_where('lmais_encumbrance', array('DocID' => $DocID));

            return $query->result();
        } 
          
    }
	
	 

    /*     * Added functions, original lmais above
     * the code below represents functionality added by
     * The Royal King Kabs
     */
}
