<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ParcelsValuationModel extends CI_Model {

    function addnewparcel($data) {
// Query to insert data in database
        //$regnumber = $data['regnumber'];

        $this->db->insert('lmais_dataentrypropertyvaluation', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {

            return false;
        }
    }

    function editparcel($RowID, $data) {

        $this->db->where('RowID', $RowID);
        $this->db->update('lmais_dataentrypropertyvaluation', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {

            return false;
        }
    }

    function GetAreaUnit($ItemType) {
        $query = $this->db->get_where('lmais_listitems', array('ItemType' => $ItemType));
        return $query->result();
    }

    function GetRegistries() {
        $query = $this->db->get_where('lmais_registry');
        return $query->result();
    }

    function GetPendingValuations() {
        //$query = $this->db->get_where('lmais_registry');
        $q = $this->db->query('SELECT * FROM lmais_receivedvaluations where TransactionID in (SELECT TransactionID FROM lmais_transfer where PaymentStatus = 1 and completionstatus = 1)');
        return $q->num_rows();
    }

    function GetUnassignedValuations() {

        //$query = $this->db->get_where('lmais_registry');
        $query = $this->db->query('select r.Title, r.AreaSize, r.NatureOfTitle, r.DocID, (select rr.TransactionID from lmais_request rr where rr.DocID = r.DocID) as TransactionID,(select rr.DateCreated from lmais_receivedrequests rr where rr.DocID = r.DocID) as DateCreated, (select s.FirstName from lmais_sellers s where s.DocID = r.DocID) as FirstName, (select s.MiddleName from lmais_sellers s where s.DocID = r.DocID) as MiddleName, (select s.LastName from lmais_sellers s where s.DocID = r.DocID) as LastName from lmais_rla r where docid in  (select docid from lmais_receivedrequests rr where transactionid in (SELECT transactionid FROM lmais_receivedvaluations rv where TransactionID in (SELECT TransactionID FROM lmais_receivedconsents rc where PaymentStatus = 1 and completionstatus = 1) and LockedBy is null and LockStatus = 0))');
        return $query->result();
    }

    /*     * Added functions, original lmais above
     * the code below represents functionality added by
     * The Royal King Kabs
     */

    function GetRoadTypes() {
        $query = $this->db->get('lmais_roadtypes');
        return $query->result();
    }

    function GetRoadNameMatchRoadType($roadTypeID) {
        $query = $this->db->get_where('lmais_roadtypes', array('RowID' => $roadTypeID));
        return json_encode($query->result());
    }

    function GetTownTypes() {
        $query = $this->db->get('lmais_towntypes');
        return $query->result();
    }

    function GetTownNamesMatchTownType($townTypeID) {
        $query = $this->db->get_where('lmais_towntypes', array('RowID' => $townTypeID));
        return json_encode($query->result());
    }

}
