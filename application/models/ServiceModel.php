<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceModel extends CI_Model {

function __construct() {
        $this->table = 'lmais_landservicetypes';
        parent::__construct($this->table);
    }


	function GetServices($RowID)
{
   $this -> db -> select('*');
   $this -> db -> from('lmais_landservicetypes');
   $this -> db -> where('RowID', $RowID);
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows() > 0)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
}


	function GetMatchedRegistries($ActID, $CountyID)
{
  
   $this -> db -> select("Name from lmais_landservicetypes where County='$CountyID' and Acts = '$ActID'");
   $query = $this -> db -> get();
 return $query->row()->Name;
}

	function GetAlllandservicetypes(){
	
$query = $this->db->get_where('lmais_landservicetypes');
	
	return $query->result();		
}

	

}