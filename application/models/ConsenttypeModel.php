<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ConsenttypeModel extends CI_Model {

    function ReturnResultsCount($DocID) {
        if ($DocID) {


            $this->db->where('DOCID', $DocID);
            $q = $this->db->get('lmais_consenttype');
            return $q->num_rows();
        }
    }

    function GetConsent($DocID) {
        if ($DocID) {

            $this->db->select('RegisteredProprietor');
            $this->db->from('lmais_consenttype');
            $this->db->where('DocID', $DocID);
            $this->db->order_by("entryno", "desc");
            $this->db->limit(1);

            $query = $this->db->get();

            return $query->row()->RegisteredProprietor;
        }
    }

    function GetAllConsent() {

        $this->db->select('*');
        $this->db->from('lmais_consenttype');

        $query = $this->db->get();

        return $query->result();
    }

}
