<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class proprietorModel extends CI_Model {

    

	
    function ReturnResultsCount($DocID) {
        if ($DocID) {


            $this->db->where('DOCID', $DocID);
            $q = $this->db->get('lmais_proprietorship');
            return $q->num_rows();
        } 
    }

    function CurrentProprietor($DocID) {
        if ($DocID) {
			
			$this -> db -> select('RegisteredProprietor');
   $this -> db -> from('lmais_proprietorship');
   $this -> db -> where('DocID', $DocID);
   $this -> db -> order_by("entryno", "desc");
   $this -> db -> limit(1);
   
       $query = $this -> db -> get();
	   
            return $query->row()->RegisteredProprietor;
        } 
          
    }
	
	function CurrentProprietorID($DocID) {
        if ($DocID) {
			
			$this -> db -> select('RegisteredID');
   $this -> db -> from('lmais_proprietorship');
   $this -> db -> where('DocID', $DocID);
   $this -> db -> order_by("entryno", "desc");
  
   
       $query = $this -> db -> get();
	   
            return $query->row()->RegisteredID;
        } 
          
    }
	
	
	function GetProprietorData($DocID) {
        if ($DocID) {
            $query = $this->db->get_where('lmais_proprietorship', array('DocID' => $DocID));

            return $query->result();
        } 
          
    }
	
	 

    /*     * Added functions, original lmais above
     * the code below represents functionality added by
     * The Royal King Kabs
     */
}
