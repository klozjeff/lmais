<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CountyModel extends CI_Model {

    function __construct() {
        $this->table = 'lmais_county';
        parent::__construct($this->table);
    }

    function GetCounty($RowID) {
        $this->db->select('*');
        $this->db->from('lmais_county');
        $this->db->where('RowID', $RowID);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    function SaveCounty($data) {// Query to insert data in database
        $IDNumber = $data['Name'];
        $this->db->select("* from lmais_county where Name='$Name' ");
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            $this->db->insert('lmais_county', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }
        } else {
            return false;
        }
    }

    function GetAllCounties() {

        $query = $this->db->get_where('lmais_county');

        return $query->result();
    }

}
