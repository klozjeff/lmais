<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BuyerModel extends CI_Model {

    function __construct() {
        $this->table = 'lmais_Buyers';
        parent::__construct($this->table);
    }

    function GetBuyers($transfer_id) {
        $this->db->select('*');
        $this->db->from('lmais_buyers');
        $this->db->where('TransactionID', $transfer_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    function GetMatchedRegistries($ActID, $CountyID) {

        $this->db->select("Name from lmais_Buyers where County='$CountyID' and Acts = '$ActID'");
        $query = $this->db->get();
        return $query->row()->Name;
    }

    function GetAllBuyers() {

        $query = $this->db->get_where('lmais_Buyers');

        return $query->result();
    }

}
