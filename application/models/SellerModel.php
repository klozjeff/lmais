<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SellerModel extends CI_Model {

function __construct() {
        $this->table = 'lmais_Sellers';
        parent::__construct($this->table);
    }


	function GetSellers($transfer_id)
{
   $this -> db -> select('*');
   $this -> db -> from('lmais_sellers');
   $this -> db -> where('TransactionID', $transfer_id);
   $this -> db -> limit(10);
   $query = $this -> db -> get();
   if($query -> num_rows() > 0)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
}


	function GetMatchedRegistries($ActID, $CountyID)
{
  
   $this -> db -> select("Name from lmais_Sellers where County='$CountyID' and Acts = '$ActID'");
   $query = $this -> db -> get();
 return $query->row()->Name;
}

	function GetAllSellers(){
	
$query = $this->db->get_where('lmais_Sellers');
	
	return $query->result();		
}

	

}