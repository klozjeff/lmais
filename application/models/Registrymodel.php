<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrymodel extends CI_Model {

function __construct() {
        $this->table = 'lmais_registry';
        parent::__construct($this->table);
    }


	function GetRegistry($RowID)
{
   $this -> db -> select('*');
   $this -> db -> from('lmais_registry');
   $this -> db -> where('RowID', $RowID);
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
}


	function GetMatchedRegistries($ActID, $CountyID)
{
  
   $this -> db -> select("Name from lmais_registry where County='$CountyID' and Acts = '$ActID'");
   $query = $this -> db -> get();
 return $query->row()->Name;
}

	function GetAllRegistries(){
	
$query = $this->db->get_where('lmais_registry');
	
	return $query->result();		
}

function GetRegistrationSections($RegistryID){
	$this->db->where('RegistryID', $RegistryID);
    $query = $this->db->get('lmais_registrationsections');
	return $query->result();		
}

function GetRegistrationSections2(){
    $query = $this->db->get_where('lmais_registrationsections');
	return $query->result();		
}

function SaveData($table,$data)
{
	    $RegistryID = $data['RegistryID'];
	    $Name=$data['Name'];
        $this->db->select("* from $table where RegistryID='$RegistryID' AND Name='$Name'");
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {

            $this->db->insert($table, $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }
        } else {

            return false;
        }
	
}

function GetSectionsCount($RegistryID){
	if($RegistryID)
	{
		 $this->db->where('RegistryID', $RegistryID);
            $q = $this->db->get('lmais_registrationsections');
            return $q->num_rows();
	}
	
}

}