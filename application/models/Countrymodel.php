<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CountryModel extends CI_Model {

    function __construct() {
        $this->table = 'lmais_country';
        parent::__construct($this->table);
    }

    function GetCountry($RowID) {
        $this->db->select('*');
        $this->db->from('lmais_country');
        $this->db->where('RowID', $RowID);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    function SaveCountry($data) {// Query to insert data in database
        $IDNumber = $data['Name'];
        $this->db->select("* from lmais_country where Name='$Name' ");
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            $this->db->insert('lmais_country', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }
        } else {
            return false;
        }
    }

    function GetAllCountries() {

        $query = $this->db->get_where('lmais_country');

        return $query->result();
    }

}
