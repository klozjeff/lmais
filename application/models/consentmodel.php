<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConsentModel extends CI_Model {

function __construct() {
        $this->table = 'LMAIS_users';
        parent::__construct($this->table);
    }


	function GetTask($ID)

{

   if ($ID)
   {
	   
   }
   Else
	   
   $query = $this->db->get_where('lmais_transfer');
	
	return $query->result();	

}

function GetAct($DocID) {
        if ($DocID) {
			
			$this -> db -> select('RegisteredProprietor');
   $this -> db -> from('lmais_acts');
   $this -> db -> where('DocID', $DocID);
   $this -> db -> order_by("entryno", "desc");
   $this -> db -> limit(1);
   
       $query = $this -> db -> get();
	   
            return $query->row()->RegisteredProprietor;
        } 
}
		
function EditTask($TransactionID, $data) {

        $this->db->where('TransactionID', $TransactionID);
        $this->db->update('lmais_transfer', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {

            return false;
        }
    }
	
function GetMyTask($userID)

{
$this -> db -> select('*');
		   $this -> db -> from('lmais_transfer');
		   $this -> db -> where('LockedBy', $userID);
		   $query = $this -> db -> get();
		   if($query -> num_rows() > 0)
		   {
			 return $query->result();
		   }
		   else
		   {
			 return false;
		   }

}

function GetSeller($TransactionID)

{
   $this -> db -> select('*');
   $this -> db -> from('lmais_sellers');
   $this -> db -> where('TransactionID', $TransactionID);
   $this -> db -> limit(1);
  $query = $this -> db -> get();
if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
  {
     return false;
   }
}	
function GetBuyer($TransactionID)
{
   $this -> db -> select('*');
   $this -> db -> from('lmais_buyers');
   $this -> db -> where('TransactionID', $TransactionID);
   $this -> db -> limit(1);
  $query = $this -> db -> get();
if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
  {
     return false;
   }
}	

function GetLRNumber($TransactionID)

{
   $this -> db -> select("* from lmais_rla where docid in (select docid from lmais_receivedrequests where TransactionID='$TransactionID' )");
   $this -> db -> limit(1);
  $query = $this -> db -> get();
if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
  {
     return false;
   }
}	   

function detailed_view($TransactionID) {
        if ($TransactionID) {
			 $this -> db -> select("* from lmais_rla where docid in (select docid from lmais_receivedrequests where TransactionID='$TransactionID' )");
             $query = $this -> db -> get();

            return $query->result();
        } else
            return false;
    }
	
	
	
	function consent_details($TransactionID) {
        if ($TransactionID) {
			 $this -> db -> select("*  from lmais_transfer where TransactionID='$TransactionID'");
             $query = $this -> db -> get();

            return $query->result();
        } else
            return false;
    }
	
	
	
	function GetRent($TransactionID) {
        if ($TransactionID) {
			 $this -> db -> select("*  from lmais_rent where LRNumber in (select LRNumber from lmais_transfer where TransactionID='$TransactionID')");
             $query = $this -> db -> get();

            return $query->result();
        } else
            return false;
    }
	
	function GetInvoice($TransactionID) {
        if ($TransactionID) {
			 $this -> db -> select("* from lmais_invoice where TransactionID ='$TransactionID'");
             $query = $this -> db -> get();

            return $query->result();
        } else
            return false;
    }

function registerusers($data)
{// Query to insert data in database
  $IDNumber = $data['IDNumber'];
   
   $this -> db -> select("* from lmais_users where IDNumber='$IDNumber' ");
  $this -> db -> limit(1);
   $query = $this -> db -> get();
  if($query -> num_rows() == 0)

   {

    $this->db->insert('lmais_users', $data);
  if ($this->db->affected_rows() > 0) 
      {
  return true;

      }

   }

   else

   {

     return false;

   }
}


function is_email_exists($email, $id = 0) {
	
	$result = $this->db->get_where('lmais_users', array('Email' => $email, 'deleted' =>'0'));
        if ($result->num_rows() && $result->row()->RowID != $id) {
            return $result->row();
        } else {
            return false;
        }
    }

	
	function GetAllUsers(){
	
$query = $this->db->get_where('lmais_users');
	
	return $query->result();		
}

}