<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usermodel extends CI_Model {

    function __construct() {
        $this->table = 'LMAIS_users';
        parent::__construct($this->table);
    }

    /**
     * S# getUserByField() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Get uset by field
     * 
     * @param str $field Field
     * @param str $value Value
     * 
     * @return mixed Model if found, false otherwise
     */
    function getUserByField($field, $value) {

        $user_model = $this->db
                        ->select('*')
                        ->where($field, $value)
                        ->get('lmais_users')->row();

        return $user_model ? $user_model : false;
    }

//E# getUserByField() function

    /**
     * S# generateSmsOtp() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Generate unique SMS One time password
     * 
     * @param str $type Type eg numeric, alphanum
     * @param int $length Length
     * 
     * @return str OTP
     */
    function generateSmsOtp($type, $length) {
        $this->load->helper('string');

        //Start with not unique
        $notUnique = true;

        while ($notUnique) {//While till you get the field value is unique
            //Generate value
            $value = strtoupper(random_string($type, $length));

            $query = $this->db->select('*')
                    ->from('lmais_users')
                    ->where('SmsOtp', $value)
                    ->limit(1)
                    ->get();

            if ($query->num_rows() != 1) {
                break;
            }//E# if statement
        }//E# while statement

        return $value;
    }

//E# generateSmsOtp() function

    /**
     * S# updateSmsOtp() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Update SMS one time password
     * 
     * @param int $user_id User id
     * @param str $otp One time password
     * 
     */
    function updateSmsOtp($user_id, $otp) {
        $user_data = array(
            'SmsOtp' => $otp,
            'SmsOtpTime' => date('Y-m-d H:i:s'),
        );

        $this->db->where('RowID', $user_id)->update('lmais_users', $user_data);
    }

//E# updateSmsOtp() function

    /**
     * S# otpCheck() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Check if One time password is valid
     * 
     * @param str $otp One time password
     * 
     * @return mixed user model if true and otherwise false
     */
    function otpCheck($otp) {
        $user_model = $this->db->select('*')
                ->from('lmais_users')
                ->where('SmsOtp', $otp)
                ->get()
                ->row();

        if ($user_model) {
            return $user_model;
        }//E# if statement
        return false;
    }

//E# otpCheck() function

    /**
     * S# clearOtp() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Clear SMS one time password
     * 
     * @param int $user_id User id
     */
    function clearOtp($user_id) {
        $user_model = $this->db->select('*')
                ->from('lmais_users')
                ->where('SmsOtp', $otp)
                ->get()
                ->row();

        if ($user_model) {
            return $user_model;
        }//E# if statement
        return false;
    }

//E# clearOtp() function

    function getusers($IDNumber) {

        $this->db->select('*');

        $this->db->from('lmais_users');

        $this->db->where('IDNumber', $IDNumber);

        //$this -> db -> where('password', ($password));

        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {

            return $query->result();
        } else {

            return false;
        }
    }

    function registerusers($data) {// Query to insert data in database
        $IDNumber = $data['IDNumber'];

        $this->db->select("* from lmais_users where IDNumber='$IDNumber' ");
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {

            $this->db->insert('lmais_users', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }
        } else {

            return false;
        }
    }

    function is_email_exists($email, $id = 0) {

        $result = $this->db->get_where('lmais_users', array('Email' => $email, 'deleted' => '0'));
        if ($result->num_rows() && $result->row()->RowID != $id) {
            return $result->row();
        } else {
            return false;
        }
    }

    function GetAllUsers() {

        $query = $this->db->get_where('lmais_users');

        return $query->result();
    }

}
