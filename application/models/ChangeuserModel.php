<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ChangeuserModel extends CI_Model {

    function ReturnResultsCount($DocID) {
        if ($DocID) {


            $this->db->where('DOCID', $DocID);
            $q = $this->db->get('lmais_changeofusertype');
            return $q->num_rows();
        }
    }

    function GetType($DocID) {
        if ($DocID) {

            $this->db->select('RegisteredProprietor');
            $this->db->from('lmais_changeofusertype');
            $this->db->where('DocID', $DocID);
            $this->db->order_by("entryno", "desc");
            $this->db->limit(1);

            $query = $this->db->get();

            return $query->row()->RegisteredProprietor;
        }
    }

    function GetAllTypes() {
        $this->db->select('*');
        $this->db->from('lmais_listitems');

        $query = $this->db->get();

        return $query->result();
    }

}
