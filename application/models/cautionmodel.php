<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CautionModel extends CI_Model {

function __construct() {
        $this->table = 'LMAIS_users';
        parent::__construct($this->table);
    }


	function GetTask($ID)

{

   if ($ID)
   {
	   
   }
   Else
	   
   $query = $this->db->get_where('lmais_caution');
	
	return $query->result();	

}



function GetAct($DocID) {
        if ($DocID) {
			
			$this -> db -> select('RegisteredProprietor');
   $this -> db -> from('lmais_acts');
   $this -> db -> where('DocID', $DocID);
   $this -> db -> order_by("entryno", "desc");
   $this -> db -> limit(1);
   
       $query = $this -> db -> get();
	   
            return $query->row()->RegisteredProprietor;
        } 
}
		
function EditTask($TransactionID, $data) {

        $this->db->where('TransactionID', $TransactionID);
        $this->db->update('lmais_caution', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {

            return false;
        }
    }
	
function GetMyTask($userID)

{
$this -> db -> select('*');
		   $this -> db -> from('lmais_caution');
		   $this -> db -> where('LockedBy', $userID);
		   $query = $this -> db -> get();
		   if($query -> num_rows() > 0)
		   {
			 return $query->result();
		   }
		   else
		   {
			 return false;
		   }

}

function GetSeller($TransactionID)

{
   $this -> db -> select('*');
   $this -> db -> from('lmais_sellers');
   $this -> db -> where('TransactionID', $TransactionID);
   $this -> db -> limit(1);
  $query = $this -> db -> get();
if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
  {
     return false;
   }
}	


function GetLRNumber($TransactionID)

{
   $this -> db -> select("* from lmais_rla where docid in (select docid from lmais_receivedrequests where TransactionID='$TransactionID' )");
   $this -> db -> limit(1);
  $query = $this -> db -> get();
if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
  {
     return false;
   }
}	   

function detailed_view($TransactionID) {
        if ($TransactionID) {
			 $this -> db -> select("* from lmais_rla where docid in (select docid from lmais_receivedrequests where TransactionID='$TransactionID' )");
             $query = $this -> db -> get();

            return $query->result();
        } else
            return false;
    }
	
	
	
	function caution_details($TransactionID) {
        if ($TransactionID) {
			 $this -> db -> select("*  from lmais_caution where TransactionID='$TransactionID'");
             $query = $this -> db -> get();

            return $query->result();
        } else
            return false;
    }
	
	
	
	function GetRent($TransactionID) {
        if ($TransactionID) {
			 $this -> db -> select("*  from lmais_rent where LRNumber in (select LRNumber from lmais_caution where TransactionID='$TransactionID')");
             $query = $this -> db -> get();

            return $query->result();
        } else
            return false;
    }
	
	function GetInvoice($TransactionID) {
        if ($TransactionID) {
			 $this -> db -> select("* from lmais_invoice where TransactionID ='$TransactionID'");
             $query = $this -> db -> get();

            return $query->result();
        } else
            return false;
    }


	
}