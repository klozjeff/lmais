<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class InvoiceModel extends CI_Model {

    function __construct() {
        $this->table = 'lmais_transactions';
        parent::__construct($this->table);
    }

    function get_payment($transaction_id, $slug) {
        $this->db->select('*');
        $this->db->from('lmais_transactions');
        $this->db->where('TransactionID', $transaction_id);
        $this->db->where('ServiceSlug', $slug);
        $this->db->where('Type', 'payment');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function get_user($user_id) {
        $this->db->select('*');
        $this->db->from('lmais_users');
        $this->db->where('RowID', $user_id);


        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function get_invoices($transaction_id, $slug) {
        $this->db->select('*');
        $this->db->from('lmais_transactions');
        $this->db->where('TransactionID', $transaction_id);
        $this->db->where('ServiceSlug', $slug);
        $this->db->where('Type', 'invoice');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function get_transaction($transaction_id) {
        $this->db->select('*');
        $this->db->from('lmais_receivedrequests');
        $this->db->where('TransactionID', $transaction_id);
        // $this -> db -> where('ServiceSlug', $slug);
        // $this -> db -> where('Type', 'payment');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function get_servicedata($transaction_id, $slug) {
		if($slug=='withdrawalofcaution'){
						$DBView='caution';
					}
					 else{
						$DBView=$slug;
					 }
        $this->db->select('*');
        $this->db->from('lmais_' . $DBView);
        $this->db->where('TransactionID', $transaction_id);
        // $this -> db -> where('ServiceSlug', $slug);
        // $this -> db -> where('Type', 'payment');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}
