<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TenureModel extends CI_Model {

    function ReturnResultsCount($DocID) {
        if ($DocID) {


            $this->db->where('DOCID', $DocID);
            $q = $this->db->get('lmais_tenure');
            return $q->num_rows();
        }
    }

    function GetTenure($DocID) {
        if ($DocID) {

            $this->db->select('RegisteredProprietor');
            $this->db->from('lmais_tenure');
            $this->db->where('DocID', $DocID);
            $this->db->order_by("entryno", "desc");
            $this->db->limit(1);

            $query = $this->db->get();

            return $query->row()->RegisteredProprietor;
        }
    }

    function GetAllTenure() {

        $this->db->select('*');
        $this->db->from('lmais_tenure');

        $query = $this->db->get();

        return $query->result();
    }

}
