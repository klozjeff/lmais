<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SearchResultsModel extends CI_Model {

    function SearchResults($LRNO) {
        if ($LRNO) {
            $query = $this->db->get_where('lmais_dataentrypropertyvaluation', array('LRNO' => $LRNO));

            return $query->result();
        } else
            $query = $this->db->get_where('lmais_dataentrypropertyvaluation');

        return $query->result();
    }
	
	function SearchResultsd($LRNO) {
        if ($LRNO) {
            $query = $this->db->get_where('lmais_rla', array('titleno' => $LRNO));

            return $query->result();
        } else
            $query = $this->db->get_where('lmais_rla');

        return $query->result();
    }

    function ReturnResultsCount($LRNO) {
        if ($LRNO) {


            $this->db->where('LRNO', $LRNO);
            $q = $this->db->get('lmais_dataentrypropertyvaluation');
            return $q->num_rows();
        } else
            $q = $this->db->get('lmais_dataentrypropertyvaluation');
        return $q->num_rows();
    }

    function searchdetails($RowID) {
        if ($RowID) {
            $query = $this->db->get_where('lmais_dataentrypropertyvaluation', array('RowID' => $RowID));

            return $query->result();
        } else
            return false;
    }
	
	 function searchdetailsd($RowID) {
        if ($RowID) {
            $query = $this->db->get_where('lmais_rla', array('RowID' => $RowID));

            return $query->result();
        } else
            return false;
    }

    function UnassignedView($TransactionID) {
        if ($TransactionID) {
            $query = $this->db->get_where('lmais_receivedrequests', array('TransactionID' => $TransactionID));

            return $query->result();
        } else
            return false;
    }

    function ParcelInfo($DocID) {
        if ($DocID) {
            $query = $this->db->get_where('lmais_rla', array('DocID' => $DocID));

            return $query->result();
        } else
            return false;
    }

    function SellerInfo($TransactionID) {
        if ($TransactionID) {
            $query = $this->db->get_where('lmais_sellers', array('TransactionID' => $TransactionID));

            return $query->first_row();
        } else
            return false;
    }

    /*     * Added functions, original lmais above
     * the code below represents functionality added by
     * The Royal King Kabs
     */
}
