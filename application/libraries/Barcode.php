<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Barcode {

    /**
     * S# bar_code() function
     * 
     * Generate bar code
     * 
     * @param str $text text
     * @param str $output_type Output type
     * @param int $total_height Height
     * @param str $color Color
     * @param int $width_factor Width factor
     * 
     * @return mixed barcode
     */
    public function bar_code($text, $output_type, $total_height = '30', $color = 'black', $width_factor = 2) {
        require FCPATH . 'vendor/autoload.php';
        if ($output_type == 'html') {
            $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
        } elseif ($output_type == 'png') {
            $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
        } elseif ($output_type == 'jpg') {
            $generator = new Picqer\Barcode\BarcodeGeneratorJPG();
        } elseif ($output_type == 'svg') {
            $generator = new Picqer\Barcode\BarcodeGeneratorSVG();
        }//E# if else statement
        echo $generator->getBarcode($text, $generator::TYPE_CODE_128, $width_factor, $total_height, $color);
    }

//E# bar_code() function
}

//E# MessageController() function