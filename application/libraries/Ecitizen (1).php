<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ecitizen {

    private $url = 'http://197.248.4.237:666';
	private $redirectUrl = 'http://197.248.119.218:8086/lmais/';
    private $ci;
    private $response;

    public function __construct() {
        $this->ci = &get_instance();
    }

    /**
     * S# get_user_by_id_number() function
     * @author Edwin Mugendi
     * 
     * Get user info by code
     * 
     * @param integer $code
     *
     * @return mixed user object or false
     * 
     */
	 public function ecitizenLogin($code)
	 {
		 
		   $param = array(
            'client_id' => 'aeb38ab4f46811e6bff70050560101de',
            'client_secret' => 'fdac04283eea42591150922ea54f59a3',
            'grant_type' => 'authorization_code',
			'code'=>$code,
			'redirect_uri'=>$this->redirectUrl,
			
        ); 
		  try {
            $effective_url = $this->url . '/oauth/access-token';

            $response = $this->curl_request('post', $effective_url, $param);

            $response_array = json_decode($response, true);
           
			
            if (is_array($response_array)) {

                $effective_url = $this->url . '/api/user-info';

                $param = array(
                    'access_token' => $response_array['access_token'],
                );

                $response = $this->curl_request('get', $effective_url, $param);
              
                $user_array = json_decode($response, true);

                if (array_key_exists('error', $user_array)) {
                    $this->response = array(
                        'status' => 0,
                        'message' => 'System error'
                    );
                } else if (array_key_exists('status', $user_array)) {
                    if ($user_array['status'] == 'invalid') {
                        $this->response = array(
                            'status' => 0,
                            'message' => 'User with this id number not found'
                        );
                    } else {
                        $this->response = array(
                            'status' => 1,
                            'message' => $user_array
                        );
                    }//E# if else statement
                } else {

                    $this->response = array(
                        'status' => 1,
                        'message' => $user_array
                    );
                }
            } else {
                $this->response = array(
                    'status' => 0,
                    'message' => 'System error'
                );
            }//E# if statement
        } catch (Exception $ex) {
            $this->response = array(
                'status' => 0,
                'message' => $ex->getMessage()
            );
        }//E# try catch block
		
		  return $this->response;
	 }
	 
	 
	 
    public function get_user_by_id_number($id_number) {
        $param = array(
            'client_id' => 'aeb38ab4f46811e6bff70050560101de',
            'client_secret' => 'fdac04283eea42591150922ea54f59a3',
            'grant_type' => 'client_credentials',
        );

        try {
            $effective_url = $this->url . '/oauth/access-token';

            $response = $this->curl_request('post', $effective_url, $param);

            $response_array = json_decode($response, true);

            if (is_array($response_array)) {

                $effective_url = $this->url . '/api/find-user';

                $param = array(
                    'access_token' => $response_array['access_token'],
                    'id_number' => $id_number,
                    'id_type' => 'citizen',
                );

                $response = $this->curl_request('get', $effective_url, $param);

                $user_array = json_decode($response, true);

                if (array_key_exists('error', $user_array)) {
                    $this->response = array(
                        'status' => 0,
                        'message' => 'System error'
                    );
                } else if (array_key_exists('status', $user_array)) {
                    if ($user_array['status'] == 'invalid') {
                        $this->response = array(
                            'status' => 0,
                            'message' => 'User with this id number not found'
                        );
                    } else {
                        $this->response = array(
                            'status' => 1,
                            'message' => $user_array
                        );
                    }//E# if else statement
                } else {

                    $this->response = array(
                        'status' => 1,
                        'message' => $user_array
                    );
                }
            } else {
                $this->response = array(
                    'status' => 0,
                    'message' => 'System error'
                );
            }//E# if statement
        } catch (Exception $ex) {
            $this->response = array(
                'status' => 0,
                'message' => $ex->getMessage()
            );
        }//E# try catch block

        return $this->response;
    }

//E# get_user_by_id_number() function

    /**
     * S# curl_request() function
     * 
     * Make a post or get curl request
     * 
     * @param str $type Type
     * @param str $url URL
     * @param array data Data
     * 
     * @return object
     */
    private function curl_request($type, $url, $data) {

        $fields_string = '';

        foreach ($data as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }//E# foreach statement

        rtrim($fields_string, '&');

        // Get cURL resource
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if ($type == 'post') {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, count($data));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        } elseif ($type == 'get') {
            curl_setopt($ch, CURLOPT_URL, $url . '?' . $fields_string);
        }//E# if else statement

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

//E# curl_request() function
}

//E# MessageController() function