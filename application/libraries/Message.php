<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Message {

    private $url = 'http://197.248.4.234/send_bulk_sms';
    private $source = '20642';
    private $ci;

    public function __construct() {
        $this->ci = &get_instance();
    }

    /**
     * S# send_sms() function
     * 
     * Send SMS test
     * 
     */
    public function send_email() {
        $recipient['to']['email'] = 'edwinmugendi@gmail.com';
        $recipient['to']['name'] = 'Edwin Mugendi';

        $parameters = array(
            'name' => 'Edwin Mugendi'
        );
        $this->email(1, array('name' => 'Edwin Mugendi', 'email' => 'edwinmugendi@gmail.com'), 1, $recipient, 'welcome', 'en', $parameters);
    }

//E# send_sms() function

    /**
     * S# email() function
     * @author Edwin Mugendi
     * Send email
     * 
     * @param integer $sender_id Sender Id
     * @param array $sender Sender name and email
     * @param integer $recipientId Recipient Id
     * @param string $recipient Recipient address
     * @param string $comCode Commication code
     * @param string $lang Language code
     * @param array $parameters Parameters data
     * 
     * @return boolean 1 if email is sent 0 otherwise
     */
    public function email($sender_id, $sender, $recipientId, $recipient, $comCode, $lang, $parameters = array()) {

        $email_template = $this->ci->db
                        ->select('*')
                        ->order_by('RowID', 'desc')
                        ->where('template_name', $comCode)
                        ->where('lang', $lang)
                        ->limit(1)
                        ->get('lmais_email_templates')->row();

        if ($email_template) {

            $body = $this->sprintf($email_template->email, $parameters);
            $subject = $this->sprintf($email_template->email_subject, $parameters);

            $this->ci->load->library('email');

            $this->ci->email->initialize(
                    array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'smtp.mailgun.org',
                        'smtp_user' => 'lmais@sapama.com',
                        'smtp_pass' => 'lmais123',
                        'smtp_port' => 587,
                        'crlf' => "\r\n",
                        'newline' => "\r\n",
                        'mailtype' => 'html',
            ));

            $this->ci->email->from($sender['email'], $sender['name']);

            $this->ci->email->to($recipient['to']['email']);
            if (array_key_exists('cc', $recipient)) {
                foreach ($recipient as $single_recipient) {
                    $this->ci->email->cc($single_recipient);
                }//E# foreach statement
            }//E# if statement

            if (array_key_exists('bcc', $recipient)) {
                foreach ($recipient as $single_recipient) {
                    $this->ci->email->bcc($single_recipient);
                }//E# foreach statement
            }//E# if statement

            if (array_key_exists('attachments', $recipient) && is_array($recipient['attachments'])) {//Attach
                foreach ($recipient['attachments'] as $attachment) {//Attachments
                    $this->ci->email->attach($attachment);
                }//E# foreach statement
            }//E# if statement

            $this->ci->email->subject($subject);
            $this->ci->email->message($body);
            $sent = $this->ci->email->send();
        }//E# if statement

        return $sent;
    }

//E# email() function

    public function email1() {
        $this->load->library('email');

        $this->ci->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.sendgrid.net',
            'smtp_user' => 'sendgridusername',
            'smtp_pass' => 'sendgridpassword',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n"
        ));

        $this->ci->email->from('your@example.com', 'Your Name');
        $this->ci->email->to('someone@example.com');
        $this->ci->email->cc('another@another-example.com');
        $this->ci->email->bcc('them@their-example.com');
        $this->ci->email->subject('Email Test');
        $this->ci->email->message('Testing the email class.');
        $this->ci->email->send();
    }

    /**
     * S# send_sms() function
     * 
     * Send SMS test
     * 
     */
    public function send_sms() {
        $parameters = array(
            'name' => 'Edwin',
        );
        $this->sms(1, 1, 1, '0722906835', 'welcome', 'en', $parameters);
    }

//E# send_sms() function

    /**
     * S# sms() function
     * @author Edwin Mugendi
     * Send SMS
     * 
     * @param string $type Message type (email, push, sms)
     * @param integer $sender_id Sender Id
     * @param string $sender Sender address
     * @param integer $recipientId Recipient Id
     * @param mixed $recipient Receipient address, single or multiple
     * @param string $comCode Commication code
     * @param string $lang Language code
     * @param array $parameters Parameters data
     *
     * @return boolean 1 if email is sent 0 otherwise
     */
    public function sms($sender_id, $sender, $recipientId, $recipient, $comCode, $lang, $parameters) {

        $is_number = $this->cleanSmsSender($recipient);

        if ($is_number) {

            $email_template = $this->ci->db
                            ->select('*')
                            ->order_by('RowID', 'desc')
                            ->where('template_name', $comCode)
                            ->where('lang', $lang)
                            ->limit(1)
                            ->get('lmais_email_templates')->row();

            if ($email_template) {
                $sms = $email_template->sms;
                $sms = $this->sprintf($sms, $parameters);

                //SMS params
                $param = array(
                    'MSISDN' => $is_number,
                    'MESSAGE' => $sms,
                    'SOURCE' => $this->source,
                );

                $response = $this->curl_post($this->url, $param);

                return $response;
            }//E# if statement
        }//E# if statement

        return false;
    }

//E# sms() function

    /**
     * S# curl_post() function
     * 
     * Make a post
     * 
     * @param str $url URL
     * @param array data Data
     * 
     * @return object
     */
    private function curl_post($url, $data) {

        $fields_string = '';

        foreach ($data as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }//E# foreach statement

        rtrim($fields_string, '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

//E# curl_post() function

    /**
     * S# sprintf() function
     * 
     * @author Edwin Mugendi <edwinmugendi@gmail.com>
     * 
     * Replace each occurrence of the :key with the value
     * 
     * eg "Welcome :name" will be "Welcome Edwin"
     * 
     * @param str $str String 
     * @param array $parameters Parameters
     * 
     * @return str 
     */
    private function sprintf($str, $parameters) {
        foreach ($parameters as $key => $value) {
            $str = str_replace(':' . $key, $value, $str);
        }//E# foreach statement
        return $str;
    }

//E# sprintf() function

    /**
     * S# cleanSmsSender() function
     * 
     * Return the number if it's valid otherwise return false
     * @param string $number Number
     * 
     * @return int The formated number otherwise 0
     */
    public function cleanSmsSender($number) {
        if (!$number) {//Number not valid
            return 0;
        }//E# if statement
        $numberParts = explode('/', $number);

        $numToTest = trim($numberParts[0]);

        if (substr($numToTest, 0, 3) == '254') {//Starts with 254
            return $numToTest;
        }//E# if statement

        if (substr($numToTest, 0, 4) == '+254') {//Starts with +254
            return substr($numToTest, 1);
        }//E# if statement

        if (substr($numToTest, 0, 1) == '7') {//Starts with 7
            return '254' . $numToTest;
        }//E# if statement

        if (substr($numToTest, 0, 2) == '07') {//Starts with 07
            return '254' . substr($numToTest, 1);
        }//E# if statement

        return 0;
    }

//E# cleanSmsSender() function
}

//E# MessageController() function