<!-- begin #content -->
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li><a href="javascript:;">Search Results</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Search Results <small>Valued Data</small></h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">

                    <h4 class="panel-title"></h4>
                </div>
                <div class="panel-body">
                    <table id="data-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>LRNO</th>
                                <th>Status</th>
                                <th>Registry</th>
                                <th>Area Size</th>
                                <th>Area Unit</th>
                                <th>Tenure</th>
                                <th>Land Use</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($Results) : ?>

                                <?php foreach ($Results as $index => $Result) : ?>


                                    <tr class="odd gradeX">
                                        <td>#</td>
                                        <td><?php echo $Result->LRNO; ?></td>
                                        <td><?php echo $Result->DevelopmentStatus; ?></td>
                                        <td><?php echo $Result->Registry; ?></td>
                                        <td><?php echo $Result->AreaSize; ?></td>
                                        <td><?php echo $Result->AreaUnit; ?></td>
                                        <td><?php echo $Result->TypeOfDevelopment; ?></td>
                                        <td><?php echo $Result->NatureOfTitle; ?></td>
                                        <td><a href="searchdetails/<?php echo $Result->RowID; ?>">View</td>
                                    </tr>


                                <?php endforeach; ?>

                            <?php endif; ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->

<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function () {
        App.init();
        TableManageCombine.init();
    });
</script>

</body>
</html>




