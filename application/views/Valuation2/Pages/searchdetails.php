<div id="content" class="content">

	<h1 class="page-header">Title Number (Tenure) - Active</h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-3 -->
		<div class="col-md-3 col-sm-6">
			<div class="widget widget-stats bg-green">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
				<div class="stats-number">Title Number</div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 40.5%;"></div>
				</div>
				<div class="stats-title">Property Status: 	Active</div>
				<div class="stats-title">Nature of Title: 	Absolute</div>
				<div class="stats-title">Property Size	: 	0.4 Ha</div>

			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-md-3 col-sm-6">
			<div class="widget widget-stats bg-blue">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-tags fa-fw"></i></div>
				<div class="stats-title">Proprietorship</div>
				<div class="stats-number">Owner Count: </div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 40.5%;"></div>
				</div>
				<div class="stats-desc">Proprietor: Teren Wanja</div>
				<div class="stats-desc">ID Number : </div>
			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-md-3 col-sm-6">
			<div class="widget widget-stats bg-purple">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-shopping-cart fa-fw"></i></div>
				<div class="stats-title">Encumbrances</div>

				<div class="stats-number">Active Count: </div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 76.3%;"></div>
				</div>
				<div class="stats-desc">Current Encumbrance: Charge</div>
				<div class="stats-desc">Transact Further: No</div>
			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-md-3 col-sm-6">
			<div class="widget widget-stats bg-red">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-comments fa-fw"></i></div>
				<div class="stats-title">Inhibitions</div>
				<div class="stats-number">Active Count: </div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 54.9%;"></div>
				</div>
				<div class="stats-desc">Current Inhibition: Court Order</div>
				<div class="stats-desc">Transact Further: No</div>
			</div>
		</div>
		<!-- end col-3 -->
	</div>
	<!-- end row -->
	<div class="row">

		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#overview">Overview</a></li>

			<li><a data-toggle="tab" href="#proprietorship">Proprietorship</a></li>
			<li><a data-toggle="tab" href="#incumberances">Incumberances</a></li>
			<li><a data-toggle="tab" href="#inhibitions">Inhibitions</a></li>
			<li><a data-toggle="tab" href="#tasks">Tasks</a></li>
			<li><a data-toggle="tab" href="#files">Files</a></li>
			<li><a data-toggle="tab" href="#comments">Comments</a></li>
			<li><a data-toggle="tab" href="#activities">Activities</a></li>
			<li><a data-toggle="tab" href="#valuations">Valuations</a></li>
			<li><a data-toggle="tab" href="#gis">GIS</a></li>
			<li><a data-toggle="tab" href="#gis">Lease Management</a></li>
		</ul>
	</div>

	<div class="row">
		<!-- begin row -->
		<div class="tab-content">
			<div id="overview" class="tab-pane fade in active">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<div class="invoice-content">
								<div class="table-responsive">
									<table class="table table-invoice">
										<thead>
											<tr>
												<th>Location Details</th>

											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													Province<br />
												</td>
												<td> Riftvalley </td>

											</tr>

											<tr>
												<td>
													County<br />
												</td>
												<td> Kajiado </td>

											</tr>

											<tr>
												<td>
													Registry<br />
												</td>
												<td> Kajiado </td>

											</tr>

											<tr>
												<td>
													Registration Section<br />
												</td>
												<td> Kitengela </td>

											</tr>

											<tr>
												<td>
													Parcel Number<br />
												</td>
												<td> 5 </td>

											</tr>

										</tbody>
									</table>
								</div>


							</div>
						</div>


						<div class="col-md-3">
							<div class="invoice-content">
								<div class="table-responsive">
									<table class="table table-invoice">
										<thead>
											<tr>
												<th>Property Info</th>

											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													Area Size<br />
												</td>
												<td> 0.52 Ha </td>

											</tr>

											<tr>
												<td>
													Regsheet No.<br />
												</td>
												<td> 5 </td>

											</tr>



											<tr>
												<td>
													Tenure<br />
												</td>
												<td> Absolute </td>

											</tr>

											<tr>
												<td>
													Open Date<br />
												</td>
												<td> 15-06-2008 </td>

											</tr>
											<tr>
												<td>
													Parent Title<br />
												</td>
												<td> Kajiado/Kitengela/5 </td>

											</tr>

										</tbody>
									</table>
								</div>


							</div>
						</div>


						<div class="col-md-5">
							<div class="invoice-content">
								<div class="table-responsive">
									<div class="widget widget-stats bg-purple">
										<div class="stats-icon stats-icon-lg"><i class="fa fa-comments fa-fw"></i></div>

										<div class="stats-number">Encumbrances </div>
										<div class="stats-progress progress">
											<div class="progress-bar" style="width: 54.9%;"></div>
										</div>
										<div class="stats-desc">1. 10 meters road passes through this plot.</div>
									</div>
								</div>





							</div>
						</div>



					</div>
				</div>
			</div>


			<div id="proprietorship" class="tab-pane fade">
				<div class="container">
					<div class="row">
						<div class="col-md-11">
							<div class="invoice-content">
								<div class="table-responsive">

									<table id="data-table" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>#</th>
												<th>Entry Date</th>
												<th>Registered Proprietor</th>
												<th>Address</th>
												<th>Residence</th>
												<th>ID/Passport</th>
												<th>Telephone</th>
												<th>Email</th>
												<th></th>
											</tr>
										</thead>
										<tbody>

											<tr class="odd gradeX">
												<td>1</td>
												<td>13-06-1997</td>
												<td>Christine Oswago Kimeo</td>
												<td>14175 - 00400, Nairobi</td>
												<td>Donholm Nairobi</td>
												<td>12345678</td>
												<td>0711111111</td>
												<td>oswago@iebc.or.ke</td>
												<td>Edit</td>
											</tr>



										</tbody>
									</table>
								</div>


							</div>


						</div>
					</div>









				</div>

			</div>
		</div>
	</div>





</div>


<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
			<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
				<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
				<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
					<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
						<!-- ================== END BASE JS ================== -->

						<!-- ================== BEGIN PAGE LEVEL JS ================== -->
						<script src="<?php echo base_url(); ?>assets/plugins/morris/raphael.min.js"></script>
							<script src="<?php echo base_url(); ?>assets/plugins/morris/morris.js"></script>
								<script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
									<script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>
										<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
											<script src="<?php echo base_url(); ?>assets/plugins/gritter/js/jquery.gritter.js"></script>
												<script src="<?php echo base_url(); ?>assets/js/dashboard-v2.min.js"></script>
													<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/AutoFill/js/dataTables.autoFill.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/KeyTable/js/dataTables.keyTable.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/RowReorder/js/dataTables.rowReorder.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Select/js/dataTables.select.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/table-manage-combine.demo.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
														<!-- ================== END PAGE LEVEL JS ================== -->

														<script>
															$(document).ready(function() {
															App.init();
															DashboardV2.init();
															});
														</script>

													</body>
												</html>
												