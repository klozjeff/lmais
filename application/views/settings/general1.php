<div id="content" class="content content-full-width">
	<!-- begin vertical-box -->
	<div class="vertical-box">
		<!-- begin vertical-box-column -->
		<div class="vertical-box-column width-250">

			<div class="wrapper">
				<p><b>General Settings</b></p>
				<ul class="nav nav-pills nav-stacked nav-sm">
					<li class="active"><a data-toggle="tab" href="#General_Settings"><i class="fa fa-inbox fa-fw m-r-5"></i> General </a></li>

					<li><a data-toggle="tab" href="#Email_Settings"><i class="fa fa-flag fa-fw m-r-5"></i> Email Settings</a></li>
					<li><a data-toggle="tab" href="#IP_Restrictions"><i class="fa fa-send fa-fw m-r-5"></i> IP Restriction</a></li>
					<li><a data-toggle="tab" href="#Email_Templates"><i class="fa fa-send fa-fw m-r-5"></i> Email Templates</a></li>
					<li><a data-toggle="tab" href="#"><i class="fa fa-send fa-fw m-r-5"></i> Registries</a></li>
					<li><a data-toggle="tab" href="#"><i class="fa fa-send fa-fw m-r-5"></i> Counties</a></li>
					<li><a data-toggle="tab" href="#"><i class="fa fa-send fa-fw m-r-5"></i> Registration Sections</a></li>
					<li><a data-toggle="tab" href="#"><i class="fa fa-send fa-fw m-r-5"></i> Roles and Rights</a></li>


				</ul>

			</div>
			<!-- end wrapper -->
		</div>
		<!-- end vertical-box-column -->
		<!-- begin vertical-box-column -->
		<div class="vertical-box-column bg-white">


			<?PHP
				echo $this->session->flashdata('settingsmsg');
				?>
			<div class="tab-content">

				<div id="General_Settings" class="tab-pane in active">

					<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
						<div class="panel-heading">

							<h4 class="panel-title">General Settings</h4>
						</div>
						<div class="panel-body">
						
							<?php echo form_open(("settings/save_general_settings"), array("id" => "general-settings-form", "class" => "form-horizontal")); ?>
							
								<div class="form-group">
									<label class="col-md-2">Application Name</label>
									<div class="col-md-7">
										<input type="text" name="app_title" class="form-control" placeholder="App Name"  value ='<?php echo $this->Settings_model->get_setting('app_title'); ?>'/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 ">Ministry Name</label>
									<div class="col-md-7">
										<input type="text" class="form-control" placeholder="Official Name" name="company_name" value ='<?php echo $this->Settings_model->get_setting('company_name'); ?>'/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 ">Address</label>
									<div class="col-md-7">
										<input type="text" class="form-control" placeholder="Default Address" name = "company_address" value ='<?php echo $this->Settings_model->get_setting('company_address'); ?>'/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 ">Phone</label>
									<div class="col-md-7">
										<input type="text" class="form-control" placeholder="Phone Number" name="company_phone" value ='<?php echo $this->Settings_model->get_setting('company_phone'); ?>'/>
									</div>
								</div>


								<div class="form-group">
									<label class="col-md-2 ">Email</label>
									<div class="col-md-7">
										<input type="text" class="form-control" placeholder="Email Address" name ="company_email" value ='<?php echo $this->Settings_model->get_setting('company_email'); ?>'/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 ">Website</label>
									<div class="col-md-7">
										<input type="text" class="form-control" placeholder="Website" name ="company_website" value ='<?php echo $this->Settings_model->get_setting('company_website'); ?>'/>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-2 ">Currency Symbol</label>
									<div class="col-md-7">
										<input type="text" class="form-control" placeholder="Default Currency" name ="currency_symbol" value ='<?php echo $this->Settings_model->get_setting('currency_symbol'); ?>'/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 ">File Formats</label>
									<div class="col-md-7">
										<input type="text" class="form-control" placeholder="File Formats" name ="accepted_file_formats" value ='<?php echo $this->Settings_model->get_setting('accepted_file_formats'); ?>'/>
									</div>
								</div>
								

				
								<div class="panel-footer">
                <button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> Save</button>
            </div>

							        <?php echo form_close(); ?>
						</div>
					</div>
				</div>
				<div id="Email_Settings" class="tab-pane">

					<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
						<div class="panel-heading">

							<h4 class="panel-title">Email Settings</h4>
						</div>
						<div class="panel-body">
							<?php echo form_open(("settings/save_email_settings"), array("id" => "general-settings-form", "class" => "form-horizontal")); ?>
								<div class="form-group">
									<label class="col-md-2">Emails Sent From</label>
									<div class="col-md-7">
										<input type="text" class="form-control" placeholder="Emails from" name ="email_sent_from_address" value ='<?php echo $this->Settings_model->get_setting('email_sent_from_address'); ?>'/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2">Emails Sent From Name</label>
									<div class="col-md-7">
										<input type="text" class="form-control" placeholder="Email sent from"  name = "email_sent_from_name" value ='<?php echo $this->Settings_model->get_setting('email_sent_from_name'); ?>'/>
									</div>
								</div>

									<div class="form-group">
									<label class="col-md-2">Email Protocol</label>
									<div class="col-md-7">
										<input type="text" class="form-control" placeholder="Default input" name ="email_protocol" value ='<?php echo $this->Settings_model->get_setting('email_protocol'); ?>'/>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-2">SMTP Host</label>
									<div class="col-md-7">
										<input type="text" class="form-control" placeholder="Default input" name = "email_smtp_host" value ='<?php echo $this->Settings_model->get_setting('email_smtp_host'); ?>'/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2">SMTP User</label>
									<div class="col-md-7">
										<input type="text" class="form-control" placeholder="Default input" name "email_smtp_user" value ='<?php echo $this->Settings_model->get_setting('email_smtp_user'); ?>'/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2">SMPT Password</label>
									<div class="col-md-7">
										<input type="text" class="form-control" placeholder="Default input" name="email_smtp_pass" value ='<?php echo $this->Settings_model->get_setting('email_smtp_pass'); ?>'/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2">SMPT Port</label>
									<div class="col-md-7">
										<input type="text" class="form-control" placeholder="Default input" name="email_smtp_port" value ='<?php echo $this->Settings_model->get_setting('email_smtp_port'); ?>'/>
									</div>
								</div>
								<div class="panel-footer">
                <button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> Save</button>
            </div>

							<?php echo form_close(); ?>
						</div>	
					</div>
				</div>


				<div id="IP_Restrictions" class="tab-pane">

					<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
						<div class="panel-heading">

							<h4 class="panel-title">IP Restrictions</h4>
						</div>
						<div class="panel-body">
							
<?php echo form_open(get_uri("settings/save_ip_settings"), array("id" => "ip-settings-form", "class" => "general-form")); ?>
       
            <div class="form-group">
                <div class="p15 col-md-12 clearfix">
                    <strong>  Allow  access from these IPs only. </strong>
                </div>
                <div class=" col-md-12 clearfix">
                    <?php
                    echo form_textarea(array(
                        "id" => "allowed_ip_addresses",
                        "name" => "allowed_ip_addresses",
                        "value" => $this->Settings_model->get_setting("allowed_ip_addresses"),
                        "class" => "form-control",
                        "style" => "min-height:100px"
                    ));
                    ?>
                </div>
                <div class="pt5 col-md-12 clearfix">
                    <i class="fa fa-info-circle"></i>  Enter one IP per line. Keep it blank to allow all IPs. *Admin users will not be affected. 
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> Save</button>
            </div>
     
        <?php echo form_close(); ?>

	
						</div>
					</div>
				</div>
				<!-- end vertical-box -->
			</div>
		</div>
	</div>
	<!-- end #content -->

	<!-- begin scroll to top btn -->
	<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
	<!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
			<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
				<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
				<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
					<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
						<!-- ================== END BASE JS ================== -->

						<!-- ================== BEGIN PAGE LEVEL JS ================== -->
						<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
							<!-- ================== END PAGE LEVEL JS ================== -->

							<script>
								$(document).ready(function() {
								App.init();
								});
							</script>
							<script>
								(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
								(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
								m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
								})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

								ga('create', 'UA-53034621-1', 'auto');
								ga('send', 'pageview');

							</script>
							
							
						</body>
					</html>