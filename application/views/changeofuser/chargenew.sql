-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.19-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5144
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table lmais.lmais_changeofuser
CREATE TABLE IF NOT EXISTS `lmais_changeofuser` (
  `RowID` int(11) NOT NULL AUTO_INCREMENT,
  `TransactionID` varchar(20) DEFAULT NULL,
  `RegistrationSectionID` int(255) DEFAULT NULL,
  `RegistryID` int(255) DEFAULT NULL,
  `ParcelNumber` int(255) DEFAULT NULL,
  `TenureID` int(255) DEFAULT NULL,
  `LRNumber` varchar(255) DEFAULT NULL,
  `DocID` varchar(255) DEFAULT NULL,
  `AmountPayable` int(255) DEFAULT NULL,
  `UserID` int(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `currentuse` varchar(255) DEFAULT NULL,
  `proposeduse` varchar(255) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ConvenienceFee` float(12,0) DEFAULT NULL,
  `ChangeofuserFee` float(12,0) DEFAULT NULL,
  `Agent` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Ip` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `EcitizenBillRef` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Status` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`RowID`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;

-- Dumping data for table lmais.lmais_changeofuser: ~4 rows (approximately)
/*!40000 ALTER TABLE `lmais_changeofuser` DISABLE KEYS */;
INSERT INTO `lmais_changeofuser` (`RowID`, `TransactionID`, `RegistrationSectionID`, `RegistryID`, `ParcelNumber`, `TenureID`, `LRNumber`, `DocID`, `AmountPayable`, `UserID`, `description`, `currentuse`, `proposeduse`, `DateCreated`, `ConvenienceFee`, `ChangeofuserFee`, `Agent`, `Ip`, `EcitizenBillRef`, `Status`) VALUES
	(95, '17', 2, 6, 236, 2, 'KILIFI/JILORE/236', '8444', 505, 41, 'lml kn', '2', '1', '2017-04-07 18:42:31', 5, 500, 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', '105.166.11.236', 'changeofuser_95', 'unpaid'),
	(96, '', 2, 6, 236, 2, 'KILIFI/JILORE/236', '8444', 505, 34, 'CHANGE OF USER', '', '', '2017-04-07 20:04:01', 5, 500, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '105.161.151.152', 'changeofuser_96', 'unpaid'),
	(97, '19', 2, 6, 236, 2, 'KILIFI/JILORE/236', '8444', 10, 34, 'CHANGE OF USERiiii', '3', '4', '2017-04-07 20:13:19', 5, 5, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '105.161.151.152', 'changeofuser_97', 'unpaid'),
	(98, 'TRA00D0C1M', 2, 6, 236, 1, 'KILIFI/JILORE/236', '8444', 10, 41, 'CHANGE OF USER', '1', '3', '2017-04-09 14:48:42', 5, 5, 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', '196.207.135.241', 'changeofuser_98', 'unpaid');
/*!40000 ALTER TABLE `lmais_changeofuser` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
