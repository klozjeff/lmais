<div class="row">
    <div class="col-md-8">
        <h4 class="page-title" style="padding: 10px 0 0 50px;">DISCHARGE OF CHARGE, LR NUMBER: <strong><?php echo $transfer_model['LRNumber']; ?></strong></h4>
    </div>
</div>


<div class="row">
    <div class="col-md-8" style="height:500px;"> <!-- begin col-12 -->
        <!--<a  href="#modal-dialog" class="btn btn-sm btn-primary pull-right" data-toggle="modal"><i class="fa fa-print"></i>
            Approve charge</a>-->
        <div class="table-responsive">
            <div class="panel panel-inverse">
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Details</th>
                                <th>Description</th>                                                                   
                            </tr>

                        </thead>
                        <tbody>
                            <tr>
                                <td>Registry</td>
                                <td><?php echo $transfer_model['RegistryID']; ?></td>
                            </tr>
                            <tr>
                                <td>Registration Section</td>
                                <td><?php echo $transfer_model['RegistrationSectionID']; ?></td>
                            </tr>
                            <tr>
                                <td>Parcel Number</td>
                                <td><?php echo $transfer_model['ParcelNumber']; ?></td>
                            </tr>
                            <tr>
                                <td>LR Number</td>
                                <td><?php echo $transfer_model['LRNumber']; ?></td>
                            </tr>
                            <tr>
                                <td>Loan Type</td>
                                <td><?php echo $transfer_model['ChargeTypeID']; ?></td>
                            </tr>
                            <tr>
                                <td>Principal Amount</td>
                                <td><?php echo $transfer_model['PrincipalAmount']; ?></td>
                            </tr>
                            <tr>
                                <td>Interest Rate (PA)</td>
                                <td><?php echo $transfer_model['InterestRate']; ?> %</td>
                            </tr>
                            <tr>
                                <td>Repaid By Date</td>
                                <td><?php echo date_format(date_create($transfer_model['RepaidByDate']), 'd/m/Y'); ?></td>
                            </tr>
                        </tbody>
                    </table>

                </div>			
            </div>
        </div>


    </div>

    <div class="col-md-4">

        <div class="box-content ">

            <h4 class="m-t-0 m-b-5">Billing Summary</h4>
            <table class="table">
                <tbody>
                    <tr>
                        <td ><strong>Discharge Bill Ref:</strong></td>
                      <td><?php echo $transfer_model['DischargeEcitizenBillRef']!='' ? $transfer_model['DischargeEcitizenBillRef']:'dischargeofcharge_'.$transfer_model['RowID']; ?></td>
                             </tr>
                    <tr>
                        <td ><strong>Discharge Payment Status:</strong></td>
                        <td>
                           
									<?php echo $transfer_model['DischargePaymentStatus']!=''?$transfer_model['DischargePaymentStatus']:'unpaid';?>
								
                        </td>
                    </tr>
                    <tr>
                        <td ><strong>Service:</strong></td>
                        <td><?php
                            $sql = "SELECT * FROM lmais_landservicetypes where Slug = 'dischargeofcharge'";
                            $query = $this->db->query($sql);
                            if ($query->num_rows() > 0) {
                                foreach ($query->result() as $row) {
                                    echo $row->ServiceName;
                                }
                            }
                            ?> </td>
                    </tr>
                    <tr>
                        <td ><strong>Total:</strong></td>
                        <td><strong><?php echo $bal;?></strong></td>
                    </tr>
                </tbody>
            </table>


            <a href="<?php echo base_url() ?>printing/invoice/<?php echo $transfer_model['TransactionID'] . '/discharge/' . $transfer_model['UserID'] ?>" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-print"></i>
                Print Invoice</a>
				
				   <?php if ($transfer_model['DischargePaymentStatus'] =='paid' && $bal==0): ?>
                                            <div class="btn-group closed">
                                            <button type="button" class="btn btn-success">Download Document</button>
                                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                           </button>
                                        <ul class="dropdown-menu" role="menu">
                                        <li><a href="<?php echo base_url(); ?>Printing/withdrawcaution/<?php echo $transfer_id; ?>" id="idXls" title="Download Caution Application">Print Caution Withdrawal</a></li>
                                        

										
                                        </ul>          
                   </div> 	
                                      <?php else: ?>
                                       <a href="<?php echo base_url(); ?>discharge/discharge?step=1&reference_number=<?php echo $transfer_model['RowID']; ?>" title="Edit" class="btn btn-success">Review & Discharge</a>
                                     <?php endif; ?>
            </td>   
        </div> 
    </div>

<div class="col-md-4">
    <div class="box-content">							   
        <h3>Quick Links</h3>
        <ul>
            <li>Department of Immigration</li>
            <li>Ministry of Lands</li>
            <li>Office of the attorney general</li>
            <li>Mombasa County</li>
        </ul>

    </div>
</div>
</div>
<!-- end col-12 -->
</div>

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->

<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/morris/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/morris/morris.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/gritter/js/jquery.gritter.js"></script>
<script src="<?php echo base_url(); ?>assets/js/dashboard-v2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function () {
        App.init();
        DashboardV2.init();
    });
</script>

</body>
</html>
