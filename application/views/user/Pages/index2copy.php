<div class="row">
    <div class="col-md-8"> <!-- begin col-12 -->
        <div class="table-responsive">
            <table id="data-table" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>FORM</th>
                        <th>Reference Number</th>
                        <th>Submission Date</th>
                        <th>Status</th>
                        <th>Type</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php
                        $index = 1;
                        $total_rent = 0;
                        ?>
                        <?php if ($received_model): ?>
                            <?php foreach ($received_model as $single_model): ?>
                            <tr>
                                <td><?php echo $index; ?></td>
                                <td><?php
                                    $ServiceSlug = $single_model['ServiceSlug'];
                                    $sql = "SELECT * FROM lmais_landservicetypes where slug = '$ServiceSlug'";
                                    $query = $this->db->query($sql);
                                    if ($query->num_rows() > 0) {
                                        foreach ($query->result() as $row) {
                                            echo $row->ServiceName;
                                        }
                                    }
                                    ?></td>
                                <td><?php echo $single_model['TransactionID']; ?></td>
                                <td><?php echo $single_model['DateCreated']; ?></td>
                                <td>
                                    <?php if ($single_model['ServiceSlug'] == 'caution'): ?>
                                        <?php if ($single_model['Balance'] > 0): ?>
                                            <i class="label label-danger">Not paid</i>
                                        <?php else: ?>
                                            <i class="fa fa-check"></i> <i class="label label-primary">Paid</i>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if ($single_model['BuyerUserID'] == $this->session->userdata('UserID')): ?>
                                        <i class="label label-primary">Applicant</i>
                                    <?php else: ?>
                                        <i class="label label-warning">Seller</i>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if ($single_model['ServiceSlug'] == 'caution'): ?>
                                        
										<a href="<?php echo base_url(); ?>userview/forminfo/<?php echo $single_model['TransactionID'].'/'.$ServiceID  = $single_model['ServiceSlug'].'/'.md5('lmais'.time().'-'.$ServiceID  = $single_model['ServiceSlug']); ?>" class="btn btn-link">View</a>                                  
    
                                    <?php endif; ?>
                                    <?php if ($single_model['ServiceSlug'] == 'transfer'): ?>
                                        <?php if ($single_model['BuyerUserID'] == $this->session->userdata('UserID')): ?>
                                            <a href="<?php echo base_url(); ?>UserView/ConsentInfo/<?php echo $single_model['TransactionID']; ?>?type=buyer" class="btn btn-link">View</a>
                                        <?php else: ?>
                                            <a href="<?php echo base_url(); ?>UserView/ConsentInfo/<?php echo $single_model['TransactionID']; ?>" class="btn btn-link">View</a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>                                   
                            </tr>  
                            </tr>
                            <?php $index++; ?>
                        <?php endforeach; ?>

                    <?php endif; ?>                          
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box-content">

            <p>NAME: <?php
                if ($this->session->userdata('Logged_in') != '') {

                    echo $this->session->userdata('FirstName') . " " . $this->session->userdata('LastName') . " " . $this->session->userdata('EmailAddress');
                } else {
                    redirect('start/index');
                }
                ?></p>
            <p>EMAIL:<?php
                if ($this->session->userdata('Logged_in') != '') {

                    echo $this->session->userdata('EmailAddress');
                }
                ?></p>
            <p>TEL: 0707111222</p>

            <p>
                <a href="<?php echo base_url(); ?>users/logout" class="btn btn-primary m-r-5">LOG OUT</a>
            </p>
        </div>
    </div>


    <div class="col-md-4">
        <div class="box-content">							   
            <h3>Quick Links</h3>
            <ul>
                <li>Department of Immigration</li>
                <li>Ministry of Lands</li>
                <li>Office of the attorney general</li>
                <li>Mombasa County</li>
            </ul>

        </div>
    </div>
    <!-- end col-12 -->
</div>

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/morris/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/morris/morris.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/gritter/js/jquery.gritter.js"></script>
<script src="<?php echo base_url(); ?>assets/js/dashboard-v2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/AutoFill/js/dataTables.autoFill.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/KeyTable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/RowReorder/js/dataTables.rowReorder.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Select/js/dataTables.select.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function () {
        App.init();
        DashboardV2.init();
    });
</script>

</body>
</html>
