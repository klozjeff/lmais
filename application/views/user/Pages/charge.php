<div class="row">
    <div class="col-md-8">
        <h4 class="page-title" style="padding: 10px 0 0 50px;">REGISTRATION OF CHANGE OF NAME</h4>
    </div>
</div>
                

<div class="row">

			   <div class="col-md-8" style="overflow-y: scroll; height:500px;"> <!-- begin col-12 -->
			    <div class="table-responsive">
                    <div class="panel panel-inverse">
                        <div class="panel-body">
                    <table class="table">
                     <thead>
                        <tr>
                                        <th>Details</th>
                                        <th>Description</th>                                                                   
                                       </tr>

                                   </thead>
                                    <tbody>
                                                       
																		
                                                                   
                                                                    <tr>
                                                                        <td>Registration Section</td>
                                                                        <td><?php echo $transfer_model['LRNumber'];?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Parcel Number</td>
                                                                        <td><?php echo $transfer_model['ParcelNumber'];?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>LR Number</td>
                                                                        <td><?php echo $transfer_model['LRNumber'];?></td>
                                                                    </tr>
																	<tr>
                                                                        <td>Current Name</td>
                                                                        <td><?php echo $transfer_model['changeofnameCurrent'];?></td>
                                                                    </tr>

                                                                     <tr>
                                                                        <td>Advocate's First Name</td>
                                                                        <td><?php echo $transfer_model['advoFName']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's Middle Name</td>
                                                                        <td><?php echo $transfer_model['advoMName']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's Last Name</td>
                                                                        <td><?php echo $transfer_model['advoLName']; ?></td>
                                                                    </tr>                                                                                                                              
																    <tr>
                                                                        <td>Advocate's Law Firm</td>
                                                                        <td><?php echo $transfer_model['advoLawFirm']; ?></td>
                                                                    </tr>
                                                                     <tr>
                                                                        <td>Advocate's PF Number</td>
                                                                        <td><?php echo $transfer_model['advoPFNumber']; ?></td>
                                                                    </tr>

                                                                        <tr>
                                                                        <td>Reasons for Change of Name</td>
                                                                        <td><?php echo $transfer_model['changeofnameReasons'];?></td>
                                                                    </tr>
                                                             
                                                                  
                                                                                                                                   
                                 </tbody>
                                </table>
                                                  
			           	   </div>			
					    </div>
					 </div>
					 
					 
				 </div>
							
							

						<div class="col-md-4">
						
							<div class="box-content ">
						
                           <h4 class="m-t-0 m-b-5">Billing Summary</h4>
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td ><strong>Bill Ref:</strong></td>
                                    <td><?php echo $transfer_model['EcitizenBillRef'];?></td>
                                </tr>
                                <tr>
                                    <td ><strong>Status:</strong></td>
                                    <td>
									<?php echo $transfer_model['Status'];?>
									</td>
                                </tr>
                                <tr>
                                    <td ><strong>Service:</strong></td>
                                    <td><?php $sql ="SELECT * FROM lmais_landservicetypes where Slug = 'changeofname'";
                                            $query = $this->db->query($sql);
                                            if ($query->num_rows() > 0) {
                                              foreach ($query->result() as $row) {
                                             echo $row->ServiceName;
                                             }
                                            } ?> </td>
                                </tr>
                                <tr>
                                    <td ><strong>Total:</strong></td>
                                    <td><strong><?php echo $transfer_model['AmountPayable'];?></strong></td>
                                </tr>
                                </tbody>
                            </table>

							
							
							
							
                            <a href="<?php echo base_url()?>printing/invoice/<?php echo $transfer_model['TransactionID'].'/charge/'.$transfer_model['UserID']?>" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-print"></i>
                                Print Invoice</a>

                                            <?php if ($transfer_model['Status'] =='Paid'): ?>
                                            <div class="btn-group closed">
                                            <button type="button" class="btn btn-success">Download Document</button>
                                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                           </button>
                                        <ul class="dropdown-menu" role="menu">
                                        <li><a href="<?php echo base_url(); ?>Prints/PrintChangeofname/<?php echo $transfer_id; ?>" id="idXls" title="Download Change of UserApplication">Print Change of Name</a></li>
                                        

										
                                        </ul>
                                      <?php else: ?>
                                       <a href="<?php echo base_url(); ?>changeofname/apply_changeofname?step=4&reference_number=<?php echo $transfer_model['RowID']; ?>" title="Edit" class="btn btn-success">Review and Make Payment</a>
                                     <?php endif; ?>                                                            
                                      </td>   
				
                         
                    
                   </div> </div>	   
                             
                          
						</div>
						<div class="col-md-4">
							<div class="box-content">							   
                            <h3>Quick Links</h3>
                            <ul>
                            	<li>Department of Immigration</li>
                            	<li>Ministry of Lands</li>
                            	<li>Office of the attorney general</li>
                            	<li>Mombasa County</li>
                            </ul>
                    
                          </div>
						</div>
                <!-- end col-12 -->
            </div>
		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->

	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="<?php echo base_url(); ?>assets/plugins/morris/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/morris/morris.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/gritter/js/jquery.gritter.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/dashboard-v2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<script>
		$(document).ready(function() {
			App.init();
			DashboardV2.init();
		});
	</script>

</body>
</html>
