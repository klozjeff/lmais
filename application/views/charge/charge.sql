-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.19-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5144
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table data2.lmais_charge
CREATE TABLE IF NOT EXISTS `lmais_charge` (
  `RowID` int(11) NOT NULL AUTO_INCREMENT,
  `ChargeFee` float(12,0) NOT NULL DEFAULT '0',
  `ConvenienceFee` float(12,0) NOT NULL DEFAULT '0',
  `ConsentFee` float(12,0) NOT NULL DEFAULT '0',
  `Agent` varchar(255) NOT NULL DEFAULT '0',
  `Ip` varchar(255) NOT NULL DEFAULT '0',
  `Institution` varchar(255) NOT NULL DEFAULT '0',
  `ApprovalStatus` varchar(255) NOT NULL DEFAULT '0',
  `IDNumber` varchar(255) NOT NULL DEFAULT '0',
  `EcitizenBillRef` varchar(255) NOT NULL DEFAULT '0',
  `Status` varchar(255) NOT NULL DEFAULT '0',
  `DateCreated` datetime NOT NULL,
  `ChargeID` varchar(50) NOT NULL,
  `Registry` varchar(20) NOT NULL,
  `RegistrationSectionID` int(255) NOT NULL,
  `RegistryID` int(255) NOT NULL,
  `ParcelNumber` int(255) NOT NULL,
  `advoTelNumber` int(255) NOT NULL,
  `TenureID` varchar(50) NOT NULL,
  `TransactionDescription` text NOT NULL,
  `ReferenceNumber` varchar(255) NOT NULL,
  `advoFName` varchar(255) NOT NULL,
  `advoMName` varchar(255) NOT NULL,
  `advoLName` varchar(255) NOT NULL,
  `advoPinNumber` varchar(255) NOT NULL,
  `advoPostalAddress` varchar(255) NOT NULL,
  `advoEmail` varchar(255) NOT NULL,
  `advoLawFirm` varchar(255) NOT NULL,
  `advoPFNumber` varchar(255) NOT NULL,
  `advoIDNumber` int(255) NOT NULL,
  `advoNationality` varchar(255) NOT NULL,
  `AttachedDocument` varchar(50) NOT NULL,
  `Applicant` varchar(255) NOT NULL,
  `TransactionAmount` int(255) NOT NULL,
  `TransactionTerm` varchar(255) NOT NULL,
  `ChargeReference` varchar(255) NOT NULL,
  `ChargeFile` varchar(255) NOT NULL,
  `TransactionTermPeriod` varchar(255) NOT NULL,
  `LRNumber` varchar(255) NOT NULL,
  `AreaSize` text NOT NULL,
  `County` text NOT NULL,
  `TransactionNature` varchar(255) NOT NULL,
  `DocID` varchar(255) NOT NULL,
  `Act` varchar(255) NOT NULL,
  `TransactionID` varchar(255) NOT NULL,
  `TypeOfTransaction` int(255) NOT NULL,
  `RateClearanceNumber` varchar(255) NOT NULL,
  `RateClearanceCertificateFile` varchar(255) NOT NULL,
  `Rate` float NOT NULL,
  `Rent` float NOT NULL,
  `AmountPayable` float NOT NULL,
  `UserID` int(255) NOT NULL,
  `ModifiedBy` int(255) NOT NULL,
  `LockedBy` tinytext,
  `Description` varchar(50) DEFAULT NULL,
  `PaymentStatus` varchar(50) DEFAULT NULL,
  `PrincipalAmount` varchar(255) NOT NULL,
  `InterestRatePA` float NOT NULL,
  `EntryNumber` varchar(255) NOT NULL,
  `PrepaymentByDate` date NOT NULL,
  PRIMARY KEY (`RowID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- Dumping data for table data2.lmais_charge: ~0 rows (approximately)
/*!40000 ALTER TABLE `lmais_charge` DISABLE KEYS */;
/*!40000 ALTER TABLE `lmais_charge` ENABLE KEYS */;

-- Dumping structure for table data2.lmais_discharge
CREATE TABLE IF NOT EXISTS `lmais_discharge` (
  `RowID` int(11) NOT NULL AUTO_INCREMENT,
  `DischargeFee` float(12,0) NOT NULL DEFAULT '0',
  `ConvenienceFee` float(12,0) NOT NULL DEFAULT '0',
  `Agent` varchar(255) NOT NULL DEFAULT '0',
  `Ip` varchar(255) NOT NULL DEFAULT '0',
  `Institution` varchar(255) NOT NULL DEFAULT '0',
  `EcitizenBillRef` varchar(255) NOT NULL DEFAULT '0',
  `Status` varchar(255) NOT NULL DEFAULT '0',
  `DateCreated` datetime NOT NULL,
  `DischargeID` varchar(50) NOT NULL,
  `Registry` varchar(20) NOT NULL,
  `RegistrationSectionID` int(255) NOT NULL,
  `RegistryID` int(255) NOT NULL,
  `ParcelNumber` int(255) NOT NULL,
  `advoTelNumber` int(255) NOT NULL,
  `TenureID` varchar(50) NOT NULL,
  `TransactionDescription` text NOT NULL,
  `ReferenceNumber` varchar(255) NOT NULL,
  `advoFName` varchar(255) NOT NULL,
  `advoMName` varchar(255) NOT NULL,
  `advoLName` varchar(255) NOT NULL,
  `advoPinNumber` varchar(255) NOT NULL,
  `advoPostalAddress` varchar(255) NOT NULL,
  `advoEmail` varchar(255) NOT NULL,
  `advoLawFirm` varchar(255) NOT NULL,
  `advoPFNumber` varchar(255) NOT NULL,
  `advoIDNumber` int(255) NOT NULL,
  `advoNationality` varchar(255) NOT NULL,
  `AttachedDocument` varchar(50) NOT NULL,
  `Applicant` varchar(255) NOT NULL,
  `TransactionAmount` int(255) NOT NULL,
  `TransactionTerm` varchar(255) NOT NULL,
  `ChargeReference` varchar(255) NOT NULL,
  `ChargeFile` varchar(255) NOT NULL,
  `TransactionTermPeriod` varchar(255) NOT NULL,
  `LRNumber` varchar(255) NOT NULL,
  `AreaSize` text NOT NULL,
  `County` text NOT NULL,
  `TransactionNature` varchar(255) NOT NULL,
  `DocID` varchar(255) NOT NULL,
  `Act` varchar(255) NOT NULL,
  `TransactionID` varchar(255) NOT NULL,
  `TypeOfTransaction` int(255) NOT NULL,
  `RateClearanceNumber` varchar(255) NOT NULL,
  `RateClearanceCertificateFile` varchar(255) NOT NULL,
  `Rate` float NOT NULL,
  `Rent` float NOT NULL,
  `AmountPayable` float NOT NULL,
  `UserID` int(255) NOT NULL,
  `LockedBy` tinytext,
  `Description` varchar(50) DEFAULT NULL,
  `PaymentStatus` varchar(50) DEFAULT NULL,
  `PrincipalAmount` varchar(255) NOT NULL,
  `InterestRatePA` float NOT NULL,
  `EntryNumber` varchar(255) NOT NULL,
  `PrepaymentByDate` date NOT NULL,
  PRIMARY KEY (`RowID`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- Dumping data for table data2.lmais_discharge: ~0 rows (approximately)
/*!40000 ALTER TABLE `lmais_discharge` DISABLE KEYS */;
/*!40000 ALTER TABLE `lmais_discharge` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
