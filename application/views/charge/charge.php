<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
<!-- begin #content -->
<script type="text/javascript">
    var step = <?php echo array_key_exists('step', $input) ? $input['step'] : -1; ?>;
    var inlineJs = {
        baseUrl: "<?php echo base_url(); ?>"
    }
</script>
<div id="content" class="content">
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <?php
                    if (isset($sessioned_action) && ($sessioned_action == 'updating')) {
                        $amount = number_format($sessioned_balance);
                    } else {
                        $amount = $input['step'] == 1 ? $convenience_model->ServiceAmount : number_format($sessioned_charge['AmountPayable']);
                    }//E# if else statement
                    ?>

                    <h4 class="panel-title"><span class="pull-left">REGISTRATION OF CHARGE <?php echo $sessioned_charge ? 'FOR LR NUMBER <b>' . $sessioned_charge['LRNumber'] . '</b>' : '' ?> 
                            <?php if ($sessioned_user['UserType'] == 'user'): ?>
                                <b><?php echo $sessioned_charge ? $sessioned_charge['LRNumber'] : '' ?></b></span><span data-toggle="tooltip" data-placement="top" title="After reviewing your application, kindly pay KES <?php echo $amount; ?>" class="pull-right label label-danger">KES <?php echo $amount; ?></span>
                        <?php endif; ?>
                    </h4>
                </div>
                <!--   <h4 class="panel-title">Registration for Charge</h4>
              </div> -->
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <div id="wizard">
                        <ol class="bwizard-steps clearfix clickable" role="tablist">
                            <li role="tab" aria-selected="true" class="<?php echo ($input['step'] == 1) ? 'active' : ''; ?>" style="z-index: 8;"><span class="label badge-inverse">1</span><a>
                                    Consent Validity
                                </a></li>
                            <li role="tab" aria-selected="false"class="<?php echo ($input['step'] == 2) ? 'active' : ''; ?>" style="z-index: 5;"><span class="label">3</span><a>
                                    Property Details
                                </a></li>
                            <li role="tab" aria-selected="false"class="<?php echo ($input['step'] == 3) ? 'active' : ''; ?>" style="z-index: 5;"><span class="label">3</span><a>
                                    Charge Details
                                </a></li>
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 4) ? 'active' : ''; ?>" style="z-index: 2;"><span class="label">6</span><a>
                                    Review Application
                                </a></li>
                            <?php if ($sessioned_user['UserType'] == 'user'): ?>
                                <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 5) ? 'active' : ''; ?>" style="z-index: 1;"><span class="label">7</span><a>
                                        Payment 
                                    </a></li>
                            <?php endif; ?>
                        </ol>
                        <?php if ($input['step'] == 1): ?>
                            <!-- begin wizard step-1 -->
                            <div class="wizard-step-1">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the first step of 7 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-8" style="padding-top: 38px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Validate Consent</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open('charge/check_consent_validity', array('data-parsley-validate' => 'true', 'name' => 'form-wizard',)); ?>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-5">
                                                                    <label class="control-label">
                                                                        Consent Reference Number
                                                                    </label>
                                                                    <input type="text"  placeholder="Consent Reference Number" class="form-control" id="ConsentReferenceNumber" name="TransactionID" value="<?php echo $sessioned_charge ? $sessioned_charge['RowID'] : ''; ?>" required>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-">
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Validate Consent <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>

                            </div>
                            <!-- end wizard step-1 -->

                        <?php elseif ($input['step'] == 2): ?>
                            <div class="wizard-step-6">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the sixth step of 7 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8" style="padding-top: 38px; overflow-y:scroll;height: 300px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Property Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open_multipart('charge/review_property', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <table class="table table-bordered table-hover">
                                                                    <tr>
                                                                        <td>Registry</td>
                                                                        <td><?php echo $registry_model ? $registry_model->Name : $sessioned_charge['RegistryID']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Registration Section</td>
                                                                        <td><?php echo $registration_section_model ? $registration_section_model->Name : $sessioned_charge['RegistrationSectionID']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Parcel Number</td>
                                                                        <td><?php echo $sessioned_charge['ParcelNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>LR Number</td>
                                                                        <td><?php echo $sessioned_charge['LRNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Tenure</td>
                                                                        <td><?php echo $tenure_model ? $tenure_model->Name : $sessioned_charge['Act']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Charge Amount</td>
                                                                        <td><?php echo $sessioned_charge['AmountPayable']; ?></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/charge/apply_charge?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Continue <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>
                            </div>
                        <?php elseif ($input['step'] == 3) : ?>
                            <div class="wizard-step-3">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the third step of 7 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8" style="padding-top: 38px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Charge Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open('Charge/save_transaction_details', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">
                                                                            Loan Type
                                                                        </label>
                                                                        <select name="ChargeTypeID" id="ChargeTypeID" class="form-control" required>
                                                                            <option value="">Select</option>

                                                                            <?php if (is_array($charge_types)): ?>
                                                                                <?php foreach ($charge_types as $single_charge): ?>
                                                                                    <option value="<?php echo $single_charge->RowID; ?>" <?php echo ($sessioned_charge && $sessioned_charge['ChargeTypeID'] == $single_charge->RowID) ? 'selected="selected"' : ''; ?>><?php echo $single_charge->ItemName; ?></option>
                                                                                <?php endforeach; ?>
                                                                            <?php else: ?>
                                                                                <option value="0">No data</option>
                                                                            <?php endif; ?>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">
                                                                            Principal Amount
                                                                        </label>
                                                                        <input type="number" name="PrincipalAmount" value="<?php echo $sessioned_charge ? $sessioned_charge['PrincipalAmount'] : ''; ?>" placeholder="Principal Amount" id="form-field-9" class="form-control" required>
                                                                    </div>
                                                                </div>


                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">
                                                                            Interest Rate Per Annum
                                                                        </label>
                                                                        <input type="number" name="InterestRate" value="<?php echo $sessioned_charge ? $sessioned_charge['InterestRate'] : ''; ?>" placeholder="Interest Rate Per Annum" id="form-field-9" class="form-control" required>
                                                                    </div>

                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">
                                                                            Repaid By Date (MM/DD/YYYY) (Click dropdown arrow to open date picker)
                                                                        </label>
                                                                        <input type="date" name="RepaidByDate" value="<?php echo $sessioned_charge ? date_format(date_create($sessioned_charge['RepaidByDate']), 'Y-m-d') : ''; ?>" placeholder="Prepayment By Date" id="form-field-9" class="form-control" required>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <p>
                                                                        *Terms &amp; Conditions.
                                                                    </p>
                                                                </div>
                                                                <!--  <div class="col-md-">
                                                                     <button class="btn btn-sm btn-success" type="submit">
                                                                         Save progress <i class="fa fa-arrow-circle-right"></i>
                                                                     </button>
                                                                 </div> -->

                                                                <div class="col-md-10">
                                                                    <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/charge/apply_charge?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                    <button class="btn btn-sm btn-success" type="submit">
                                                                        Continue <i class="fa fa-arrow-circle-right"></i>
                                                                    </button>
                                                                </div>


                                                            </div>
                                                            <?php echo form_close(); ?>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- end row -->
                                </fieldset>
                            </div>
                        <?php elseif ($input['step'] == 4): ?>
                            <div class="wizard-step-6">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the sixth step of 7 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8" style="padding-top: 38px; overflow-y:scroll;height: 300px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Review your Charge application</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open_multipart('Charge/review_application', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <table class="table table-bordered table-hover">
                                                                    <tr>
                                                                        <td>Registry</td>
                                                                        <td><?php echo $registry_model ? $registry_model->Name : $sessioned_charge['RegistryID']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Registration Section</td>
                                                                        <td><?php echo $registration_section_model ? $registration_section_model->Name : $sessioned_charge['RegistrationSectionID']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Parcel Number</td>
                                                                        <td><?php echo $sessioned_charge['ParcelNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>LR Number</td>
                                                                        <td><?php echo $sessioned_charge['LRNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Tenure</td>
                                                                        <td><?php echo $tenure_model ? $tenure_model->Name : $sessioned_charge['Tenure']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Loan Type</td>
                                                                        <td><?php echo $charge_type_model ? $charge_type_model->ItemName : $sessioned_charge['ChargeTypeID']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Principal Amount</td>
                                                                        <td><?php echo $sessioned_charge['PrincipalAmount']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Interest Rate (PA)</td>
                                                                        <td><?php echo $sessioned_charge['InterestRate']; ?> %</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Repaid By Date</td>
                                                                        <td><?php echo date_format(date_create($sessioned_charge['RepaidByDate']), 'd/m/Y'); ?></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/charge/apply_charge?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Confirm Charge <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>
                            </div> 

                        <?php elseif ($input['step'] == 5) : ?>
                            <div class="wizard-step-7">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="panel panel-default panel-blog panel-checkout">
                                            <div class="panel-body">
                                                <ul class="blog-meta mb5">
                                                    <!--<li>Jan 09, 2015</li>-->
                                                </ul>
                                                <?php if (array_key_exists('step', $input) && $input['step'] == 5): ?>
                                                    <div style="background-color: #FFF;">
                                                        <?php
                                                        $apiClientId = '13';
                                                        $amount = $sessioned_charge['AmountPayable'];
                                                        // $amount = '10';
                                                        $serviceId = '29';
                                                        $idNumber = $sessioned_user['IDNumber'];
                                                        $currency = 'KES';
                                                        $billRef = $sessioned_charge['EcitizenBillRef'];
                                                        $serviceDesc = $sessioned_charge['Description'];
                                                        $clientName = $sessioned_user['FirstName'] . ' ' . $sessioned_user['LastName'];
                                                        $key = 'TYV0FUD0BY';
                                                        $secret = 'BGI0ZTSPL2WX0X6';
                                                        $payload = "{$apiClientId}{$amount}{$serviceId}{$idNumber}{$currency}{$billRef}{$serviceDesc}{$clientName}{$secret}";
                                                        $hash = base64_encode(hash_hmac("sha256", $payload, $key));
                                                        ?>
                                                        <form id="idPaymentForm" method="post" action="https://pesaflow.ecitizen.go.ke/PaymentAPI/iframev2.1.php" target="my_frame">
                                                            <input type="hidden" name="apiClientID" value="13" />
                                                            <input type="hidden" name="secureHash" value="<?php echo $hash; ?>" />
                                                            <input type="hidden" name="billDesc" value="<?php echo $sessioned_charge['Description']; ?>" />
                                                            <input type="hidden" name="billRefNumber" value="<?php echo $billRef; ?>" />
                                                            <input type="hidden" name="currency" value="KES" />
                                                            <input type="hidden" name="serviceID" value="29" />
                                                            <input type="hidden" name="clientMSISDN" value="<?php echo $sessioned_user['Phone']; ?>" />
                                                            <input type="hidden" name="clientName" value="<?php echo $clientName; ?>" />
                                                            <input type="hidden" name="clientIDNumber" value="<?php echo $sessioned_user['IDNumber']; ?>" />
                                                            <input type="hidden" name="clientType" value="citizen" />
                                                            <input type="hidden" name="clientEmail" value="<?php echo $sessioned_user['Email']; ?>" />
                                                            <input type="hidden" name="callBackURLOnSuccess" value="<?php echo base_url(); ?>/Charge/payment_completed?bill_ref=<?php echo $billRef; ?>" />
                                                            <input type="hidden" name="callBackURLOnFail" value="<?php echo base_url(); ?>/charge/apply_charge?step=6" />
                                                            <input type="hidden" name="notificationURL" value="<?php echo base_url(); ?>/charge/apply_charge?step=6" />
                                                            <input type="hidden" name="amountExpected" value="<?php echo $amount; ?>" />
                                                        </form>
                                                        <iframe width="100%" height="600px" style="border: none;" name="my_frame"></iframe>
                                                        <script type="text/javascript">
                                                            document.getElementById('idPaymentForm').submit();
                                                        </script>
                                                    </div>
                                                    <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/charge/apply_charge?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                <?php endif; ?>
                                            </div><!-- panel-body -->
                                        </div>
                                    </div>

                                    <!-- end row -->
                                </fieldset>
                            </div>
                        <?php endif; ?>
                        <!-- end wizard step-5 -->


                    </div>

                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<!-- end #content -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->


<!-- Add Seller Individual and Company-->
<div id="idAddChargeApplicantModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('charge/save_seller', array('data-parsley-validate' => 'true', 'id' => 'idSaveChargeApplicantForm')); ?>
        <input type="hidden" name="ChargeID" value="<?php echo $sessioned_charge ? $sessioned_charge['RowID'] : ''; ?>"/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Charge Applicant</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idApplicantAlert" class="alert alert-success hidden"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                ID Number
                            </label>
                            <input id="idChargeApplicantIdNumber"  type="number" name="Identification" value="" placeholder="ID Number/Passport" id="form-field-9" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <button id="idChargeApplicantGetDetails" class="btn btn-primary" style="margin-top: 20px;" disabled>Validate</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Is Primary Contact Person?
                            </label>
                            <br>
                            <label><input type="radio" name="IsContactPerson" value="Y"  checked=""/> Yes</label>
                            <label><input type="radio" name="IsContactPerson" value="N"/> No</label>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                First Name
                            </label>
                            <input id="idApplicantFirstName" type="text"  name="FirstName" value="" placeholder="First Name" id="form-field-9" class="form-control" disabled >
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Middle Name
                            </label>
                            <input id="idApplicantMiddleName" type="text" name="MiddleName" value="" placeholder="Middle Name" id="form-field-10" class="form-control" disabled >
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input id="idApplicantLastName" type="text" name="LastName" value="" placeholder="Last Name" id="form-field-11" class="form-control" disabled required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                KRA PIN Number
                            </label>
                            <input  id="idPinNumber" type="text" name="PinNumber" value="" placeholder="KRA PIN Number" id="form-field-11" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Mobile Number
                            </label>
                            <input  id="idApplicantMobileNumber" type="number" name="MobileNumber" value="" placeholder="Mobile Number" id="form-field-10" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input  id="idApplicantEmailAddress" type="email" name="EmailAddress" value="" placeholder="Email" id="form-field-11" class="form-control" required>

                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Postal Address
                            </label>
                            <input  id="idApplicantAddress" type="text" name="Address" value="" placeholder="Address" id="form-field-11" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label class="control-label">
                                Nationality
                            </label>

                            <select id="idApplicantNationality" name="CountryID" class="form-control" required>
                                <option value="">Select</option>
                                <?php
                                $Countries = $this->Countrymodel->GetAllCountries();
                                if (is_array($Countries)) {
                                    foreach ($Countries as $Country) {
                                        ?>
                                        <option value="<?= $Country->RowID; ?>"><?= $Country->CountryName; ?></option>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <option value="0">No data</option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idSaveChargeApplicantButton" type="button" class="btn btn-primary" type="submit">
                    Save changes
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<div id="idDeleteChargeApplicantModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('charge/delete_seller', array('data-parsley-validate' => 'true', 'id' => 'idDeleteChargeApplicantForm')); ?>
        <input id="idDeleteApplicantID" type="hidden" name="SellerID" value=""/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Applicant</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idBuyerAlert" class="alert alert-success hidden">Applicant deleted</p>
                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idDeleteChargeButton" type="button" class="btn btn-primary" type="submit">
                    Delete Applicant
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>


<div id="idAddChargeCompanyModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('charge/save_seller_company', array('data-parsley-validate' => 'true', 'id' => 'idSaveChargeCompanyForm')); ?>
        <input type="hidden" name="ChargeID" value="<?php echo $sessioned_charge ? $sessioned_charge['ChargeID'] : ''; ?>"/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Charge Company</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idSellerCompanyAlert" class="alert alert-success hidden"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Is Primary Contact Person?
                            </label>
                            <br>
                            <label><input type="radio" name="IsContactPerson" value="Y"  checked=""/> Yes</label>
                            <label><input type="radio" name="IsContactPerson" value="N"/> No</label>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Contact Person's First Name
                            </label>
                            <input id="idSellerCompanyFirstName" type="text"  name="FirstName" value="" placeholder="First Name" id="form-field-9" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Contact Person's Middle Name
                            </label>
                            <input id="idSellerCompanyMiddleName" type="text" name="MiddleName" value="" placeholder="Middle Name" id="form-field-10" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label>Contact Person's Last Name</label>
                                <input id="idSellerCompanyLastName" type="text" name="LastName" value="" placeholder="Last Name" id="form-field-11" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Contact Person's Mobile Number
                            </label>
                            <input  id="idSellerCompanyMobileNumber" type="number" name="MobileNumber" value="" placeholder="Mobile Number" id="form-field-10" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Contact Person's  Email Address</label>
                            <input  id="ididSellerCompanyEmailAddress" type="email" name="EmailAddress" value="" placeholder="Email" id="form-field-11" class="form-control" required>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Company's Name
                            </label>
                            <input  id="idSellerCompanyMobileNumber" type="text" name="CompanyName" value="" placeholder="Company's Name" id="form-field-10" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's Registration Number
                            </label>
                            <input  id="idSellerCompanyAddress" type="text" name="CompanyRegistrationNumber" value="" placeholder="Company's Registration Number" id="form-field-11" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's Phone
                            </label>
                            <input  id="idSellerCompanyAddress" type="number" name="CompanyPhone" value="" placeholder="Company's Phone" id="form-field-11" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's Email
                            </label>
                            <input  id="idSellerCompanyAddress" type="text" name="CompanyEmail" value="" placeholder="Company's Email" id="form-field-11" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's Postal Address
                            </label>
                            <input  id="idSellerCompanyAddress" type="text" name="CompanyAddress" value="" placeholder="Company's Address" id="form-field-11" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's KRA PIN Number
                            </label>
                            <input  id="idSellerCompanyAddress" type="text" name="CompanyPinNumber" value="" placeholder="Company's Pin Number" id="form-field-11" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Share (Percentage)
                            </label>
                            <div class="input-group">
                                <input type="number" class="form-control" placeholder="Percentage share" name="Share" required>
                                <span class="input-group-addon" id="basic-addon2">%</span>
                            </div>                  
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's Nationality
                            </label>

                            <select id="idSellerCompanyNationality" name="CountryID" class="form-control" required>
                                <option value="">Select</option>
                                <?php
                                $Countries = $this->Countrymodel->GetAllCountries();
                                if (is_array($Countries)) {
                                    foreach ($Countries as $Country) {
                                        ?>
                                        <option value="<?= $Country->RowID; ?>"><?= $Country->CountryName; ?></option>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <option value="0">No data</option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idSaveChargeCompanyButton" type="button" class="btn btn-primary" type="submit">
                    Save changes
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>




<div id="idDeleteChargeCompanyModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('charge/delete_seller_company', array('data-parsley-validate' => 'true', 'id' => 'idDeleteChargeCompanyForm')); ?>
        <input id="idDeleteApplicantID" type="hidden" name="SellerID" value=""/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Charge Company</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idBuyerAlert" class="alert alert-success hidden">Charge Company Deleted</p>
                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idDeleteChargeCompanyButton" type="button" class="btn btn-primary" type="submit">
                    Delete Charge Company
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>






<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/parsley/dist/parsley.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-wizards-validation.demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/apps.min.js?time=<?php echo time(); ?>"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<script></script>
<script>
                                                            $("#County").change(function (evt) {
                                                                var ActID = document.getElementById('Acts').value;
                                                                var CountyID = document.getElementById('County').value;
                                                                var select = $("#County");
                                                                var selectData = select.serialize();
                                                                var ajax = $.ajax({
                                                                    dataType: "json",
                                                                    type: 'POST',
                                                                    url: "<?php echo base_url(); ?>Transfer/GetRegistriesMatch/" + ActID + "/" + CountyID,
                                                                    data: selectData
                                                                });
                                                                ajax.done(function (response) {
                                                                    $("#testurl").html(response);
                                                                    var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                                    for (var key in response) {
                                                                        if (response.hasOwnProperty(key)) {
                                                                            //alert(key);
                                                                            //alert(response[key].RowID);
                                                                            //alert(response[key].NameOfRoad);
                                                                            listItems += "<option value='" + response[key].Registres + "'>" + response[key].Registries + "</option>";
                                                                        }
                                                                    }
                                                                    $("#Registry").html(listItems);
                                                                });
                                                                ajax.fail(function () {

                                                                });
                                                            });

                                                            $("#towntype").change(function (evt) {
                                                                var townTypeID = document.getElementById('towntype').value;
                                                                var select = $("#towntype");
                                                                var selectData = select.serialize();
                                                                var ajax = $.ajax({
                                                                    dataType: "json",
                                                                    type: 'POST',
                                                                    url: "http://localhost/lmais/start/GetTownNamesMatchTownType/" + townTypeID,
                                                                    data: selectData
                                                                });
                                                                ajax.done(function (response) {
                                                                    var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                                    for (var key in response) {
                                                                        if (response.hasOwnProperty(key)) {
                                                                            //alert(key);
                                                                            //alert(response[key].RowID);
                                                                            //alert(response[key].NameOfRoad);
                                                                            listItems += "<option value='" + response[key].NameOfTown + "'>" + response[key].NameOfTown + "</option>";
                                                                        }
                                                                    }
                                                                    $("#townNames").html(listItems);
                                                                });
                                                                ajax.fail(function () {

                                                                });
                                                            });

</script>

<script>
    $(document).ready(function () {
        App.init();
        FormWizardValidation.init();
    });
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>