<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
<!-- begin #content -->
<script type="text/javascript">
    var step = <?php echo array_key_exists('step', $input) ? $input['step'] : -1; ?>;
    var inlineJs = {
        baseUrl: "<?php echo base_url(); ?>"
    }
</script>
<div id="content" class="content">
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <?php
                    if (isset($sessioned_action) && ($sessioned_action == 'updating')) {
                        $amount = number_format($sessioned_balance);
                    } else {
                        $amount = $input['step'] == 1 ? $convenience_model->ServiceAmount : number_format($sessioned_rent['AmountPayable']);
                    }//E# if else statement
                    ?>
                    <h4 class="panel-title"><span class="pull-left">PAY LAND RENT <?php echo $sessioned_rent ? 'FOR LR NUMBER' : '' ?> <b><?php echo $sessioned_rent ? $sessioned_rent['LRNumber'] : '' ?></b></span><span data-toggle="tooltip" data-placement="top" title="After reviewing your application, kindly pay KES <?php echo $amount; ?>" class="pull-right label label-danger">KES <?php echo $amount; ?></span></h4>
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <div id="wizard">
                        <ol class="bwizard-steps clearfix clickable" role="tablist">
                            <li role="tab" aria-selected="true" class="<?php echo ($input['step'] == 1) ? 'active' : ''; ?>" style="z-index: 7;"><span class="label badge-inverse">1</span><a>
                                    Property Details
                                </a></li>
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 2) ? 'active' : ''; ?>" style="z-index: 3;"><span class="label">2</span><a>
                                    Land Rent
                                </a></li>
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 3) ? 'active' : ''; ?>" style="z-index: 2;"><span class="label">3</span><a>
                                    Review application
                                </a></li>
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 4) ? 'active' : ''; ?>" style="z-index: 1;"><span class="label">4</span><a>
                                    Payment 
                                </a></li>
                        </ol>
                        <?php if ($input['step'] == 1): ?>
                            <!-- begin wizard step-1 -->
                            <div class="wizard-step-1">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the first step of 5 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, applicant id number, rent interest and the reason for rent</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-8" style="padding-top: 38px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Check Availability</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open('rent/check_property_details', array('data-parsley-validate' => 'true', 'name' => 'form-wizard',)); ?>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-6">
                                                                    <label class="control-label">
                                                                        Land Registration or Title Deed Number
                                                                    </label>
                                                                    <input type="text"  placeholder="Land Registration Number" class="form-control" id="ParcelNo" name="Title" value="<?php echo $sessioned_rent ? $sessioned_rent['LRNumber'] : ''; ?>" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-">
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Check Property and continue <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>
                            </div>
                            <!-- end wizard step-1 -->
                            <!-- begin wizard step-2 -->

                        <?php elseif ($input['step'] == 2) : ?>
                            <div class="wizard-step-6">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the fifth step of 7 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-8" style="padding-top: 38px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Land Rent for <?php echo $sessioned_rent['LRNumber']; ?></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open('rent/rent_clearance', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <table class="table table-condensed table-bordered table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>Description</th>
                                                                            <th>Amount (KES)</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <th scope="row">1</th>
                                                                            <td>Annual Rent</td>
                                                                            <td><?php echo number_format($sessioned_rent['RentPrincipal'], 2); ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row">2</th>
                                                                            <td>Rent Interest</td>
                                                                            <td><?php echo number_format($sessioned_rent['RentInterest'], 2); ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row">3</th>
                                                                            <td>Convinience Fee</td>
                                                                            <td><?php echo number_format($sessioned_rent['ConvenienceFee'], 2); ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row"><h4>4</h4></th>
                                                                            <td><h4>Total</h4></td>
                                                                            <td><h4><?php echo number_format($sessioned_rent['AmountPayable'], 2); ?></h4></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <?php if ($sessioned_rent['Rent']): ?>
                                                                    <h4>Rent Schedule</h4>
                                                                    <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#schedule">Click to view schedule</button>
                                                                    <table id="schedule" class="collapse table table-condensed table-bordered" style="margin-top: 10px;">
                                                                            <thead>
                                                                            <th>#</th>
                                                                            <th>Description</th>
                                                                            <th>Amount (KES)</th>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                $index = 1;
                                                                                $rent_data = json_decode($sessioned_rent['RentData'], true);
                                                                                ?>
                                                                                <?php if (array_key_exists('items', $rent_data)): ?>
                                                                                    <?php foreach ($rent_data['items'] as $single_rent): ?>
                                                                                        <tr>
                                                                                            <td><?php echo $index; ?></td>
                                                                                            <td><?php echo $single_rent['description']; ?></td>
                                                                                            <td><?php echo number_format($single_rent['amount'], 2); ?></td>
                                                                                        </tr>
                                                                                        <?php
                                                                                        $index++;
                                                                                        ?>
                                                                                    <?php endforeach; ?>
                                                                                <?php endif; ?>
                                                                            </tbody>
                                                                        </table>
                                                                        <?php else: ?>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-10">
                                                                        <p>
                                                                            *Terms &amp; Conditions.
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-10">
                                                                        <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/rent/apply_rent?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                        <button class="btn btn-sm btn-success" type="submit">
                                                                            Continue <i class="fa fa-arrow-circle-right"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <?php echo form_close(); ?>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end row -->
                                            </fieldset>

                                        </div>
                                    <?php elseif ($input['step'] == 3): ?>
                                        <div class="wizard-step-8">
                                            <fieldset>
                                                <div class="col-md-12">
                                                    <div class="col-md-4">

                                                        <div class="panel panel-primary">
                                                            <div class="panel-heading">

                                                                <h4 class="panel-title">Notes </h4>
                                                            </div>
                                                            <div class="panel-body bg-blue text-white">
                                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the sixth step of 7 to complete your application. </li><br/>
                                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8" style="padding-top: 38px; overflow-y:scroll;height: 300px;">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                <h4 class="modal-title">Review your transfer application</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div id="panel_edit_account" class="tab-pane in active">
                                                                    <?php echo form_open_multipart('rent/review_application', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <table class="table table-bordered table-hover">
                                                                                <td>LR Number</td>
                                                                                <td><?php echo $sessioned_rent['LRNumber']; ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Rent arrears</td>
                                                                                    <td><h4>KES <?php echo number_format($sessioned_rent['AmountPayable']); ?></h4></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <div class="col-md-10">
                                                                            <p>
                                                                                *Terms &amp; Conditions.
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-10">
                                                                            <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/rent/apply_rent?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                            <button class="btn btn-sm btn-success" type="submit">
                                                                                Make payment <i class="fa fa-arrow-circle-right"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                    <?php echo form_close(); ?>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end row -->
                                            </fieldset>
                                        </div>
                                    <?php elseif ($input['step'] == 4) : ?>
                                        <div class="wizard-step-5">
                                            <fieldset>
                                                <div class="col-md-12">
                                                    <div class="panel panel-default panel-blog panel-checkout">
                                                        <div class="panel-body">
                                                            <ul class="blog-meta mb5">
                                                                <!--<li>Jan 09, 2015</li>-->
                                                            </ul>
                                                            <?php if (array_key_exists('step', $input) && $input['step'] == 4): ?>
                                                                <div style="background-color: #FFF;">
                                                                    <?php
                                                                    $apiClientId = '13';
                                                                    $amount = $sessioned_rent['AmountPayable'];
                                                                     $amount = '10';
                                                                    $serviceId = '29';
                                                                    $serviceId = $this->config->item('environment') == 'live' ? $service_model->LiveServiceCode : $service_model->TestServiceCode;
                                                                    
                                                                    $idNumber = $sessioned_user['IDNumber'];
                                                                    $currency = 'KES';
                                                                    $billRef = $sessioned_rent['EcitizenBillRef'];
                                                                    $serviceDesc = $sessioned_rent['Description'];
                                                                    $clientName = $sessioned_user['FirstName'] . ' ' . $sessioned_user['LastName'];
                                                                    $key = 'TYV0FUD0BY';
                                                                    $secret = 'BGI0ZTSPL2WX0X6';

                                                                    $payload = "{$apiClientId}{$amount}{$serviceId}{$idNumber}{$currency}{$billRef}{$serviceDesc}{$clientName}{$secret}";
                                                                    $hash = base64_encode(hash_hmac("sha256", $payload, $key));
                                                                    ?>
                                                                    <form id="idPaymentForm" method="post" action="https://pesaflow.ecitizen.go.ke/PaymentAPI/iframev2.1.php" target="my_frame">
                                                                        <input type="hidden" name="apiClientID" value="13" />
                                                                        <input type="hidden" name="secureHash" value="<?php echo $hash; ?>" />
                                                                        <input type="hidden" name="billDesc" value="<?php echo $sessioned_rent['Description']; ?>" />
                                                                        <input type="hidden" name="billRefNumber" value="<?php echo $billRef; ?>" />
                                                                        <input type="hidden" name="currency" value="KES" />
                                                                        <input type="hidden" name="serviceID" value="29" />
                                                                        <input type="hidden" name="clientMSISDN" value="<?php echo $sessioned_user['Phone']; ?>" />
                                                                        <input type="hidden" name="clientName" value="<?php echo $clientName; ?>" />
                                                                        <input type="hidden" name="clientIDNumber" value="<?php echo $sessioned_user['IDNumber']; ?>" />
                                                                        <input type="hidden" name="clientType" value="citizen" />
                                                                        <input type="hidden" name="clientEmail" value="<?php echo $sessioned_user['Email']; ?>" />
                                                                        <input type="hidden" name="callBackURLOnSuccess"  value="<?php echo base_url(); ?>/rent/payment_completed?bill_ref=<?php echo $billRef; ?>" />
                                                                        <input type="hidden" name="callBackURLOnFail"  value="<?php echo base_url(); ?>/rent/apply_rent?step=6" />
                                                                        <input type="hidden" name="notificationURL"  value="<?php echo base_url(); ?>/rent/apply_rent?step=6" />
                                                                        <input type="hidden" name="amountExpected" value="<?php echo $amount; ?>" />
                                                                    </form>
                                                                    <iframe width="100%" height="600px" style="border: none;" name="my_frame"></iframe>
                                                                    <script type="text/javascript">
                                                                        document.getElementById('idPaymentForm').submit();
                                                                    </script>
                                                                </div>
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/rent/apply_rent?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                            <?php endif; ?>
                                                        </div><!-- panel-body -->
                                                    </div>
                                                </div>

                                                <!-- end row -->
                                            </fieldset>
                                        </div>
                                    <?php endif; ?>
                                    <!-- end wizard step-5 -->


                                </div>

                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
        <!-- end #content -->

        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
        <!-- end scroll to top btn -->
    </div>
    <!-- end page container -->




    <!-- ================== BEGIN BASE JS ================== -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!--[if lt IE 9]>
            <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
            <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
    <!-- ================== END BASE JS ================== -->

    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="<?php echo base_url(); ?>assets/plugins/parsley/dist/parsley.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/form-wizards-validation.demo.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/apps.min.js?time=<?php echo time(); ?>"></script>
    <!-- ================== END PAGE LEVEL JS ================== -->


    <script></script>
    <script>
                                                                $("#County").change(function (evt) {
                                                                    var ActID = document.getElementById('Acts').value;
                                                                    var CountyID = document.getElementById('County').value;
                                                                    var select = $("#County");
                                                                    var selectData = select.serialize();
                                                                    var ajax = $.ajax({
                                                                        dataType: "json",
                                                                        type: 'POST',
                                                                        url: "<?php echo base_url(); ?>Transfer/GetRegistriesMatch/" + ActID + "/" + CountyID,
                                                                        data: selectData
                                                                    });
                                                                    ajax.done(function (response) {
                                                                        $("#testurl").html(response);
                                                                        var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                                        for (var key in response) {
                                                                            if (response.hasOwnProperty(key)) {
                                                                                //alert(key);
                                                                                //alert(response[key].RowID);
                                                                                //alert(response[key].NameOfRoad);
                                                                                listItems += "<option value='" + response[key].Registres + "'>" + response[key].Registries + "</option>";
                                                                            }
                                                                        }
                                                                        $("#Registry").html(listItems);
                                                                    });
                                                                    ajax.fail(function () {

                                                                    });
                                                                });

                                                                $("#towntype").change(function (evt) {
                                                                    var townTypeID = document.getElementById('towntype').value;
                                                                    var select = $("#towntype");
                                                                    var selectData = select.serialize();
                                                                    var ajax = $.ajax({
                                                                        dataType: "json",
                                                                        type: 'POST',
                                                                        url: "http://localhost/lmais/start/GetTownNamesMatchTownType/" + townTypeID,
                                                                        data: selectData
                                                                    });
                                                                    ajax.done(function (response) {
                                                                        var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                                        for (var key in response) {
                                                                            if (response.hasOwnProperty(key)) {
                                                                                //alert(key);
                                                                                //alert(response[key].RowID);
                                                                                //alert(response[key].NameOfRoad);
                                                                                listItems += "<option value='" + response[key].NameOfTown + "'>" + response[key].NameOfTown + "</option>";
                                                                            }
                                                                        }
                                                                        $("#townNames").html(listItems);
                                                                    });
                                                                    ajax.fail(function () {

                                                                    });
                                                                });

    </script>

    <script>
        $(document).ready(function () {
            App.init();
            //FormWizardValidation.init();
        });
    </script>


    <script>

        $('#advoNationality option').each(function () {
            $(this).attr('value', $(this).text());
        });

    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-53034621-1', 'auto');
        ga('send', 'pageview');

    </script>

</body>
</html>