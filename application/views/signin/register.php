<div id="page-container" class="fade">
    <!-- begin login -->
    <div class="login login-with-news-feed">
        <!-- begin news-feed -->

        <!-- end news-feed -->
        <!-- begin right-content -->
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-5">
                <div class="">
                    <!-- begin login-header -->

                    <div class="login-header">
                        <h4>Dear <?php echo ucwords(strtolower($user['first_name'] . ' ' . $user['surname'])); ?>, kindly complete your registration by filling in the correct details below:</h4>
                    </div>
                    <!-- end login-header -->
                    <!-- begin login-content -->
                    <div class="login-content">
                        <?php echo form_open("start/complete_registration", array('data-parsley-validate' => 'true', 'name' => 'form-wizard')); ?>
                        <input type="hidden" name="IDNumber" value="<?php echo $user['id_number'] ?>">
                        <div class="form-group m-b-20">
                            <input type="text" class="form-control input-lg  parsley-error" name="Pin" placeholder="KRA PIN"  required />
                        </div>
                        <div class="login-buttons">
                            <button type="submit" class="btn btn-success btn-block btn-lg">Complete registration</button>
                        </div>
                        </form>
                    </div>
                    <!-- end login-content -->
                </div>
            </div>
            <div class="col-md-4">
            </div>
        </div>
        <!-- end right-container -->
    </div>
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/js/login-v2.demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function () {
        App.init();
        LoginV2.init();
    });
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
