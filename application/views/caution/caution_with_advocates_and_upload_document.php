<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
<!-- begin #content -->
<script type="text/javascript">
    var step = <?php echo array_key_exists('step', $input) ? $input['step'] : -1; ?>;
    var inlineJs = {
        baseUrl: "<?php echo base_url(); ?>"
    }
</script>
<div id="content" class="content">
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">

                    <h4 class="panel-title">Registration for Caution</h4>
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                      <div id="wizard">
                        <ol class="bwizard-steps clearfix clickable" role="tablist">
                            <li role="tab" aria-selected="true" class="<?php echo ($input['step'] == 1) ? 'active' : ''; ?>" style="z-index: 7;"><span class="label badge-inverse">1</span><a>
                                    Property details
                                </a></li>
                            <li role="tab" aria-selected="true" class="<?php echo ($input['step'] == 2) ? 'active' : ''; ?>" style="z-index: 6;"><span class="label">2</span><a>
                                    Caution Applicant 

                                </a></li>
                            <li role="tab" aria-selected="false"class="<?php echo ($input['step'] == 3) ? 'active' : ''; ?>" style="z-index: 5;"><span class="label">3</span><a>
                                    Caution Details
   
                                </a></li>
                            <li role="tab" aria-selected="false"class="<?php echo ($input['step'] == 4) ? 'active' : ''; ?>" style="z-index: 4;"><span class="label">4</span><a>
                                   Advocate Details

                                </a></li>
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 5) ? 'active' : ''; ?>" style="z-index: 3;"><span class="label">5</span><a>
                                   Upload Document

                                </a></li>
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 6) ? 'active' : ''; ?>" style="z-index: 2;"><span class="label">6</span><a>
                                   Review Caution Application

                                </a></li>
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 7) ? 'active' : ''; ?>" style="z-index: 1;"><span class="label">7</span><a>
                                    Payment 
                                </a></li>
                        </ol>
                        <?php if ($input['step'] == 1): ?>
                        <!-- begin wizard step-1 -->
                        <div class="wizard-step-1">
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="col-md-4">

                                        <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                            <div class="panel-heading">

                                                <h4 class="panel-title">Notes </h4>
                                            </div>
                                            <div class="panel-body bg-blue text-white">
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the first step of 7 to complete your application. </li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8" style="padding-top: 38px;">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Check Availability</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div id="panel_edit_account" class="tab-pane in active">
                                                    <?php echo form_open('caution/check_property_details', array('data-parsley-validate' => 'true', 'name' => 'form-wizard',)); ?>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                    Registry
                                                                </label>
                                                                <select name="RegistryID" id="RegistryID" class="form-control" required>
                                                                    <option value=""></option>

                                                                    <?php
                                                                    $GetRegistries = $this->ParcelsValuationModel->GetRegistries();

                                                                    if (is_array($GetRegistries)) {
                                                                        foreach ($GetRegistries as $Registry) {
                                                                            ?>
                                                                            <option value="<?= $Registry->RowID; ?>"><?= $Registry->Name; ?></option>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <option value="0">No data</option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Registration Section</label>
                                                                    <select name="RegistrationSectionID" onchange="selectOption()" id="RegistrationSectionID" class="form-control" data-parsley-group="wizard-step-1" required/>
                                                                    <option></option>
                                                                    <?php
                                                                    $GetRegistrationSections = $this->Registrymodel->GetRegistrationSections();

                                                                    if (is_array($GetRegistrationSections)) {
                                                                        foreach ($GetRegistrationSections as $Section) {
                                                                            ?>
                                                                            <option value="<?= $Section->RowID; ?>"><?= $Section->Name; ?></option>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <option value="0">No data</option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                    Parcel Number
                                                                </label>
                                                                <input type="text"  placeholder="Parcel Number" class="form-control" id="ParcelNo" name="ParcelNo" value="<?php echo $sessioned_caution ? $sessioned_caution['ParcelNumber'] : ''; ?>" required>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                    Tenure
                                                                </label>
                                                                <select name="NatureOfTitle" id="NatureOfTitle" class="form-control" required>
                                                                    <option value=""></option>

                                                                    <?php
                                                            //$GetRegistries = $this->ParcelsValuationModel->GetRegistries();
                                                                    $Acts = $this->actsModel->GetAllActs();
                                                                    if (is_array($Acts)) {
                                                                        foreach ($Acts as $Act) {
                                                                            ?>
                                                                            <option value="<?= $Act->RowID; ?>"><?= $Act->ItemName; ?></option>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <option value="0">No data</option>
                                                                        <?php
                                                                    }
                                                                    ?>

                                                                </select>
                                                            </div>
                                                        </div>




                                                          </div>

                                                      <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-9">
                                                                <div class="form-group connected-group">

                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label class="control-label">
                                                                                Title Number
                                                                            </label>
                                                                            <input type="text"  placeholder="Title Number" class="form-control" id="LRNumber" name="LRNumber" value="<?php echo $sessioned_caution ? $sessioned_caution['LRNumber'] : ''; ?>"  disabled>
                                                                        </div>

                                                                    </div>
                                                                </div>




                                                            </div>




                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <p>
                                                                *Terms &amp; Conditions.
                                                            </p>
                                                        </div>
                                                        <div class="col-md-">
                                                            <button class="btn btn-sm btn-success" type="submit">
                                                                Check Property and continue <i class="fa fa-arrow-circle-right"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>

                        </div>
                        <!-- end wizard step-1 -->
                        <!-- begin wizard step-2 -->
                        <?php elseif ($input['step'] == 2) : ?>
                        <div class="wizard-step-2">
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="col-md-4">

                                        <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                            <div class="panel-heading">

                                                <h4 class="panel-title">Notes </h4>
                                            </div>
                                            <div class="panel-body bg-blue text-white">
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the second step of 7 to complete your application. </li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8" style="padding-top: 38px;">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Caution Applicant Details</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div id="panel_edit_account" class="tab-pane in active">
                                                    <?php echo form_open('caution/save_seller_continue', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                    <div class="row">
                                                        <div class="col-md-">
                                                            <button id="idAddApplicant" class="btn btn-sm btn-success" type="button">
                                                                <i class="fa fa-user"></i> Add Caution Applicant
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <?php if ($sessioned_sellers): ?>
                                                                <table class="table table-condensed table-responsive">
                                                                    <thead>
                                                                    <th>#</th>
                                                                    <th>Is contact person?</th>
                                                                    <th>First name</th>
                                                                    <th>Middle name</th>
                                                                    <th>Last name</th>
                                                                    <th>Nationality</th>
                                                                    <th>Action</th>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        $index = 1;
                                                                        ?>
                                                                        <?php foreach ($sessioned_sellers as $single_seller): ?>
                                                                            <tr>
                                                                                <td><?php echo $index; ?></td>
                                                                                <td><?php echo $single_seller['IsContactPerson']; ?></td>
                                                                                <td><?php echo $single_seller['FirstName']; ?></td>
                                                                                <td><?php echo $single_seller['MiddleName']; ?></td>
                                                                                <td><?php echo $single_seller['LastName']; ?></td>
                                                                                <td><?php echo $single_seller['CountryID']; ?></td>
                                                                                <td><a href="" class="deleteApplicant" style="color: red;font-size: 16px;" data-id="<?php echo $single_seller['RowID']; ?>"><i class="fa fa-trash-o "></i></a></i></td>
                                                                            </tr>
                                                                            <?php $index++; ?>
                                                                        <?php endforeach; ?>

                                                                    </tbody>
                                                                </table>
                                                            <?php endif; ?>
                                                        </div>        
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <p>
                                                                *Terms &amp; Conditions.
                                                            </p>
                                                        </div>
                                                       <!--  <div class="col-md-">
                                                            <button class="btn btn-sm btn-success" type="submit">
                                                                Save progress <i class="fa fa-arrow-circle-right"></i>
                                                            </button>
                                                        </div> -->
                                                        <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/caution/apply_caution?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Save Applicant and Continue <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                    </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>

                        </div>
  
                      <?php elseif ($input['step'] == 3) : ?>
                        <div class="wizard-step-3">
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="col-md-4">

                                        <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                            <div class="panel-heading">

                                                <h4 class="panel-title">Notes </h4>
                                            </div>
                                            <div class="panel-body bg-blue text-white">
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the third step of 7 to complete your application. </li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8" style="padding-top: 38px;">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Caution Details</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div id="panel_edit_account" class="tab-pane in active">
                                                    <?php echo form_open('caution/save_transaction_details', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12">

                                                            <div class="col-md-6">
                                                               <div class="form-group">
                                                                    <label class="control-label">
                                                                        Relationship (son, daughter, spouce etc.)
                                                                    </label>
                                                                    <input type="text" name="CautionInterest" value="<?php echo $sessioned_caution ? $sessioned_caution['CautionInterest'] : ''; ?>" placeholder="Caution Interest" id="form-field-9" class="form-control" required>
                                                                </div>
                                                                


                                                           </div>


                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">
                                                                       Reasons for caution 
                                                                    </label>
                                                                    <input type="text" name="CautionDescription" value="<?php echo $sessioned_caution ? $sessioned_caution['CautionDescription'] : ''; ?>" placeholder="Caution Description" id="form-field-9" class="form-control" required>
                                                                </div>
                                                            
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <p>
                                                                *Terms &amp; Conditions.
                                                            </p>
                                                        </div>
                                                       <!--  <div class="col-md-">
                                                            <button class="btn btn-sm btn-success" type="submit">
                                                                Save progress <i class="fa fa-arrow-circle-right"></i>
                                                            </button>
                                                        </div> -->

                                                         <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/caution/apply_caution?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Continue <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                        </div>


                                                    </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>

                        </div>

                    <?php elseif ($input['step'] == 4) : ?>
                     <div class="wizard-step-4">
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="col-md-4">

                                        <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                            <div class="panel-heading">

                                                <h4 class="panel-title">Notes </h4>
                                            </div>
                                            <div class="panel-body bg-blue text-white">
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the fourth step of 7 to complete your application. </li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8" style="padding-top: 38px;">
                                        <div class="modal-content">

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title">Advocate Details</h4>
                                            </div>
                                               

                                                <div class="modal-body">
                                                <div id="panel_edit_account" class="tab-pane in active">
                                                    <?php echo form_open('caution/save_advocate_details', array('data-parsley-validate' => 'true', 'name' => 'form-wizard')); ?>

                                                     <div class="row">
                                                        <div class="col-md-12">
                                                            
                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  First  Name
                                                                </label>
                                                                <input type="text"  placeholder="First Name" class="form-control" id="advoFName" name="advoFName" value="<?php echo $sessioned_caution ? $sessioned_caution['advoFName'] : ''; ?>" required/>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  Middle  Name
                                                                </label>
                                                                <input type="text"  placeholder="Middle Name" class="form-control" id="advoMName" name="advoMName" value="<?php echo $sessioned_caution ? $sessioned_caution['advoMName'] : ''; ?>"  required/>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  Last  Name
                                                                </label>
                                                                <input type="text"  placeholder="Last Name" class="form-control" id="advoLName" name="advoLName" value="<?php echo $sessioned_caution ? $sessioned_caution['advoLName'] : ''; ?>"  required/>
                                                            </div>


                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                 KRA Number
                                                                </label>
                                                                <input type="text"  placeholder="Pin Number" class="form-control" id="pinNumber" name="pinNumber" value="<?php echo $sessioned_caution ? $sessioned_caution['advoPinNumber'] : ''; ?>"  required/>
                                                            </div>
                                                           
                                                        </div>

                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            
                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  Postal Address
                                                                </label>
                                                                <input type="text"  placeholder="Postal Address" class="form-control" id="advoPostalAddress" name="advopostalAddress" value="<?php echo $sessioned_caution ? $sessioned_caution['advoPostalAddress'] : ''; ?>"  required/>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  Email
                                                                </label>
                                                                <input type="email"  placeholder="Email" class="form-control" id="advoEmail" name="advoEmail" value="<?php echo $sessioned_caution ? $sessioned_caution['advoEmail'] : ''; ?>" required/>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  Telephone Number
                                                                </label>
                                                                <input type="number"  placeholder="Telephone Number" class="form-control" id="advoTelNumber" name="advoTelNumber" value="<?php echo $sessioned_caution ? $sessioned_caution['advoTelNumber'] : ''; ?>" required/>
                                                            </div>


                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  Law Firm
                                                                </label>
                                                                <input type="text"  placeholder="Law Firm" class="form-control" id="advoLawFirm" name="advoLawFirm" value="<?php echo $sessioned_caution ? $sessioned_caution['advoLawFirm'] : ''; ?>"  required/>
                                                            </div>
                                                           
                                                        </div>

                                                    </div>

                                                     <div class="row">
                                                        <div class="col-md-12">
                                                            
                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  PF Number
                                                                </label>
                                                                <input type="text"  placeholder="PF Number" class="form-control" id="advoPFNumber" name="advoPFNumber" value="<?php echo $sessioned_caution ? $sessioned_caution['advoPFNumber'] : ''; ?>"  required/>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  ID Number
                                                                </label>
                                                                <input type="number"  placeholder="ID Number" class="form-control" id="advoIDNumber" name="advoIDNumber" value="<?php echo $sessioned_caution ? $sessioned_caution['advoIDNumber'] : ''; ?>"  required/>
                                                            </div>

                                                            <div class="col-md-3">
                                                                
                                                         <label class="control-label">
                                                                Nationality
                                                            </label>

                                                            <!-- <input type="text"  placeholder="Nationality" class="form-control" id="advoNationality" name="advoNationality" required/> -->
                                                              <select type="text" id="advoNationality" name="advoNationality" class="form-control" required>
                                                                <option value="">Select</option>
                                                                <?php
                                                                $Countries = $this->Countrymodel->GetAllCountries();
                                                                if (is_array($Countries)) {
                                                                    foreach ($Countries as $Country) {
                                                                        ?>
                                                                           <!-- <option value="<?= $Country->RowID; ?>"><?= $Country->CountryName; ?></option> -->
                                                                         <option type="text" value="<?= $Country->RowID; ?>" <?php echo ($sessioned_caution && $sessioned_caution['advoNationality'] == $Country->CountryName) ? 'selected="selected"' : ''; ?> ><?= $Country->CountryName; ?></option>
                                                                        <?php
                                                                    }
                                                                    } else {
                                                                    ?>
                                                                    <option value="0">No data</option>
                                                                    <?php
                                                                }
                                                                ?>

                                                            </select>


                                                           </div>
                                                        </div>

                                                     </div>

                                                   
                                                 <div class="row">
                                                        <div class="col-md-10">
                                                            <p>
                                                                *Terms &amp; Conditions.
                                                            </p>
                                                        </div>

                                                       <!--  <div class="col-md-">
                                                            <button class="btn btn-sm btn-success"  type="submit">
                                                                Save Advocate Details <i class="fa fa-arrow-circle-right"></i>
                                                            </button>
                                                        </div> -->
                                                         <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/caution/apply_caution?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Continue <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>

                                                    </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>


                        </div>


                        <?php elseif ($input['step'] == 5) : ?>
                        <div class="wizard-step-5">
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="col-md-4">

                                        <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                            <div class="panel-heading">

                                                <h4 class="panel-title">Notes </h4>
                                            </div>
                                            <div class="panel-body bg-blue text-white">
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the fifth step of 7 to complete your application. </li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8" style="padding-top: 38px;">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Upload supporting Document</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div id="panel_edit_account" class="tab-pane in active">

                                                    <?php echo form_open_multipart('caution/rate_clearance', array('data-parsley-validate' => 'true', 'name' => 'form-wizard')); ?>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label class="control-label">
                                                             Document Reference Number</label>
                                                            <input type="text" name="RateClearanceNumber" value="<?php echo $sessioned_caution ? $sessioned_caution['RateClearanceNumber'] : ''; ?>" placeholder="Rate Clearance Number" class="form-control" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="control-label">
                                                                Document Number (.png,.jpeg,.pdf)
                                                            </label>
                                                            <div class="form-group">

                                                                <input type="file" name="clearance_certificate" value="<?php echo $sessioned_caution ? $sessioned_caution['RateClearanceCertificateFile'] : ''; ?>" placeholder="Rate Clearance Number" class="form-control" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <p>
                                                                *Terms &amp; Conditions.
                                                            </p>
                                                        </div>
                                                        <!-- <div class="col-md-">
                                                            <button class="btn btn-sm btn-success" type="submit">
                                                                Upload certificate <i class="fa fa-arrow-circle-right"></i>
                                                            </button>
                                                        </div> -->
                                                         <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/caution/apply_caution?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Continue <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                    </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>

                        </div>

                        <?php elseif ($input['step'] == 6): ?>
                            <div class="wizard-step-7">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the sixth step of 7 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8" style="padding-top: 38px; overflow-y:scroll;height: 300px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Review your caution application</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open_multipart('caution/review_application', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <table class="table table-bordered table-hover">
                                                                    <tr>
                                                                        <td>Registry</td>
                                                                        <td><?php echo $sessioned_caution['RegistryID']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Registration Section</td>
                                                                        <td><?php echo $sessioned_caution['RegistrationSectionID']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Parcel Number</td>
                                                                        <td><?php echo $sessioned_caution['ParcelNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>LR Number</td>
                                                                        <td><?php echo $sessioned_caution['LRNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Tenure</td>
                                                                        <td><?php echo $sessioned_caution['Act']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Owners</td>
                                                                        <td> <?php if ($sessioned_sellers): ?>
                                                                                <table class="table table-condensed">
                                                                                    <thead>
                                                                                    <th>#</th>
                                                                                    <th>Type</th>
                                                                                    <th>Is contact person?</th>
                                                                                    <th>First name</th>
                                                                                    <th>Middle name</th>
                                                                                    <th>Last name</th>
                                                                                    <th>Nationality</th>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        $index = 1;
                                                                                        
                                                                                        ?>
                                                                                        <?php foreach ($sessioned_sellers as $single_seller): ?>
                                                                                            <tr>
                                                                                                <td><?php echo $index; ?></td>
                                                                                                <td><label class="label <?php echo $single_seller['Type'] == 'individual' ? 'label-warning' : 'label-info'; ?>"><?php echo ucfirst($single_seller['Type']); ?></label></td>
                                                                                                <td><?php echo $single_seller['IsContactPerson']; ?></td>
                                                                                                <td><?php echo $single_seller['FirstName']; ?></td>
                                                                                                <td><?php echo $single_seller['MiddleName']; ?></td>
                                                                                                <td><?php echo $single_seller['LastName']; ?></td>
                                                                                                <td><?php echo $single_seller['CountryID']; ?></td>
                                                                                            </tr>
                                                                                            <?php $index++; ?>
                                                                                        <?php endforeach; ?>

                                                                                    </tbody>
                                                                                </table>
                                                                            <?php endif; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's First Name</td>
                                                                        <td><?php echo $sessioned_caution['advoFName']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's Middle Name</td>
                                                                        <td><?php echo $sessioned_caution['advoMName']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's Last Name</td>
                                                                        <td><?php echo $sessioned_caution['advoLName']; ?></td>
                                                                    </tr>
                                                                   <!--  <tr>
                                                                        <td>Advocate's KRA PIN Number</td>
                                                                        <td><?php echo $sessioned_caution['advoPinNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's Postal Address</td>
                                                                        <td><?php echo $sessioned_caution['advoPostalAddress']; ?></td>
                                                                    </tr> -->
                                                                    <tr>
                                                                        <td>Advocate's Email</td>
                                                                        <td><?php echo $sessioned_caution['advoEmail']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's Phone</td>
                                                                        <td><?php echo $sessioned_caution['advoTelNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's Law Firm</td>
                                                                        <td><?php echo $sessioned_caution['advoLawFirm']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's PF Number</td>
                                                                        <td><?php echo $sessioned_caution['advoPFNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's ID Number</td>
                                                                        <td><?php echo $sessioned_caution['advoIDNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's Country</td>
                                                                        <td><?php echo $sessioned_caution['advoNationality']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Caution Interest</td>
                                                                        <td><?php echo $sessioned_caution['CautionInterest']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Caution description</td>
                                                                        <td><?php echo $sessioned_caution['CautionDescription']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Type of transaction</td>
                                                                        <td><?php echo $sessioned_caution['TypeOfTransaction']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Caution Amount</td>
                                                                        <td><?php echo $sessioned_caution['AmountPayable']; ?></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/caution/apply_caution?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Make payment <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>
                            </div> 

                      <?php elseif ($input['step'] == 7) : ?>
                            <div class="wizard-step-8">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="panel panel-default panel-blog panel-checkout">
                                            <div class="panel-body">
                                                <ul class="blog-meta mb5">
                                                    <!--<li>Jan 09, 2015</li>-->
                                                </ul>
                                                <?php if (array_key_exists('step', $input) && $input['step'] == 7): ?>
                                                    <div style="background-color: #FFF;">
                                                        <?php
                                                        $apiClientId = '13';
                                                        $amount = $sessioned_caution['AmountPayable'];
                                                        $amount = '10';
                                                        $serviceId = '29';
                                                        $idNumber = $sessioned_user['IDNumber'];
                                                        $currency = 'KES';
                                                        $billRef = $sessioned_caution['RowID'];
                                                        $serviceDesc = $sessioned_caution['Description'];
                                                        $clientName = $sessioned_user['FirstName'] . ' ' . $sessioned_user['LastName'];
                                                        $key = 'TYV0FUD0BY';
                                                        $secret = 'BGI0ZTSPL2WX0X6';

                                                        $payload = "{$apiClientId}{$amount}{$serviceId}{$idNumber}{$currency}{$billRef}{$serviceDesc}{$clientName}{$secret}";
                                                        $hash = base64_encode(hash_hmac("sha256", $payload, $key));
                                                        ?>
                                                        <form id="idPaymentForm" method="post" action="https://pesaflow.ecitizen.go.ke/PaymentAPI/iframev2.1.php" target="my_frame">
                                                            <input type="hidden" name="apiClientID" value="13" />
                                                            <input type="hidden" name="secureHash" value="<?php echo $hash; ?>" />
                                                            <input type="hidden" name="billDesc" value="<?php echo $sessioned_caution['Description']; ?>" />
                                                            <input type="hidden" name="billRefNumber" value="<?php echo $sessioned_caution['RowID']; ?>" />
                                                            <input type="hidden" name="currency" value="KES" />
                                                            <input type="hidden" name="serviceID" value="29" />
                                                            <input type="hidden" name="clientMSISDN" value="<?php echo $sessioned_user['Phone']; ?>" />
                                                            <input type="hidden" name="clientName" value="<?php echo $clientName; ?>" />
                                                            <input type="hidden" name="clientIDNumber" value="<?php echo $sessioned_user['IDNumber']; ?>" />
                                                            <input type="hidden" name="clientType" value="citizen" />
                                                            <input type="hidden" name="clientEmail" value="<?php echo $sessioned_user['Email']; ?>" />
                                                            <input type="hidden" name="callBackURLOnSuccess"  value="<?php echo base_url(); ?>/caution/payment_completed?bill_ref=<?php echo $sessioned_caution['RowID']; ?>" />
                                                            <input type="hidden" name="callBackURLOnFail"  value="<?php echo base_url(); ?>/caution/apply_caution?step=6" />
                                                            <input type="hidden" name="notificationURL"  value="<?php echo base_url(); ?>/caution/apply_caution?step=6" />
                                                            <input type="hidden" name="amountExpected" value="<?php echo $amount; ?>" />
                                                        </form>
                                                        <iframe width="100%" height="600px" style="border: none;" name="my_frame"></iframe>
                                                        <script type="text/javascript">
                                                            document.getElementById('idPaymentForm').submit();
                                                        </script>
                                                    </div>
                                                    <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/caution/apply_caution?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                <?php endif; ?>
                                            </div><!-- panel-body -->
                                        </div>
                                    </div>

                                    <!-- end row -->
                                </fieldset>
                            </div>
                        <?php endif; ?>
                        <!-- end wizard step-5 -->


                    </div>

                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<!-- end #content -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->



<div id="idAddApplicantModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('caution/save_seller', array('data-parsley-validate' => 'true', 'id' => 'idSaveApplicantForm')); ?>
        <input type="hidden" name="CautionID" value="<?php echo $sessioned_caution ? $sessioned_caution['RowID'] : ''; ?>"/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Caution Applicant</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idApplicantAlert" class="alert alert-success hidden"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                ID Number
                            </label>
                            <input id="idApplicantIdNumber"  type="number" name="Identification" value="" placeholder="ID Number/Passport" id="form-field-9" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <button id="idApplicantGetDetails" class="btn btn-primary" style="margin-top: 20px;" disabled>Validate</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Is Primary Contact Person?
                            </label>
                            <br>
                            <label><input type="radio" name="IsContactPerson" value="Y"  checked=""/> Yes</label>
                            <label><input type="radio" name="IsContactPerson" value="N"/> No</label>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                First Name
                            </label>
                            <input id="idApplicantFirstName" type="text"  name="FirstName" value="" placeholder="First Name" id="form-field-9" class="form-control" disabled >
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Middle Name
                            </label>
                            <input id="idApplicantMiddleName" type="text" name="MiddleName" value="" placeholder="Middle Name" id="form-field-10" class="form-control" disabled >
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input id="idApplicantLastName" type="text" name="LastName" value="" placeholder="Last Name" id="form-field-11" class="form-control" disabled required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                KRA PIN Number
                            </label>
                            <input  id="idPinNumber" type="text" name="PinNumber" value="" placeholder="KRA PIN Number" id="form-field-11" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Mobile Number
                            </label>
                            <input  id="idApplicantMobileNumber" type="number" name="MobileNumber" value="" placeholder="Mobile Number" id="form-field-10" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input  id="idApplicantEmailAddress" type="email" name="EmailAddress" value="" placeholder="Email" id="form-field-11" class="form-control" required>

                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Postal Address
                            </label>
                            <input  id="idApplicantAddress" type="text" name="Address" value="" placeholder="Address" id="form-field-11" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label class="control-label">
                                Nationality
                            </label>

                            <select id="idApplicantNationality" name="CountryID" class="form-control" required>
                                <option value="">Select</option>
                                <?php
                                $Countries = $this->Countrymodel->GetAllCountries();
                                if (is_array($Countries)) {
                                    foreach ($Countries as $Country) {
                                        ?>
                                        <option value="<?= $Country->RowID; ?>"><?= $Country->CountryName; ?></option>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <option value="0">No data</option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idSaveApplicantButton" type="button" class="btn btn-primary" type="submit">
                    Save changes
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<div id="idDeleteApplicantModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('caution/delete_seller', array('data-parsley-validate' => 'true', 'id' => 'idDeleteApplicantForm')); ?>
        <input id="idDeleteApplicantID" type="hidden" name="SellerID" value=""/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Applicant</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idBuyerAlert" class="alert alert-success hidden">Applicant deleted</p>
                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idDeleteApplicantButton" type="button" class="btn btn-primary" type="submit">
                    Delete Applicant
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>



<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/parsley/dist/parsley.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-wizards-validation.demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/apps.min.js?time=<?php echo time(); ?>"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<script></script>
<script>
                                                    $("#County").change(function (evt) {
                                                        var ActID = document.getElementById('Acts').value;
                                                        var CountyID = document.getElementById('County').value;
                                                        var select = $("#County");
                                                        var selectData = select.serialize();
                                                        var ajax = $.ajax({
                                                            dataType: "json",
                                                            type: 'POST',
                                                            url: "<?php echo base_url(); ?>Transfer/GetRegistriesMatch/" + ActID + "/" + CountyID,
                                                            data: selectData
                                                        });
                                                        ajax.done(function (response) {
                                                            $("#testurl").html(response);
                                                            var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                            for (var key in response) {
                                                                if (response.hasOwnProperty(key)) {
                                                                    //alert(key);
                                                                    //alert(response[key].RowID);
                                                                    //alert(response[key].NameOfRoad);
                                                                    listItems += "<option value='" + response[key].Registres + "'>" + response[key].Registries + "</option>";
                                                                }
                                                            }
                                                            $("#Registry").html(listItems);
                                                        });
                                                        ajax.fail(function () {

                                                        });
                                                    });

                                                    $("#towntype").change(function (evt) {
                                                        var townTypeID = document.getElementById('towntype').value;
                                                        var select = $("#towntype");
                                                        var selectData = select.serialize();
                                                        var ajax = $.ajax({
                                                            dataType: "json",
                                                            type: 'POST',
                                                            url: "http://localhost/lmais/start/GetTownNamesMatchTownType/" + townTypeID,
                                                            data: selectData
                                                        });
                                                        ajax.done(function (response) {
                                                            var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                            for (var key in response) {
                                                                if (response.hasOwnProperty(key)) {
                                                                    //alert(key);
                                                                    //alert(response[key].RowID);
                                                                    //alert(response[key].NameOfRoad);
                                                                    listItems += "<option value='" + response[key].NameOfTown + "'>" + response[key].NameOfTown + "</option>";
                                                                }
                                                            }
                                                            $("#townNames").html(listItems);
                                                        });
                                                        ajax.fail(function () {

                                                        });
                                                    });

</script>

<script>
    $(document).ready(function () {
        App.init();
        FormWizardValidation.init();
    });
</script>


<script>

$('#advoNationality option').each(function(){
   $(this).attr('value', $(this).text()); 
});

</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>