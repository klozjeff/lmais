<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
<!-- begin #content -->
<script type="text/javascript">
    var step = <?php echo array_key_exists('step', $input) ? $input['step'] : -1; ?>;
    var inlineJs = {
        baseUrl: "<?php echo base_url(); ?>"
    }
</script>
<div id="content" class="content">
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <?php
                 if (isset($sessioned_action) && ($sessioned_action == 'updating')) {
                        $amount = number_format($sessioned_balance);
                    } else {
                        $amount = $input['step'] == 1 ? $convenience_model->ServiceAmount : number_format($sessioned_caution['AmountPayable']);
                    }//E# if else statement
                        //$amount = $input['step'] == 1 ? $withdraw_caution_model->ServiceAmount : number_format($sessioned_caution['AmountPayable']);
                   
                    ?>
                    <h4 class="panel-title"><span class="pull-left">WITHDRAWAL OF A CAUTION <?php echo $sessioned_caution ? 'FOR LR NUMBER' : '' ?> <b><?php echo $sessioned_caution ? $sessioned_caution['LRNumber'] : '' ?></b></span><span data-toggle="tooltip" data-placement="top" title="After reviewing your application, kindly pay KES <?php echo $amount; ?>" class="pull-right label label-danger">KES <?php echo $amount; ?></span></h4>
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <div id="wizard">
                        <ol class="bwizard-steps clearfix clickable" role="tablist">
                         
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 1) ? 'active' : ''; ?>" style="z-index: 1;"><span class="label">1</span><a>
                                    Payment 
                                </a></li>
                        </ol>
                       
                        <?php if ($input['step'] == 1) : ?>
                            <div class="wizard-step-5">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="panel panel-default panel-blog panel-checkout">
                                            <div class="panel-body">
                                                <ul class="blog-meta mb5">
                                                    <!--<li>Jan 09, 2015</li>-->
                                                </ul>
                                                <?php if (array_key_exists('step', $input) && $input['step'] == 1): ?>
                                                    <div style="background-color: #FFF;">
                                                        <?php
														$slug=$withdraw_caution_model->Slug;
													
                                                        $apiClientId = '13';
                                                        $amount = $sessioned_balance;
                                                        $serviceId = $sessioned_caution['UserID'];
                                                        $idNumber = $sessioned_user['IDNumber'];
                                                        $currency = 'KES';
                                                        $billRef = $slug.'_'.$sessioned_caution['RowID'];
                                                        $serviceDesc = $sessioned_caution['Description'];
                                                        $clientName = $sessioned_user['FirstName'] . ' ' . $sessioned_user['LastName'];
                                                        $key = 'TYV0FUD0BY';
                                                        $secret = 'BGI0ZTSPL2WX0X6';

                                                        $payload = "{$apiClientId}{$amount}{$serviceId}{$idNumber}{$currency}{$billRef}{$serviceDesc}{$clientName}{$secret}";
                                                        $hash = base64_encode(hash_hmac("sha256", $payload, $key));
                                                        ?>
                                                        <form id="idPaymentForm" method="post" action="https://pesaflow.ecitizen.go.ke/PaymentAPI/iframev2.1.php" target="my_frame">
                                                            <input type="hidden" name="apiClientID" value="13" />
                                                            <input type="hidden" name="secureHash" value="<?php echo $hash; ?>" />
                                                            <input type="hidden" name="billDesc" value="<?php echo $sessioned_caution['Description']; ?>" />
                                                            <input type="hidden" name="billRefNumber" value="<?php echo $billRef; ?>" />
                                                            <input type="hidden" name="currency" value="KES" />
                                                            <input type="hidden" name="serviceID" value="29" />
                                                            <input type="hidden" name="clientMSISDN" value="<?php echo $sessioned_user['Phone']; ?>" />
                                                            <input type="hidden" name="clientName" value="<?php echo $clientName; ?>" />
                                                            <input type="hidden" name="clientIDNumber" value="<?php echo $sessioned_user['IDNumber']; ?>" />
                                                            <input type="hidden" name="clientType" value="citizen" />
                                                            <input type="hidden" name="clientEmail" value="<?php echo $sessioned_user['Email']; ?>" />
                                                            <input type="hidden" name="callBackURLOnSuccess"  value="<?php echo base_url(); ?>/withdrawal_caution/payment_completed?bill_ref=<?php echo $billRef; ?>" />
                                                            <input type="hidden" name="callBackURLOnFail"  value="<?php echo base_url(); ?>/withdrawal_caution/withdraw_caution?step=2" />
                                                            <input type="hidden" name="notificationURL"  value="<?php echo base_url(); ?>/withdrawal_caution/withdraw_caution?step=2" />
                                                            <input type="hidden" name="amountExpected" value="<?php echo $amount; ?>" />
                                                        </form>
                                                        <iframe width="100%" height="600px" style="border: none;" name="my_frame"></iframe>
                                                        <script type="text/javascript">
                                                            document.getElementById('idPaymentForm').submit();
                                                        </script>
                                                    </div>
                                                   <!-- <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/caution/apply_caution?step=' . ($input['step'] - 1); ?>">Previous</a>-->
                                                <?php endif; ?>
                                            </div><!-- panel-body -->
                                        </div>
                                    </div>

                                    <!-- end row -->
                                </fieldset>
                            </div>
                        <?php endif; ?>
                        <!-- end wizard step-5 -->


                    </div>

                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<!-- end #content -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->



<div id="idAddApplicantModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('caution/save_seller', array('data-parsley-validate' => 'true', 'id' => 'idSaveApplicantForm')); ?>
        <input type="hidden" name="CautionID" value="<?php echo $sessioned_caution ? $sessioned_caution['RowID'] : ''; ?>"/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Caution Applicant</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idApplicantAlert" class="alert alert-success hidden"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                ID Number
                            </label>
                            <input id="idApplicantIdNumber"  type="number" name="Identification" value="" placeholder="ID Number/Passport" id="form-field-9" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <button id="idApplicantGetDetails" class="btn btn-primary" style="margin-top: 20px;" disabled>Validate</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Is Primary Contact Person?
                            </label>
                            <br>
                            <label><input type="radio" name="IsContactPerson" value="Y"  checked=""/> Yes</label>
                            <label><input type="radio" name="IsContactPerson" value="N"/> No</label>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                First Name
                            </label>
                            <input id="idApplicantFirstName" type="text"  name="FirstName" value="" placeholder="First Name" id="form-field-9" class="form-control" disabled >
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Middle Name
                            </label>
                            <input id="idApplicantMiddleName" type="text" name="MiddleName" value="" placeholder="Middle Name" id="form-field-10" class="form-control" disabled >
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input id="idApplicantLastName" type="text" name="LastName" value="" placeholder="Last Name" id="form-field-11" class="form-control" disabled required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Mobile Number
                            </label>
                            <input  id="idApplicantMobileNumber" type="number" name="MobileNumber" value="" placeholder="Mobile Number" id="form-field-10" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input  id="idApplicantEmailAddress" type="email" name="EmailAddress" value="" placeholder="Email" id="form-field-11" class="form-control" required>

                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Postal Address
                            </label>
                            <input  id="idApplicantAddress" type="text" name="Address" value="" placeholder="Address" id="form-field-11" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label class="control-label">
                                Nationality
                            </label>

                            <select id="idApplicantNationality" name="CountryID" class="form-control" required>
                                <option value="">Select</option>
                                <?php
                                $Countries = $this->Countrymodel->GetAllCountries();
                                if (is_array($Countries)) {
                                    foreach ($Countries as $Country) {
                                        ?>
                                        <option value="<?= $Country->RowID; ?>"><?= $Country->CountryName; ?></option>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <option value="0">No data</option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idSaveApplicantButton" type="button" class="btn btn-primary" type="submit">
                    Save changes
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<div id="idDeleteApplicantModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('caution/delete_seller', array('data-parsley-validate' => 'true', 'id' => 'idDeleteApplicantForm')); ?>
        <input id="idDeleteApplicantID" type="hidden" name="SellerID" value=""/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Applicant</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idBuyerAlert" class="alert alert-success hidden">Applicant deleted</p>
                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idDeleteApplicantButton" type="button" class="btn btn-primary" type="submit">
                    Delete Applicant
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>



<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/parsley/dist/parsley.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-wizards-validation.demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/apps.min.js?time=<?php echo time(); ?>"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<script></script>
<script>
                                                            $("#County").change(function (evt) {
                                                                var ActID = document.getElementById('Acts').value;
                                                                var CountyID = document.getElementById('County').value;
                                                                var select = $("#County");
                                                                var selectData = select.serialize();
                                                                var ajax = $.ajax({
                                                                    dataType: "json",
                                                                    type: 'POST',
                                                                    url: "<?php echo base_url(); ?>Transfer/GetRegistriesMatch/" + ActID + "/" + CountyID,
                                                                    data: selectData
                                                                });
                                                                ajax.done(function (response) {
                                                                    $("#testurl").html(response);
                                                                    var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                                    for (var key in response) {
                                                                        if (response.hasOwnProperty(key)) {
                                                                            //alert(key);
                                                                            //alert(response[key].RowID);
                                                                            //alert(response[key].NameOfRoad);
                                                                            listItems += "<option value='" + response[key].Registres + "'>" + response[key].Registries + "</option>";
                                                                        }
                                                                    }
                                                                    $("#Registry").html(listItems);
                                                                });
                                                                ajax.fail(function () {

                                                                });
                                                            });

                                                            $("#towntype").change(function (evt) {
                                                                var townTypeID = document.getElementById('towntype').value;
                                                                var select = $("#towntype");
                                                                var selectData = select.serialize();
                                                                var ajax = $.ajax({
                                                                    dataType: "json",
                                                                    type: 'POST',
                                                                    url: "http://localhost/lmais/start/GetTownNamesMatchTownType/" + townTypeID,
                                                                    data: selectData
                                                                });
                                                                ajax.done(function (response) {
                                                                    var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                                    for (var key in response) {
                                                                        if (response.hasOwnProperty(key)) {
                                                                            //alert(key);
                                                                            //alert(response[key].RowID);
                                                                            //alert(response[key].NameOfRoad);
                                                                            listItems += "<option value='" + response[key].NameOfTown + "'>" + response[key].NameOfTown + "</option>";
                                                                        }
                                                                    }
                                                                    $("#townNames").html(listItems);
                                                                });
                                                                ajax.fail(function () {

                                                                });
                                                            });

</script>

<script>
    $(document).ready(function () {
        App.init();
        //FormWizardValidation.init();
    });
</script>


<script>

    $('#advoNationality option').each(function () {
        $(this).attr('value', $(this).text());
    });

</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>