		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<?PHP
				echo $this->session->flashdata('registrymsg');
				?>
			<div class="row">
		
			    <div class="col-md-12">
                    <div class="panel panel-inverse">
                        <div class="panel-heading" style="border-bottom:1px solid aliceblue">
                            <div class="wrapper bg-silver-lighter clearfix pull-right" >
                       
                        <div class="pull-right">
						
					<a href="#modal-section" class="btn btn-sm btn-primary" data-toggle="modal">Add New Section</a>
                           
                           
                        </div>
                    </div>
                            
                            <h4 class="panel-title">Registry Section Management </h4>
							
                        </div>
						
                        <div class="panel-body">
						
					</br>
                            <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                         <th>#</th>
                                        <th>Section Name</th>
										<th>Date Created</th>
										<th> <i class="fa fa-bars"></th>

                                    </tr>
                                </thead>
                                <tbody>
								<?php if($Results) : ?>
									<?php $i=1; ?>
				<?php foreach ($Results as $index => $Result) : 
				$RegistryID=$Result->RegistryID;
          			
				?>
                  
					
                                    <tr class="odd gradeX">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $Result -> Name; ?></td>
                                     <td><?php echo $Result -> DateCreated; ?></td>
                                        
										 <td><a href="#">View</td>
                                    </tr>
									
                                    <?php $i += 1; ?>
									<?php endforeach;?>
		            
					<?php endif; ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
			    </div>
			    <!-- end col-10 -->
			</div>
			
			
									 <div class="modal fade" id="modal-section">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											<h4 class="modal-title">Add Registry Section</h4>
										</div>
										<div class="modal-body">
											<div id="panel_edit_account" class="tab-pane in active">
									
									<?php echo form_open('Registry/AddRegistrySection' , array('class'=>'margin-bottom-0', 'id'=>'AddRegistrySection'));?>
											<div class="row">
												
												<div class="col-md-10">
													<div class="form-group">
														<label class="control-label">
															Registry Section Name *
														</label>
															<input type="hidden" placeholder="" value="<?php echo $RegistryID;?>" class="form-control" id="RegistryID" name="RegistryID" required>
														<input type="text" placeholder="KILIFI/JILORE" class="form-control" id="SectionName" name="SectionName" required>
													</div>
													<div class="form-group">
														<label class="control-label">
															Registry Name *
														</label>
														<input type="text" placeholder="" value="" class="form-control" id="RegistryName" name="RegistryName" disabled>
													</div>
												
											
													
														
													</div>
													
													
													
												</div>
											
											
											
											<div class="row">
												<div class="col-md-12">
													<div>
														* Required Fields
														<hr>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-8">
													<p>
														By clicking Add Registry, you are agreeing to the Policy and Terms &amp; Conditions.
													</p>
												</div>
												<div class="col-md-4">
													<button class="btn btn-sm btn-success" type="submit">
														Add Section <i class="fa fa-arrow-circle-right"></i>
													</button>
													
											<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
																
												</div>
											</div>
										<?php echo form_close(); ?>
									</div>
										</div>
										
									</div>
								</div>
							</div>
			
			
							
							
							
							
		</div>
		<!-- end #content -->
		
        
		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/AutoFill/js/dataTables.autoFill.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/KeyTable/js/dataTables.keyTable.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/RowReorder/js/dataTables.rowReorder.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Select/js/dataTables.select.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/table-manage-combine.demo.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<script>
		$(document).ready(function() {
			App.init();
			TableManageCombine.init();
		});
	</script>

</body>
</html>
				


			
		          