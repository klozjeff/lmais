<div class="row">
    <div class="col-md-9">
        <h4 class="page-title" style="padding: 10px 0 0 50px;">Applications History</h4>
    </div>
     <div class="col-md-3">
        <h4 class="page-title" style="padding: 10px 0 0 50px;"><a class="btn btn-xs btn-default" style="padding:10px;" title="View Application" href="<?php echo base_url(); ?>UserView/LoadApplications">Make Application</a></h4>
    </div>
</div>

	
<div class="row">
          <!-- begin col-2 -->
          
          <!-- end col-2 -->
          <!-- begin col-10 -->
        <div class="col-md-12 ui-sortable" style="padding: 10px 50px 0 50px;">
              <!-- begin panel -->
                    <div class="panel panel-inverse">
                                               
                        <div class="panel-body">
                            <div id="data-table_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="row"><div class="col-sm-9"><div class="dataTables_length" id="data-table_length"><label>Show <select name="data-table_length" aria-controls="data-table" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-3"><div id="data-table_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="data-table"></label></div></div></div><div class="row"><div class="col-sm-12"><table id="data-table" class="table table-striped table-bordered nowrap dataTable no-footer dtr-inline" width="100%" role="grid" aria-describedby="data-table_info" style="width: 100%;">
                                <thead>
                                    <tr role="row">
                                      <th class="sorting_asc" tabindex="0" aria-controls="data-table" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 100px;">Form</th>
                                      <th class="sorting" tabindex="0" aria-controls="data-table" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 150px;">Ref Number </th>
                                      <th class="sorting" tabindex="0" aria-controls="data-table" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 150px;">Bill Status</th>
                                      <th class="sorting" tabindex="0" aria-controls="data-table" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 111px;">Approval</th>
                                      <th class="sorting" tabindex="0" aria-controls="data-table" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 79px;">Submitted On</th>
                                      <th class="sorting" tabindex="0" aria-controls="data-table" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 79px;">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php if($transfer_model):?>

                                        <?php foreach($transfer_model as $single_model):?>
                                                                      
                                <tr class="gradeA odd" role="row">
                                        <td class="sorting_1" tabindex="0"><?php echo $single_model['Description'];?></td>
                                        <td><a href="<?php echo base_url(); ?>UserView/ConsentInfo/<?php echo $single_model['TransactionID']; ?>"><?php echo $single_model['TransactionID'];?></a></td>
                                        <td><?php echo $single_model['PaymentStatus'];?></td>
                                        <td><?php echo $single_model['Status'];?></td>
                                        <td><?php echo $single_model['PaymentDate'];?></td>
                                        <td><a class="btn btn-xs btn-default" title="View Application" href="<?php echo base_url(); ?>UserView/ConsentInfo/<?php echo $single_model['TransactionID']; ?>">View </a></td>
                                    </tr>

                                    <?php endforeach?>   
                                    <?php endif;?>

                                    </tbody>
                            </table></div></div><div class="row"><div class="col-sm-4"><div class="dataTables_info" id="data-table_info" role="status" aria-live="polite"></div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="data-table_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="data-table_previous"><a href="#" aria-controls="data-table" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="data-table" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="data-table" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="data-table" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="data-table" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="data-table" data-dt-idx="5" tabindex="0">5</a></li><li class="paginate_button "><a href="#" aria-controls="data-table" data-dt-idx="6" tabindex="0">6</a></li><li class="paginate_button next" id="data-table_next"><a href="#" aria-controls="data-table" data-dt-idx="7" tabindex="0">Next</a></li></ul></div></div></div></div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
            </div>

		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="<?php echo base_url(); ?>assets/plugins/morris/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/morris/morris.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/gritter/js/jquery.gritter.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/dashboard-v2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<script>
		$(document).ready(function() {
			App.init();
			DashboardV2.init();
		});
	</script>

</body>
</html>
