<div class="row">
    <div class="col-md-8">
        <h4 class="page-title" style="padding: 10px 0 0 50px;">Apply for Caution</h4>
    </div>
</div>

<div class="row">
    <div class="col-md-8"> <!-- begin col-12 -->
        <div class="table-responsive">
            <div class="panel panel-inverse">
                <div class="panel-body">
                    <p>
                        1.	Please fill in all the details as guided by the forms. 
                    </p>
                    <p>
                        2.	At the end of the application, you will receive an invoice with all the payable items (Consent fee, Registration Fee, Land Rent Arrears (If not paid). 
                    </p>
                    <p>
                        3.	Ensure you have paid the County Land Rates arrears and you have the scanned receipts ready. 
                    </p>
                    <p>
                        4.	The proposed owner will receive the valuation report and Stamp Duty payable. 
                    </p>

                    <p>
                        5.	When stamp duty is paid, download the automated, auto-filled transfer form, attach passport size photos for both the seller and the buyer, have the advocate sign and present the signed form to the registrar in the respective registry, together with the original title, the e-citizen payment receipts.
                    </p>

                    <p>
                        <a href="<?php echo base_url(); ?>caution/apply_caution" class="btn btn-primary m-r-5">APPLY FOR CAUTION</a>
                    </p>



                </div>			
            </div>
        </div>
    </div>


    <div class="col-md-4">
        <div class="box-content ">

            <p>NAME: <?php
                if ($this->session->userdata('Logged_in') != '') {

                    echo $this->session->userdata('FirstName') . " " . $this->session->userdata('LastName') . " " . $this->session->userdata('Phone');

                    // echo "</p><p>EMAIL: "
                }
                ?></p>
            <p>TEL: 0707111222</p>

            <p>
                <a href="<?php echo base_url(); ?>users/logout" class="btn btn-primary m-r-5">LOG OUT</a>
            </p>
        </div>
    </div>
    <div class="col-md-4 col-md-offset-8">
        <div class="box-content">							   
            <h3>Quick Links</h3>
            <ul>
                <li>Department of Immigration</li>
                <li>Ministry of Lands</li>
                <li>Office of the attorney general</li>
                <li>Mombasa County</li>
            </ul>

        </div>
    </div>
    <!-- end col-12 -->
</div>

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/morris/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/morris/morris.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/gritter/js/jquery.gritter.js"></script>
<script src="<?php echo base_url(); ?>assets/js/dashboard-v2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function () {
        App.init();
        DashboardV2.init();
    });
</script>

</body>
</html>
