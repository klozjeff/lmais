<div class="row">
    <div class="col-md-8">
        <h4 class="page-title" style="padding: 10px 0 0 50px;">Transfer of Land</h4>
    </div>
</div>
<?php if (array_key_exists('type', $input) && $input['type'] == 'buyer'): ?>
    <div class="row">
        <div class="col-md-11" style="overflow-y: scroll; height:500px; padding-left: 40px"> <!-- begin col-12 -->
            
            <div class="panel panel-default panel-blog panel-checkout">
                <div class="panel-body">
                    <h3>Please pay stamp duty of KES <?php echo $transfer_model['StampDuty'];?> plus Registration fee of KES 500</h3>
                    <hr>
                    <ul class="blog-meta mb5">
                        <!--<li>Jan 09, 2015</li>-->
                    </ul>
                    <div style="background-color: #FFF;">
                        <?php
                        $apiClientId = '13';
                        $amount = $transfer_model['StampDuty'];
                        //$amount = '10';
                        $serviceId = '29';
                        $idNumber = $this->session->userdata('IDNumber');
                        $currency = 'KES';
                        $billRef = $transfer_model['RowID'];
                        $serviceDesc = 'Stamp Duty';
                        $clientName = $this->session->userdata('FirstName') . ' ' . $this->session->userdata('LastName');
                        $key = 'TYV0FUD0BY';
                        $secret = 'BGI0ZTSPL2WX0X6';

                        $payload = "{$apiClientId}{$amount}{$serviceId}{$idNumber}{$currency}{$billRef}{$serviceDesc}{$clientName}{$secret}";
                        $hash = base64_encode(hash_hmac("sha256", $payload, $key));
                        ?>
                        <form id="moodleform" method="post" action="https://pesaflow.ecitizen.go.ke/PaymentAPI/iframev2.1.php" target="my_frame">
                            <input type="hidden" name="apiClientID" value="13" />
                            <input type="hidden" name="secureHash" value="<?php echo $hash; ?>" />
                            <input type="hidden" name="billDesc" value="<?php echo $serviceDesc; ?>" />
                            <input type="hidden" name="billRefNumber" value="<?php echo $transfer_model['RowID']; ?>" />
                            <input type="hidden" name="currency" value="KES" />
                            <input type="hidden" name="serviceID" value="29" />
                            <input type="hidden" name="clientMSISDN" value="<?php echo $this->session->userdata('Phone'); ?>" />
                            <input type="hidden" name="clientName" value="<?php echo $clientName; ?>" />
                            <input type="hidden" name="clientIDNumber" value="<?php echo $idNumber; ?>" />
                            <input type="hidden" name="clientType" value="citizen" />
                            <input type="hidden" name="clientEmail" value="<?php echo $this->session->userdata('Email'); ?>" />
                            <input type="hidden" name="callBackURLOnSuccess" value="http://localhost:8080/lmais/consent/stampduty_paid?bill_ref=<?php echo $transfer_model['RowID']; ?>" />
                            <input type="hidden" name="callBackURLOnFail" value="http://localhost:8080/lmais/consent/apply_consent?step=8" />
                            <input type="hidden" name="notificationURL" value="http://localhost:8080/lmais/consent/apply_consent?step=8" />
                            <input type="hidden" name="amountExpected" value="<?php echo $amount; ?>" />
                        </form>
                        <iframe width="100%" height="600px" style="border: none;" name="my_frame"></iframe>
                        <script type="text/javascript">
                            document.getElementById('moodleform').submit();
                        </script>
                    </div>
                </div>
            </div>

        </div>
        <!-- end col-12 -->
    </div>
<?php else: ?>
    <div class="row">

        <div class="col-md-8" style="overflow-y: scroll; height:500px;"> <!-- begin col-12 -->
            <div class="table-responsive">
                <div class="panel panel-inverse">
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Details</th>
                                    <th>Description</th>                                                                   
                                </tr>

                            </thead>
                            <tbody>


                                <tr>
                                    <td>Registry</td>
                                    <td><?php echo $transfer_model['Registry']; ?></td>

                                </tr>
                                <tr>
                                    <td>Registration Section</td>
                                    <td><?php echo $transfer_model['LRNumber']; ?></td>
                                </tr>
                                <tr>
                                    <td>Parcel Number</td>
                                    <td><?php echo $transfer_model['ParcelNumber']; ?></td>
                                </tr>
                                <tr>
                                    <td>LR Number</td>
                                    <td><?php echo $transfer_model['LRNumber']; ?></td>
                                </tr>
                                <tr>
                                    <td>Nature of transaction</td>
                                    <td><?php echo $transfer_model['TransactionNature']; ?></td>
                                </tr>
                                <tr>
                                    <td>Tenure</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Transaction description</td>
                                    <td><?php echo $transfer_model['TransactionDescription']; ?></td>
                                </tr>
                                <tr>
                                    <td>Term</td>
                                    <?php echo $transfer_model['TransactionTerm']; ?>

                                </tr>

                                <tr>
                                    <td>Type of transaction</td>
                                    <td><?php echo $transfer_model['TypeOfTransaction']; ?></td>

                                </tr>
                                <tr>
                                    <td>Value</td>
                                    <td><?php echo $transfer_model['TransactionAmount']; ?></td>

                                </tr>
                                <tr>
                                    <td>Rent arrears</td>
                                    <td><?php echo $transfer_model['Rent']; ?></td>
                                </tr>   
                                <tr>
                                    <td>Owners</td>

                                    <td> 
                                        <?php if ($SellerData = $this->SellerModel->GetSellers($transfer_model['TransactionID'])): ?>
                                            <table class="table table-condensed">
                                                <thead>
                                                <th>#</th>
                                                <th>Is contact person?</th>
                                                <th>First name</th>
                                                <th>Middle name</th>
                                                <th>Last name</th>
                                                <th>Nationality</th>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $index = 1;
                                                    $total_rent = 0;
                                                    ?>
                                                    <?php foreach ($SellerData as $single_seller): ?>
                                                        <tr>
                                                            <td><?php echo $index; ?></td>
                                                            <td><?php echo $single_seller->IsContactPerson; ?></td>
                                                            <td><?php echo $single_seller->FirstName; ?></td>
                                                            <td><?php echo $single_seller->MiddleName; ?></td>
                                                            <td><?php echo $single_seller->LastName; ?></td>
                                                            <td><?php echo $single_seller->CountryID; ?></td>
                                                        </tr>
                                                        <?php $index++; ?>
                                                    <?php endforeach; ?>

                                                </tbody>
                                            </table>
                                        <?php endif; ?></td>
                                </tr>
                                <tr>
                                    <td>Buyers</td>
                                    <td> 
                                        <?php if ($BuyerData = $this->BuyerModel->GetBuyers($transfer_model['TransactionID'])): ?>
                                            <table class="table table-condensed">
                                                <thead>
                                                <th>#</th>
                                                <th>Is contact person?</th>
                                                <th>First name</th>
                                                <th>Middle name</th>
                                                <th>Last name</th>
                                                <th>Nationality</th>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $index = 1;
                                                    $total_rent = 0;
                                                    ?>
                                                    <?php foreach ($BuyerData as $single_buyer): ?>
                                                        <tr>
                                                            <td><?php echo $index; ?></td>
                                                            <td><?php echo $single_buyer->IsContactPerson; ?></td>
                                                            <td><?php echo $single_buyer->FirstName; ?></td>
                                                            <td><?php echo $single_buyer->MiddleName; ?></td>
                                                            <td><?php echo $single_buyer->LastName; ?></td>
                                                            <td><?php echo $single_buyer->CountryID; ?></td>
                                                        </tr>
                                                        <?php $index++; ?>
                                                    <?php endforeach; ?>

                                                </tbody>
                                            </table>
                                        <?php endif; ?></td>
                                </tr>



                            </tbody>
                        </table>

                    </div>			
                </div>
            </div>


        </div>



        <div class="col-md-4">

            <div class="box-content ">

                <h4 class="m-t-0 m-b-5">Billing Summary</h4>
                <table class="table">
                    <tbody>
                        <tr>
                            <td ><strong>Bill Ref:</strong></td>
                            <td>5141/36071/193615</td>
                        </tr>
                        <tr>
                            <td ><strong>Status:</strong></td>
                            <td>Paid</td>
                        </tr>
                        <tr>
                            <td ><strong>Service:</strong></td>
                            <td>4. Transfer Application </td>
                        </tr>
                        <tr>
                            <td ><strong>Total:</strong></td>
                            <td><strong>1450</strong></td>
                        </tr>
                    </tbody>
                </table>





                <a href="#" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-print"></i>
                    Print Invoice</a>

                <?php if ($transfer_model['PaymentStatus'] == 'Paid'): ?>
                    <div class="btn-group closed">
                        <button type="button" class="btn btn-success">Download Document</button>
                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo base_url(); ?>Prints/Consent_Application_Doc/<?php echo $transfer_model['TransactionID']; ?>" id="idPdf" title="Download Consent Application">Consent Application</a></li>
                            <li><a href="<?php echo base_url(); ?>Prints/RLA_Transfer/<?php echo $transfer_model['TransactionID']; ?>" " id="idXls" title="Download Transfer Application">Transfer Application</a></li>



                        </ul>
                    <?php else: ?>
                        <a href="<?php echo base_url(); ?>consent/apply_consent/" title="Edit" class="btn btn-success">Edit and Make Payment</a>
                    <?php endif; ?>                                                            
                    </td>   



                </div> </div>	   
        </div>
        <div class="col-md-4">
            <div class="box-content">							   
                <h3>Quick Links</h3>
                <ul>
                    <li>Department of Immigration</li>
                    <li>Ministry of Lands</li>
                    <li>Office of the attorney general</li>
                    <li>Mombasa County</li>
                </ul>

            </div>
        </div>
        <!-- end col-12 -->
    </div>
<?php endif; ?>
<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->

<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/morris/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/morris/morris.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/gritter/js/jquery.gritter.js"></script>
<script src="<?php echo base_url(); ?>assets/js/dashboard-v2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
                        $(document).ready(function () {
                            App.init();
                            DashboardV2.init();
                        });
</script>

</body>
</html>
