		<div id="content" class="content content-full-width">
			<div class="p-20">
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-2 -->
			    <div class="col-md-2">
			        <form>
			            <div class="input-group m-b-15">
                            <input type="text" class="form-control input-sm input-white" placeholder="Search Mail" />
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-inverse" type="button"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
			        </form>
			        <div class="hidden-sm hidden-xs">
                       
                        <ul class="nav nav-pills nav-stacked nav-inbox">
                            <li class="active">
                                <a href="#">
                                    <i class="fa fa-inbox fa-fw m-r-5"></i> All (10)
                                </a>
                            </li>
                             <li><a href="#"><i class="fa fa-flag fa-fw m-r-5"></i> Industrial</a></li>
                            <li><a href="#"><i class="fa fa-send fa-fw m-r-5"></i> Commercial</a></li>
                            <li><a href="#"><i class="fa fa-pencil fa-fw m-r-5"></i> Residential</a></li>
                            <li><a href="#"><i class="fa fa-trash fa-fw m-r-5"></i> Agricultural</a></li>
                        </ul>
                        <h5 class="m-t-20">Payment Status</h5>
                        <ul class="nav nav-pills nav-stacked nav-inbox">
                            <li><a href="#"><i class="fa fa-folder fa-fw m-r-5"></i> Paid</a></li>
                            <li><a href="#"><i class="fa fa-folder fa-fw m-r-5"></i> Pending</a></li>

                        </ul>
                    </div>
                </div>
			    <!-- end col-2 -->
			    <!-- begin col-10 -->
			    <div class="col-md-10">
                    <div class="email-btn-row hidden-xs">
                        <a href="#" class="btn btn-sm btn-inverse"><i class="fa fa-plus m-r-5"></i> Claim</a>
                        <a href="#" class="btn btn-sm btn-default ">Refresh</a>
                        <a href="#" class="btn btn-sm btn-default ">Open</a>
                        <a href="#" class="btn btn-sm btn-default ">Edit</a>

			        <div class="email-content">
                        <table class="table table-email">
                            <thead>
                                <tr>
                                    <th class="email-select"><a href="email_inbox.html#" data-click="email-select-all"><i class="fa fa-square-o fa-fw"></i></a></th>
                                    <th colspan="2">
                                        <div class="dropdown">
                                            <a href="email_inbox.html#" class="email-header-link" data-toggle="dropdown">View All <i class="fa fa-angle-down m-l-5"></i></a>
                                            <ul class="dropdown-menu">
                                                <li class="active"><a href="email_inbox.html#">All</a></li>
                                                <li><a href="#">Leasehold</a></li>
                                                <li><a href="#">Absolute</a></li>

                                            </ul>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="dropdown">
                                            <a href="email_inbox.html#" class="email-header-link" data-toggle="dropdown">Arrange by <i class="fa fa-angle-down m-l-5"></i></a>
                                            <ul class="dropdown-menu">
                                                <li class="active"><a href="email_inbox.html#">Date</a></li>
                                                <li><a href="email_inbox.html#">RegisteredProprietor</a></li>
                                            
                                            </ul>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
							<?php if($Results) : ?>
				<?php foreach ($Results as $index => $Result) : ?>
				
                                <tr>
                                    <td class="email-select"><a href="UnassignedView/<?php echo $Result -> TransactionID; ?>" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                                    <td class="email-sender">
                                       <?php echo $Result -> TransactionID; ?> (<?php echo $Result -> Title; ?>)
                                    </td>
                                    <td class="email-subject">
                                        <a href="UnassignedView/<?php echo $Result -> TransactionID; ?>" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                        <a href="" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                        <a href="" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                      <?php echo $Result -> LastName; ?>, <?php echo $Result -> FirstName; ?> <?php echo $Result -> MiddleName; ?>
                                    </td>
                                    <td class="email-date"><?php echo $Result -> DateCreated; ?></td>
                                </tr>
                                <?php endforeach;?>
								
                            </tbody>
                        </table>
                        <div class="email-footer clearfix">
                            737 messages
                            <ul class="pagination pagination-sm m-t-0 m-b-0 pull-right">
                                <li class="disabled"><a href="javascript:;"><i class="fa fa-angle-double-left"></i></a></li>
                                <li class="disabled"><a href="javascript:;"><i class="fa fa-angle-left"></i></a></li>
                                <li><a href="javascript:;"><i class="fa fa-angle-right"></i></a></li>
                                <li><a href="javascript:;"><i class="fa fa-angle-double-right"></i></a></li>
                            </ul>
                        </div>
						<?php else: ?>
                        
						
					
					<div class="m-t-5">No results found</div>
					<?php endif; ?>
			        </div>
			    </div>
			    <!-- end col-10 -->
			</div>
			<!-- end row -->
			</div>
		</div>
		<!-- end #content -->
		
        
		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	
	<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url(); ?>assets/js/inbox.demo.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<script>
		$(document).ready(function() {
			App.init();
			Inbox.init();
		});
	</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53034621-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
