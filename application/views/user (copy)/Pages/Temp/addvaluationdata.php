<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
<!-- begin #content -->
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li><a href="javascript:;">Parcels</a></li>
        <li class="active">Add Parcel</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Add Parcel <small>Enter new valuation data</small></h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">

                    <h4 class="panel-title">Add Parcel</h4>
                </div>
                <div class="panel-body">
                    <?PHP
                    echo $this->session->flashdata('msg');
                    ?>
                    <?php echo form_open('start/AddParcelValuation', array('data-parsley-validate' => 'true', 'name' => 'form-wizard', 'id' => 'AddParcelValuation')); ?>

                    <div id="wizard">
                        <ol>
                            <li>
                                Property Information 
                                <small>Please input correctly LR Number, Area size and Registered Proprietor</small>
                            </li>
                            <li>
                                Land Use
                                <small>Please select property type, nearby developments and input nearby value.</small>
                            </li>
                            <li>
                                Road Information
                                <small>Please select road type, road name and input distance from in KM.</small>
                            </li>
                            <li>
                                Town/City Information
                                <small>Please select town type, town name and input distance from in KM.</small>
                            </li>
                            <li>
                                Finish
                                <small>Complete Valuation</small>
                            </li>
                        </ol>
                        <!-- begin wizard step-1 -->
                        <div class="wizard-step-1">
                            <fieldset>
                                <legend class="pull-left width-full">Property Information</legend>
                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-4 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Registry</label>
                                            <select name="Registry" onchange="selectOption()" class="form-control" data-parsley-group="wizard-step-1" required/>
                                            <option></option>
                                            <?php
                                            $GetRegistries = $this->ParcelsValuationModel->GetRegistries();
                                            if (is_array($GetRegistries)) {
                                                foreach ($GetRegistries as $Registry) {
                                                    //echo $courserow->coursename;
                                                    ?>
                                                    <option value="<?= $Registry->RowID; ?>"><?= $Registry->Name; ?></option>
                                                    <?php
                                                }
                                            } else {
                                                echo 'No Registry.';
                                            }
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- begin col-4 -->
                                    <div class="col-md-3">
                                        <div class="form-group block1">
                                            <label>LR NO</label>
                                            <input type="text" name="LRNO" placeholder="LR NO" class="form-control" data-parsley-group="wizard-step-1" required />
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <!-- begin col-4 -->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Area Size</label>
                                            <input type="text" name="AreaSize" placeholder="Area Size" class="form-control" data-parsley-group="wizard-step-1" required />
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <!-- begin col-4 -->
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>Area Unit</label>
                                            <select name="AreaUnit" class="form-control" data-parsley-group="wizard-step-1" required>
                                                <option></option>
                                                <?php
                                                $AreaUnit = $this->ParcelsValuationModel->GetAreaUnit('AreaUnit');
                                                if (is_array($AreaUnit)) {
                                                    foreach ($AreaUnit as $Unit) {
                                                        //echo $courserow->coursename;
                                                        ?>
                                                        <option value="<?= $Unit->RowID; ?>"><?= $Unit->ItemName; ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    echo 'No Campus.';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Nature of Title</label>
                                            <select name="NatureOfTitle" class="form-control" data-parsley-group="wizard-step-1" required/>
                                            <option></option>
                                            <?php
                                            $AreaUnit = $this->ParcelsValuationModel->GetAreaUnit('NatureOfTitle');
                                            if (is_array($AreaUnit)) {
                                                foreach ($AreaUnit as $Unit) {
                                                    //echo $courserow->coursename;
                                                    ?>
                                                    <option value="<?= $Unit->RowID; ?>"><?= $Unit->ItemName; ?></option>
                                                    <?php
                                                }
                                            } else {
                                                echo 'No Campus.';
                                            }
                                            ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Registered Proprietor</label>
                                            <input type="text" name="RegisteredProprietor" placeholder="Registered Name" class="form-control" data-parsley-group="wizard-step-1" required />
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                </div>
                                <!-- end row -->
                            </fieldset>
                        </div>
                        <!-- end wizard step-1 -->
                        <!-- begin wizard step-2 -->
                        <div class="wizard-step-2">
                            <fieldset>
                                <legend class="pull-left width-full">Land Use</legend>
                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-6 -->

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Type of Development</label>
                                            <select name="TypeOfDevelopment" class="form-control" data-parsley-group="wizard-step-2" required>
                                                <option></option>
                                                <?php
                                                $AreaUnit = $this->ParcelsValuationModel->GetAreaUnit('TypeOfDevelopment');
                                                if (is_array($AreaUnit)) {
                                                    foreach ($AreaUnit as $Unit) {
                                                        //echo $courserow->coursename;
                                                        ?>
                                                        <option value="<?= $Unit->RowID; ?>"><?= $Unit->ItemName; ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    echo 'No Campus.';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nearby Development/Infrastructure</label>
                                            <select name="NearbyInfrastructure" class="form-control" data-parsley-group="wizard-step-2" required/>
                                            <option></option>
                                            <?php
                                            $LandUse = $this->ParcelsValuationModel->GetAreaUnit('NearbyInfrastructure');
                                            if (is_array($LandUse)) {
                                                foreach ($LandUse as $Use) {
                                                    //echo $courserow->coursename;
                                                    ?>
                                                    <option value="<?= $Use->RowID; ?>"><?= $Use->ItemName; ?></option>
                                                    <?php
                                                }
                                            } else {
                                                echo 'No Campus.';
                                            }
                                            ?>
                                            <option>International Airport </option>
                                            <option>Shopping Mall</option>
                                            <option>Export Processing Zones</option>
                                            <option>High Income Estates</option>
                                            <option>Middle Income Estates</option>
                                            <option>Low Income Estates</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- begin col-6 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nearby Property Value (ha/FtSQ)</label>
                                            <input type="text" name="NearbyValue" placeholder="5000000" class="form-control" data-parsley-group="wizard-step-2" data-parsley-type="number" required />
                                        </div>
                                    </div>

                                </div>
                                <!-- end row -->
                            </fieldset>
                        </div>



                        <div class="wizard-step-3">
                            <fieldset>
                                <legend class="pull-left width-full"> Road Proximity</legend>
                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-6 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Type of Road</label>
                                            <select name="TypeOfRoad" id="roadtype" name="bb" class="form-control" data-parsley-group="wizard-step-3" required >
                                                <option></option>
                                                <?php
                                                $RoadTypes = $this->ParcelsValuationModel->GetRoadTypes();
                                                if (is_array($RoadTypes)) {
                                                    foreach ($RoadTypes as $RoadType) {
                                                        //echo $courserow->coursename;
                                                        ?>
                                                        <option value="<?= $RoadType->RowID; ?>"><?= $RoadType->TypeOfRoad; ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    echo 'No Campus.';
                                                }
                                                ?>                                              

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4" >
                                        <div class="form-group">
                                            <label>Name of Road</label>
                                            <select id="roadNames" name="NameOfRoad" class="form-control" data-parsley-group="wizard-step-3" required>
                                                <option></option>
                                                <?php
                                                $RoadNames = $this->ParcelsValuationModel->GetRoadNameMatchRoadType(2);

                                                if (is_array($RoadNames)) {
                                                    foreach ($RoadNames as $RoadName) {
                                                        //echo $courserow->coursename;
                                                        ?>
                                                        <option value="<?= $RoadName->RowID; ?>"><?= $RoadName->NameOfRoad; ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    echo 'No Campus.';
                                                }
                                                ?>


                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Distance from the road (KM)</label>
                                            <input type="text" name="DistanceFromRoad" placeholder="5.2" class="form-control" data-parsley-group="wizard-step-3" data-parsley-type="number" required />
                                        </div>
                                    </div>
                                    <!-- end col-6 -->
                                </div>
                                <!-- end row -->
                            </fieldset>
                        </div>
                        <!-- end wizard step-2 -->
                        <!-- begin wizard step-3 -->
                        <div class="wizard-step-4">
                            <fieldset>
                                <legend class="pull-left width-full">Town Proximity</legend>
                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-6 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Type of Town</label>
                                            <select name="TypeOfTown" id="towntype" class="form-control" data-parsley-group="wizard-step-4" required>
                                                <option></option>
                                                <?php
                                                $TownTypes = $this->ParcelsValuationModel->GetTownTypes();
                                                if (is_array($TownTypes)) {
                                                    foreach ($TownTypes as $TownType) {
                                                        //echo $courserow->coursename;
                                                        ?>
                                                        <option value="<?= $TownType->RowID; ?>"><?= $TownType->TypeOfTown; ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    echo 'No Campus.';
                                                }
                                                ?> 
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Name of Town</label>
                                            <select id="townNames" name="NameOfTown" class="form-control" data-parsley-group="wizard-step-4" required>
                                                <option></option> 
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Distance from the town (KM)</label>
                                            <input type="text" name="DistanceFromTown" placeholder="5.2" class="form-control" data-parsley-group="wizard-step-4" data-parsley-type="number" required />
                                        </div>
                                    </div>
                                    <!-- end col-6 -->
                                </div>
                            </fieldset>
                        </div>
                        <!-- end wizard step-3 -->
                        <!-- begin wizard step-4 -->
                        <div>
                            <div class="jumbotron m-b-0 text-center">
                                <h1>Finish</h1>
                                <p>Review new parcel information </p>
                                <p><button type="submit" class="btn btn-success btn-lg" role="button">Save and Add New Parcel</button></p>

                            </div>
                        </div>
                        <!-- end wizard step-4 -->


                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<!-- end #content -->



<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/parsley/dist/parsley.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-wizards-validation.demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<script></script>
<script>
                                                $("#roadtype").change(function (evt) {
                                                    var roadTypeID = document.getElementById('roadtype').value;
                                                    var select = $("#roadtype");
                                                    var selectData = select.serialize();
                                                    var ajax = $.ajax({
                                                        dataType: "json",
                                                        type: 'POST',
                                                        url: "http://localhost/lmais/start/GetRoadNameMatchRoadType/" + roadTypeID,
                                                        data: selectData
                                                    });
                                                    ajax.done(function (response) {
                                                        $("#testurl").html(response);
                                                        var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                        for (var key in response) {
                                                            if (response.hasOwnProperty(key)) {
                                                                //alert(key);
                                                                //alert(response[key].RowID);
                                                                //alert(response[key].NameOfRoad);
                                                                listItems += "<option value='" + response[key].NameOfRoad + "'>" + response[key].NameOfRoad + "</option>";
                                                            }
                                                        }
                                                        $("#roadNames").html(listItems);
                                                    });
                                                    ajax.fail(function () {

                                                    });
                                                });

                                                $("#towntype").change(function (evt) {
                                                    var townTypeID = document.getElementById('towntype').value;
                                                    var select = $("#towntype");
                                                    var selectData = select.serialize();
                                                    var ajax = $.ajax({
                                                        dataType: "json",
                                                        type: 'POST',
                                                        url: "http://localhost/lmais/start/GetTownNamesMatchTownType/" + townTypeID,
                                                        data: selectData
                                                    });
                                                    ajax.done(function (response) {
                                                        var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                        for (var key in response) {
                                                            if (response.hasOwnProperty(key)) {
                                                                //alert(key);
                                                                //alert(response[key].RowID);
                                                                //alert(response[key].NameOfRoad);
                                                                listItems += "<option value='" + response[key].NameOfTown + "'>" + response[key].NameOfTown + "</option>";
                                                            }
                                                        }
                                                        $("#townNames").html(listItems);
                                                    });
                                                    ajax.fail(function () {

                                                    });
                                                });

</script>

<script>
    $(document).ready(function () {
        App.init();
        FormWizardValidation.init();
    });
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>