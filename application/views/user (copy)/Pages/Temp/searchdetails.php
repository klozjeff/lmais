<?php if ($Results) : ?>
    <?php foreach ($Results as $index => $Result) : ?>
        <div id="content" class="content content-full-width">
            <div class="container-fluid">
                <div class="row" style="background: #008A8A; padding: 5px;">
                    <div class="col-sm-8" style="background: #008A8A; height: 170px; padding: 5px; ">
                        <div class="col-sm-6" style="height: 150px; border-right: 1px dashed #FFF;">
                            <p style="font-weight: bold; color: #FFF;"><span style="color: #000">Title:</span> Kajiado/Kitengela/5</p>
                            <p style="font-weight: bold; color: #FFF;"><span style="color: #000">Area:</span> 0.9ha</p>
                            <p style="font-weight: bold; color: #FFF;"><span style="color: #000">Tenure:</span> Leasehold</p>
                            <p style="font-weight: bold; color: #FFF;"><span style="color: #000">Regsheet:</span> 4&5</p>
                            <p style="font-weight: bold; color: #FFF;"><span style="color: #000">Status:</span> Active</p>
                            <p style="font-weight: bold; color: #FFF;"><span style="color: #000">Parent:</span> Kajiado/Kitengela/2</p>
                        </div>
                        <div class="col-sm-6" style="height: 150px;">
                            <h4 class="page-header">Easements</h4><hr>
                            <p style="font-weight: bold; color: #000">1.9 meters of road passes through this plot</p>
                        </div>
                    </div>

                    <div class="col-sm-4" style="background: #c1bfbf; height: 170px; ">
                        <div class="row" style="border-bottom: 1px dashed #FFF;padding: 15px;">
                            <div class="col-sm-6 text-center" style="font-weight: bold; border-right: 1px dashed #FFF;">
                                <p class="">Proprietorships</p>
                                <h4><span class="badge badge-success text-center">1</span></h4>
                            </div>
                            <div class="col-sm-6 text-center" style="font-weight: bold;">
                                <p class="">Encumberances</p>
                                <h4><span class="badge badge-success text-center">5</span></h4>
                            </div>
                        </div>
                        <div class="row" style="padding: 15px;">
                            <div class="col-sm-6 text-center" style="font-weight: bold; border-right: 1px dashed #FFF;">
                                <p class="">Inhibitions</p>
                                <h4><span class="badge badge-success text-center">2</span></h4>
                            </div>
                            <div class="col-sm-6 text-center" style="font-weight: bold;">
                                <p class="">Tasks</p>
                                <h4><span class="badge badge-success text-center">0</span></h4>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="" style="margin: 10px 0 0 0;">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#overview">Overview</a></li>
                        <!--i><a data-toggle="tab" href="#tasks">Tasks</a></li>
                        <li><a data-toggle="tab" href="#files">Files</a></li>
                        <li><a data-toggle="tab" href="#comments">Comments</a></li-->
                        <li><a data-toggle="tab" href="#proprietorships">Proprietorships</a></li>
                        <li><a data-toggle="tab" href="#incumberances">Incumberances</a></li>
                        <li><a data-toggle="tab" href="#inhibitions">Inhibitions</a></li>
                        <li><a data-toggle="tab" href="#activities">Activities</a></li>
                        <!--li><a data-toggle="tab" href="#valuations">Valuations</a></li>
                        <li><a data-toggle="tab" href="#gis">GIS</a></li-->
                    </ul>

                    <div class="tab-content">
                        <div id="overview" class="tab-pane fade in active">
                            <div class="container">
                                <div class="row">
                                    <h3>Overview</h3>                                    
                                    <div class="col-sm-5" style="border: 1px dashed #008A8A; padding: 5px;">
                                        <p style="font-weight: bold;"><span class="text-success">PROVINCE:</span> RiftValley</p>
                                        <p style="font-weight: bold;"><span class="text-success">COUNTY:</span> Kajiado</p>
                                        <p style="font-weight: bold;"><span class="text-success">REGISTRATION SECTION:</span> Kitengela</p>
                                        <p style="font-weight: bold;"><span class="text-success">PARCEL No:</span> 5</p>
                                        <p style="font-weight: bold;"><span class="text-success">REG SHEET:</span> 4&5</p>
                                        <p style="font-weight: bold;"><span class="text-success">Parent:</span> Kajiado/Kitengela/2</p>
                                        <p style="font-weight: bold; color: #FFF;"><span class="">Parent:</span> Kajiado/Kitengela/2</p>
                                    </div>
                                    <div class="col-sm-5" style="border: 1px dashed #008A8A; padding: 5px; margin-left: 15px;">
                                        <p style="font-weight: bold;"><span class="text-success">LESSER:</span> GoK</p>
                                        <p style="font-weight: bold;"><span class="text-success">LESSEE:</span> Kimani Migwi</p>
                                        <p style="font-weight: bold;"><span class="text-success">FROM:</span> 10/20/2015</p>
                                        <p style="font-weight: bold;"><span class="text-success">TERM:</span> 99 Years</p>
                                        <p style="font-weight: bold;"><span class="text-success">PREMIUM:</span> 3,000,000</p>
                                        <p style="font-weight: bold;"><span class="text-success">RENT:</span> 3,000/pm</p>
                                        <p style="font-weight: bold;"><span class="text-success">REV:</span> Y</p>                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div id="tasks" class="tab-pane fade">
                            <h3>Tasks</h3>
                            <div style="margin: 3px;">
                                <table id="data-table" class="table table-bordered table-hover table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Nature Of Encumbrance</th>
                                            <th>Encumbrance To</th>
                                            <th>Encumbrance Amount</th>
                                            <th>Lease From</th>
                                            <th>Lease To</th>
                                            <th>Lease Rent</th>
                                            <th>Revisable Status</th>
                                            <th>Encumbrance Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>datafromsource</td>
                                            <td>datafromsource</td>
                                            <td>datafromsource</td>
                                            <td>datafromsource</td>
                                            <td>datafromsource</td>
                                            <td>datafromsource</td>
                                            <td>datafromsource</td>
                                            <td>datafromsource</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="files" class="tab-pane fade">
                            <h3>Files</h3>
                            <p>Some content in menu 2.</p>
                        </div>
                        <div id="comments" class="tab-pane fade">
                            <h3>Comments</h3>
                            <p>Some content in menu 2.</p>
                        </div>
                        <div id="proprietorships" class="tab-pane fade">
                            <h3>Proprietorships</h3>
                            <p style="font-weight: bold;"><span class="text-success">CURRENT PROPRIETOR:</span> Kim Khan</p>
                        </div>
                        <div id="incumberances" class="tab-pane fade">
                            <h3>Encumberances</h3>
                            <p style="font-weight: bold;"><span class="text-success">No Encumbrances</span></p>
                        </div>
                        <div id="inhibitions" class="tab-pane fade">
                            <h3>Inhibitions</h3>
                            <p style="font-weight: bold;"><span class="text-success">No further dealings until divorce is finalised</span></p>
                        </div>
                        <div id="activities" class="tab-pane fade">
                            <h3>Activities</h3>
                            <p style="font-weight: bold;"><span class="text-success">John Gatobu, Dec 23, 2015 08:16am</span></p>
                            <p>Added transfer_details.pdf</p>
                        </div>
                        <div id="valuations" class="tab-pane fade">
                            <h3>Valuations</h3>
                            <p>Some content in menu 2.</p>
                        </div>
                        <div id="gis" class="tab-pane fade">
                            <h3>GIS</h3>
                            <p>Some content in menu 2.</p>
                        </div>
                    </div>
                </div>

            </div>
            <!-- begin vertical-box -->
            <div class="vertical-box">

                <!-- begin vertical-box-column -->
                <div class="vertical-box-column width-250">

                    <!-- begin wrapper -->
                    <div class="wrapper bg-silver text-center">
                        <a href="<?php echo base_url(); ?>start/EditValuation/<?php echo $Result->RowID; ?>" class="btn btn-success p-l-40 p-r-40 btn-sm">
                            Edit Valuation
                        </a>
                    </div>
                    <!-- end wrapper -->
                    <!-- begin wrapper -->
                    <div class="wrapper">
                        <p><b>FOLDERS</b></p>
                        <ul class="nav nav-pills nav-stacked nav-sm">
                            <li class="active"><a data-toggle="tab" href="#"><i class="fa fa-inbox fa-fw m-r-5"></i> Details </a></li>
                            <li><a data-toggle="tab" href="#"><i class="fa fa-flag fa-fw m-r-5"></i> History</a></li>
                            <li><a data-toggle="tab" href="#"><i class="fa fa-flag fa-fw m-r-5"></i> History</a></li>
                            <li><a data-toggle="tab" href="#"><i class="fa fa-send fa-fw m-r-5"></i> Attached Documents</a></li>

                        </ul>

                    </div>
                    <!-- end wrapper -->
                </div>
                <!-- end vertical-box-column -->
                <!-- begin vertical-box-column -->
                <div class="vertical-box-column bg-white">
                    <!-- begin wrapper -->
                    <div class="wrapper bg-silver-lighter clearfix">
                        <div class="btn-group m-r-5">
                            <a href="<?php echo base_url(); ?>start/SearchLR" class="btn btn-white btn-sm"><i class="fa fa-reply"></i></a>
                        </div>
                        <div class="btn-group m-r-5">
                            <a href="#" class="btn btn-white btn-sm p-l-20 p-r-20"><i class="fa fa-trash-o"></i></a>
                            <a href="#" onclick="window.print()" class="btn btn-white btn-sm p-l-20 p-r-20"><i class="fa fa-print"></i></a>
                        </div>
                        <div class="pull-right">
                            <div class="btn-group btn-toolbar">
                                <a href="#" class="btn btn-white btn-sm disabled"><i class="fa fa-arrow-up"></i></a>
                                <a href="#" class="btn btn-white btn-sm"><i class="fa fa-arrow-down"></i></a>
                            </div>
                            <div class="btn-group m-l-5">
                                <a href="#" class="btn btn-white btn-sm"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- end wrapper -->
                    <!-- begin wrapper -->

                    <!-- begin invoice -->
                    <?PHP
                    echo $this->session->flashdata('msg');
                    ?>
                    <div class="invoice">

                        <div class="invoice-company">
                            <span class="pull-right hidden-print">
                                <a href="javascript:;" class="btn btn-sm btn-success m-b-10"><i class="fa fa-download m-r-5"></i> Export as PDF</a>
                                <a href="javascript:;" onclick="window.print()" class="btn btn-sm btn-success m-b-10"><i class="fa fa-print m-r-5"></i> Print</a>
                            </span>
                            Valuation Report
                        </div>
                        <div class="invoice-header">
                            <table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: 'Roboto', sans-serif; text-align:center;">
                                <thead>
                                    <tr>
                                        <td align="center"><img alt="" src="https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png" style="height:100px;" />
                                            <h4><strong></strong></h4>


                                            <strong>MINISTRY OF LAND, HOUSING AND URBAN DEVELOPMENT</strong>

                                            <p style="margin-bottom:0px;">_____________________________________</p>
                                            <strong><?php echo $Result->Registry; ?> Land Registry</strong><br />
                                            P.O. Box XXXXXX, Nairobi Kenya<br />

                                            <p style="margin-bottom:0px;">_____________________________________</p>
                                            <strong>VALUATION REQUISITION FOR STAMP DUTY</strong></td>
                                    </tr>
                                </thead>
                            </table>
                            <div class="invoice-from">
                                <small></small>
                                <address class="m-t-5 m-b-5">
                                    <strong><?php echo $Result->CreatedBy; ?>,</strong><br />
                                    <?php echo $Result->Registry; ?>,<br />

                                    Valuation Officer
                                </address>
                            </div>

                            <div class="invoice-date">
                                <small><?php echo $Result->NatureOfTitle; ?></small>
                                <div class="date m-t-5"><?php echo $Result->DateCreated; ?></div>
                                <div class="invoice-detail">
                                    #Val-<?php echo $Result->RowID; ?> <br />

                                </div>
                            </div>
                        </div>
                        <div class="invoice-content">
                            <div class="table-responsive">
                                <table class="table table-invoice">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Description</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                1. Seller's/Vendor's Name if Full<br />

                                            </td>
                                            <td><?php echo $Result->RegisteredProprietor; ?></td>

                                        </tr>

                                        <tr>
                                            <td>
                                                2. Purchaser's Name if Full<br />

                                            </td>
                                            <td></td>

                                        </tr>

                                        <tr>
                                            <td>
                                                3. LR NO.<br />

                                            </td>
                                            <td><?php echo $Result->LRNO; ?></td>

                                        </tr>
                                        <tr>
                                            <td>
                                                4. AreaSize (Ha/Acre/SQFT)<br />

                                            </td>
                                            <td><?php echo $Result->AreaSize; ?> <?php echo $Result->AreaUnit; ?></td>

                                        </tr>
                                        <tr>
                                            <td>
                                                5. Situation<br />

                                            </td>
                                            <td><?php echo $Result->DistanceFromTown; ?> From <?php echo $Result->NameOfTown; ?></td>

                                        </tr>
                                        <tr>
                                            <td>
                                                6. Status<br />

                                            </td>
                                            <td><?php echo $Result->DevelopmentStatus; ?> </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                7. Interest Passing Whole ( ) or Part () Indicate share <br />

                                            </td>
                                            <td></td>

                                        </tr>

                                        <tr>
                                            <td>
                                                8. Tenure (Leasehold / Freehold)<br />

                                            </td>
                                            <td><?php echo $Result->NatureOfTitle; ?> </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                9. Value Submitted By Parties<br />

                                            </td>
                                            <td> </td>

                                        </tr>

                                        <tr>
                                            <td>
                                                10. Date Of Transfer<br />

                                            </td>
                                            <td></td>

                                        </tr>


                                        <tr>
                                            <td>
                                                11. Date when first presented in Land Registry<br />

                                            </td>
                                            <td> </td>

                                        </tr>

                                        <tr>
                                            <td>
                                                12. Advocate Acting<br />

                                            </td>
                                            <td> </td>

                                        </tr>

                                        <tr>
                                            <td>
                                                13. Contact Person<br />

                                            </td>
                                            <td><?php echo $Result->RegisteredProprietor; ?> </td>

                                        </tr>

                                        <tr>
                                            <td>
                                                14.  Telephone Number (Mobile / Landline)<br />

                                            </td>
                                            <td> <?php echo $Result->Telephone; ?> </td>

                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="invoice-price">
                                <div class="invoice-price-left">
                                    <div class="invoice-price-row">
                                        <div class="sub-price">
                                            <small>Valuation formula</small>
                                            (Used Valuation formula)
                                        </div>
                                        <div class="sub-price">


                                        </div>
                                    </div>
                                    <div class="invoice-price-right">
                                        <small>Valued Amount </small> Kshs. <?php echo $Result->LandValue; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="invoice-footer text-muted">
                                <p class="text-center m-b-5">
                                    Valuer Information
                                </p>
                                <p class="text-center">
                                    <span class="m-r-10"><i class="fa fa-globe"></i> www.ardhi.go.ke</span>
                                    <span class="m-r-10"><i class="fa fa-phone"></i> T:0724375147</span>
                                    <span class="m-r-10"><i class="fa fa-envelope"></i> simon@ardhi.go.ke</span>
                                </p>
                            </div>
                        </div>
                        <!-- end invoice -->
                    <?php endforeach; ?>

                <?php else: ?>



                    <div class="m-t-5">No results found</div>
                <?php endif; ?>
                <!-- end wrapper -->

            </div>
            <!-- end vertical-box-column -->
        </div>
        <!-- end vertical-box -->
    </div>
    <!-- end #content -->

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
                            $(document).ready(function () {
                                App.init();
                            });
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
