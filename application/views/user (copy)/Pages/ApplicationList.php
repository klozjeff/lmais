<div class="row">
    <div class="col-md-8">
        <h4 class="page-title" style="padding: 10px 0 0 50px;">Make Application</h4>
        <?php echo $this->session->flashdata('msg'); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-8"> 	
        <div class="panel-group" id="accordion">
            <div class="panel panel-inverse overflow-hidden">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion" href="ui_tabs_accordions.html#collapseOne" aria-expanded="true">
                            <i class="more-less fa fa-plus-circle"></i> 
                            A. LAND REGISTRATION
                        </a>
                    </h3>
                </div>
                <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <ul class="list-group">
                            <li class="list-group-item"><a href="<?php echo base_url(); ?>UserView/LoadInfo/transfer" style="text-decoration : none">1. TRANSFER OF LAND (RL 1)</a></li>
                            <li class="list-group-item"><a href="<?php echo base_url(); ?>UserView/LoadInfo/rent" style="text-decoration : none">2. LAND RENT</a></li>
                            <li class="list-group-item"><a href="<?php echo base_url(); ?>UserView/LoadInfo/caution" style="text-decoration : none">3. CAUTION (RL 22)</a></li>
                            <li class="list-group-item"><a href="<?php echo base_url(); ?>UserView/LoadInfo/caution_withdrawal" style="text-decoration : none">4. WITHDRAWAL OF CAUTION</a></li>
                            <li class="list-group-item"><a href="<?php echo base_url(); ?>UserView/LoadInfo/search" style="text-decoration : none">5. LAND SEARCH (RL 27)</a></li>
                            <li class="list-group-item"><a href="<?php echo base_url(); ?>UserView/LoadInfo/discharge" style="text-decoration : none">6. DISCHARGE OF CHARGE</a></li>
                            <!--                           
                                                                               <li class="list-group-item">4. SUCCESSION</li>
                                                        <li class="list-group-item">5. CORRECTION OF NAMES</li>
                                                        <li class="list-group-item">7. POWER OF ATTORNEY (RL 17)</li>
                                                        <li class="list-group-item">8. LEASE</li>
                                                        <li class="list-group-item">9. EXTENSION OF LEASE</li>
                                                        <li class="list-group-item">10. CHARGE</li>                          
                                                        <li class="list-group-item">12. STAMP DUTY PAYMENT</li>							
                                                                                    <li class="list-group-item">14. TRANSFER OF LEASE</li>
                            -->


                        </ul>

                    </div>
                </div>
            </div>
            <div class="panel panel-inverse overflow-hidden">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="ui_tabs_accordions.html#collapseTwo" aria-expanded="false">
                            <i class="more-less fa fa-plus-circle"></i> 
                            B. MAPS
                        </a>
                    </h3>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <ul class="list-group">
                            <li class="list-group-item">1. SALE OF MAPS</li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-inverse overflow-hidden">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="ui_tabs_accordions.html#collapseThree" aria-expanded="false">
                            <i class="more-less fa fa-plus-circle"></i> 
                            C. OTHERS
                        </a>
                    </h3>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" aria-expanded="false">
                    <div class="panel-body">
                        <ul class="list-group">
                            <li class="list-group-item">GENERAL INQUIRY</li>

                        </ul>
                    </div>
                </div>
            </div>




        </div>



    </div>  <!-- End column 8 -->

    <div class="col-md-4">
        <div class="box-content ">

            <p>NAME: <?php
                if ($this->session->userdata('Logged_in') != '') {

                    echo $this->session->userdata('FirstName') . " " . $this->session->userdata('LastName');
                } else {
                    redirect('start/index');
                }
                ?></p>
            <p>EMAIL:<?php
                if ($this->session->userdata('Logged_in') != '') {

                    echo $this->session->userdata('EmailAddress');
                } else {
                    redirect('start/index');
                }
                ?></p>
            <p>TEL: 0707111222</p>

            <p>
                <a href="<?php echo base_url(); ?>users/logout" class="btn btn-primary m-r-5">LOG OUT</a>
            </p>


        </div>
    </div>
    <div class="col-md-4 col-md-offset-8">
        <div class="box-content">							   
            <h3>Quick Links</h3>
            <ul style="font-size:14px;">
                <li>Department of Immigration</li>
                <li>Ministry of Lands</li>
                <li>Office of the attorney general</li>
                <li>Mombasa County</li>
            </ul>

        </div>
    </div>


    <!-- end col-3 -->
</div>
<!-- end row -->



</div>

<!-- begin row -->


<!-- end row -->
</div>
<!-- end #content -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/morris/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/morris/morris.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/gritter/js/jquery.gritter.js"></script>
<script src="<?php echo base_url(); ?>assets/js/dashboard-v2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function () {
        App.init();
        DashboardV2.init();
    });
</script>


<script>
    function toggleIcon(e) {
        $(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('fa-plus-circle fa-minus-circle');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);

</script>

</body>
</html>
