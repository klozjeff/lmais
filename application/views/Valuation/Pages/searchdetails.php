<?php if($Results) : ?>
				<?php foreach ($Results as $index => $Result) : ?>
		<div id="content" class="content content-full-width">
		    <!-- begin vertical-box -->
		    <div class="vertical-box">
		        <!-- begin vertical-box-column -->
		        <div class="vertical-box-column width-250">
		            <!-- begin wrapper -->
                    <div class="wrapper bg-silver text-center">
                        <a href="<?php echo base_url(); ?>start/EditValuation/<?php echo $Result -> RowID; ?>" class="btn btn-success p-l-40 p-r-40 btn-sm">
                            Edit Valuation
                        </a>
                    </div>
		            <!-- end wrapper -->
		            <!-- begin wrapper -->
                    <div class="wrapper">
                        <p><b>FOLDERS</b></p>
                        <ul class="nav nav-pills nav-stacked nav-sm">
                            <li class="active"><a data-toggle="tab" href="#"><i class="fa fa-inbox fa-fw m-r-5"></i> Last Valuation </a></li>
                            <li><a data-toggle="tab" href="#"><i class="fa fa-flag fa-fw m-r-5"></i> Previous Valuations</a></li>
							
                           
                        </ul>
                        
                    </div>
		            <!-- end wrapper -->
		        </div>
		        <!-- end vertical-box-column -->
		        <!-- begin vertical-box-column -->
		        <div class="vertical-box-column bg-white">
		            <!-- begin wrapper -->
                    <div class="wrapper bg-silver-lighter clearfix">
                        <div class="btn-group m-r-5">
                            <a href="<?php echo base_url(); ?>start/SearchLR" class="btn btn-white btn-sm"><i class="fa fa-reply"></i></a>
                        </div>
                        <div class="btn-group m-r-5">
                            <a href="#" class="btn btn-white btn-sm p-l-20 p-r-20"><i class="fa fa-trash-o"></i></a>
                            <a href="#" onclick="window.print()" class="btn btn-white btn-sm p-l-20 p-r-20"><i class="fa fa-print"></i></a>
                        </div>
                        <div class="pull-right">
                            <div class="btn-group btn-toolbar">
                                <a href="#" class="btn btn-white btn-sm disabled"><i class="fa fa-arrow-up"></i></a>
                                <a href="#" class="btn btn-white btn-sm"><i class="fa fa-arrow-down"></i></a>
                            </div>
                            <div class="btn-group m-l-5">
                                <a href="#" class="btn btn-white btn-sm"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                    </div>
		            <!-- end wrapper -->
		            <!-- begin wrapper -->
					
                    <!-- begin invoice -->
						<?PHP
				echo $this->session->flashdata('msg');
				?>
			<div class="invoice">

                <div class="invoice-company">
                    <span class="pull-right hidden-print">
                    <a href="javascript:;" class="btn btn-sm btn-success m-b-10"><i class="fa fa-download m-r-5"></i> Export as PDF</a>
                    <a href="javascript:;" onclick="window.print()" class="btn btn-sm btn-success m-b-10"><i class="fa fa-print m-r-5"></i> Print</a>
                    </span>
                    Valuation Report
                </div>
                <div class="invoice-header">
							<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: 'Roboto', sans-serif; text-align:center;">
	<thead>
		<tr>
			<td align="center"><img alt="" src="https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png" style="height:100px;" />
			<h4><strong></strong></h4>

			
			<strong>MINISTRY OF LAND, HOUSING AND URBAN DEVELOPMENT</strong>

			<p style="margin-bottom:0px;">_____________________________________</p>
			<strong><?php echo $Result -> Registry; ?> Land Registry</strong><br />
			P.O. Box XXXXXX, Nairobi Kenya<br />
	
			<p style="margin-bottom:0px;">_____________________________________</p>
			<strong>VALUATION REQUISITION FOR STAMP DUTY</strong></td>
		</tr>
	</thead>
</table>
                    <div class="invoice-from">
                        <small></small>
                        <address class="m-t-5 m-b-5">
                            <strong><?php echo $Result -> CreatedBy; ?>,</strong><br />
                           <?php echo $Result -> Registry; ?>,<br />
                            
                            Valuation Officer
                        </address>
                    </div>
                    
                    <div class="invoice-date">
                        <small><?php echo $Result -> NatureOfTitle; ?></small>
                        <div class="date m-t-5"><?php echo $Result -> DateCreated; ?></div>
                        <div class="invoice-detail">
                            #Val-<?php echo $Result -> RowID; ?> <br />
                            
                        </div>
                    </div>
                </div>
                <div class="invoice-content">
                    <div class="table-responsive">
                        <table class="table table-invoice">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Description</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        1. Seller's/Vendor's Name if Full<br />
                                        
                                    </td>
                                    <td><?php echo $Result -> RegisteredProprietor; ?></td>

                                </tr>
								
								<tr>
                                    <td>
                                        2. Purchaser's Name if Full<br />
                                        
                                    </td>
                                    <td></td>

                                </tr>
                                
								<tr>
                                    <td>
                                       3. LR NO.<br />
                                        
                                    </td>
                                    <td><?php echo $Result -> LRNO; ?></td>

                                </tr>
								<tr>
                                    <td>
                                        4. AreaSize (Ha/Acre/SQFT)<br />
                                        
                                    </td>
                                    <td><?php echo $Result -> AreaSize; ?> <?php echo $Result -> AreaUnit; ?></td>

                                </tr>
								<tr>
                                    <td>
                                        5. Situation<br />
                                        
                                    </td>
                                    <td><?php echo $Result -> DistanceFromTown; ?> From <?php echo $Result -> NameOfTown; ?></td>

                                </tr>
								<tr>
                                    <td>
                                        6. Status<br />
                                        
                                    </td>
                                    <td><?php echo $Result -> DevelopmentStatus; ?> </td>

                                </tr>
								<tr>
                                    <td>
                                        7. Interest Passing Whole ( ) or Part () Indicate share <br />
                                        
                                    </td>
                                    <td></td>

                                </tr>
								
								<tr>
                                    <td>
                                        8. Tenure (Leasehold / Freehold)<br />
                                        
                                    </td>
                                    <td><?php echo $Result -> NatureOfTitle; ?> </td>

                                </tr>
								<tr>
                                    <td>
                                       9. Value Submitted By Parties<br />
                                        
                                    </td>
                                    <td> </td>

                                </tr>
								
								<tr>
                                    <td>
                                        10. Date Of Transfer<br />
                                        
                                    </td>
                                    <td></td>

                                </tr>
								
								
								<tr>
                                    <td>
                                       11. Date when first presented in Land Registry<br />
                                        
                                    </td>
                                    <td> </td>

                                </tr>
								
								<tr>
                                    <td>
                                      12. Advocate Acting<br />
                                        
                                    </td>
                                    <td> </td>

                                </tr>
								
								<tr>
                                    <td>
                                       13. Contact Person<br />
                                        
                                    </td>
                                    <td><?php echo $Result -> RegisteredProprietor; ?> </td>

                                </tr>
								
								<tr>
                                    <td>
                                       14.  Telephone Number (Mobile / Landline)<br />
                                        
                                    </td>
                                    <td> <?php echo $Result -> Telephone; ?> </td>

                                </tr>
								
                            </tbody>
                        </table>
                    </div>
                    <div class="invoice-price">
                        <div class="invoice-price-left">
                            <div class="invoice-price-row">
                                <div class="sub-price">
                                    <small>Valuation formula</small>
                                    (Used Valuation formula)
                                </div>
                                <div class="sub-price">
                                 
                                
                            </div>
                        </div>
                        <div class="invoice-price-right">
                            <small>Valued Amount </small> Kshs. <?php echo $Result -> LandValue; ?>
                        </div>
                    </div>
                </div>
               
                <div class="invoice-footer text-muted">
                    <p class="text-center m-b-5">
                        Valuer Information
                    </p>
                    <p class="text-center">
                        <span class="m-r-10"><i class="fa fa-globe"></i> www.ardhi.go.ke</span>
                        <span class="m-r-10"><i class="fa fa-phone"></i> T:0724375147</span>
                        <span class="m-r-10"><i class="fa fa-envelope"></i> simon@ardhi.go.ke</span>
                    </p>
                </div>
            </div>
			<!-- end invoice -->
					<?php endforeach;?>
					
					<?php else: ?>
                        
						
					
					<div class="m-t-5">No results found</div>
					<?php endif; ?>
		            <!-- end wrapper -->
		            
		        </div>
		        <!-- end vertical-box-column -->
		    </div>
		    <!-- end vertical-box -->
		</div>
		<!-- end #content -->
		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<script>
		$(document).ready(function() {
			App.init();
		});
	</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53034621-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
