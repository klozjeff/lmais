<div id="header" class="header navbar navbar-default navbar-fixed-top">
			<!-- begin container-fluid -->
            <div class="col-md-12">
			<div class="container-fluid">
				<!-- begin mobile sidebar expand / collapse button -->
				<div class="navbar-header">
					<a href="<?php echo base_url(); ?>/start/loadindex" class="navbar-brand">MINISTRY OF LANDS AND PHYSICAL PLANNING</a>
					<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

				</div>
				<!-- end mobile sidebar expand / collapse button -->
				

			
<!-- begin header navigation right -->
				<ul class="nav navbar-nav navbar-right">
					
					<li class="dropdown navbar-user">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
							<img src="<?php echo base_url(); ?>assets/img/user-13.jpg" alt="" /> 
							<span class="hidden-xs"> 
							<?php 
							
							if($this->session->userdata('Logged_in')!='')
							{

							echo $this->session->userdata('FirstName') ." ". $this->session->userdata('LastName');

							}
							else
							{
								redirect('start/index');
							}
							?></span> <b class="caret"></b>
						</a>
						<ul class="dropdown-menu animated fadeInLeft">
							<li class="arrow"></li>
							<li><a href="javascript:;">Edit Profile</a></li>
							<li class="divider"></li>
							<li><a href="<?php echo base_url(); ?>users/logout">Log Out</a></li>
						</ul>
					</li>
				</ul>

			
				
				<!-- end header navigation right -->
				</div>
			<!-- end container-fluid -->
		</div>
		</div>






		