<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <style type="text/css">
            body {
                background-color: #fff;
                margin: 40px;
                font-family: Lucida Grande, Verdana, Sans-serif;
                font-size: 14px;
                color: #4F5155;
            }

            a {
                color: #003399;
                background-color: transparent;
                font-weight: normal;
            }

            h1 {
                color: #444;
                background-color: transparent;
                border-bottom: 1px solid #D0D0D0;
                font-size: 16px;
                font-weight: bold;
                margin: 24px 0 2px 0;
                padding: 5px 0 6px 0;
            }

            code {
                font-family: Monaco, Verdana, Sans-serif;
                font-size: 12px;
                background-color: #f9f9f9;
                border: 1px solid #D0D0D0;
                color: #002166;
                display: block;
                margin: 14px 0 14px 0;
                padding: 12px 10px 12px 10px;
            }

            .text-center{
                text-align: center;
            }
            .centered {
                margin: 0 auto;

                margin-left: 250px;
            }
            .text-bold{
                font-weight: bold;
            }
            .pull-left{
                float: left;
            }
            .pull-right{
                float: right;
            }
            .dotted{
                border-bottom: 1px dotted #000;
                text-decoration: none;
            }
            .text-justify{
                text-align: justify;
            }
            .text-font-size{
                font-size: 18px;
            }

        </style>
    </head>
    <body>
        <div>
        <div class="centered">
            <img width="100" height="100"src="<?php echo $_SERVER["DOCUMENT_ROOT"] . '/lmais/assets/title/coatofarms.png'; ?>">
        </div>
        <div>
            <p class="text-center">REPUBLIC OF KENYA</p>
            <p class="text-center">_________</p>
            <p class="text-center">THE REGISTERED LAND ACT</p>
            <p class="text-center">(Chapter 300)</p>
            <p class="text-center" style="font-style: italic;font-size: 32px">Title Deed</p>
            <div class="text-font-size">
                <p>Title Number <span class="text-bold dotted"> MITI MINGI  / NAIVASHA / BLOCK 12</span></p>
                <p>Approximate Area <span class="text-bold dotted"> .5 HECTARES</span></p>
                <p>Registry Map Sheet No <span class="text-bold  dotted"></span></p>
                <p>This is to certify that <span class="text-bold dotted">JAMES KAMAU<br> ID: 12313123, P.O.Box 312312-20101, NAIROBI</span></p>
                <p class="text-justify">
                    is now registered as the absolute proprietor of the land comprised in the above mentioned title, subject to the entries in the register relating to the land and to such of the overriding interests set out in section 30  of the Registered Land Act as may for the time being subsist and affect the land.
                </p>
            </div>
            <div style="clear: both;">
                <table>
                    <tr>
                        <td>
                            <div class="pull-left" style="width: 150px;"> 
                                <img width="100" height="100"src="<?php echo $_SERVER["DOCUMENT_ROOT"] . '/lmais/assets/title/octagon.jpg'; ?>">
                            </div>
                        </td>
                        <td class="text-font-size">
                            <p class="text-justify">GIVEN under my hand and seal of the
                                <span class="text-bold">NAIROBI</span> District Land Registry
                                this <span class="text-bold">12<super>th</super></span> day of <span class="text-bold">February, 2010</span>
                            </p>
                            <p></p>
                            <p></p>
                            <p>................................</p>
                            <p><i>Land Registrar</i></p>
                        </td>
                    </tr>
                </table>
            </div>
        </p>
    </div>
</body>
<body>
    <div class="centered">
        <img width="100" height="100"src="<?php echo $_SERVER["DOCUMENT_ROOT"] . '/lmais/assets/title/coatofarms.png'; ?>">
    </div>
    <div>
        <p class="text-center">REPUBLIC OF KENYA</p>
        <p class="text-center">_________</p>
        <p class="text-center">THE REGISTERED LAND ACT</p>
        <p class="text-center">(Chapter 300)</p>
        <p class="text-center" style="font-style: italic;font-size: 32px">Title Deed</p>
        <div class="text-font-size">
            <p>Title Number <span class="text-bold dotted"> MITI MINGI  / NAIVASHA / BLOCK 12</span></p>
            <p>Approximate Area <span class="text-bold dotted"> .5 HECTARES</span></p>
            <p>Registry Map Sheet No <span class="text-bold  dotted"></span></p>
            <p>This is to certify that <span class="text-bold dotted">JAMES KAMAU<br> ID: 12313123, P.O.Box 312312-20101, NAIROBI</span></p>
            <p class="text-justify">
                is now registered as the absolute proprietor of the land comprised in the above mentioned title, subject to the entries in the register relating to the land and to such of the overriding interests set out in section 30  of the Registered Land Act as may for the time being subsist and affect the land.
            </p>
        </div>
        <div style="clear: both;">
            <table>
                <tr>
                    <td>
                        <div class="pull-left" style="width: 150px;"> 
                            <img width="100" height="100"src="<?php echo $_SERVER["DOCUMENT_ROOT"] . '/lmais/assets/title/octagon.jpg'; ?>">
                        </div>
                    </td>
                    <td class="text-font-size">
                        <p class="text-justify">GIVEN under my hand and seal of the
                            <span class="text-bold">NAIROBI</span> District Land Registry
                            this <span class="text-bold">12<super>th</super></span> day of <span class="text-bold">February, 2010</span>
                        </p>
                        <p></p>
                        <p></p>
                        <p>................................</p>
                        <p><i>Land Registrar</i></p>
                    </td>
                </tr>
            </table>
        </div>
    </p>
</div>
</body>
</html>