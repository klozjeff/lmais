<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
<!-- begin #content -->
<script type="text/javascript">
    var step = <?php echo array_key_exists('step', $input) ? $input['step'] : -1; ?>;
    var inlineJs = {
        baseUrl: "<?php echo base_url(); ?>"
    }
</script>
<div id="content" class="content">
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">

                    <h4 class="panel-title">Search for Land</h4>
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                      <div id="wizard">
                        <ol class="bwizard-steps clearfix clickable" role="tablist">
                            <li role="tab" aria-selected="true" class="<?php echo ($input['step'] == 1) ? 'active' : ''; ?>" style="z-index: 3;"><span class="label badge-inverse">1</span><a>
                                  Property details
                                </a></li>
                                                        
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 2) ? 'active' : ''; ?>" style="z-index: 2;"><span class="label">2</span><a>
                                  Review of Search Application

                                </a></li>
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 3) ? 'active' : ''; ?>" style="z-index: 1;"><span class="label">3</span><a>
                                  Payment 
                                </a></li>
                        </ol>
                        <?php if ($input['step'] == 1): ?>
                        <!-- begin wizard step-1 -->
                        <div class="wizard-step-1">
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="col-md-4">

                                        <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                            <div class="panel-heading">

                                                <h4 class="panel-title">Notes </h4>
                                            </div>
                                            <div class="panel-body bg-blue text-white">
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the first step of 7 to complete your application. </li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8" style="padding-top: 38px;">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Check Availability</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div id="panel_edit_account" class="tab-pane in active">
                                                    <?php echo form_open('Search/check_property_details', array('data-parsley-validate' => 'true', 'name' => 'form-wizard',)); ?>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                    Registry
                                                                </label>
                                                                <select name="RegistryID" id="RegistryID" class="form-control" required>
                                                                    <option value=""></option>

                                                                    <?php
                                                                    $GetRegistries = $this->ParcelsValuationModel->GetRegistries();

                                                                    if (is_array($GetRegistries)) {
                                                                        foreach ($GetRegistries as $Registry) {
                                                                            ?>
                                                                            <option value="<?= $Registry->RowID; ?>"><?= $Registry->Name; ?></option>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <option value="0">No data</option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Registration Section</label>
                                                                    <select name="RegistrationSectionID" onchange="selectOption()" id="RegistrationSectionID" class="form-control" data-parsley-group="wizard-step-1" required/>
                                                                    <option></option>
                                                                    <?php
                                                                    $GetRegistrationSections = $this->Registrymodel->GetRegistrationSections();

                                                                    if (is_array($GetRegistrationSections)) {
                                                                        foreach ($GetRegistrationSections as $Section) {
                                                                            ?>
                                                                            <option value="<?= $Section->RowID; ?>"><?= $Section->Name; ?></option>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <option value="0">No data</option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                    Parcel Number
                                                                </label>
                                                                <input type="text"  placeholder="Parcel Number" class="form-control" id="ParcelNo" name="ParcelNo" value="<?php echo $sessioned_Search ? $sessioned_Search['ParcelNumber'] : ''; ?>" required>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                    Tenure
                                                                </label>
                                                                <select name="NatureOfTitle" id="NatureOfTitle" class="form-control" required>
                                                                    <option value=""></option>

                                                                    <?php
                                                            //$GetRegistries = $this->ParcelsValuationModel->GetRegistries();
                                                                    $Acts = $this->actsModel->GetAllActs();
                                                                    if (is_array($Acts)) {
                                                                        foreach ($Acts as $Act) {
                                                                            ?>
                                                                            <option value="<?= $Act->RowID; ?>"><?= $Act->ItemName; ?></option>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <option value="0">No data</option>
                                                                        <?php
                                                                    }
                                                                    ?>

                                                                </select>
                                                            </div>
                                                        </div>




                                                          </div>

                                                      <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-9">
                                                                <div class="form-group connected-group">

                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label class="control-label">
                                                                                Title Number
                                                                            </label>
                                                                            <input type="text"  placeholder="Title Number" class="form-control" id="LRNumber" name="LRNumber" value="<?php echo $sessioned_Search ? $sessioned_Search['LRNumber'] : ''; ?>"  disabled>
                                                                        </div>

                                                                    </div>
                                                                </div>




                                                            </div>




                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <p>
                                                                *Terms &amp; Conditions.
                                                            </p>
                                                        </div>
                                                        <div class="col-md-">
                                                            <button class="btn btn-sm btn-success" type="submit">
                                                                Check Property and continue <i class="fa fa-arrow-circle-right"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>

                        </div>
                        <!-- end wizard step-1 -->
                        <!-- begin wizard step-2 -->
                                 
                       <?php elseif ($input['step'] == 2): ?>
                            <div class="wizard-step-3">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the third step of 4 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8" style="padding-top: 38px; overflow-y:scroll;height: 300px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Review your Search Application</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open_multipart('Search/review_application', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <table class="table table-bordered table-hover">
                                                                    <tr>
                                                                        <td>Registry</td>
                                                                        <td><?php echo $sessioned_Search['RegistryID']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Registration Section</td>
                                                                        <td><?php echo $sessioned_Search['RegistrationSectionID']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Parcel Number</td>
                                                                        <td><?php echo $sessioned_Search['ParcelNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>LR Number</td>
                                                                        <td><?php echo $sessioned_Search['LRNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Tenure</td>
                                                                        <td><?php echo $sessioned_Search['Act']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Owners</td>
                                                                        <td> <?php if ($sessioned_sellers): ?>
                                                                                <table class="table table-condensed">
                                                                                    <thead>
                                                                                    <th>#</th>
                                                                                    <th>Type</th>
                                                                                    <th>Is contact person?</th>
                                                                                    <th>First name</th>
                                                                                    <th>Middle name</th>
                                                                                    <th>Last name</th>
                                                                                    <th>Nationality</th>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        $index = 1;
                                                                                        
                                                                                        ?>
                                                                                        <?php foreach ($sessioned_sellers as $single_seller): ?>
                                                                                            <tr>
                                                                                                <td><?php echo $index; ?></td>
                                                                                                <td><label class="label <?php echo $single_seller['Type'] == 'individual' ? 'label-warning' : 'label-info'; ?>"><?php echo ucfirst($single_seller['Type']); ?></label></td>
                                                                                                <td><?php echo $single_seller['IsContactPerson']; ?></td>
                                                                                                <td><?php echo $single_seller['FirstName']; ?></td>
                                                                                                <td><?php echo $single_seller['MiddleName']; ?></td>
                                                                                                <td><?php echo $single_seller['LastName']; ?></td>
                                                                                                <td><?php echo $single_seller['CountryID']; ?></td>
                                                                                            </tr>
                                                                                            <?php $index++; ?>
                                                                                        <?php endforeach; ?>

                                                                                    </tbody>
                                                                                </table>
                                                                            <?php endif; ?></td>
                                                                    </tr>
                                
                                                                    <tr>
                                                                        <td>Type of transaction</td>
                                                                        <td><?php echo $sessioned_Search['TypeOfTransaction']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Search Amount</td>
                                                                        <td><?php echo $sessioned_Search['AmountPayable']; ?></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/Search/apply_Search?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Make payment <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>
                            </div> 

                      <?php elseif ($input['step'] == 3) : ?>
                            <div class="wizard-step-8">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="panel panel-default panel-blog panel-checkout">
                                            <div class="panel-body">
                                                <ul class="blog-meta mb5">
                                                    <!--<li>Jan 09, 2015</li>-->
                                                </ul>
                                                <?php if (array_key_exists('step', $input) && $input['step'] == 3): ?>
                                                    <div style="background-color: #FFF;">
                                                        <?php
                                                        $apiClientId = '13';
                                                        $amount = $sessioned_Search['AmountPayable'];
                                                        $amount = '10';
                                                        $serviceId = '29';
                                                        $idNumber = $sessioned_user['IDNumber'];
                                                        $currency = 'KES';
                                                        $billRef = $sessioned_Search['RowID'];
                                                        $serviceDesc = $sessioned_Search['Description'];
                                                        $clientName = $sessioned_user['FirstName'] . ' ' . $sessioned_user['LastName'];
                                                        $key = 'TYV0FUD0BY';
                                                        $secret = 'BGI0ZTSPL2WX0X6';

                                                        $payload = "{$apiClientId}{$amount}{$serviceId}{$idNumber}{$currency}{$billRef}{$serviceDesc}{$clientName}{$secret}";
                                                        $hash = base64_encode(hash_hmac("sha256", $payload, $key));
                                                        ?>
                                                        <form id="idPaymentForm" method="post" action="https://pesaflow.ecitizen.go.ke/PaymentAPI/iframev2.1.php" target="my_frame">
                                                            <input type="hidden" name="apiClientID" value="13" />
                                                            <input type="hidden" name="secureHash" value="<?php echo $hash; ?>" />
                                                            <input type="hidden" name="billDesc" value="<?php echo $sessioned_Search['Description']; ?>" />
                                                            <input type="hidden" name="billRefNumber" value="<?php echo $sessioned_Search['RowID']; ?>" />
                                                            <input type="hidden" name="currency" value="KES" />
                                                            <input type="hidden" name="serviceID" value="29" />
                                                            <input type="hidden" name="clientMSISDN" value="<?php echo $sessioned_user['Phone']; ?>" />
                                                            <input type="hidden" name="clientName" value="<?php echo $clientName; ?>" />
                                                            <input type="hidden" name="clientIDNumber" value="<?php echo $sessioned_user['IDNumber']; ?>" />
                                                            <input type="hidden" name="clientType" value="citizen" />
                                                            <input type="hidden" name="clientEmail" value="<?php echo $sessioned_user['Email']; ?>" />
                                                            <input type="hidden" name="callBackURLOnSuccess" value="<?php echo base_url(); ?>/Search/payment_completed?bill_ref=<?php echo $sessioned_Search['RowID']; ?>" />
                                                            <input type="hidden" name="callBackURLOnFail" value="<?php echo base_url(); ?>/Search/apply_Search?step=6" />
                                                            <input type="hidden" name="notificationURL" value="<?php echo base_url(); ?>/Search/apply_Search?step=6" />
                                                            <input type="hidden" name="amountExpected" value="<?php echo $amount; ?>" />
                                                        </form>
                                                        <iframe width="100%" height="600px" style="border: none;" name="my_frame"></iframe>
                                                        <script type="text/javascript">
                                                            document.getElementById('idPaymentForm').submit();
                                                        </script>
                                                    </div>
                                                    <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/Search/apply_Search?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                <?php endif; ?>
                                            </div><!-- panel-body -->
                                        </div>
                                    </div>

                                    <!-- end row -->
                                </fieldset>
                            </div>
                        <?php endif; ?>
                        <!-- end wizard step-5 -->


                    </div>

                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<!-- end #content -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->




<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/parsley/dist/parsley.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-wizards-validation.demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/apps.min.js?time=<?php echo time(); ?>"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<script></script>
<script>
                                                    $("#County").change(function (evt) {
                                                        var ActID = document.getElementById('Acts').value;
                                                        var CountyID = document.getElementById('County').value;
                                                        var select = $("#County");
                                                        var selectData = select.serialize();
                                                        var ajax = $.ajax({
                                                            dataType: "json",
                                                            type: 'POST',
                                                            url: "<?php echo base_url(); ?>Transfer/GetRegistriesMatch/" + ActID + "/" + CountyID,
                                                            data: selectData
                                                        });
                                                        ajax.done(function (response) {
                                                            $("#testurl").html(response);
                                                            var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                            for (var key in response) {
                                                                if (response.hasOwnProperty(key)) {
                                                                    //alert(key);
                                                                    //alert(response[key].RowID);
                                                                    //alert(response[key].NameOfRoad);
                                                                    listItems += "<option value='" + response[key].Registres + "'>" + response[key].Registries + "</option>";
                                                                }
                                                            }
                                                            $("#Registry").html(listItems);
                                                        });
                                                        ajax.fail(function () {

                                                        });
                                                    });

                                                    $("#towntype").change(function (evt) {
                                                        var townTypeID = document.getElementById('towntype').value;
                                                        var select = $("#towntype");
                                                        var selectData = select.serialize();
                                                        var ajax = $.ajax({
                                                            dataType: "json",
                                                            type: 'POST',
                                                            url: "http://localhost:8080/lmais/start/GetTownNamesMatchTownType/" + townTypeID,
                                                            data: selectData
                                                        });
                                                        ajax.done(function (response) {
                                                            var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                            for (var key in response) {
                                                                if (response.hasOwnProperty(key)) {
                                                                    //alert(key);
                                                                    //alert(response[key].RowID);
                                                                    //alert(response[key].NameOfRoad);
                                                                    listItems += "<option value='" + response[key].NameOfTown + "'>" + response[key].NameOfTown + "</option>";
                                                                }
                                                            }
                                                            $("#townNames").html(listItems);
                                                        });
                                                        ajax.fail(function () {

                                                        });
                                                    });

</script>

<script>
    $(document).ready(function () {
        App.init();
        FormWizardValidation.init();
    });
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>