<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
<!-- begin #content -->
<script type="text/javascript">
    var step = <?php echo array_key_exists('step', $input) ? $input['step'] : -1; ?>;
    var inlineJs = {
        baseUrl: "<?php echo base_url(); ?>"
    }
</script>
<div id="content" class="content">
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">

                    <h4 class="panel-title">Registration for a Transfer</h4>
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <div id="wizard">
                        <ol>
                            <li>
                                Property  details

                            </li>
                            <li>
                                Seller (Applicant) 

                            </li>
                            <li>
                                Buyer(s) Proposed

                            </li>

                            <li>
                                Advocate Details

                            </li>

                            <li>
                                Transfer Details

                            </li>
                            <li>
                                Due Rent

                            </li>
                            <li>
                                Rates Clearance  Certificate

                            </li>

                            <li>
                                Payment 

                            </li>


                        </ol>
                        <!-- begin wizard step-1 -->
                        <div class="wizard-step-1">
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="col-md-4">

                                        <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                            <div class="panel-heading">

                                                <h4 class="panel-title">Notes </h4>
                                            </div>
                                            <div class="panel-body bg-blue text-white">
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the first step of 7 to complete your application. </li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Check Availability</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div id="panel_edit_account" class="tab-pane in active">
                                                    <?php echo form_open('consent/check_property_details', array('data-parsley-validate' => 'true', 'name' => 'form-wizard',)); ?>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                    Registry
                                                                </label>
                                                                <select name="RegistryID" id="RegistryID" class="form-control"  required>
                                                                    <option value=""></option>

                                                                    <?php
                                                                    $GetRegistries = $this->ParcelsValuationModel->GetRegistries();

                                                                    if (is_array($GetRegistries)) {
                                                                        foreach ($GetRegistries as $Registry) {
                                                                            ?>
                                                                            <option value="<?= $Registry->RowID; ?>"><?= $Registry->Name; ?></option>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <option value="0">No data</option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Registration Section</label>
                                                                    <select name="RegistrationSectionID" onchange="selectOption()" id="RegistrationSectionID" class="form-control" data-parsley-group="wizard-step-1"/>
                                                                    <option></option>
                                                                    <?php
                                                                    $GetRegistrationSections = $this->Registrymodel->GetRegistrationSections();

                                                                    if (is_array($GetRegistrationSections)) {
                                                                        foreach ($GetRegistrationSections as $Section) {
                                                                            ?>
                                                                            <option value="<?= $Section->RowID; ?>"><?= $Section->Name; ?></option>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <option value="0">No data</option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                    Parcel Number
                                                                </label>
                                                                <input type="text"  placeholder="Parcel Number" class="form-control" id="ParcelNo" name="ParcelNo" value="<?php echo $sessioned_consent ? $sessioned_consent['ParcelNumber'] : ''; ?>">
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                    Tenure
                                                                </label>
                                                                <select name="NatureOfTitle" id="NatureOfTitle" class="form-control" >
                                                                    <option value=""></option>

                                                                    <?php
//$GetRegistries = $this->ParcelsValuationModel->GetRegistries();
                                                                    $Acts = $this->actsModel->GetAllActs();
                                                                    if (is_array($Acts)) {
                                                                        foreach ($Acts as $Act) {
                                                                            ?>
                                                                            <option value="<?= $Act->RowID; ?>"><?= $Act->ItemName; ?></option>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <option value="0">No data</option>
                                                                        <?php
                                                                    }
                                                                    ?>

                                                                </select>
                                                            </div>
                                                        </div>




                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-9">
                                                                <div class="form-group connected-group">

                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label class="control-label">
                                                                                Title Number
                                                                            </label>
                                                                            <input type="text"  placeholder="Title Number" class="form-control" id="LRNumber" name="LRNumber" value="<?php echo $sessioned_consent ? $sessioned_consent['LRNumber'] : ''; ?>"  disabled>
                                                                        </div>

                                                                    </div>
                                                                </div>




                                                            </div>




                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <p>
                                                                *Terms &amp; Conditions.
                                                            </p>
                                                        </div>
                                                        <div class="col-md-">
                                                            <button class="btn btn-sm btn-success" type="submit">
                                                                Check Property <i class="fa fa-arrow-circle-right"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>

                        </div>
                        <!-- end wizard step-1 -->
                        <!-- begin wizard step-2 -->
                        <div class="wizard-step-2">
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="col-md-4">

                                        <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                            <div class="panel-heading">

                                                <h4 class="panel-title">Notes </h4>
                                            </div>
                                            <div class="panel-body bg-blue text-white">
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the second step of 7 to complete your application. </li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Proprietor Details</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div id="panel_edit_account" class="tab-pane in active">
                                                    <?php echo form_open('consent/save_seller_continue', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                    <div class="row">
                                                        <div class="col-md-">
                                                            <button id="idAddSeller" class="btn btn-sm btn-success" type="button">
                                                                <i class="fa fa-user"></i> Add seller
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <?php if ($sessioned_sellers): ?>
                                                                <table class="table table-condensed table-responsive">
                                                                    <thead>
                                                                    <th>#</th>
                                                                    <th>Is contact person?</th>
                                                                    <th>First name</th>
                                                                    <th>Middle name</th>
                                                                    <th>Last name</th>
                                                                    <th>Nationality</th>
                                                                    <th>Action</th>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        $index = 1;
                                                                        $total_rent = 0;
                                                                        ?>
                                                                        <?php foreach ($sessioned_sellers as $single_seller): ?>
                                                                            <tr>
                                                                                <td><?php echo $index; ?></td>
                                                                                <td><?php echo $single_seller['IsContactPerson']; ?></td>
                                                                                <td><?php echo $single_seller['FirstName']; ?></td>
                                                                                <td><?php echo $single_seller['MiddleName']; ?></td>
                                                                                <td><?php echo $single_seller['LastName']; ?></td>
                                                                                <td><?php echo $single_seller['CountryID']; ?></td>
                                                                                <td><i class="fa fa-trash-o red"></i> <i class="fa fa-edit red"></i></td>
                                                                            </tr>
                                                                            <?php $index++; ?>
                                                                        <?php endforeach; ?>

                                                                    </tbody>
                                                                </table>
                                                            <?php endif; ?>
                                                        </div>        
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <p>
                                                                *Terms &amp; Conditions.
                                                            </p>
                                                        </div>
                                                        <div class="col-md-">
                                                            <button class="btn btn-sm btn-success" type="submit">
                                                                Save progress <i class="fa fa-arrow-circle-right"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>

                        </div>




                        <div class="wizard-step-3">
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="col-md-4">

                                        <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                            <div class="panel-heading">

                                                <h4 class="panel-title">Notes </h4>
                                            </div>
                                            <div class="panel-body bg-blue text-white">
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the third step of 7 to complete your application. </li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Proposed party details</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div id="panel_edit_account" class="tab-pane in active">
                                                    <?php echo form_open('consent/save_buyer_continue', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                    <div class="row">
                                                        <div class="col-md-">
                                                            <button id="idAddBuyer" class="btn btn-sm btn-success" type="button">
                                                                <i class="fa fa-user"></i> Add party 
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <?php if ($sessioned_buyers): ?>
                                                                <table class="table table-condensed table-responsive">
                                                                    <thead>
                                                                    <th>#</th>
                                                                    <th>Is contact person?</th>
                                                                    <th>First name</th>
                                                                    <th>Middle name</th>
                                                                    <th>Last name</th>
                                                                    <th>Nationality</th>
                                                                    <th>Action</th>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        $index = 1;
                                                                        $total_rent = 0;
                                                                        ?>
                                                                        <?php foreach ($sessioned_buyers as $single_buyer): ?>
                                                                            <tr>
                                                                                <td><?php echo $index; ?></td>
                                                                                <td><?php echo $single_buyer['IsContactPerson']; ?></td>
                                                                                <td><?php echo $single_buyer['FirstName']; ?></td>
                                                                                <td><?php echo $single_buyer['MiddleName']; ?></td>
                                                                                <td><?php echo $single_buyer['LastName']; ?></td>
                                                                                <td><?php echo $single_buyer['CountryID']; ?></td>
                                                                                <td><i class="fa fa-trash-o red"></i> <i class="fa fa-edit red"></i></td>
                                                                            </tr>
                                                                            <?php $index++; ?>
                                                                        <?php endforeach; ?>

                                                                    </tbody>
                                                                </table>
                                                            <?php endif; ?>
                                                        </div>        
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <p>
                                                                *Terms &amp; Conditions.
                                                            </p>
                                                        </div>
                                                        <div class="col-md-">
                                                            <button class="btn btn-sm btn-success" type="submit">
                                                                Save progress <i class="fa fa-arrow-circle-right"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>

                        </div>

                        <div class="wizard-step-4">
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="col-md-4">

                                        <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                            <div class="panel-heading">

                                                <h4 class="panel-title">Notes </h4>
                                            </div>
                                            <div class="panel-body bg-blue text-white">
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the fourth step of 8 to complete your application. </li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="modal-content">

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title">Advocate Details</h4>
                                            </div>
                                                <div id="msgadvo">
                                                    

                                                </div>

                                                <div class="modal-body">
                                                <div id="panel_edit_account" class="tab-pane in active">

                                                    <?php echo form_open('', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser', 'onsubmit'=>'return false')); ?>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            
                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  First  Name
                                                                </label>
                                                                <input type="text"  placeholder="First Name" class="form-control" id="advoFName" name="advoFName" />
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  Middle  Name
                                                                </label>
                                                                <input type="text"  placeholder="Middle Name" class="form-control" id="advoMName" name="advoMName" />
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  Last  Name
                                                                </label>
                                                                <input type="text"  placeholder="Last Name" class="form-control" id="advoLName" name="advoLName" />
                                                            </div>


                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  Pin Number
                                                                </label>
                                                                <input type="text"  placeholder="Pin Number" class="form-control" id="pinNumber" name="pinNumber" />
                                                            </div>
                                                           
                                                        </div>

                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            
                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  Postal Address
                                                                </label>
                                                                <input type="text"  placeholder="Postal Address" class="form-control" id="postalAddress" name="postalAddress" />
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  Email
                                                                </label>
                                                                <input type="text"  placeholder="Email" class="form-control" id="advoEmail" name="advoEmail" />
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  Telephone Number
                                                                </label>
                                                                <input type="text"  placeholder="Telephone Number" class="form-control" id="advoTelNumber" name="advoTelNumber" />
                                                            </div>


                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  Law Firm
                                                                </label>
                                                                <input type="text"  placeholder="Law Firm" class="form-control" id="advoLawFirm" name="advoLawFirm" />
                                                            </div>
                                                           
                                                        </div>

                                                    </div>

                                                     <div class="row">
                                                        <div class="col-md-12">
                                                            
                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  PF Number
                                                                </label>
                                                                <input type="text"  placeholder="PF Number" class="form-control" id="advoPFNumber" name="advoPFNumber" />
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                  ID Number
                                                                </label>
                                                                <input type="text"  placeholder="ID Number" class="form-control" id="advoIDNumber" name="advoIDNumber" />
                                                            </div>

                                                            <div class="col-md-3">
                                                                
                                                         <label class="control-label">
                                                                Nationality
                                                            </label>

                                                            <input type="text"  placeholder="Nationality" class="form-control" id="advoNationality" name="advoNationality" />
                                                            
                                                           </div>
                                                        </div>

                                                     </div>

                                                   
                                                 <div class="row">
                                                        <div class="col-md-10">
                                                            <p>
                                                                *Terms &amp; Conditions.
                                                            </p>
                                                        </div>

                                                        <div class="col-md-">
                                                            <button class="btn btn-sm btn-success"  type="submit" onClick="saveAdvocateDetails()">
                                                                Save Advocate Details <i class="fa fa-arrow-circle-right"></i>
                                                            </button>
                                                        </div>

                                                    </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>


                        </div>

                        <div class="wizard-step-5">
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="col-md-4">

                                        <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                            <div class="panel-heading">

                                                <h4 class="panel-title">Notes</h4>
                                            </div>
                                            <div class="panel-body bg-blue text-white">
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the fifth step of 8 to complete your application. </li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Consent Details</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div id="panel_edit_account" class="tab-pane in active">
                                                    <?php echo form_open('consent/save_transaction_details', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                    <div class="row">
                                                        <div class="col-md-5">

                                                        </div>
                                                        <div class="col-md-12">

                                                            <div class="col-md-6">
                                                                <div class="form-group connected-group">

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <label class="control-label">
                                                                                Nature of Transaction
                                                                            </label>
                                                                            <?php
                                                                            $options = array(
                                                                                '' => 'Select',
                                                                                'lease' => 'Lease',
                                                                                'transfer' => 'Transfer',
                                                                                'charge' => 'Charge'
                                                                            );
                                                                            $selected = 'transfer';
                                                                            echo form_dropdown('Nature_Of_Transaction', $options, $selected, array('class' => 'form-control', 'disabled' => 'disabled'));
                                                                            ?>

                                                                        </div>

                                                                    </div>
                                                                </div>




                                                            </div>



                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">
                                                                        Transaction Description
                                                                    </label>
                                                                    <input type="text" name="TransactionDescription" value="<?php echo $sessioned_consent ? $sessioned_consent['TransactionDescription'] : ''; ?>" placeholder="Transaction Description" id="form-field-9" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-5">


                                                        </div>
                                                        <div class="col-md-12">

                                                            <div class="col-md-6">
                                                                <div class="form-group connected-group">

                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label class="control-label">
                                                                                Term
                                                                            </label>
                                                                            <input type="text" name="TransactionTerm" value="<?php echo $sessioned_consent ? $sessioned_consent['TransactionTerm'] : ''; ?>" placeholder="Period" id="form-field-9" class="form-control">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label class="control-label">
                                                                                Term Period
                                                                            </label>
                                                                            <select class="form-control" name="TransactionTermPeriod">
                                                                                <option value="">Select</option>
                                                                                <option value="months">Months</option>
                                                                                <option value="years">Years</option>
                                                                            </select>
                                                                        </div>

                                                                    </div>
                                                                </div>




                                                            </div>

                                                            <div class="col-md-3">
                                                                <label class="control-label">
                                                                    Type of Transaction
                                                                </label>
                                                                <?php
                                                                $options = array(
                                                                    '' => 'Select',
                                                                    'sale' => 'Sale',
                                                                    'lease' => 'Lease',
                                                                    'spouse_transfer' => 'Spouse transfer',
                                                                    'gift' => 'Gift',
                                                                );
                                                                $selected = '';
                                                                echo form_dropdown('Type_Of_Transaction', $options, $selected, array('class' => 'form-control'));
                                                                ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Value (Estimated if gift)</label>
                                                                    <input type="text" name="TransactionAmount" value="<?php echo $sessioned_consent ? $sessioned_consent['TransactionAmount'] : ''; ?>" placeholder="Email" id="form-field-11" class="form-control">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <p>
                                                                *Terms &amp; Conditions.
                                                            </p>
                                                        </div>
                                                        <div class="col-md-">
                                                            <button class="btn btn-sm btn-success" type="submit">
                                                                Save progress <i class="fa fa-arrow-circle-right"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>

                        </div>

                        <div class="wizard-step-6">
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="col-md-4">

                                        <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                            <div class="panel-heading">

                                                <h4 class="panel-title">Notes </h4>
                                            </div>
                                            <div class="panel-body bg-blue text-white">
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the sixth step of 8 to complete your application. </li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Rent Clearance for <?php echo $sessioned_consent['LRNumber']; ?></h4>
                                            </div>
                                            <div class="modal-body">
                                                <div id="panel_edit_account" class="tab-pane in active">
                                                    <?php echo form_open('consent/rent_clearance', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <?php if ($sessioned_rent): ?>
                                                                <table class="table table-condensed table-bordered">
                                                                    <thead>
                                                                    <th>#</th>
                                                                    <th>From</th>
                                                                    <th>To</th>
                                                                    <th>Amount (KES)</th>
                                                                    <th>Status</th>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        $index = 1;
                                                                        $total_rent = 0;
                                                                        ?>
                                                                        <?php foreach ($sessioned_rent as $single_rent): ?>
                                                                            <tr>
                                                                                <td><?php echo $index; ?></td>
                                                                                <td><?php
                                                                                    $date = date_create($single_rent['From']);
                                                                                    echo date_format($date, "d/m/Y");
                                                                                    ?></td>
                                                                                <td><?php
                                                                                    $date = date_create($single_rent['To']);
                                                                                    echo date_format($date, "d/m/Y");
                                                                                    ?></td>
                                                                                <td><?php echo number_format($single_rent['Amount']); ?></td>
                                                                                <td>UNPAID</td>
                                                                            </tr>
                                                                            <?php
                                                                            $index++;
                                                                            $total_rent += $single_rent['Amount'];
                                                                            ?>
                                                                        <?php endforeach; ?>
                                                                    </tbody>
                                                                </table>
                                                                <h3>Total unpaid rent: KES <?php echo number_format($total_rent); ?></h3>
                                                            <?php else: ?>
                                                                <p>You've cleared all your rent.</p>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <p>
                                                                *Terms &amp; Conditions.
                                                            </p>
                                                        </div>
                                                        <div class="col-md-">
                                                            <button class="btn btn-sm btn-success" type="submit">
                                                                Save progress <i class="fa fa-arrow-circle-right"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>

                        </div>


                        <div class="wizard-step-7">
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="col-md-4">

                                        <div class="panel panel-primary" data-sortable-id="ui-widget-1">
                                            <div class="panel-heading">

                                                <h4 class="panel-title">Notes </h4>
                                            </div>
                                            <div class="panel-body bg-blue text-white">
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the seventh step of 8 to complete your application. </li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Upload Rates Clearance Certificate</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div id="panel_edit_account" class="tab-pane in active">

                                                    <?php echo form_open_multipart('consent/rate_clearance', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label class="control-label">
                                                                Rate Clearance Number
                                                            </label>
                                                            <input type="text" name="RateClearanceNumber" value="<?php echo $sessioned_consent ? $sessioned_consent['RateClearanceNumber'] : ''; ?>" placeholder="Rate Clearance Number" class="form-control">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="control-label">
                                                                Rate Clearance Certificate (.png,.jpeg,.pdf)
                                                            </label>
                                                            <div class="form-group">

                                                                <input type="file" name="clearance_certificate" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <p>
                                                                *Terms &amp; Conditions.
                                                            </p>
                                                        </div>
                                                        <div class="col-md-">
                                                            <button class="btn btn-sm btn-success" type="submit">
                                                                Upload certificate <i class="fa fa-arrow-circle-right"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>

                        </div>
                        <div class="wizard-step-8">
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="panel panel-default panel-blog panel-checkout">
                                        <div class="panel-body">
                                            <ul class="blog-meta mb5">
                                                <!--<li>Jan 09, 2015</li>-->
                                            </ul>
                                            <?php if (array_key_exists('step', $input) && $input['step'] == 7): ?>
                                                <div style="background-color: #FFF;">
                                                    <?php
                                                    $apiClientId = '13';
                                                    $amount = $sessioned_consent['Rent'] + 1550;
                                                    $amount = '10';
                                                    $serviceId = '29';
                                                    $idNumber = $sessioned_user['IDNumber'];
                                                    $currency = 'KES';
                                                    $billRef = $sessioned_consent['RowID'];
                                                    $serviceDesc = $sessioned_consent['Description'];
                                                    $clientName = $sessioned_user['FirstName'] . ' ' . $sessioned_user['LastName'];
                                                    $key = 'TYV0FUD0BY';
                                                    $secret = 'BGI0ZTSPL2WX0X6';

                                                    $payload = "{$apiClientId}{$amount}{$serviceId}{$idNumber}{$currency}{$billRef}{$serviceDesc}{$clientName}{$secret}";
                                                    $hash = base64_encode(hash_hmac("sha256", $payload, $key));
                                                    ?>
                                                    <form id="moodleform" method="post" action="https://pesaflow.ecitizen.go.ke/PaymentAPI/iframev2.1.php" target="my_frame">
                                                        <input type="hidden" name="apiClientID" value="13" />
                                                        <input type="hidden" name="secureHash" value="<?php echo $hash; ?>" />
                                                        <input type="hidden" name="billDesc" value="<?php echo $sessioned_consent['Description']; ?>" />
                                                        <input type="hidden" name="billRefNumber" value="<?php echo $sessioned_consent['RowID']; ?>" />
                                                        <input type="hidden" name="currency" value="KES" />
                                                        <input type="hidden" name="serviceID" value="29" />
                                                        <input type="hidden" name="clientMSISDN" value="<?php echo $sessioned_user['Phone']; ?>" />
                                                        <input type="hidden" name="clientName" value="<?php echo $clientName; ?>" />
                                                        <input type="hidden" name="clientIDNumber" value="<?php echo $sessioned_user['IDNumber']; ?>" />
                                                        <input type="hidden" name="clientType" value="citizen" />
                                                        <input type="hidden" name="clientEmail" value="<?php echo $sessioned_user['Email']; ?>" />
                                                        <input type="hidden" name="callBackURLOnSuccess" value="http://localhost:8080/lmais/consent/payment_completed?bill_ref=<?php echo $sessioned_consent['RowID']; ?>" />
                                                        <input type="hidden" name="callBackURLOnFail" value="http://localhost:8080/lmais/consent/apply_consent?step=8" />
                                                        <input type="hidden" name="notificationURL" value="http://localhost:8080/lmais/consent/apply_consent?step=8" />
                                                        <input type="hidden" name="amountExpected" value="<?php echo $amount; ?>" />
                                                        <button id="idPay" type="submit">Submit</button>
                                                    </form>
                                                    <iframe width="100%" height="600px" style="border: none;" name="my_frame"></iframe>
                                                    <script type="text/javascript">
                                                        document.getElementById('moodleform').submit();
                                                    </script>
                                                </div>
                                            <?php endif; ?>
                                        </div><!-- panel-body -->
                                    </div>
                                </div>

                                <!-- end row -->
                            </fieldset>

                        </div>
                        <!-- end wizard step-4 -->


                    </div>

                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<!-- end #content -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->



<div id="idAddSellerModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('consent/save_seller', array('class' => 'margin-bottom-0', 'id' => 'idSaveSellerForm')); ?>
        <input type="hidden" name="ConsentID" value="<?php echo $sessioned_consent ? $sessioned_consent['ConsentID'] : ''; ?>"/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add seller</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idSellerAlert" class="alert alert-success hidden"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                ID Number
                            </label>
                            <input id="idSellerIdNumber" type="text" name="Identification" value="" placeholder="ID Number/Passport" id="form-field-9" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <button id="idSellerGetDetails" class="btn btn-primary" style="margin-top: 20px;" disabled>Get details from Ecitizen</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Is Primary Contact Person?
                            </label>
                            <br>
                            <label><input type="radio" name="IsContactPerson" value="Y"  checked=""/> Yes</label>
                            <label><input type="radio" name="IsContactPerson" value="N"/> No</label>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                First Name
                            </label>
                            <input id="idSellerFirstName" type="text"  name="FirstName" value="" placeholder="First Name" id="form-field-9" class="form-control" disabled>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Middle Name
                            </label>
                            <input id="idSellerMiddleName" type="text" name="MiddleName" value="" placeholder="Middle Name" id="form-field-10" class="form-control" disabled>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input id="idSellerLastName" type="text" name="LastName" value="" placeholder="Last Name" id="form-field-11" class="form-control" disabled>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Mobile Number
                            </label>
                            <input  id="idSellerMobileNumber" type="text" name="MobileNumber" value="" placeholder="Mobile Number" id="form-field-10" class="form-control" disabled>
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input  id="idSellerEmailAddress" type="text" name="EmailAddress" value="" placeholder="Email" id="form-field-11" class="form-control" disabled>

                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Postal Address
                            </label>
                            <input  id="idSellerAddress" type="text" name="Address" value="" placeholder="Address" id="form-field-11" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Nationality
                            </label>

                            <select id="idSellerNationality" name="CountryID" class="form-control" required>
                                <option value="">Select</option>
                                <?php
                                $Countries = $this->Countrymodel->GetAllCountries();
                                if (is_array($Countries)) {
                                    foreach ($Countries as $Country) {
                                        ?>
                                        <option value="<?= $Country->RowID; ?>"><?= $Country->CountryName; ?></option>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <option value="0">No data</option>
                                    <?php
                                }
                                ?>

                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idSaveSellerButton" type="button" class="btn btn-primary" type="submit">
                    Save changes
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<div id="idAddBuyerModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('consent/save_seller', array('class' => 'margin-bottom-0', 'id' => 'idSaveBuyerForm')); ?>
        <input type="hidden" name="ConsentID" value="<?php echo $sessioned_consent ? $sessioned_consent['ConsentID'] : ''; ?>"/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add seller</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idBuyerAlert" class="alert alert-success hidden"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                ID Number
                            </label>
                            <input id="idBuyerIdNumber" type="text" name="Identification" value="" placeholder="ID Number/Passport" id="form-field-9" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <button id="idBuyerGetDetails" class="btn btn-primary" style="margin-top: 20px;" disabled>Get details from Ecitizen</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Is Primary Contact Person?
                            </label>
                            <br>
                            <label><input type="radio" name="IsContactPerson" value="Y"  checked=""/> Yes</label>
                            <label><input type="radio" name="IsContactPerson" value="N"/> No</label>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                First Name
                            </label>
                            <input id="idBuyerFirstName" type="text"  name="FirstName" value="" placeholder="First Name" id="form-field-9" class="form-control" disabled>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Middle Name
                            </label>
                            <input id="idBuyerMiddleName" type="text" name="MiddleName" value="" placeholder="Middle Name" id="form-field-10" class="form-control" disabled>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input id="idBuyerLastName" type="text" name="LastName" value="" placeholder="Last Name" id="form-field-11" class="form-control" disabled>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Mobile Number
                            </label>
                            <input  id="idBuyerMobileNumber" type="text" name="MobileNumber" value="" placeholder="Mobile Number" id="form-field-10" class="form-control" disabled>
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input  id="idBuyerEmailAddress" type="text" name="EmailAddress" value="" placeholder="Email" id="form-field-11" class="form-control" disabled>

                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Postal Address
                            </label>
                            <input  id="idBuyerAddress" type="text" name="Address" value="" placeholder="Address" id="form-field-11" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Nationality
                            </label>

                            <select id="idBuyerNationality" name="CountryID" class="form-control" required>
                                <option value="">Select</option>
                                <?php
                                $Countries = $this->Countrymodel->GetAllCountries();
                                if (is_array($Countries)) {
                                    foreach ($Countries as $Country) {
                                        ?>
                                        <option value="<?= $Country->RowID; ?>"><?= $Country->CountryName; ?></option>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <option value="0">No data</option>
                                    <?php
                                }
                                ?>

                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idSaveBuyerButton" type="button" class="btn btn-primary" type="submit">
                    Save changes
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/parsley/dist/parsley.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-wizards-validation.demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/apps.min.js?time=<?php echo time(); ?>"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<script></script>
<script>
                                                    $("#County").change(function (evt) {
                                                        var ActID = document.getElementById('Acts').value;
                                                        var CountyID = document.getElementById('County').value;
                                                        var select = $("#County");
                                                        var selectData = select.serialize();
                                                        var ajax = $.ajax({
                                                            dataType: "json",
                                                            type: 'POST',
                                                            url: "<?php echo base_url(); ?>Transfer/GetRegistriesMatch/" + ActID + "/" + CountyID,
                                                            data: selectData
                                                        });
                                                        ajax.done(function (response) {
                                                            $("#testurl").html(response);
                                                            var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                            for (var key in response) {
                                                                if (response.hasOwnProperty(key)) {
                                                                    //alert(key);
                                                                    //alert(response[key].RowID);
                                                                    //alert(response[key].NameOfRoad);
                                                                    listItems += "<option value='" + response[key].Registres + "'>" + response[key].Registries + "</option>";
                                                                }
                                                            }
                                                            $("#Registry").html(listItems);
                                                        });
                                                        ajax.fail(function () {

                                                        });
                                                    });

                                                    $("#towntype").change(function (evt) {
                                                        var townTypeID = document.getElementById('towntype').value;
                                                        var select = $("#towntype");
                                                        var selectData = select.serialize();
                                                        var ajax = $.ajax({
                                                            dataType: "json",
                                                            type: 'POST',
                                                            url: "<?php echo base_url(); ?>start/GetTownNamesMatchTownType/" + townTypeID,
                                                            data: selectData
                                                        });
                                                        ajax.done(function (response) {
                                                            var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                            for (var key in response) {
                                                                if (response.hasOwnProperty(key)) {
                                                                    //alert(key);
                                                                    //alert(response[key].RowID);
                                                                    //alert(response[key].NameOfRoad);
                                                                    listItems += "<option value='" + response[key].NameOfTown + "'>" + response[key].NameOfTown + "</option>";
                                                                }
                                                            }
                                                            $("#townNames").html(listItems);
                                                        });
                                                        ajax.fail(function () {

                                                        });
                                                    });

</script>

<script>
    $(document).ready(function () {
        App.init();
        FormWizardValidation.init();
    });
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>