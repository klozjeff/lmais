<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
<!-- begin #content -->
<script type="text/javascript">
    var step = <?php echo array_key_exists('step', $input) ? $input['step'] : -1; ?>;
    var inlineJs = {
        baseUrl: "<?php echo base_url(); ?>"
    }
</script>
<div id="content" class="content">
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title"><span class="pull-left">Registration for a Transfer</span><span class="pull-right">KES 50</span></h4>
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <div id="wizard">
                        <ol class="bwizard-steps clearfix clickable" role="tablist">
                            <li role="tab" aria-selected="true" class="<?php echo ($input['step'] == 1) ? 'active' : ''; ?>" style="z-index: 7;"><span class="label badge-inverse">1</span><a>
                                    Property  details
                                </a></li>
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 2) ? 'active' : ''; ?>" style="z-index: 6;"><span class="label">2</span><a>
                                    Consent Applicant 

                                </a></li>
                            <li role="tab" aria-selected="false"class="<?php echo ($input['step'] == 3) ? 'active' : ''; ?>" style="z-index: 5;"><span class="label">3</span><a>
                                    Proposed Party
                                </a></li>

                            <li role="tab" aria-selected="false"class="<?php echo ($input['step'] == 4) ? 'active' : ''; ?>" style="z-index: 4;"><span class="label">4</span><a>
                                    Advocates Details
                                </a></li>
                            <li role="tab" aria-selected="false"class="<?php echo ($input['step'] == 5) ? 'active' : ''; ?>" style="z-index: 4;"><span class="label">5</span><a>
                                    Consent Details
                                </a></li>
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 6) ? 'active' : ''; ?>" style="z-index: 3;"><span class="label">6</span><a>
                                    Rent Clearance
                                </a></li>
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 7) ? 'active' : ''; ?>" style="z-index: 2;"><span class="label">7</span><a>
                                    Rates Clearance 
                                </a></li>
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 8) ? 'active' : ''; ?>" style="z-index: 2;"><span class="label">8</span><a>
                                    Review consent application 
                                </a></li>
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 9) ? 'active' : ''; ?>" style="z-index: 1;"><span class="label">9</span><a>
                                    Payment 
                                </a></li>
                        </ol>
                        <?php if ($input['step'] == 1): ?>
                            <!-- begin wizard step-1 -->
                            <div class="wizard-step-1">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the first step of 7 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8" style="padding-top: 38px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Check Availability</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open('consent/check_property_details', array('data-parsley-validate' => 'true', 'name' => 'form-wizard')); ?>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        Registry
                                                                    </label>
                                                                    <select name="RegistryID" id="RegistryID" class="form-control"  required>
                                                                        <option value=""></option>

                                                                        <?php
                                                                        $GetRegistries = $this->ParcelsValuationModel->GetRegistries();

                                                                        if (is_array($GetRegistries)) {
                                                                            foreach ($GetRegistries as $Registry) {
                                                                                ?>
                                                                                <option value="<?= $Registry->RowID; ?>" <?php echo ($sessioned_consent && $sessioned_consent['AdvocateCountryID'] == $Registry->RowID) ? 'selected="selected"' : ''; ?>><?= $Registry->Name; ?></option>
                                                                                <?php
                                                                            }
                                                                        } else {
                                                                            ?>
                                                                            <option value="0">No data</option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label>Registration Section</label>
                                                                        <select name="RegistrationSectionID" onchange="selectOption()" id="RegistrationSectionID" class="form-control" data-parsley-group="wizard-step-1" required/>
                                                                        <option></option>
                                                                        <?php
                                                                        $GetRegistrationSections = $this->Registrymodel->GetRegistrationSections();

                                                                        if (is_array($GetRegistrationSections)) {
                                                                            foreach ($GetRegistrationSections as $Section) {
                                                                                ?>
                                                                                <option value="<?= $Section->RowID; ?>"  <?php echo ($sessioned_consent && $sessioned_consent['AdvocateCountryID'] == $Section->RowID) ? 'selected="selected"' : ''; ?>><?= $Section->Name; ?></option>
                                                                                <?php
                                                                            }
                                                                        } else {
                                                                            ?>
                                                                            <option value="0">No data</option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        Parcel Number
                                                                    </label>
                                                                    <input type="text"  placeholder="Parcel Number" class="form-control" id="ParcelNo" name="ParcelNo" value="<?php echo $sessioned_consent ? $sessioned_consent['ParcelNumber'] : ''; ?>" required>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        Tenure
                                                                    </label>
                                                                    <select name="NatureOfTitle" id="NatureOfTitle" class="form-control" required>
                                                                        <option value=""></option>

                                                                        <?php
//$GetRegistries = $this->ParcelsValuationModel->GetRegistries();
                                                                        $Acts = $this->actsModel->GetAllActs();
                                                                        if (is_array($Acts)) {
                                                                            foreach ($Acts as $Act) {
                                                                                ?>
                                                                                <option value="<?= $Act->RowID; ?>" <?php echo ($sessioned_consent && $sessioned_consent['AdvocateCountryID'] == $Act->RowID) ? 'selected="selected"' : ''; ?>><?= $Act->ItemName; ?></option>
                                                                                <?php
                                                                            }
                                                                        } else {
                                                                            ?>
                                                                            <option value="0">No data</option>
                                                                            <?php
                                                                        }
                                                                        ?>

                                                                    </select>
                                                                </div>
                                                            </div>




                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-9">
                                                                    <div class="form-group connected-group">

                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <label class="control-label">
                                                                                    Title Number
                                                                                </label>
                                                                                <input type="text"  placeholder="Title Number" class="form-control" id="LRNumber" name="LRNumber" value="<?php echo $sessioned_consent ? $sessioned_consent['LRNumber'] : ''; ?>"  disabled>
                                                                            </div>

                                                                        </div>
                                                                    </div>




                                                                </div>




                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Check Property <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>

                            </div>
                            <!-- end wizard step-1 -->
                        <?php elseif ($input['step'] == 2) : ?>

                            <!-- begin wizard step-2 -->
                            <div class="wizard-step-2">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the second step of 7 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-8" style="padding-top: 38px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Proprietor Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open('consent/save_seller_continue', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <button id="idAddSeller" class="btn btn-sm btn-success" type="button">
                                                                    <i class="fa fa-user"></i> Add seller
                                                                </button>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button id="idAddSellerCompany" class="btn btn-sm btn-success" type="button">
                                                                    <i class="fa fa-building"></i> Add company
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <?php if ($sessioned_sellers): ?>
                                                                    <table class="table table-condensed table-responsive">
                                                                        <thead>
                                                                        <th>#</th>
                                                                        <th>Type</th>
                                                                        <th>Is contact person?</th>
                                                                        <th>First name</th>
                                                                        <th>Middle name</th>
                                                                        <th>Last name</th>
                                                                        <th>Nationality</th>
                                                                        <th>Action</th>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php
                                                                            $index = 1;
                                                                            $total_rent = 0;
                                                                            ?>
                                                                            <?php foreach ($sessioned_sellers as $single_seller): ?>
                                                                                <tr>
                                                                                    <td><?php echo $index; ?></td>
                                                                                    <td><label class="label <?php echo $single_seller['Type'] == 'individual' ? 'label-warning' : 'label-info'; ?>"><?php echo ucfirst($single_seller['Type']); ?></label></td>
                                                                                    <td><?php echo $single_seller['IsContactPerson']; ?></td>
                                                                                    <td><?php echo $single_seller['FirstName']; ?></td>
                                                                                    <td><?php echo $single_seller['MiddleName']; ?></td>
                                                                                    <td><?php echo $single_seller['LastName']; ?></td>
                                                                                    <td><?php echo $single_seller['CountryID']; ?></td>
                                                                                    <td><a href="" class="deleteSeller" style="color: red;font-size: 16px;" data-id="<?php echo $single_seller['RowID']; ?>"><i class="fa fa-trash-o "></i></a></i></td>
                                                                                </tr>
                                                                                <?php $index++; ?>
                                                                            <?php endforeach; ?>
                                                                        </tbody>
                                                                    </table>
                                                                <?php endif; ?>
                                                            </div>        
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/consent/apply_consent?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Continue <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>

                            </div>
                        <?php elseif ($input['step'] == 3) : ?>
                            <div class="wizard-step-3">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the third step of 7 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8" style="padding-top: 38px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Proposed party details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open('consent/save_buyer_continue', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <button id="idAddBuyer" class="btn btn-sm btn-success" type="button">
                                                                    <i class="fa fa-user"></i> Add party 
                                                                </button>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button id="idAddBuyerCompany" class="btn btn-sm btn-success" type="button">
                                                                    <i class="fa fa-building"></i> Add company
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <?php if ($sessioned_buyers): ?>
                                                                    <table class="table table-condensed table-responsive">
                                                                        <thead>
                                                                        <th>#</th>
                                                                        <th>Type</th>
                                                                        <th>Is contact person?</th>
                                                                        <th>First name</th>
                                                                        <th>Middle name</th>
                                                                        <th>Last name</th>
                                                                        <th>Nationality</th>
                                                                        <th>Action</th>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php
                                                                            $index = 1;
                                                                            $total_rent = 0;
                                                                            ?>
                                                                            <?php foreach ($sessioned_buyers as $single_buyer): ?>
                                                                                <tr>
                                                                                    <td><?php echo $index; ?></td>
                                                                                    <td><label class="label <?php echo $single_buyer['Type'] == 'individual' ? 'label-warning' : 'label-info'; ?>"><?php echo ucfirst($single_buyer['Type']); ?></label></td>
                                                                                    <td><?php echo $single_buyer['IsContactPerson']; ?></td>
                                                                                    <td><?php echo $single_buyer['FirstName']; ?></td>
                                                                                    <td><?php echo $single_buyer['MiddleName']; ?></td>
                                                                                    <td><?php echo $single_buyer['LastName']; ?></td>
                                                                                    <td><?php echo $single_buyer['CountryID']; ?></td>
                                                                                    <td><a href="" class="deleteBuyer" style="color: red;font-size: 16px;" data-id="<?php echo $single_buyer['RowID']; ?>"><i class="fa fa-trash-o "></i></a></i></td>
                                                                                </tr>
                                                                                <?php $index++; ?>
                                                                            <?php endforeach; ?>

                                                                        </tbody>
                                                                    </table>
                                                                <?php endif; ?>
                                                            </div>        
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/consent/apply_consent?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Continue <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>

                            </div>
                        <?php elseif ($input['step'] == 4): ?>
                            <div class="wizard-step-4">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the fourth step of 8 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-8" style="padding-top: 38px;">
                                            <div class="modal-content">

                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Advocate Details</h4>
                                                </div>
                                                <div id="msgadvo">


                                                </div>

                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open('consent/save_advocate_details', array('data-parsley-validate' => 'true', 'name' => 'form-wizard')); ?>

                                                        <div class="row">
                                                            <div class="col-md-12">

                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        First  Name
                                                                    </label>
                                                                    <input type="text"  placeholder="First Name" class="form-control" name="AdvocateFirstName" value="<?php echo $sessioned_consent ? $sessioned_consent['AdvocateFirstName'] : ''; ?>" required/>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        Middle  Name
                                                                    </label>
                                                                    <input type="text"  placeholder="Middle Name" class="form-control" name="AdvocateMiddleName" value="<?php echo $sessioned_consent ? $sessioned_consent['AdvocateMiddleName'] : ''; ?>" required/>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        Last  Name
                                                                    </label>
                                                                    <input type="text"  placeholder="Last Name" class="form-control" name="AdvocateLastName" value="<?php echo $sessioned_consent ? $sessioned_consent['AdvocateLastName'] : ''; ?>" required/>
                                                                </div>


                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        KRA Pin Number
                                                                    </label>
                                                                    <input type="text"  placeholder="Pin Number" class="form-control"  name="AdvocatePinNumber" value="<?php echo $sessioned_consent ? $sessioned_consent['AdvocatePinNumber'] : ''; ?>" required/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">

                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        Postal Address
                                                                    </label>
                                                                    <input type="text"  placeholder="Postal Address" class="form-control"  name="AdvocatePostalAddress" value="<?php echo $sessioned_consent ? $sessioned_consent['AdvocatePostalAddress'] : ''; ?>" required/>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        Email
                                                                    </label>
                                                                    <input  placeholder="Email" class="form-control"  name="AdvocateEmail" value="<?php echo $sessioned_consent ? $sessioned_consent['AdvocateEmail'] : ''; ?>" required type="email"/>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        Telephone Number
                                                                    </label>
                                                                    <input type="number"  placeholder="Mobile Number" class="form-control"   name="AdvocatePhone" value="<?php echo $sessioned_consent ? $sessioned_consent['AdvocatePhone'] : ''; ?>" required/>
                                                                </div>


                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        Law Firm
                                                                    </label>
                                                                    <input type="text"  placeholder="Law Firm" class="form-control"  name="AdvocateLawFirm" value="<?php echo $sessioned_consent ? $sessioned_consent['AdvocateLawFirm'] : ''; ?>" required/>
                                                                </div>

                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">

                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        PF Number
                                                                    </label>
                                                                    <input type="text"  placeholder="PF Number" class="form-control"  name="AdvocatePFNumber" value="<?php echo $sessioned_consent ? $sessioned_consent['AdvocatePFNumber'] : ''; ?>" required/>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        ID Number
                                                                    </label>
                                                                    <input type="number"  placeholder="ID Number" class="form-control"  name="AdvocateIDNumber" value="<?php echo $sessioned_consent ? $sessioned_consent['AdvocateIDNumber'] : ''; ?>" required/>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        Nationality
                                                                    </label>

                                                                    <select name="AdvocateCountryID" class="form-control" required>
                                                                        <option value="">Select</option>
                                                                        <?php
                                                                        $Countries = $this->Countrymodel->GetAllCountries();
                                                                        if (is_array($Countries)) {
                                                                            foreach ($Countries as $Country) {
                                                                                ?>
                                                                                <option value="<?= $Country->RowID; ?>" <?php echo ($sessioned_consent && $sessioned_consent['AdvocateCountryID'] == $Country->RowID) ? 'selected="selected"' : ''; ?>><?= $Country->CountryName; ?></option>
                                                                                <?php
                                                                            }
                                                                        } else {
                                                                            ?>
                                                                            <option value="0">No data</option>
                                                                            <?php
                                                                        }
                                                                        ?>

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>

                                                            <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/consent/apply_consent?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Continue <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>
                            </div>
                        <?php elseif ($input['step'] == 5): ?>
                            <div class="wizard-step-5">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the fourth step of 7 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-8" style="padding-top: 38px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Consent Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open('consent/save_transaction_details', array('data-parsley-validate' => 'true', 'name' => 'form-wizard')); ?>
                                                        <div class="row">
                                                            <div class="col-md-5">

                                                            </div>
                                                            <div class="col-md-12">

                                                                <div class="col-md-6">
                                                                    <div class="form-group connected-group">

                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <label class="control-label">
                                                                                    Nature of Transaction
                                                                                </label>
                                                                                <?php
                                                                                $options = array(
                                                                                    '' => 'Select',
                                                                                    'lease' => 'Lease',
                                                                                    'transfer' => 'Transfer',
                                                                                    'charge' => 'Charge'
                                                                                );
                                                                                $selected = 'transfer';
                                                                                echo form_dropdown('Nature_Of_Transaction', $options, $selected, array('class' => 'form-control', 'disabled' => 'disabled'));
                                                                                ?>

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">
                                                                            Transaction Description
                                                                        </label>
                                                                        <input type="text" name="TransactionDescription" value="<?php echo $sessioned_consent ? $sessioned_consent['TransactionDescription'] : ''; ?>" placeholder="Transaction Description" id="form-field-9" class="form-control" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-5">


                                                            </div>
                                                            <div class="col-md-12">

                                                                <div class="col-md-6">
                                                                    <div class="form-group connected-group">

                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <label class="control-label">
                                                                                    Term
                                                                                </label>
                                                                                <input type="text" name="TransactionTerm" value="<?php echo $sessioned_consent ? $sessioned_consent['TransactionTerm'] : ''; ?>" placeholder="Period" id="form-field-9" class="form-control" required>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <label class="control-label">
                                                                                    Term Period
                                                                                </label>
                                                                                <select name="TransactionTermPeriod" class="form-control" required>
                                                                                    <option value="">Select</option>
                                                                                    <option value="months" <?php echo ($sessioned_consent && $sessioned_consent['TransactionTermPeriod'] == 'months') ? 'selected="selected"' : ''; ?>>Months</option>
                                                                                    <option value="years" <?php echo ($sessioned_consent && $sessioned_consent['TransactionTermPeriod'] == 'years') ? 'selected="selected"' : ''; ?>>Years</option>
                                                                                </select>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        Type of Transaction
                                                                    </label>
                                                                    <?php
                                                                    $options = array(
                                                                        '' => 'Select',
                                                                        'sale' => 'Sale',
                                                                        'lease' => 'Lease',
                                                                        'spouse_transfer' => 'Spouse transfer',
                                                                        'gift' => 'Gift',
                                                                    );
                                                                    $selected = '';
                                                                    echo form_dropdown('TypeOfTransaction', $options, $sessioned_consent && $sessioned_consent['TypeOfTransaction'], array('class' => 'form-control', 'required' => 'required'));
                                                                    ?>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label>Value (Estimated if gift)</label>
                                                                        <input type="number" name="TransactionAmount" value="<?php echo $sessioned_consent ? $sessioned_consent['TransactionAmount'] : ''; ?>" placeholder="Amount" id="form-field-11"  class="form-control" required integer>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/consent/apply_consent?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Continue <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>

                            </div>
                        <?php elseif ($input['step'] == 6) : ?>
                            <div class="wizard-step-6">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the fifth step of 7 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-8" style="padding-top: 38px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Rent Clearance for <?php echo $sessioned_consent['LRNumber']; ?></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open('consent/rent_clearance', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <h3>Annual Rent: KES <?php echo number_format($sessioned_consent['RentPrincipal'], 2); ?></h3>
                                                                <h3>Rent Interest: KES <?php echo number_format($sessioned_consent['RentInterest'], 2); ?></h3>
                                                                <h3>Rent Arrears: KES <?php echo number_format($sessioned_consent['Rent'], 2); ?></h3>

                                                                <?php if ($sessioned_consent['Rent']): ?>
                                                                    <table class="table table-condensed table-bordered">
                                                                        <thead>
                                                                        <th>#</th>
                                                                        <th>Description</th>
                                                                        <th>Amount (KES)</th>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php
                                                                            $index = 1;
                                                                            $rent_data = json_decode($sessioned_consent['RentData'], true);
                                                                            ?>
                                                                            <?php if (array_key_exists('items', $rent_data)): ?>
                                                                                <?php foreach ($rent_data['items'] as $single_rent): ?>
                                                                                    <tr>
                                                                                        <td><?php echo $index; ?></td>
                                                                                        <td><?php echo $single_rent['description']; ?></td>
                                                                                        <td><?php echo number_format($single_rent['amount'], 2); ?></td>
                                                                                    </tr>
                                                                                    <?php
                                                                                    $index++;
                                                                                    ?>
                                                                                <?php endforeach; ?>
                                                                            <?php endif; ?>
                                                                        </tbody>
                                                                    </table>
                                                                <?php else: ?>
                                                                    <p>You've cleared all your rent.</p>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/consent/apply_consent?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Continue <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>

                            </div>
                        <?php elseif ($input['step'] == 7) : ?>

                            <div class="wizard-step-7">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the sixth step of 7 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>


                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-8" style="padding-top: 38px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Upload Rates Clearance Certificate</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">

                                                        <?php echo form_open_multipart('consent/rate_clearance', array('data-parsley-validate' => 'true', 'name' => 'form-wizard')); ?>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <label class="control-label">
                                                                    Rate Clearance Number
                                                                </label>
                                                                <input type="text" name="RateClearanceNumber" value="<?php echo $sessioned_consent ? $sessioned_consent['RateClearanceNumber'] : ''; ?>" placeholder="Rate Clearance Number" class="form-control" required>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label class="control-label">
                                                                    Rate Clearance Certificate (.png,.jpeg,.pdf)
                                                                </label>
                                                                <div class="form-group">
                                                                    <input type="file" name="clearance_certificate" class="form-control" accept=".png,.jpeg,.jpg,.pdf">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/consent/apply_consent?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Upload certificate <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>
                            </div>
                        <?php elseif ($input['step'] == 8): ?>
                            <div class="wizard-step-8">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the sixth step of 7 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, affected party details and rent and rates clearance certificate.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8" style="padding-top: 38px; overflow-y:scroll;height: 300px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Review your transfer application</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open_multipart('consent/review_application', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <table class="table table-bordered table-hover">
                                                                    <tr>
                                                                        <td>Registry</td>
                                                                        <td><?php echo $sessioned_consent['RegistryID']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Registration Section</td>
                                                                        <td><?php echo $sessioned_consent['RegistrationSectionID']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Parcel Number</td>
                                                                        <td><?php echo $sessioned_consent['ParcelNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>LR Number</td>
                                                                        <td><?php echo $sessioned_consent['LRNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Tenure</td>
                                                                        <td><?php echo $sessioned_consent['Act']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Owners</td>
                                                                        <td> <?php if ($sessioned_sellers): ?>
                                                                                <table class="table table-condensed">
                                                                                    <thead>
                                                                                    <th>#</th>
                                                                                    <th>Type</th>
                                                                                    <th>Is contact person?</th>
                                                                                    <th>First name</th>
                                                                                    <th>Middle name</th>
                                                                                    <th>Last name</th>
                                                                                    <th>Nationality</th>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        $index = 1;
                                                                                        $total_rent = 0;
                                                                                        ?>
                                                                                        <?php foreach ($sessioned_sellers as $single_seller): ?>
                                                                                            <tr>
                                                                                                <td><?php echo $index; ?></td>
                                                                                                <td><label class="label <?php echo $single_seller['Type'] == 'individual' ? 'label-warning' : 'label-info'; ?>"><?php echo ucfirst($single_seller['Type']); ?></label></td>
                                                                                                <td><?php echo $single_seller['IsContactPerson']; ?></td>
                                                                                                <td><?php echo $single_seller['FirstName']; ?></td>
                                                                                                <td><?php echo $single_seller['MiddleName']; ?></td>
                                                                                                <td><?php echo $single_seller['LastName']; ?></td>
                                                                                                <td><?php echo $single_seller['CountryID']; ?></td>
                                                                                            </tr>
                                                                                            <?php $index++; ?>
                                                                                        <?php endforeach; ?>

                                                                                    </tbody>
                                                                                </table>
                                                                            <?php endif; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Buyers</td>
                                                                        <td> <?php if ($sessioned_buyers): ?>
                                                                                <table class="table table-condensed">
                                                                                    <thead>
                                                                                    <th>#</th>
                                                                                    <th>Type</th>
                                                                                    <th>Is contact person?</th>
                                                                                    <th>First name</th>
                                                                                    <th>Middle name</th>
                                                                                    <th>Last name</th>
                                                                                    <th>Nationality</th>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        $index = 1;
                                                                                        $total_rent = 0;
                                                                                        ?>
                                                                                        <?php foreach ($sessioned_buyers as $single_buyer): ?>
                                                                                            <tr>
                                                                                                <td><?php echo $index; ?></td>
                                                                                                <td><label class="label <?php echo $single_buyer['Type'] == 'individual' ? 'label-warning' : 'label-info'; ?>"><?php echo ucfirst($single_buyer['Type']); ?></label></td>
                                                                                                <td><?php echo $single_buyer['IsContactPerson']; ?></td>
                                                                                                <td><?php echo $single_buyer['FirstName']; ?></td>
                                                                                                <td><?php echo $single_buyer['MiddleName']; ?></td>
                                                                                                <td><?php echo $single_buyer['LastName']; ?></td>
                                                                                                <td><?php echo $single_buyer['CountryID']; ?></td>
                                                                                            </tr>
                                                                                            <?php $index++; ?>
                                                                                        <?php endforeach; ?>

                                                                                    </tbody>
                                                                                </table>
                                                                            <?php endif; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's First Name</td>
                                                                        <td><?php echo $sessioned_consent['AdvocateFirstName']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's Middle Name</td>
                                                                        <td><?php echo $sessioned_consent['AdvocateMiddleName']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's Last Name</td>
                                                                        <td><?php echo $sessioned_consent['AdvocateLastName']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's KRA PIN Number</td>
                                                                        <td><?php echo $sessioned_consent['AdvocatePinNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's Postal Address</td>
                                                                        <td><?php echo $sessioned_consent['AdvocatePostalAddress']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's Email</td>
                                                                        <td><?php echo $sessioned_consent['AdvocateEmail']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's Phone</td>
                                                                        <td><?php echo $sessioned_consent['AdvocatePhone']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's Law Firm</td>
                                                                        <td><?php echo $sessioned_consent['AdvocateLawFirm']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's PF Number</td>
                                                                        <td><?php echo $sessioned_consent['AdvocatePFNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's ID Number</td>
                                                                        <td><?php echo $sessioned_consent['AdvocateIDNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advocate's Country</td>
                                                                        <td><?php echo $sessioned_consent['AdvocateCountryID']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Nature of transaction</td>
                                                                        <td><?php echo $sessioned_consent['TransactionNature']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Nature of transaction</td>
                                                                        <td><?php echo $sessioned_consent['TransactionNature']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Transaction description</td>
                                                                        <td><?php echo $sessioned_consent['TransactionDescription']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Term</td>
                                                                        <td><?php echo $sessioned_consent['TransactionTerm']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Transaction term period</td>
                                                                        <td><?php echo $sessioned_consent['TransactionTermPeriod']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Type of transaction</td>
                                                                        <td><?php echo $sessioned_consent['TypeOfTransaction']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Value</td>
                                                                        <td><?php echo $sessioned_consent['TransactionAmount']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Rent arrears</td>
                                                                        <td><?php echo $sessioned_consent['Rent']; ?></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/consent/apply_consent?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Make payment <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>
                            </div>
                        <?php elseif ($input['step'] == 9) : ?>
                            <div class="wizard-step-9">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="panel panel-default panel-blog panel-checkout">
                                            <div class="panel-body">
                                                <ul class="blog-meta mb5">
                                                    <!--<li>Jan 09, 2015</li>-->
                                                </ul>
                                                <?php if (array_key_exists('step', $input) && $input['step'] == 9): ?>
                                                    <div style="background-color: #FFF;">
                                                        <?php
                                                        $apiClientId = '13';
                                                        $amount = $sessioned_consent['Rent'] + 1550;
                                                        $amount = '10';
                                                        $serviceId = '29';
                                                        $idNumber = $sessioned_user['IDNumber'];
                                                        $currency = 'KES';
                                                        $billRef = $sessioned_consent['RowID'];
                                                        $serviceDesc = $sessioned_consent['Description'];
                                                        $clientName = $sessioned_user['FirstName'] . ' ' . $sessioned_user['LastName'];
                                                        $key = 'TYV0FUD0BY';
                                                        $secret = 'BGI0ZTSPL2WX0X6';

                                                        $payload = "{$apiClientId}{$amount}{$serviceId}{$idNumber}{$currency}{$billRef}{$serviceDesc}{$clientName}{$secret}";
                                                        $hash = base64_encode(hash_hmac("sha256", $payload, $key));
                                                        ?>
                                                        <form id="idPaymentForm" method="post" action="https://pesaflow.ecitizen.go.ke/PaymentAPI/iframev2.1.php" target="my_frame">
                                                            <input type="hidden" name="apiClientID" value="13" />
                                                            <input type="hidden" name="secureHash" value="<?php echo $hash; ?>" />
                                                            <input type="hidden" name="billDesc" value="<?php echo $sessioned_consent['Description']; ?>" />
                                                            <input type="hidden" name="billRefNumber" value="<?php echo $sessioned_consent['RowID']; ?>" />
                                                            <input type="hidden" name="currency" value="KES" />
                                                            <input type="hidden" name="serviceID" value="29" />
                                                            <input type="hidden" name="clientMSISDN" value="<?php echo $sessioned_user['Phone']; ?>" />
                                                            <input type="hidden" name="clientName" value="<?php echo $clientName; ?>" />
                                                            <input type="hidden" name="clientIDNumber" value="<?php echo $sessioned_user['IDNumber']; ?>" />
                                                            <input type="hidden" name="clientType" value="citizen" />
                                                            <input type="hidden" name="clientEmail" value="<?php echo $sessioned_user['Email']; ?>" />
                                                            <input type="hidden" name="callBackURLOnSuccess" value="<?php echo base_url(); ?>consent/payment_completed?bill_ref=<?php echo $sessioned_consent['RowID']; ?>" />
                                                            <input type="hidden" name="callBackURLOnFail" value="<?php echo base_url(); ?>consent/apply_consent?step=7" />
                                                            <input type="hidden" name="notificationURL" value="<?php echo base_url(); ?>lmais/consent/apply_consent?step=7" />
                                                            <input type="hidden" name="amountExpected" value="<?php echo $amount; ?>" />
                                                        </form>
                                                        <iframe width="100%" height="600px" style="border: none;" name="my_frame"></iframe>
                                                        <script type="text/javascript">
                                                            document.getElementById('idPaymentForm').submit();
                                                        </script>
                                                    </div>
                                                    <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/consent/apply_consent?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                <?php endif; ?>
                                            </div><!-- panel-body -->
                                        </div>
                                    </div>

                                    <!-- end row -->
                                </fieldset>
                            </div>
                        <?php endif; ?>
                        <!-- end wizard step-4 -->
                    </div>

                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<!-- end #content -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->

<div id="idAddSellerModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('consent/save_seller', array('data-parsley-validate' => 'true', 'id' => 'idSaveSellerForm')); ?>
        <input type="hidden" name="ConsentID" value="<?php echo $sessioned_consent ? $sessioned_consent['RowID'] : ''; ?>"/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add seller</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idSellerAlert" class="alert alert-success hidden"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                ID Number
                            </label>
                            <input id="idSellerIdNumber"  type="number" name="Identification" value="" placeholder="ID Number/Passport" id="form-field-9" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <button id="idSellerGetDetails" class="btn btn-primary" style="margin-top: 20px;" disabled>Validate</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Is Primary Contact Person?
                            </label>
                            <br>
                            <label><input type="radio" name="IsContactPerson" value="Y"  checked=""/> Yes</label>
                            <label><input type="radio" name="IsContactPerson" value="N"/> No</label>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                First Name
                            </label>
                            <input id="idSellerFirstName" type="text"  name="FirstName" value="" placeholder="First Name" id="form-field-9" class="form-control" disabled required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Middle Name
                            </label>
                            <input id="idSellerMiddleName" type="text" name="MiddleName" value="" placeholder="Middle Name" id="form-field-10" class="form-control" disabled required>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input id="idSellerLastName" type="text" name="LastName" value="" placeholder="Last Name" id="form-field-11" class="form-control" disabled required>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Mobile Number
                            </label>
                            <input  id="idSellerMobileNumber" type="number" name="MobileNumber" value="" placeholder="Mobile Number" id="form-field-10" class="form-control" disabled required>
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input  id="idSellerEmailAddress" type="email" name="EmailAddress" value="" placeholder="Email" id="form-field-11" class="form-control" disabled required>

                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Postal Address
                            </label>
                            <input  id="idSellerAddress" type="text" name="Address" value="" placeholder="Address" id="form-field-11" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label class="control-label">
                                Nationality
                            </label>

                            <select id="idSellerNationality" name="CountryID" class="form-control" required>
                                <option value="">Select</option>
                                <?php
                                $Countries = $this->Countrymodel->GetAllCountries();
                                if (is_array($Countries)) {
                                    foreach ($Countries as $Country) {
                                        ?>
                                        <option value="<?= $Country->RowID; ?>"><?= $Country->CountryName; ?></option>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <option value="0">No data</option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idSaveSellerButton" type="button" class="btn btn-primary" type="submit">
                    Save changes
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<div id="idAddSellerCompanyModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('consent/save_seller_company', array('data-parsley-validate' => 'true', 'id' => 'idSaveSellerCompanyForm')); ?>
        <input type="hidden" name="ConsentID" value="<?php echo $sessioned_consent ? $sessioned_consent['ConsentID'] : ''; ?>"/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add company</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idSellerCompanyAlert" class="alert alert-success hidden"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Is Primary Contact Person?
                            </label>
                            <br>
                            <label><input type="radio" name="IsContactPerson" value="Y"  checked=""/> Yes</label>
                            <label><input type="radio" name="IsContactPerson" value="N"/> No</label>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Contact Person's First Name
                            </label>
                            <input id="idSellerCompanyFirstName" type="text"  name="FirstName" value="" placeholder="First Name" id="form-field-9" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Contact Person's Middle Name
                            </label>
                            <input id="idSellerCompanyMiddleName" type="text" name="MiddleName" value="" placeholder="Middle Name" id="form-field-10" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label>Contact Person's Last Name</label>
                                <input id="idSellerCompanyLastName" type="text" name="LastName" value="" placeholder="Last Name" id="form-field-11" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Contact Person's Mobile Number
                            </label>
                            <input  id="idSellerCompanyMobileNumber" type="number" name="MobileNumber" value="" placeholder="Mobile Number" id="form-field-10" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Contact Person's  Email Address</label>
                            <input  id="idSellerCompanyEmailAddress" type="email" name="EmailAddress" value="" placeholder="Email" id="form-field-11" class="form-control" required>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Company's Name
                            </label>
                            <input  id="idSellerCompanyMobileNumber" type="text" name="CompanyName" value="" placeholder="Company's Name" id="form-field-10" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's Registration Number
                            </label>
                            <input  id="idSellerCompanyAddress" type="text" name="CompanyRegistrationNumber" value="" placeholder="Company's Registration Number" id="form-field-11" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's Phone
                            </label>
                            <input  id="idSellerCompanyAddress" type="number" name="CompanyPhone" value="" placeholder="Company's Phone" id="form-field-11" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's Email
                            </label>
                            <input  id="idSellerCompanyAddress" type="email" name="CompanyEmail" value="" placeholder="Company's Email" id="form-field-11" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's Postal Address
                            </label>
                            <input  id="idSellerCompanyAddress" type="text" name="CompanyAddress" value="" placeholder="Company's Address" id="form-field-11" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's KRA PIN Number
                            </label>
                            <input  id="idSellerCompanyAddress" type="text" name="CompanyPinNumber" value="" placeholder="Company's Pin Number" id="form-field-11" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label class="control-label">
                                Company's Nationality
                            </label>

                            <select id="idSellerCompanyNationality" name="CountryID" class="form-control" required>
                                <option value="">Select</option>
                                <?php
                                $Countries = $this->Countrymodel->GetAllCountries();
                                if (is_array($Countries)) {
                                    foreach ($Countries as $Country) {
                                        ?>
                                        <option value="<?= $Country->RowID; ?>"><?= $Country->CountryName; ?></option>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <option value="0">No data</option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idSaveSellerCompanyButton" type="button" class="btn btn-primary" type="submit">
                    Save changes
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<div id="idAddBuyerCompanyModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('consent/save_buyer_company', array('data-parsley-validate' => 'true', 'id' => 'idSaveBuyerCompanyForm')); ?>
        <input type="hidden" name="ConsentID" value="<?php echo $sessioned_consent ? $sessioned_consent['ConsentID'] : ''; ?>"/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add company</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idBuyerCompanyAlert" class="alert alert-success hidden"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Is Primary Contact Person?
                            </label>
                            <br>
                            <label><input type="radio" name="IsContactPerson" value="Y"  checked=""/> Yes</label>
                            <label><input type="radio" name="IsContactPerson" value="N"/> No</label>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Contact Person's First Name
                            </label>
                            <input id="idBuyerCompanyFirstName" type="text"  name="FirstName" value="" placeholder="First Name" id="form-field-9" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Contact Person's Middle Name
                            </label>
                            <input id="idBuyerCompanyMiddleName" type="text" name="MiddleName" value="" placeholder="Middle Name" id="form-field-10" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label>Contact Person's Last Name</label>
                                <input id="idBuyerCompanyLastName" type="text" name="LastName" value="" placeholder="Last Name" id="form-field-11" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Contact Person's Mobile Number
                            </label>
                            <input  id="idBuyerCompanyMobileNumber" type="number" name="MobileNumber" value="" placeholder="Mobile Number" id="form-field-10" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Contact Person's  Email Address</label>
                            <input  id="idBuyerCompanyEmailAddress" type="email" name="EmailAddress" value="" placeholder="Email" id="form-field-11" class="form-control" required>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Company's Name
                            </label>
                            <input  id="idBuyerCompanyMobileNumber" type="text" name="CompanyName" value="" placeholder="Company's Name" id="form-field-10" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's Registration Number
                            </label>
                            <input  id="idBuyerCompanyAddress" type="text" name="CompanyRegistrationNumber" value="" placeholder="Company's Registration Number" id="form-field-11" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's Phone
                            </label>
                            <input  id="idBuyerCompanyAddress" type="number" name="CompanyPhone" value="" placeholder="Company's Phone" id="form-field-11" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's Email
                            </label>
                            <input  id="idBuyerCompanyAddress" type="text" name="CompanyEmail" value="" placeholder="Company's Email" id="form-field-11" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's Postal Address
                            </label>
                            <input  id="idBuyerCompanyAddress" type="text" name="CompanyAddress" value="" placeholder="Company's Address" id="form-field-11" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's KRA PIN Number
                            </label>
                            <input  id="idBuyerCompanyAddress" type="text" name="CompanyPinNumber" value="" placeholder="Company's Pin Number" id="form-field-11" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Share (Percentage)
                            </label>
                            <div class="input-group">
                                <input type="number" class="form-control" placeholder="Percentage share" name="Share" required>
                                <span class="input-group-addon" id="basic-addon2">%</span>
                            </div>                  
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Company's Nationality
                            </label>

                            <select id="idBuyerCompanyNationality" name="CountryID" class="form-control" required>
                                <option value="">Select</option>
                                <?php
                                $Countries = $this->Countrymodel->GetAllCountries();
                                if (is_array($Countries)) {
                                    foreach ($Countries as $Country) {
                                        ?>
                                        <option value="<?= $Country->RowID; ?>"><?= $Country->CountryName; ?></option>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <option value="0">No data</option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idSaveBuyerCompanyButton" type="button" class="btn btn-primary" type="submit">
                    Save changes
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<div id="idAddBuyerModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('consent/save_seller', array('data-parsley-validate' => 'true', 'id' => 'idSaveBuyerForm')); ?>
        <input type="hidden" name="ConsentID" value="<?php echo $sessioned_consent ? $sessioned_consent['RowID'] : ''; ?>"/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add buyer</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idBuyerAlert" class="alert alert-success hidden"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                ID Number
                            </label>
                            <input id="idBuyerIdNumber"  type="number" name="Identification" value="" placeholder="ID Number/Passport" id="form-field-9" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <button id="idBuyerGetDetails" class="btn btn-primary" style="margin-top: 20px;" disabled>Validate</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Is Primary Contact Person?
                            </label>
                            <br>
                            <label><input type="radio" name="IsContactPerson" value="Y"  checked=""/> Yes</label>
                            <label><input type="radio" name="IsContactPerson" value="N"/> No</label>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                First Name
                            </label>
                            <input id="idBuyerFirstName" type="text"  name="FirstName" value="" placeholder="First Name" id="form-field-9" class="form-control" disabled required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Middle Name
                            </label>
                            <input id="idBuyerMiddleName" type="text" name="MiddleName" value="" placeholder="Middle Name" id="form-field-10" class="form-control" disabled required>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input id="idBuyerLastName" type="text" name="LastName" value="" placeholder="Last Name" id="form-field-11" class="form-control" disabled required>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">
                                Mobile Number
                            </label>
                            <input  id="idBuyerMobileNumber" type="text" name="MobileNumber" value="" placeholder="Mobile Number" id="form-field-10" class="form-control" disabled required>
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input  id="idBuyerEmailAddress" type="text" name="EmailAddress" value="" placeholder="Email" id="form-field-11" class="form-control" disabled required>

                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Postal Address
                            </label>
                            <input  id="idBuyerAddress" type="text" name="Address" value="" placeholder="Address" id="form-field-11" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Share (Percentage)
                            </label>
                            <div class="input-group">
                                <input type="number" class="form-control" placeholder="Percentage share" name="Share" required>
                                <span class="input-group-addon" id="basic-addon2">%</span>
                            </div>                  
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Nationality
                            </label>
                            <select id="idBuyerNationality" name="CountryID" class="form-control" required>
                                <option value="">Select</option>
                                <?php
                                $Countries = $this->Countrymodel->GetAllCountries();
                                if (is_array($Countries)) {
                                    foreach ($Countries as $Country) {
                                        ?>
                                        <option value="<?= $Country->RowID; ?>"><?= $Country->CountryName; ?></option>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <option value="0">No data</option>
                                    <?php
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idSaveBuyerButton" type="button" class="btn btn-primary" type="submit">
                    Save changes
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<!--S# Delete Seller Modal-->
<div id="idDeleteSellerModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('consent/delete_seller', array('data-parsley-validate' => 'true', 'id' => 'idDeleteSellerForm')); ?>
        <input id="idDeleteSellerID" type="hidden" name="SellerID" value=""/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Seller</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idBuyerAlert" class="alert alert-success hidden"></p>
                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idDeleteSellerButton" type="button" class="btn btn-primary" type="submit">
                    Delete Seller
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<!--E# Delete Seller Modal-->
<!--S# Delete Buyer Modal-->
<div id="idDeleteBuyerModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open('consent/delete_buyer', array('data-parsley-validate' => 'true', 'id' => 'idDeleteBuyerForm')); ?>
        <input id="idDeleteBuyerID" type="hidden" name="BuyerID" value=""/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Buyer</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="idBuyerAlert" class="alert alert-success hidden"></p>
                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="idDeleteBuyerButton" type="button" class="btn btn-primary" type="submit">
                    Delete Buyer
                </button>
                <button type="button" class="btn"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<!--E# Delete Buyer Modal-->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/parsley/dist/parsley.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-wizards-validation.demo.min.js"></script>
<!--Edwin-->

<script src="<?php echo base_url(); ?>assets/js/apps.min.js?time=<?php echo time(); ?>"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<script></script>
<script>
                                                            $("#County").change(function (evt) {
                                                                var ActID = document.getElementById('Acts').value;
                                                                var CountyID = document.getElementById('County').value;
                                                                var select = $("#County");
                                                                var selectData = select.serialize();
                                                                var ajax = $.ajax({
                                                                    dataType: "json",
                                                                    type: 'POST',
                                                                    url: "<?php echo base_url(); ?>Transfer/GetRegistriesMatch/" + ActID + "/" + CountyID,
                                                                    data: selectData
                                                                });
                                                                ajax.done(function (response) {
                                                                    $("#testurl").html(response);
                                                                    var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                                    for (var key in response) {
                                                                        if (response.hasOwnProperty(key)) {
                                                                            //alert(key);
                                                                            //alert(response[key].RowID);
                                                                            //alert(response[key].NameOfRoad);
                                                                            listItems += "<option value='" + response[key].Registres + "'>" + response[key].Registries + "</option>";
                                                                        }
                                                                    }
                                                                    $("#Registry").html(listItems);
                                                                });
                                                                ajax.fail(function () {

                                                                });
                                                            });

                                                            $("#towntype").change(function (evt) {
                                                                var townTypeID = document.getElementById('towntype').value;
                                                                var select = $("#towntype");
                                                                var selectData = select.serialize();
                                                                var ajax = $.ajax({
                                                                    dataType: "json",
                                                                    type: 'POST',
                                                                    url: "http://localhost/lmais/start/GetTownNamesMatchTownType/" + townTypeID,
                                                                    data: selectData
                                                                });
                                                                ajax.done(function (response) {
                                                                    var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                                    for (var key in response) {
                                                                        if (response.hasOwnProperty(key)) {
                                                                            //alert(key);
                                                                            //alert(response[key].RowID);
                                                                            //alert(response[key].NameOfRoad);
                                                                            listItems += "<option value='" + response[key].NameOfTown + "'>" + response[key].NameOfTown + "</option>";
                                                                        }
                                                                    }
                                                                    $("#townNames").html(listItems);
                                                                });
                                                                ajax.fail(function () {

                                                                });
                                                            });

</script>

<script>
    $(document).ready(function () {
        App.init();
        FormWizardValidation.init();
    });
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>