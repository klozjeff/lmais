<!DOCTYPE html>
<html>


    <body class="fixed-left" >

        <!-- Begin page -->
        <div id="wrapper">


            <div class="row" >
                <div class="col-md-12">
                    <div class="panel-default">
                        <!-- <div class="panel-heading">
                            <h4>Invoice</h4>
                        </div> -->
                        <table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
                               <thead>
                                <tr>
                                    <td align="center">
                                        <img alt="" src="https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png" style="height:100px;" /><br/>
                                        __________<br/> <br/>
                                        THE REGISTERED LAND ACT<br />
                                        __________<br/>

                                        <br/> 
                                        <P CLASS='H3'> OFFICIAL SEARCH CERTIFICATE </p><br>
                                    </td>
                                </tr>
                            </thead>
                        </table>

                        <hr>
                        <div class="m-h-50"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="">
                                    <?php if ($TransactionData) : ?>

                                        <?php foreach ($TransactionData as $Result) :
                                            ?>
                                            <P CLASS='H5'> A. PROPERTY SECTION </P>

                                            <?php
                                            if ($PropertyData = $this->PropertyModel->Property_details($Result->DocID)):

                                                foreach ($PropertyData as $Data => $PropertyColumn) :
                                                    ?> 
                                                    <table class="table">
                                                        <TBODY>
                                                            <tr>
                                                                <td width = "25%">Title Number: </td>
                                                                <th> <?php echo $PropertyColumn->Title; ?> </th>
                                                            </tr>

                                                            <tr>
                                                                <td width = "25%">Registration Section</td>
                                                                <th> <?php echo $PropertyColumn->RegistrationSection; ?> </th>

                                                            </tr>
                                                            <tr>
                                                                <td width = "25%">Parcel Number</td>
                                                                <th> <?php echo $PropertyColumn->ParcelNo; ?> </th>

                                                            </tr>
                                                            <tr> 
                                                                <td width = "25%">Tenure</td>
                                                                <th> <?php echo $PropertyColumn->NatureOfTitle; ?> </th>
                                                            </tr>


                                                            <tr> 
                                                                <td width = "25%">Lease From</td>
                                                                <th> <?php echo $PropertyColumn->LeaseFrom; ?> </th>
                                                            </tr>
                                                            <tr> 
                                                                <td width = "25%">Lease Term And Rent</td>
                                                                <th> <?php echo $PropertyColumn->LeaseTerm; ?> </th>
                                                            </tr>


                                                        </tbody>

                                                    </table>
                                                    <hr>


                                                    <P CLASS='H5'>B. PROPRIETOR SECTION </P>




                                                    <table class="table table-bordered">

                                                        <?php
                                                        if ($ProprietorData = $this->proprietorModel->GetProprietorData($Result->DocID)):

                                                            foreach ($ProprietorData as $Proprietor):
                                                                ?>

                                                                <thead>

                                                                    <tr>
                                                                        <td width="15%"><?php echo $Proprietor->EntryNo; ?></td>
                                                                        <td width="15%"> <?php echo $Proprietor->EntryDate; ?> </td>
                                                                        <td width="70%">Name: <?php echo $Proprietor->RegisteredProprietor; ?> <br />ID : <?php echo $Proprietor->RegisteredID; ?> <br />Address : <?php echo $Proprietor->RegisteredAddress; ?> <br />Entry Remarks : ' <?php echo $Proprietor->RemarksAndConsideration; ?>'</td>

                                                                    </tr>
                    <?php endforeach; ?>
                <?php endif; ?> 


                                                        </thead>


                                                    </table>





                                                    <P CLASS='H5'>B. ENCUMBRANCE SECTION </P>

                                                    <table class="table table-bordered">



                                                        <thead>

                                                            <?php
                                                            if ($EncumbranceData = $this->encumbranceModel->GetEncumbranceData($Result->DocID)):

                                                                foreach ($EncumbranceData as $Encumbrance):
                                                                    ?>



                                                                    <tr nobr="true">
                                                                        <td width="15%"> <?php echo $Encumbrance->EntryNo; ?> </td>
                                                                        <td width="15%"> <?php echo $Encumbrance->EntryDate; ?> </td>
                                                                        <td width="70%">Nature:  <?php echo $Encumbrance->NatureOfEncumbrance; ?>  <br />Further Particulars :  <?php echo $Encumbrance->FurtherParticulars; ?> </td>
                                                                    </tr>
                    <?php endforeach; ?>
                <?php endif; ?> 

                                                        </thead>


                                                    </table>

                                                    <br><br>
                                                    <div class="barcodecell " ><barcode code="<?php echo $Result->TransactionID; ?>" type="C128B" class="barcode" /></div>

                                                    <br/><br/>
                                                    <table class="table">
                                                        <tr>


                                                            <td width = "20%"> POWERED BY :</td>
                                                            <th width = "50%"> <strong>Rerence Number:  <?php echo $Result->TransactionID; ?></strong></th>
                                                        </tr>
                                                        <br/><br/>
                                                        <tr>

                                                            <th width = "20%"> <img alt="" src="https://www.ecitizen.go.ke/assets/img/ecitizen-logo.png" style=";" /><br/></th>

                                                        </tr>
                                                    </table>
                                                    Date Created:	<?php echo $Result->DateCreated; ?>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="alert alert-info" role="alert">
                                            <b>Note:</b> This document is computer generated and therefore not signed. It is valid document issued under the authority of The
                                            Ministry of Lands and Physical Planning
                                        </div>


                                    </div>

                                </div>

                            </div>




                        </div>
                        <!-- end container -->

                    </div>
                    <!-- end content -->




                </div>
                <!-- ============================================================== -->
                <!-- End Right content here -->
                <!-- ============================================================== -->


                <!-- Right Sidebar -->

                <!-- /Right-bar -->

                </div>
                <!-- END wrapper -->
            <?php endforeach; ?> 
        <?php endif; ?> 
    <?php endforeach; ?> 
<?php endif; ?>     
</body>
</html>