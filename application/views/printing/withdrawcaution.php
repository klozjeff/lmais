<!DOCTYPE html>
<html>
  

    <body class="fixed-left" >
        
        <!-- Begin page -->
        <div id="wrapper">
  

                        <div class="row" >
                            <div class="col-md-12">
                                <div class="panel-default">
                                    <!-- <div class="panel-heading">
                                        <h4>Invoice</h4>
                                    </div> -->
									<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>
		<tr>
			<td align="center">
			<img alt="" src="https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png" style="height:100px;" /><br/>
			__________<br/> <br/>
			THE REGISTERED LAND ACT<br />
			(No. 3 of 2012, Section 108) <br/>
			(Chapter 300)

			<br/> 
			<P CLASS='H3'> WITHDRAW OF CAUTION </p><br>
			</td>
			</tr>
			</thead>
			</table>
			
                <hr>
                                        <div class="m-h-50"></div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="">
												<?php if ($TransactionData) : ?>
												<?php foreach ($TransactionData as $tdata) : ?>
													<P CLASS='H5'> A. WITHDRAW OF CAUTION PARTICULARS </P>
													<table class="table">
                                                        <TBODY>
                                                          <tr>
															<td width = "25%">Title Number: </td>
                                                            <th> <?php echo $tdata->LRNumber;?> </th>
                                                          </tr>
														
														<tr>
															<td width = "25%">Caution Interest</td>
                                                            <th> <?php echo $tdata->CautionInterest;?> </th>
                                                           
                                                        </tr>
														<tr> </tr>
														<tr> </tr>
														 <tr>
															<td width = "25%">Caution Description</td>
                                                              <th> <?php echo $tdata->CautionDescription;?> </th>
                                                           
                                                        </tr>
														
														</tbody>
                                                       
                                                    </table>
													  <hr>
													   <?php if ($ApplicantsData) : ?>
													
													<P CLASS='H5'>B. WITHDRAW OF CAUTION APPLICANT(S) </P>
													  <?php
                                                                                        $index = 1;
                                                                                      
                                                                                        ?>
													<?php foreach ($ApplicantsData as $Result) : ?>
												<table class="table table-bordered">
												
														
													
                                                        <thead>
														
														
														 
														 <tr>
															<td> NAME: </td>
                                                            <td> <?php echo $Result->FirstName.' '.$Result->MiddleName.' '. $Result->LastName; ?> </td>
                                                          </tr>
														   <tr>
															<td>EMAIL: </td>
                                                            <td> <?php echo $Result->EmailAddress;?>	</td>
                                                          </tr>
														   <tr>
															<td>ID NUMBER: </td>
                                                            <td> <?php echo $Result->Identification;?> </td>
                                                          </tr>
														   <tr>
															<td>TELEPHONE: </td>
                                                            <td> <?php echo $Result->MobileNumber;?> </td>
                                                          </tr>
                                                       
													    
														</thead>
                                                     

													 </table>
<?php $index++; ?>
													   <?php endforeach; ?>
													 <?php endif; ?> 
                                                   
													<br><br>
													<div class="barcodecell " ><barcode code="<?php echo $tdata->TransactionID;?>" type="C128B" class="barcode" /></div>
													
													 <br/><br/>
													<table class="table">
													<tr>
                                                              
															  
                                                               <td width = "20%"> POWERED BY :</td>
                                                            <th width = "50%"> <strong>Rerence Number:  <?php echo $tdata->TransactionID;?></strong></th>
                                                            </tr>
																<br/><br/>
																<tr>
                                                               
                                                               <th width = "20%"> <img alt="" src="https://www.ecitizen.go.ke/assets/img/ecitizen-logo.png" style=";" /><br/></th>
                                                            
                                                            </tr>
															</table>
														Date Created:	<?php echo $tdata->DateCreated;?>
													
                                                </div>
                                            </div>
                                        </div>
                                        
                                       <div class="alert alert-info" role="alert">
  <b>Note:</b> This document is computer generated and therefore not signed. It is valid document issued under the authority of The
Ministry of Lands and Physical Planning
</div>
                                       
                                 
                                </div>

                            </div>

                        </div>




                    </div>
                    <!-- end container -->

                </div>
                <!-- end content -->




            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->
 <?php endforeach; ?>
<?php endif; ?> 
    
    </body>
</html>