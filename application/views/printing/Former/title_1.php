<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <style type="text/css">
            p{
                padding-top: 0;
                padding-bottom: 0;
            }
            .text-justify{
                text-align: justify;
            }
            .dotted{
                border-bottom: 1px dotted #000;
                text-decoration: none;
            }
            .text-bold{
                font-weight: bold;
            }
            .pull-left{
                float: left;
            }
            .pull-right{
                float: right;
            }

            .text-center{
                text-align: center;
            }
            .centered {
                margin: 0 auto;
                margin-left: 200px;
            }
            .font-weight-bold{
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="centered" style="width: 50%">
        </div>
        <div class="centered" style="width: 50%">
        </div>

        <div style="clear: both;">
            <table>
                <tr>
                    <td style="width: 50%;">
                        <?php echo $page_1; ?>
                    </td>
                    <td style="width: 50%;">
                        <?php echo $page_1; ?>
                    </td>
                </tr>
            </table>
        </div>
    </body>
    <html>