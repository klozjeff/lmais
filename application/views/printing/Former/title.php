<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <style type="text/css">
            p{
                padding-top: 0;
                padding-bottom: 0;
            }
            .text-justify{
                text-align: justify;
            }
            .dotted{
                border-bottom: 1px dotted #000;
                text-decoration: none;
            }
            .text-bold{
                font-weight: bold;
            }
            .pull-left{
                float: left;
            }
            .pull-right{
                float: right;
            }

            .text-center{
                text-align: center;
            }
            .centered {
                margin: 0 auto;
                margin-left: 200px;
            }
            .font-weight-bold{
                font-weight: bold;
            }

            /*TABLE CSS*/
            .table th,
            .table td {
                border-top: 1px solid #eceeef;
            }

            .table thead th {
                border-bottom: 2px solid #eceeef;
            }
        </style>
    </head>
    <body>
        <div style="width: 100%">
            <table>
                <tr>
                    <td style=" width: 45%">
                        <?php echo $page_1; ?>
                    </td>
                    <td style=" width: 45%">
                        <?php echo $page_2; ?>
                    </td>
                </tr>
            </table>
        </div>
    </body>
    <body>
        <div style="width: 100%">
            <table>
                <tr>
                    <td style=" width: 45%">
                        <?php echo $page_3; ?>
                    </td>
                    <td style=" width: 45%">
                        <?php echo $page_4; ?>
                    </td>
                </tr>
            </table>
        </div>
    </body>
    <html>