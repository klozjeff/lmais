<div style="width: 100%">
    <p class="text-center"><i>(To be completed only when the applicant has paid the fee of Sh 50)</i></p>
    <p class="text-center">At the date state on the front hereof, the following entries appeared in the register relating to the land:</p>
</div>
<div>
    <table style="width:100%" class="table">
        <tr>
            <td>EDITION:1<br>
                OPENED: 12/12/2017</td>
            <td>PART A - PROPERTY SECTION</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>REGISTRATION SECTION</td>
            <td>BASEMENTS</td>
            <td>NATURE OF TITLE</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</div>
<div style="width: 100%">
    <hr>
    <p class="text-center font-weight-bold">PART B: PROPRIETORSHIP SECTION</p>
    <hr>
</div>
<div>
    <table style="width:100%" class="table">
        <tr>
            <th>ENTRY NO</th>
            <th>DATE</th>
            <th>NAME OF REGISTERED PROPRIETOR</th>
            <th>ADDRESS AND DESCRIPTION OF REGISTERED PROPRIETOR</th>
            <th>CONSIDERATION AND REMARKS</th>
            <th>SIGNATURE OF REGISTRAR</th>
        </tr>
        <tr>
            <td>1</td>
            <td>01/03/2017</td>
            <td>JOHN MUTUA</td>
            <td>100322 - NAIROBI</td>
             <td>200,0000</td>
            <td></td>
        </tr>
        <tr>
            <td>1</td>
            <td>01/03/2017</td>
            <td>JOHN MUTUA</td>
            <td>100322 - NAIROBI</td>
             <td>200,0000</td>
            <td></td>
        </tr>
        <tr>
            <td>1</td>
            <td>01/03/2017</td>
            <td>JOHN MUTUA</td>
            <td>100322 - NAIROBI</td>
             <td>200,0000</td>
            <td></td>
        </tr>
        <tr>
            <td>1</td>
            <td>01/03/2017</td>
            <td>JOHN MUTUA</td>
            <td>100322 - NAIROBI</td>
             <td>200,0000</td>
            <td></td>
        </tr>
        <tr>
            <td>1</td>
            <td>01/03/2017</td>
            <td>JOHN MUTUA</td>
            <td>100322 - NAIROBI</td>
             <td>200,0000</td>
            <td></td>
        </tr>
        <tr>
            <td>1</td>
            <td>01/03/2017</td>
            <td>JOHN MUTUA</td>
            <td>100322 - NAIROBI</td>
             <td>200,0000</td>
            <td></td>
        </tr>
    </table>
</div>

