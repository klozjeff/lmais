<div style="width: 100%">
    <hr>
    <p class="text-center font-weight-bold">PART C: ENCUMBRANCE SECTION</p>
    <hr>
</div>
<div>
    <table style="width:50%" class="table">
        <tr>
            <th>ENTRY NO</th>
            <th>DATE</th>
            <th>NATURE OF ENCUMBRANCE</th>
            <th>FURTHER PARTICULARS</th>
            <th>SIGNATURE OF REGISTRAR</th>
        </tr>
        <tr>
            <td>1</td>
            <td>01/03/2017</td>
            <td>CHARGE</td>
            <td>BANK LOAN</td>
            <td></td>
        </tr>
        <tr>
            <td>1</td>
            <td>01/03/2017</td>
            <td>CHARGE</td>
            <td>BANK LOAN</td>
            <td></td>
        </tr>
        <tr>
            <td>1</td>
            <td>01/03/2017</td>
            <td>CHARGE</td>
            <td>BANK LOAN</td>
            <td></td>
        </tr>
        <tr>
            <td>1</td>
            <td>01/03/2017</td>
            <td>CHARGE</td>
            <td>BANK LOAN</td>
            <td></td>
        </tr>
        <tr>
            <td>1</td>
            <td>01/03/2017</td>
            <td>CHARGE</td>
            <td>BANK LOAN</td>
            <td></td>
        </tr>
        <tr>
            <td>1</td>
            <td>01/03/2017</td>
            <td>CHARGE</td>
            <td>BANK LOAN</td>
            <td></td>
        </tr>
    </table>
</div>
