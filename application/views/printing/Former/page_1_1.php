<div class="centered" style="width: 50%">
    <img height="50" width="40" src="<?php echo $_SERVER["DOCUMENT_ROOT"] . '/lmais/assets/title/coatofarms.png'; ?>">
</div>
<div style="width: 50%">
    <p class="text-center font-weight-bold">REPUBLIC OF KENYA</p>
    <p class="text-center font-weight-bold">_________</p>
    <p class="text-center font-weight-bold">THE REGISTERED LAND ACT</p>
    <p class="text-center font-weight-bold">(Chapter 300)</p>
    <p class="text-center font-weight-bold" style="font-style: italic;">Title Deed</p>
    <div class="text-font-size">
        <p>Title Number <span class="text-bold dotted"> MITI MINGI  / NAIVASHA / BLOCK 12</span><br>
            Approximate Area <span class="text-bold dotted"> .5 HECTARES</span><br>
            Registry Map Sheet No <span class="text-bold  dotted"></span></p>
        <p>This is to certify that <span class="text-bold dotted">JAMES KAMAU<br> ID: 12313123, P.O.Box 312312-20101, NAIROBI</span></p>
        <p class="text-justify">
            is now registered as the absolute proprietor of the land comprised in the above mentioned title, subject to the entries in the register relating to the land and to such of the overriding interests set out in section 30  of the Registered Land Act as may for the time being subsist and affect the land.
        </p>
    </div>
</div>
<div style="width: 50%">
    <table>
        <tr>
            <td style=" width: 40%">
                <div style="border: 1px solid red;" class="pull-left"> 
                    <img height="50" width="50" src="<?php echo $_SERVER["DOCUMENT_ROOT"] . '/lmais/assets/title/octagon.jpg'; ?>">
                </div>
            </td>
            <td style=" width: 60%">
                <div style="border: 1px solid red;" class="pull-left"> 
                    <p class="text-justify">GIVEN under my hand and seal of the
                        <span class="text-bold">NAIROBI</span> District Land Registry
                        this <span class="text-bold">12<super>th</super></span> day of <span class="text-bold">February, 2010</span>
                    </p>
                    <p><i>Land Registrar</i></p>
                </div>
            </td>
        </tr>
    </table>
</div>

