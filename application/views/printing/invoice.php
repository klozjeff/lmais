<!DOCTYPE html>
<html>
 

                               

    <body class="fixed-left" >
        
        <!-- Begin page -->
        <div id="wrapper-">
  

                        <div class="row" >
                            <div class="col-md-12">
                                <div class="panel-default">
                                    <!-- <div class="panel-heading">
                                        <h4>Invoice</h4>
                                    </div> -->
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="text-right">
											 <?php if ($TransactionData) : ?>
											 <?php foreach ($TransactionData as $t => $transaction) : ?>
														 <?php if ($ServiceData) : ?>
											 <?php foreach ($ServiceData as $s => $serviceinfo) : ?>
														
														 
                                              <p class="h3">
											
											  <?php 
											  $Slug = $transaction->ServiceSlug;
											  $sql ="SELECT * FROM lmais_landservicetypes where Slug = '$Slug'";
                                            $query = $this->db->query($sql);
                                            if ($query->num_rows() > 0) {
                                              foreach ($query->result() as $row) {
                                             echo $row->ServiceName;
                                             }
											}
											 ?>
										
											 </p>
                                            </div>
                                            <div class="text-right">
											
											<?php 
										$Paid = 0;
										$TotalInvoice = 0;
										if ($PaymentData) {
                                                foreach ($PaymentData as $r) {
                                                    $Paid = ($r->Amount)+$Paid;
                                                }
                                            }
											
											if ($InvoiceData) {
                                                foreach ($InvoiceData as $i) {
                                                    $TotalInvoice = ($i->Amount)+$TotalInvoice;
                                                }
                                            }
											
											
										
										?>
                                               <p class="h3">INVOICE <?php 
												
												if($Paid == $TotalInvoice) 
												{echo 'PAID';
												}
												else
													echo 'UNPAID';
													?> 
													<br></p>
												
												
                                                    <strong>Date: <?php echo $transaction->DateCreated; ?></strong><br>
													<strong>Invoice: <?php echo $transaction->RowID.'/'.$transaction->ServiceID; ?></strong>
                                                
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="text-left m-t-30">
												<table>
												<tr>
			<td align="left">
			<img alt="" src="https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png" style="height:100px;" /><br/>
			
			</td>
			</tr>
			
			<tr>
			</table><br>
                                                    <address>
                                                      <b>MINISTRY OF LANDS AND PHYSICAL PLANNING</b><br>
                                                     1st Ngong Avenue,<br>
													 P.O. Box 30450-00100,<br>
													 Nairobi,Kenya.<br>
													 Tel: 254-020-2718050<br>
                                                   
                                                      </address>
                                                </div><br><br>
												 <div class="c">
                                                <div class="text-left ">
                                                   <table class="table table-striped" cellpadding="15" >
                                                      <?php if ($UserData) : ?>
														 <?php foreach ($UserData as $i => $customer) : ?>
                                                            <tr>
															<strong><th width = "25%">Customer ID</th></strong>
															
                                                            <td width = "50%"><?php echo $customer->IDNumber; ?></td>
                                                             <td></td>    
															 
                                                        </tr>
														 <tr>
															<strong><th width = "25%">Name</th></strong>
                                                            <td width = "50%"><?php echo $customer->FirstName. ' ' .$customer->MiddleName . ' ' . $customer->LastName; ?></td>
                                                                                                                      
                                                        </tr>
														 <tr>
															<strong><th width = "25%">Email</th></strong>
                                                            <td width = "50%"><?php echo $customer->Email; ?></td>
                                                                                                                      
                                                        </tr>
														 <tr>
															<strong><th width = "25%">Residential Address</th></strong>
                                                            <td width = "50%"></td>
                                                                                                                      
                                                        </tr>
														
														<tr>
															<strong><th width = "25%">Tel</th></strong>
                                                            <td width = "50%"><?php echo $customer->Phone; ?></td>
                                                                                                                      
                                                        </tr>
														 <?php endforeach; ?>
                                                            <?php endif; ?>
														
														</table>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-h-50"></div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
															<th width = "25%">Service Code</th>
                                                            <th width = "50%">Service Description</th>
                                                            <th width = "25%">Amount (KES)</th>
                                                           
                                                        </tr></thead>
                                                        <tbody>
														 <?php if ($InvoiceData) : ?>
														 <?php $bill_ref  = "";?>
														 
														 <?php $t=0; foreach ($InvoiceData as $index => $Result) : $t+=1;?>
													
                                                            <tr>
                                                                <td><?php echo $t;?></td>
                                                                <td><?php $bill_ref =$Result->ServiceID. '_'.$Result->ServiceSlug; 
																echo $Result->Description.' ('. $serviceinfo->LRNumber.')';  ?></td>
                                                                <td><?php echo $Result->Amount; ?></td>
                                                             
                                                            </tr>
                                                            <?php endforeach; ?>
															
															<tr> <td> </td>
															<td>Total </td>
															<td><?php echo $TotalInvoice; ?></td>
															
                                                        </tbody>
                                                    </table>
													<br><br>
													
													<div class="barcodecell " ><barcode code="<?php echo $bill_ref;?>" type="C128B" class="barcode" /></div>
													
													 <br/><br/>
													<table class="table">
													<tr>
                                                              
                                                               <td width = "20%"> Payment Mode :</td>
                                                            <th width = "50%"> <strong>Rerence Number: <?php echo $bill_ref;?></strong></th>
                                                            </tr>
																<br/><br/>
																<tr>
                                                               
                                                               <th width = "20%"> <img alt="" src="https://www.ecitizen.go.ke/assets/img/ecitizen-logo.png" style=";" /><br/></th>
                                                            
                                                            </tr>
															</table>
															 
                                                            <?php endif; ?>
													
                                                </div>
                                            </div>
                                        </div>
                                        
                                       <div class="alert alert-info" role="alert">
  <b>Note:</b> This document is computer generated and therefore not signed. It is valid document issued under the authority of The
Ministry of Land and Physical Planning.
</div>
                                       
                                    </div>
                                </div>

                            </div>

                        </div>




                    </div>
                    <!-- end container -->

                </div>
                <!-- end content -->

														 <?php endforeach; ?>
                                                            <?php endif; ?>
															<?php endforeach; ?>
                                                            <?php endif; ?>


            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->


    
    </body>
</html>