<!DOCTYPE html>
<html>


    <body class="fixed-left" >

        <!-- Begin page -->
        <div id="wrapper">


            <div class="row" >
                <div class="col-md-12">
                    <div class="panel-default">
                        <!-- <div class="panel-heading">
                            <h4>Invoice</h4>
                        </div> -->
                        <table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
                               <thead>
                                <tr>
                                    <td align="center">
                                        <img alt="" src="https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png" style="height:100px;" /><br/>
                                        __________<br/> <br/>
                                        GOVERNMENT LAND ACT<br /> 
                                        __________<br/>

                                        <br/><br/><br/>    
                                        <P CLASS='H3'> RENT CLEARANCE CERTIFICATE </p><br/>

                                        <br/>
                                    </td>
                                </tr>
                            </thead>
                        </table>
                        <div class="panel-body">

                            <br/><br/><br/><br/><br/><br/>     <br/><br/>                   
                            <div class="m-h-50"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="">

                                        <table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
                                               <thead>
                                                <tr>
                                                    <td align="center">
                                                        This is to certify that the Land Rent in respect of plot No. <span CLASS='H4'><?php echo $rent_model->LRNumber; ?></span>

                                                        has been cleared for and upto <span CLASS='H4'>31st December <?php echo (date_create_from_format('Y-m-d H:i:s', $rent_model->DateCreated)->format('Y') - 1); ?></span>.
                                                        <br/>
                                                    </td>
                                                </tr>
                                            </thead>
                                        </table>
                                        <br><br><br><br> <br/><br/><br/><br/><br/><br/>            
                                        <div class="barcodecell" ><barcode code="<?php echo $rent_model->TransactionID; ?>" type="C128B" class="barcode" /></div>

                                        <br/><br/>
                                        <table class="table">
                                            <tr>

                                                <td width = "20%"> POWERED BY :</td>
                                                <th width = "50%"> <strong>Rerence Number:  <?php echo $rent_model->TransactionID; ?></strong></th>
                                            </tr>
                                            <br/><br/>
                                            <tr>

                                                <th width = "20%"> <img alt="" src="https://www.ecitizen.go.ke/assets/img/ecitizen-logo.png" style=";" /><br/></th>

                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>

                            <div class="alert alert-info" role="alert">
                                <b>Note:</b> This document is computer generated and therefore not signed. It is valid document issued under the authority of THE MINISTRY OF LANDS & PHYSICAL PLANNING
                            </div>

                        </div>
                    </div>

                </div>

            </div>




        </div>
        <!-- end container -->

    </div>
    <!-- end content -->




</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


<!-- Right Sidebar -->

<!-- /Right-bar -->

</div>
<!-- END wrapper -->



</body>
</html>