<div id="content" class="content">
<?php if ($Results) : ?>

                                <?php foreach ($Results as $index => $Result) : ?>
								
	<h1 class="page-header"><?php echo $Result->Title; ?> (<?php echo $Result->NatureOfTitle; ?>)</h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-3 -->
		<div class="col-md-3 col-sm-6">
			<div class="widget widget-stats bg-green">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
				<div class="stats-number"><?php echo $Result->DocID; ?></div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 40.5%;"></div>
				</div>
				<div class="stats-title">Property Status: 	Active</div>
				<div class="stats-title">Nature of Title: 	<?php echo $Result->NatureOfTitle; ?></div>
				<div class="stats-title">Property Size	: 	<?php echo $Result->AreaSize; ?> <?php echo $Result->AreaUnit; ?></div>

			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-md-3 col-sm-6">
			<div class="widget widget-stats bg-blue">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-tags fa-fw"></i></div>
				<div class="stats-title">Proprietorship</div>
				<div class="stats-number">Owner Count: <?php echo $this->proprietorModel->ReturnResultsCount($Result->DocID);?></div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 40.5%;"></div>
				</div>
				<div class="stats-desc">Proprietor: <?php $r=$this->proprietorModel->CurrentProprietor($Result->DocID); echo $r;?></div>
				<div class="stats-desc">ID Number : <?php $r=$this->proprietorModel->CurrentProprietorID($Result->DocID); echo $r;?></div>
			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-md-3 col-sm-6">
			<div class="widget widget-stats bg-purple">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-shopping-cart fa-fw"></i></div>
				<div class="stats-title">Encumbrances</div>

				<div class="stats-number">Active Count: <?php echo $this->encumbranceModel->ReturnResultsCount($Result->DocID);?></div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 76.3%;"></div>
				</div>
				<div class="stats-desc">Current Encumbrance: <?php echo $this->encumbranceModel->CurrentEncumbrance($Result->DocID);?></div>
				<div class="stats-desc">Transact Further: </div>
			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-md-3 col-sm-6">
			<div class="widget widget-stats bg-red">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-comments fa-fw"></i></div>
				<div class="stats-title">Inhibitions</div>
				<div class="stats-number">Active Count: </div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 54.9%;"></div>
				</div>
				<div class="stats-desc">Current Inhibition: Court Order</div>
				<div class="stats-desc">Transact Further: No</div>
			</div>
		</div>
		<!-- end col-3 -->
	</div>
	<!-- end row -->
	<div class="row">

		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#overview">Overview</a></li>

			<li><a data-toggle="tab" href="#proprietorship">Proprietorship</a></li>
			<li><a data-toggle="tab" href="#Encumbrance">Encumberances</a></li>
			<li><a data-toggle="tab" href="#inhibitions">Inhibitions</a></li>
			<li><a data-toggle="tab" href="#tasks">Tasks</a></li>
			<li><a data-toggle="tab" href="#files">Files</a></li>
			<li><a data-toggle="tab" href="#comments">Comments</a></li>
			<li><a data-toggle="tab" href="#activities">Activities</a></li>
			<li><a data-toggle="tab" href="#valuations">Valuations</a></li>
			<li><a data-toggle="tab" href="#gis">GIS</a></li>
			<li><a data-toggle="tab" href="#gis">Lease Management</a></li>
		</ul>
	</div>

	<div class="row">
		<!-- begin row -->
		<div class="tab-content">
			<div id="overview" class="tab-pane fade in active">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<div class="invoice-content">
								<div class="table-responsive">
									<table class="table table-invoice">
										<thead>
											<tr>
												<th>Location Details</th>

											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													Province<br />
												</td>
												<td> Riftvalley </td>

											</tr>

											<tr>
												<td>
													County<br />
												</td>
												<td> Kajiado </td>

											</tr>

											<tr>
												<td>
													Registry<br />
												</td>
												<td> <?php echo $Result->Registry; ?> </td>

											</tr>

											<tr>
												<td>
													Registration Section<br />
												</td>
												<td> <?php echo $Result->RegistrationSection; ?> </td>

											</tr>

											<tr>
												<td>
													Parcel Number<br />
												</td>
												<td> <?php echo $Result->ParcelNo; ?> </td>

											</tr>

										</tbody>
									</table>
								</div>


							</div>
						</div>


						<div class="col-md-3">
							<div class="invoice-content">
								<div class="table-responsive">
									<table class="table table-invoice">
										<thead>
											<tr>
												<th>Property Info</th>

											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													Area Size<br />
												</td>
												<td> <?php echo $Result->AreaSize; ?> </td>

											</tr>

											<tr>
												<td>
													Regsheet No.<br />
												</td>
												<td> <?php echo $Result->RegSheet; ?> </td>

											</tr>



											<tr>
												<td>
													Tenure<br />
												</td>
												<td> <?php echo $Result->NatureOfTitle; ?> </td>

											</tr>

											<tr>
												<td>
													Open Date<br />
												</td>
												<td> <?php echo $Result->OpenDate; ?> </td>

											</tr>
											<tr>
												<td>
													Parent Title<br />
												</td>
												<td> <?php echo $Result->History; ?> </td>

											</tr>

										</tbody>
									</table>
								</div>


							</div>
						</div>


						<div class="col-md-5">
			        <div class="panel-group" id="accordion">
						<div class="panel panel-inverse overflow-hidden">
							<div class="panel-heading">
								<h3 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion" href="ui_tabs_accordions.html#collapseOne">
									    <i class="fa fa-plus-circle pull-right"></i> 
										Easements
									</a>
								</h3>
							</div>
							<div id="collapseOne" class="panel-collapse collapse in">
								<div class="panel-body">
									<?php  if ($Result->Easements=="")
									{
										echo "This property has no easements";
									}
else
echo 	$Result->Easements;?>
								</div>
							</div>
						</div>
						<div class="panel panel-inverse overflow-hidden">
							<div class="panel-heading">
								<h3 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									    <i class="fa fa-plus-circle pull-right"></i> 
										Proprietor Overview
									</a>
								</h3>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse">
								<div class="panel-body">
									Proprietor: <?php $r=$this->proprietorModel->CurrentProprietor($Result->DocID); echo $r;?> </br>
									ID Number : <?php $r=$this->proprietorModel->CurrentProprietorID($Result->DocID); echo $r;?>
								</div>
							</div>
						</div>
						<div class="panel panel-inverse overflow-hidden">
							<div class="panel-heading">
								<h3 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									    <i class="fa fa-plus-circle pull-right"></i> 
										Encumberance Overview
									</a>
								</h3>
							</div>
							<div id="collapseThree" class="panel-collapse collapse">
								<div class="panel-body">
								
								Last Encumbrance: <?php echo $this->encumbranceModel->CurrentEncumbrance($Result->DocID);?></br> <hr>
								Particulars: <?php echo $this->encumbranceModel->CurrentFurtherParticulars($Result->DocID);?></br><hr>
								Acive Status: 
								</div>
							</div>
							</div>
						<div class="panel panel-inverse overflow-hidden">
							<div class="panel-heading">
								<h3 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
									    <i class="fa fa-plus-circle pull-right"></i> 
										Inhibition Overview
									</a>
								</h3>
							</div>
							<div id="collapseFour" class="panel-collapse collapse">
								<div class="panel-body">
									Inhibition data not available.
								</div>
							</div>
						</div>
						
					
			    </div>



					</div>
				</div>
			</div>
</div>
<?php if ($propdata=$this->proprietorModel->GetProprietorData($Result->DocID)):?>

			<div id="proprietorship" class="tab-pane fade">
				<div class="container">
					<div class="row">
						<div class="col-md-11">
							<div class="invoice-content">
								<div class="table-responsive">

									<table id="data-table" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>#</th>
												<th>Entry Date</th>
												<th>Registered Proprietor</th>
												<th>Address</th>
												<th>Residence</th>
												<th>ID/Passport</th>
												<th>Telephone</th>
												<th>Email</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
<?php foreach ($propdata as $prop => $propcolumn) : ?>
											<tr class="odd gradeX">
												<td><?php echo $propcolumn->EntryNo; ?></td>
												<td><?php echo $propcolumn->EntryDate; ?></td>
												<td><?php echo $propcolumn->RegisteredProprietor; ?></td>
												<td><?php echo $propcolumn->RegisteredBoxOffice; ?></td>
												<td><?php echo $propcolumn->RegisteredResidence; ?></td>
												<td><?php echo $propcolumn->RegisteredID; ?></td>
												<td><?php echo $propcolumn->RegisteredTel1; ?></td>
												<td><?php echo $propcolumn->RegisteredEmail; ?></td>
												<td>Edit</td>
											</tr>

<?php endforeach; ?>

                            <?php endif; ?>

										</tbody>
									</table>
								</div>


							</div>


						</div>
					</div>









				</div>

			</div>
<?php if ($encumdata=$this->encumbranceModel->GetencumbranceData($Result->DocID)):?>
			
			<div id="Encumbrance" class="tab-pane fade">
				<div class="container">
					<div class="row">
						<div class="col-md-11">
							<div class="invoice-content">
								<div class="table-responsive">

									<table id="data-table" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>#</th>
												<th>Entry Date</th>
												<th>NatureOfEncumbrance</th>
												<th>FurtherParticulars</th>
												<th>DateOfTransaction</th>
												<th>To</th>
												<th>Amount</th>
												
												<th></th>
											</tr>
										</thead>
										<tbody>
<?php foreach ($encumdata as $encum => $encumcolumn) : ?>
											<tr class="odd gradeX">
												<td><?php echo $encumcolumn->EntryNo; ?></td>
												<td><?php echo $encumcolumn->EntryDate; ?></td>
												<td><?php echo $encumcolumn->NatureOfEncumbrance; ?></td>
												<td><?php echo $encumcolumn->FurtherParticulars; ?></td>
												<td><?php echo $encumcolumn->DateOfTransaction; ?></td>
												<td><?php echo $encumcolumn->EncumbranceTo; ?></td>
												<td><?php echo $encumcolumn->EncumbranceAmount; ?></td>
												
												<td>Edit</td>
											</tr>

<?php endforeach; ?>

                            <?php endif; ?>

										</tbody>
									</table>
								</div>


							</div>


						</div>
					</div>









				</div>

			</div>
		</div>
	



  <?php endforeach; ?>

                            <?php endif; ?>

</div>


		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/AutoFill/js/dataTables.autoFill.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/KeyTable/js/dataTables.keyTable.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/RowReorder/js/dataTables.rowReorder.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Select/js/dataTables.select.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/table-manage-combine.demo.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<script>
		$(document).ready(function() {
			App.init();
			TableManageCombine.init();
		});
	</script>

</body>
</html>
				