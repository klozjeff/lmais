<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
<!-- begin #content -->
<div id="content" class="content">
   
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">

                    <h4 class="panel-title">Registration for a Transfer</h4>
                </div>
                <div class="panel-body">
                    <?PHP
                    echo $this->session->flashdata('msg');
                    ?>
                    <?php echo form_open('start/AddParcelValuation', array('data-parsley-validate' => 'true', 'name' => 'form-wizard', 'id' => 'AddParcelValuation')); ?>

                    <div id="wizard">
                        <ol>
                            <li>
                                Search 
                               
                            </li>
                            <li>
                                Seller details
                              
                            </li>
                            <li>
                                Buyer details
                               
                            </li>
                            <li>
                                Advocate details
                              
                            </li>
                            <li>
                                Transfer details
                             
                            </li>
							<li>
                                Consent particulars
                             
                            </li>
							<li>
                                Rate Clearance
                             
                            </li>
                        </ol>
                        <!-- begin wizard step-1 -->
                        <div class="wizard-step-1">
                            <fieldset>
                                <div class="col-md-12">
												<div class="col-md-4">
												
												<div class="panel panel-primary" data-sortable-id="ui-widget-16">
                        <div class="panel-heading">
                           
                            <h4 class="panel-title">Notes </h4>
                        </div>
                        <div class="panel-body bg-blue text-white">
						<li><i class="fa-li fa fa-spinner fa-spin"></i>This is the first step of 7 to complete your transfer. </li><br/>
						<li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the transfer document, buyer and advocate details and rates clearance certificate.</li><br/>
						<li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
						<li><i class="fa-li fa fa-spinner fa-spin"></i>Select the correct act for your registered land.</li><br/>
						
                            
                        </div>
                    </div>
												</div>
								
								<div class="col-md-8">
                                <div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											<h4 class="modal-title">Check Availability</h4>
										</div>
										<div class="modal-body">
											<div id="panel_edit_account" class="tab-pane in active">
									
									<?php echo form_open('users/RegisterUsertoDB' , array('class'=>'margin-bottom-0', 'id'=>'RegisterUser'));?>
											<div class="row">
												<div class="col-md-5 eq-box-md text-center box-vmiddle-wrap bg-primary">
					
								
							</div>
												<div class="col-md-12">
												<div class="col-md-6">
													<div class="form-group">
                                            <label>Registered Act</label>
                                            <select name="Acts" onchange="selectOption()" id="Acts" class="form-control" data-parsley-group="wizard-step-1" required/>
                                            <option></option>
                                            <?php
                                            //$GetRegistries = $this->ParcelsValuationModel->GetRegistries();
											$Acts = $this->actsModel->GetAllActs();
                                            if (is_array($Acts)) {
                                                foreach ($Acts as $Act) {
                                                    
                                                    ?>
                                                    <option value="<?= $Act->RowID; ?>"><?= $Act->ItemName; ?></option>
                                                    <?php
                                                }
                                            } else {
                                               ?>
                                                    <option value="0">No data</option>
                                                    <?php
                                            }
                                            ?>
                                            </select>
                                        </div>
										 </div>
										 
										 <div class="col-md-6">
													<div class="form-group connected-group">
														
														<div class="row">
															<div class="col-md-6">
															<label class="control-label">
															County
														</label>
																<select name="County" id="County" class="form-control" required>
																	<option value=""></option>
																	
																	<?php
                                            //$GetRegistries = $this->ParcelsValuationModel->GetRegistries();
											$Counties = $this->Countymodel->GetAllCounties();
                                            if (is_array($Counties)) {
                                                foreach ($Counties as $County) {
                                                    
                                                    ?>
                                                    <option value="<?= $County->RowID; ?>"><?= $County->CountyName; ?></option>
                                                    <?php
                                                }
                                            } else {
                                               ?>
                                                    <option value="0">No data</option>
                                                    <?php
                                            }
                                            ?>
																	
																</select>
															</div>
															<div class="col-md-6">
															<label class="control-label">
															Registry
														</label>
																<select name="Registries" id="Registry" class="form-control"  required>
																	<option value=""></option>
																	
																<?php
                                            $GetRegistries = $this->ParcelsValuationModel->GetRegistries();
											
                                            if (is_array($GetRegistries)) {
                                                foreach ($GetRegistries as $Registry) {
                                                    
                                                    ?>
                                                    <option value="<?= $Registry->RowID; ?>"><?= $Registry->Name; ?></option>
                                                    <?php
                                                }
                                            } else {
                                               ?>
                                                    <option value="0">No data</option>
                                                    <?php
                                            }
                                            ?>
																</select>
															</div>
															
														</div>
													</div>
													
													
													
													
												</div>
												
										 </div>
													
													
												
												
											</div>
											
											<div class="row">
												<div class="col-md-12">
												<div class="col-md-3">
													<div class="form-group">
                                            <label>Registration Section</label>
                                            <select name="RegistrionSection" onchange="selectOption()" id="RegistrionSection" class="form-control" data-parsley-group="wizard-step-1" required/>
                                            <option></option>
                                            <?php
                                            $GetRegistrationSections = $this->Registrymodel->GetRegistrationSections();
											
                                            if (is_array($GetRegistrationSections)) {
                                                foreach ($GetRegistrationSections as $Section) {
                                                    
                                                    ?>
                                                    <option value="<?= $Section->RowID; ?>"><?= $Section->Name; ?></option>
                                                    <?php
                                                }
                                            } else {
                                               ?>
                                                    <option value="0">No data</option>
                                                    <?php
                                            }
                                            ?>
                                            </select>
                                        </div>
										 </div>
										 
										 <div class="col-md-9">
													<div class="form-group connected-group">
														
														<div class="row">
															<div class="col-md-4">
															<label class="control-label">
															Tenure
														</label>
																<select name="NatureOfTitle" id="NatureOfTitle" class="form-control" required>
																	<option value=""></option>
																	
																	<?php
                                            //$GetRegistries = $this->ParcelsValuationModel->GetRegistries();
											$Acts = $this->actsModel->GetAllActs();
                                            if (is_array($Acts)) {
                                                foreach ($Acts as $Act) {
                                                    
                                                    ?>
                                                    <option value="<?= $Act->RowID; ?>"><?= $Act->ItemName; ?></option>
                                                    <?php
                                                }
                                            } else {
                                               ?>
                                                    <option value="0">No data</option>
                                                    <?php
                                            }
                                            ?>
																	
																</select>
															</div>
															<div class="col-md-4">
															<label class="control-label">
															Parcel Number
														</label>
																<input type="text"  placeholder="Parcel Number" class="form-control" id="ParcelNo" name="ParcelNo" required>
															</div>
															
															<div class="col-md-4">
															<label class="control-label">
															Title Number
														</label>
																<input type="text"  placeholder="Title Number" class="form-control" id="TitleNo" name="TitleNo" required disabled>
															</div>
															
														</div>
													</div>
													
													
												
												
										 </div>
													
													
												
												
											</div>
											</div>
											
											
											<div class="row">
												<div class="col-md-10">
													<p>
														By clicking Check Property, you are agreeing to the Policy and Terms &amp; Conditions.
													</p>
												</div>
												<div class="col-md-">
													<button class="btn btn-sm btn-success" type="submit">
														Check Property <i class="fa fa-arrow-circle-right"></i>
													</button>
													
											
																
												</div>
											</div>
										<?php echo form_close(); ?>
									</div>
										</div>
										
									</div>
									</div>
									</div>
                                <!-- end row -->
                            </fieldset>
					
                        </div>
                        <!-- end wizard step-1 -->
                        <!-- begin wizard step-2 -->
                        <div class="wizard-step-2">
                            <fieldset>

                               
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											<h4 class="modal-title">Add System User</h4>
										</div>
										<div class="modal-body">
											<div id="panel_edit_account" class="tab-pane in active">
									
									<?php echo form_open('users/RegisterUsertoDB' , array('class'=>'margin-bottom-0', 'id'=>'RegisterUser'));?>
											<div class="row">
												
												<div class="col-md-12">
												
										 <div class="col-md-6">
													<div class="form-group connected-group">
														
														<div class="row">
															<div class="col-md-6">
															<label class="control-label">
															County
														</label>
																<select name="County" id="County" class="form-control" required>
																	<option value=""></option>
																	
																	
																</select>
															</div>
															<div class="col-md-6">
															<label class="control-label">
															Registry
														</label>
																<select name="Registries" id="Registry" class="form-control"  required>
																	<option value=""></option>
																	
																<?php
                                            $GetRegistries = $this->ParcelsValuationModel->GetRegistries();
											
                                            if (is_array($GetRegistries)) {
                                                foreach ($GetRegistries as $Registry) {
                                                    
                                                    ?>
                                                    <option value="<?= $Registry->RowID; ?>"><?= $Registry->Name; ?></option>
                                                    <?php
                                                }
                                            } else {
                                               ?>
                                                    <option value="0">No data</option>
                                                    <?php
                                            }
                                            ?>
																</select>
															</div>
															
														</div>
													</div>
													
													
													
													
												</div>
												
										 </div>
													
													
												
												
											</div>
													<div class="row">
														<div class="col-md-4">
															<div class="form-group">
																<label class="control-label">
																	Zip Code
																</label>
																<input class="form-control" placeholder="00100" type="text" name="ZipCode" id="ZipCode">
															</div>
														</div>
														<div class="col-md-8">
															<div class="form-group">
																<label class="control-label">
																	City
																</label>
																<input class="form-control tooltips" placeholder="Nairobi" type="text" data-original-title="We'll display it when you write reviews" data-rel="tooltip"  title="" data-placement="top" name="City" id="City">
															</div>
														</div>
														
														<div class="col-md-10">
																<select name="Department" id="Department" class="form-control" >
																	<option value="">Department</option>
																	
																</select>
															</div>
													</div>
													
													
													
												</div>
											</div>
											
											
										<?php echo form_close(); ?>
									</div>
										
								
                            </fieldset>
                        </div>
	



                        <div class="wizard-step-3">
                            <fieldset>
                                <legend class="pull-left width-full"> Road Proximity</legend>
                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-6 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Type of Road</label>
                                            <select name="TypeOfRoad" id="roadtype" name="bb" class="form-control" data-parsley-group="wizard-step-3" required >
                                                <option></option>
                                                <?php
                                                $RoadTypes = $this->ParcelsValuationModel->GetRoadTypes();
                                                if (is_array($RoadTypes)) {
                                                    foreach ($RoadTypes as $RoadType) {
                                                        //echo $courserow->coursename;
                                                        ?>
                                                        <option value="<?= $RoadType->RowID; ?>"><?= $RoadType->TypeOfRoad; ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    echo 'No Campus.';
                                                }
                                                ?>                                              

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4" >
                                        <div class="form-group">
                                            <label>Name of Road</label>
                                            <select id="roadNames" name="NameOfRoad" class="form-control" data-parsley-group="wizard-step-3" required>
                                                <option></option>
                                                <?php
                                                $RoadNames = $this->ParcelsValuationModel->GetRoadNameMatchRoadType(2);

                                                if (is_array($RoadNames)) {
                                                    foreach ($RoadNames as $RoadName) {
                                                        //echo $courserow->coursename;
                                                        ?>
                                                        <option value="<?= $RoadName->RowID; ?>"><?= $RoadName->NameOfRoad; ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    echo 'No Campus.';
                                                }
                                                ?>


                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Distance from the road (KM)</label>
                                            <input type="text" name="DistanceFromRoad" placeholder="5.2" class="form-control" data-parsley-group="wizard-step-3" data-parsley-type="number" required />
                                        </div>
                                    </div>
                                    <!-- end col-6 -->
                                </div>
                                <!-- end row -->
                            </fieldset>
                        </div>
                        <!-- end wizard step-2 -->
                        <!-- begin wizard step-3 -->
                        <div class="wizard-step-4">
                            <fieldset>
                                <legend class="pull-left width-full">Town Proximity</legend>
                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-6 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Type of Town</label>
                                            <select name="TypeOfTown" id="towntype" class="form-control" data-parsley-group="wizard-step-4" required>
                                                <option></option>
                                                <?php
                                                $TownTypes = $this->ParcelsValuationModel->GetTownTypes();
                                                if (is_array($TownTypes)) {
                                                    foreach ($TownTypes as $TownType) {
                                                        //echo $courserow->coursename;
                                                        ?>
                                                        <option value="<?= $TownType->RowID; ?>"><?= $TownType->TypeOfTown; ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    echo 'No Campus.';
                                                }
                                                ?> 
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Name of Town</label>
                                            <select id="townNames" name="NameOfTown" class="form-control" data-parsley-group="wizard-step-4" required>
                                                <option></option> 
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Distance from the town (KM)</label>
                                            <input type="text" name="DistanceFromTown" placeholder="5.2" class="form-control" data-parsley-group="wizard-step-4" data-parsley-type="number" required />
                                        </div>
                                    </div>
                                    <!-- end col-6 -->
                                </div>
                            </fieldset>
                        </div>
                        <!-- end wizard step-3 -->
                        <!-- begin wizard step-4 -->
                        <div>
                            <div class="jumbotron m-b-0 text-center">
                                <h1>Finish</h1>
                                <p>Review new parcel information </p>
                                <p><button type="submit" class="btn btn-success btn-lg" role="button">Save and Add New Parcel</button></p>

                            </div>
                        </div>
                        <!-- end wizard step-4 -->


                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<!-- end #content -->



<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/parsley/dist/parsley.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-wizards-validation.demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<script></script>
<script>
                                                $("#County").change(function (evt) {
                                                    var ActID = document.getElementById('Acts').value;
													var CountyID = document.getElementById('County').value;
                                                    var select = $("#County");
                                                    var selectData = select.serialize();
                                                    var ajax = $.ajax({
                                                        dataType: "json",
                                                        type: 'POST',
                                                        url: "<?php echo base_url(); ?>Transfer/GetRegistriesMatch/" + ActID +"/"+CountyID,
                                                        data: selectData
                                                    });
                                                    ajax.done(function (response) {
                                                        $("#testurl").html(response);
                                                        var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                        for (var key in response) {
                                                            if (response.hasOwnProperty(key)) {
                                                                //alert(key);
                                                                //alert(response[key].RowID);
                                                                //alert(response[key].NameOfRoad);
                                                                listItems += "<option value='" + response[key].Registres + "'>" + response[key].Registries + "</option>";
                                                            }
                                                        }
                                                        $("#Registry").html(listItems);
                                                    });
                                                    ajax.fail(function () {

                                                    });
                                                });

                                                $("#towntype").change(function (evt) {
                                                    var townTypeID = document.getElementById('towntype').value;
                                                    var select = $("#towntype");
                                                    var selectData = select.serialize();
                                                    var ajax = $.ajax({
                                                        dataType: "json",
                                                        type: 'POST',
                                                        url: "http://localhost/lmais/start/GetTownNamesMatchTownType/" + townTypeID,
                                                        data: selectData
                                                    });
                                                    ajax.done(function (response) {
                                                        var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                        for (var key in response) {
                                                            if (response.hasOwnProperty(key)) {
                                                                //alert(key);
                                                                //alert(response[key].RowID);
                                                                //alert(response[key].NameOfRoad);
                                                                listItems += "<option value='" + response[key].NameOfTown + "'>" + response[key].NameOfTown + "</option>";
                                                            }
                                                        }
                                                        $("#townNames").html(listItems);
                                                    });
                                                    ajax.fail(function () {

                                                    });
                                                });

</script>

<script>
    $(document).ready(function () {
        App.init();
        FormWizardValidation.init();
    });
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>