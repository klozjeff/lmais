
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            
                            <h4 class="panel-title">Edit Parcel</h4>
                        </div>
						
						<div class="panel-body">
					
									
									<?php echo form_open('start/EditParcelValuation' , array('data-parsley-validate'=>'true', 'name'=>'form-wizard', 'class'=>'form-horizontal', 'id'=>'EditParcelValuation'));?>
										<div class="form-group">
											<div class="col-sm-0">
												<input type="hidden" name="RowID" value="" placeholder="Text Field" id="form-field-1" class="form-control" >
											</div>
											<label class="col-sm-2 control-label" for="form-field-7">
												LRNO
											</label>
											<div class="col-sm-3">
												<input type="text" name="LRNO" value="<" placeholder="Text Field" id="form-field-1" class="form-control" >
											</div>
											<label class="col-sm-1 control-label" for="form-field-7">
												Registry
											</label>
											<div class="col-sm-3">
												<input type="text" name="Registry" value="<?php echo $Result -> Registry; ?>" placeholder="Text Field" id="form-field-1" class="form-control" >
											</div>
											<span class="help-inline col-sm-2"> <i class="fa fa-info-circle"></i> Be careful changing this! </span>
										</div>
								
										<div class="form-group">
											<label class="col-sm-2 control-label">
												Area Size
											</label>
											<div class="col-sm-2">
												<input type="text" name="AreaSize" value="<?php echo $Result -> AreaSize; ?>" placeholder="Text Field" id="form-field-9" class="form-control">
											</div>
											<label class="col-sm-1 control-label">
												Area Unit
											</label>
											<div class="col-sm-2">
												<input type="text" name="AreaUnit" value="<?php echo $Result -> AreaUnit; ?>" placeholder="Text Field" id="form-field-10" class="form-control">
											</div>
											<label class="col-sm-1 control-label">
												Tenure
											</label>
											<div class="col-sm-3">
												<input type="text" name="NatureOfTitle" value="<?php echo $Result -> NatureOfTitle; ?>" placeholder="Text Field" id="form-field-11" class="form-control">
											</div>
										</div>
										
									
										
										<div>
									    <div class="jumbotron m-b-0 text-center">
                                            <p><button type="submit" class="btn btn-success btn-lg" role="button">Save Draft</button></p>
											 <p><button type="submit" class="btn btn-success btn-lg" role="button">Complete and Send</button></p>
											
                                        </div>
									</div>
									<?php echo form_close();?>
						
						</div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
		</div>
		<!-- end #content -->
		</div>
		
        <?php endforeach;?>
					
					<?php else: ?>
                        
						
					
					<div class="m-t-5">No results found</div>
					<?php endif; ?>
		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url(); ?>assets/plugins/parsley/dist/parsley.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/form-wizards-validation.demo.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<script>
		$(document).ready(function() {
			App.init();
			FormWizardValidation.init();
		});
	</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-53034621-1', 'auto');
  ga('send', 'pageview');
</script>
</body>
</html>