<!-- begin #sidebar -->
<div id="sidebar" class="sidebar">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <ul class="nav">
            <li class="nav-profile">
                <div class="image">
                    <a href="javascript:;"><img src="<?php echo base_url(); ?>assets/img/user-14.jpg" alt="" /></a>
                </div>
                <div class="info">
                    <?php
                    if ($this->session->userdata('Logged_in') != '') {

                        echo $this->session->userdata('FirstName') . " " . $this->session->userdata('LastName');
                    } else {
                        redirect(base_url() . 'start/start/index');
                    }
                    ?>
                    <small><?php echo $this->session->userdata('UserType'); ?> </small>
                </div>
            </li>
        </ul>
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->
        <ul class="nav">
            <li class="nav-header">Navigation</li>
            <li class="has-sub active">
            <li><a href="<?php echo base_url(); ?>/start/loadindex"><i class="fa fa-laptop"></i> <span>Dashboard</span></a></li>
            </li>
            <li class="has-sub">
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-inbox"></i> 
                    <span>Quick Actions</span>
                </a>
                <ul class="sub-menu">
                    <li><a href="<?php echo base_url(); ?>start/addvaluationdata">New Land Registration</a></li>

                    <li><a href="<?php echo base_url(); ?>start/SearchLR">Trace Process</a></li>

                </ul>
            </li>

            <li class="has-sub">
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-suitcase"></i>
                    <span>Greencards</span> 
                </a>
                <ul class="sub-menu">
                    <li><a href="#">Tasks</a></li>
                    <li><a href="<?php echo base_url(); ?>start/SearchLRd">Manage Property</a></li>


                </ul>
            </li>

            <li class="has-sub">
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-suitcase"></i>
                    <span>Whitecards</span> 
                </a>
                <ul class="sub-menu">
                    <li><a href="#">Tasks</a></li>
                    <li><a href="<?php echo base_url(); ?>start/SearchLRd">Manage Property</a></li>


                </ul>
            </li>






            <li class="has-sub">
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-map-marker"></i>
                    <span>Map</span>
                </a>
                <ul class="sub-menu">

                    <li><a href="">Country Map</a></li>
                </ul>
            </li>

            <li class="has-sub">
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-inbox"></i> 
                    <span>Administration</span>
                </a>
                <ul class="sub-menu">
                    <li><a href="<?php echo base_url(); ?>settings"><i class="fa fa-cog"></i> <span>Settings</span></a></li>
                    <li><a href="<?php echo base_url(); ?>Users"><i class="fa fa-user"></i> <span>Users</span></a></li>
                    <li><a href="<?php echo base_url(); ?>Registry"><i class="fa fa-user"></i> <span>Registries</span></a></li>

                </ul>
            </li>





            <li><a href=""><i class="fa fa-cog"></i> <span>Reports</span></a></li>

            <!-- begin sidebar minify button -->
            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
            <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->

