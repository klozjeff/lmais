<div id="content" class="content">
<?PHP
				echo $this->session->flashdata('msg');
				?>

<?php if ($Results) : ?>

                                <?php foreach ($Results as $index => $Result) : ?>
								
	<h1 class="page-header"><?php echo $Result->Title; ?> (<?php echo $Result->NatureOfTitle; ?>) </h1>
	
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-3 -->
		<div class="col-md-3 col-sm-6">
			<div class="widget widget-stats bg-green">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
				<div class="stats-number"><?php echo $TransactionID; ?></div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 40.5%;"></div>
				</div>
				<div class="stats-title">Nature of Title: 	<?php echo $Result->NatureOfTitle; ?></div>
				<div class="stats-title">Property Size	: 	<?php echo $Result->AreaSize; ?> <?php echo $Result->AreaUnit; ?></div>

			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-md-3 col-sm-6">
			<div class="widget widget-stats bg-blue">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-tags fa-fw"></i></div>
				
				<div class="stats-number"><?php $r=$this->proprietorModel->CurrentProprietor($Result->DocID); echo $r;?></div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 40.5%;"></div>
				</div>
				
				<div class="stats-desc">ID Number : <?php $r=$this->proprietorModel->CurrentProprietorID($Result->DocID); echo $r;?></div>
			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-md-3 col-sm-6">
			<div class="widget widget-stats bg-purple">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-comments fa-fw"></i></div>
				<div class="stats-title">PENDING CONSENT APPLICATIONS</div>

				<div class="stats-number">PENDING: <?php echo $this->encumbranceModel->ReturnResultsCount($Result->DocID);?></div>
				<br>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 76.3%;"></div>
				</div>
				
			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-md-3 col-sm-6">
			<div class="widget widget-stats bg-purple">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-comments fa-fw"></i></div>
				<div class="stats-title">COMPLETED CONSENT APPLICATIONS</div>

				<div class="stats-number">COMPLETED: <?php echo $this->encumbranceModel->ReturnResultsCount($Result->DocID);?></div>
				<br>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 76.3%;"></div>
				</div>
				
			</div>
		</div>
		<!-- end col-3 -->
	</div>
	<!-- end row -->
	<div class="row">

		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#consentdetails">Task Overview</a></li>
			<li><a data-toggle="tab" href="#overview">Property Overview</a></li>
			<li><a data-toggle="tab" href="#ProposedParties">Proposed Parties</a></li>
			<li><a data-toggle="tab" href="#Encumbrance">Comments</a></li>
			<li><a data-toggle="tab" href="#tasks">History</a></li>
			<li><a data-toggle="tab" href="#Invoice">Invoices</a></li>
			<li><a data-toggle="tab" href="#gis">Payments</a></li>
		
		<div class="btn-group pull-right">
       <a href="#modal-dialog" class="btn btn-sm btn-primary" data-toggle="modal">Complete Task</a>
        
      </div>
		</ul>
		 
	</div>

	<div class="row">
		<!-- begin row -->
		<div class="tab-content">
		<div id="consentdetails" class="tab-pane fade in active">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<div class="invoice-content">
							<div class="profile-image">
                            <img src="<?php echo base_url(); ?>assets/img/profile-cover.jpg" />
                            <i class="fa fa-user hide"></i>
                        </div>
								<div class="table-responsive">
									<table class="table table-invoice">
										<?php if ($ApplicantData=$this->ConsentModel->GetSeller($TransactionID)):?>
								<?php foreach ($ApplicantData as $Applicant => $ApplicantColumn) : ?>
										<thead>
											<tr>
												<th>Applicant</th>

											</tr>
										</thead>
										<tbody>
									
											<tr>
												<td>
													Name<br />
												</td>
												<td> <?php echo $ApplicantColumn->FirstName; ?> <?php echo $ApplicantColumn->LastName; ?></td>

											</tr>

											<tr>
												<td>
													ID Number<br />
												</td>
												<td> <?php echo $ApplicantColumn->IDNumber; ?> </td>

											</tr>

											<tr>
												<td>
													Email<br />
												</td>
												<td> <?php echo $ApplicantColumn->EmailAddress; ?> </td>

											</tr>

											<tr>
												<td>
													Telephone<br />
												</td>
												<td> <?php echo $ApplicantColumn->TelephoneNumber; ?> </td>

											</tr>

											
										</tbody>
										<?php endforeach; ?>

                            <?php endif; ?>
									</table>
								</div>


							</div>
						</div>

<div class="col-md-8">
						<div class="col-md-6">
							<div class="invoice-content">
								<div class="table-responsive">
									<table class="table table-invoice">
										<?php if ($ConsentData=$this->ConsentModel->consent_details($TransactionID)):?>
								<?php foreach ($ConsentData as $Consent => $ConsentColumn) : ?>
										<thead>
											<tr>
												<th>Consent Details</th>

											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													Consent Status<br />
												</td>
												<td> <?php echo $ConsentColumn->Status; ?> </td>

											</tr>

											<tr>
												<td>
													Application Date:<br />
												</td>
												<td> <?php echo $ConsentColumn->PaymentDate; ?> </td>

											</tr>



											<tr>
												<td>
													Due Date:<br />
												</td>
												<td> <?php echo $ConsentColumn-> PaymentDate; ?> </td>

											</tr>

											<tr>
												<td>
													Rent Status<br />
												</td>
												<td> 
												<?php $Rent = $this->ConsentModel->GetRent($TransactionID); 
										$rx = 0;
										if (is_array($Rent)) {
                                                foreach ($Rent as $r) {
                                                    $rx = ($r->Amount)+$rx;
                                                }
                                            }
											echo "Ksh ".$rx;
										
										?>
												
												
												</td>

											</tr>
											

										</tbody>
								
									</table>
								</div>


							</div>
						</div>


						<div class="col-md-6">
							<div class="invoice-content">
								<div class="table-responsive">
									<table class="table table-invoice">
										
										<tbody>
											<tr>
												<td>
													Term<br />
												</td>
												<td> <?php echo $ConsentColumn->TransactionTerm; ?> <?php echo $ConsentColumn->TransactionTermPeriod; ?>  </td>

											</tr>

											<tr>
												<td>
													Type of Transaction<br />
												</td>
												<td> <?php echo $ConsentColumn->TypeOfTransaction; ?> </td>

											</tr>



											<tr>
												<td>
													Transaction/Estimated Amount:<br />
												</td>
												<td> <?php echo $ConsentColumn->TransactionAmount; ?> </td>

											</tr>

											
											<tr>
												<td>
													Registry:<br />
												</td>
												<td> <?php echo $ConsentColumn->Registry; ?> </td>

											</tr>
											
											<tr>
												<td>
													Rates Clearance:<br />
												</td>
												<td>  </td>

											</tr>

										</tbody>
									</table>
									
								</div>
	
							</div>
						</div>
				</div>
				
				<div class="col-md-8">
				
                                                                <div class="form-group">
                                                                    <label class="control-label">
                                                                         Description
                                                                    </label>
                                                                    <textarea class="form-control textarea_2" name="Transaction_Description" placeholder="Transaction Description" name="description"><?php ?></textarea>
                                                                </div>

				</div>
				
				</div>
			</div>
</div>


			<div id="overview" class="tab-pane fade">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<div class="invoice-content">
								<div class="table-responsive">
									<table class="table table-invoice">
										<thead>
											<tr>
												<th>Location Details</th>

											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													Province<br />
												</td>
												<td> <?php echo $Result->Province; ?> </td>

											</tr>

											<tr>
												<td>
													County<br />
												</td>
												<td> <?php echo $Result->County; ?></td>

											</tr>

											<tr>
												<td>
													Registry<br />
												</td>
												<td> <?php echo $Result->Registry; ?> </td>

											</tr>

											<tr>
												<td>
													Registration Section<br />
												</td>
												<td> <?php echo $Result->RegistrationSection; ?> </td>

											</tr>

											<tr>
												<td>
													Parcel Number<br />
												</td>
												<td> <?php echo $Result->ParcelNo; ?> </td>

											</tr>

										</tbody>
									</table>
								</div>


							</div>
						</div>


						<div class="col-md-3">
							<div class="invoice-content">
								<div class="table-responsive">
									<table class="table table-invoice">
										<thead>
											<tr>
												<th>Property Info</th>

											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													Area Size<br />
												</td>
												<td> <?php echo $Result->AreaSize; ?> </td>

											</tr>

											<tr>
												<td>
													Regsheet No.<br />
												</td>
												<td> <?php echo $Result->RegSheet; ?> </td>

											</tr>



											<tr>
												<td>
													Tenure<br />
												</td>
												<td> <?php echo $Result->NatureOfTitle; ?> </td>

											</tr>

											<tr>
												<td>
													Open Date<br />
												</td>
												<td> <?php echo $Result->OpenDate; ?> </td>

											</tr>
											<tr>
												<td>
													Parent Title<br />
												</td>
												<td> <?php echo $Result->History; ?> </td>

											</tr>

										</tbody>
									</table>
								</div>


							</div>
						</div>


						<div class="col-md-5">
			        <div class="panel-group" id="accordion">
						<div class="panel panel-inverse overflow-hidden">
							<div class="panel-heading">
								<h3 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion" href="ui_tabs_accordions.html#collapseOne">
									    <i class="fa fa-plus-circle pull-right"></i> 
										Easements
									</a>
								</h3>
							</div>
							<div id="collapseOne" class="panel-collapse collapse in">
								<div class="panel-body">
									<?php  if ($Result->Easements=="")
									{
										echo "This property has no easements";
									}
else
echo 	$Result->Easements;?>
								</div>
							</div>
						</div>
						<div class="panel panel-inverse overflow-hidden">
							<div class="panel-heading">
								<h3 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									    <i class="fa fa-plus-circle pull-right"></i> 
										Proprietor Overview
									</a>
								</h3>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse">
								<div class="panel-body">
									Proprietor: <?php $r=$this->proprietorModel->CurrentProprietor($Result->DocID); echo $r;?> </br>
									ID Number : <?php $r=$this->proprietorModel->CurrentProprietorID($Result->DocID); echo $r;?>
								</div>
							</div>
						</div>
						<div class="panel panel-inverse overflow-hidden">
							<div class="panel-heading">
								<h3 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									    <i class="fa fa-plus-circle pull-right"></i> 
										Encumberance Overview
									</a>
								</h3>
							</div>
							<div id="collapseThree" class="panel-collapse collapse">
								<div class="panel-body">
								
								Last Encumbrance: <?php echo $this->encumbranceModel->CurrentEncumbrance($Result->DocID);?></br> <hr>
								Particulars: <?php echo $this->encumbranceModel->CurrentFurtherParticulars($Result->DocID);?></br><hr>
								Acive Status: 
								</div>
							</div>
							</div>
						<div class="panel panel-inverse overflow-hidden">
							<div class="panel-heading">
								<h3 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
									    <i class="fa fa-plus-circle pull-right"></i> 
										Inhibition Overview
									</a>
								</h3>
							</div>
							<div id="collapseFour" class="panel-collapse collapse">
								<div class="panel-body">
									Inhibition data not available.
								</div>
							</div>
						</div>
						
					
			    </div>



					</div>
				</div>
			</div>
</div>
<?php if ($propdata=$this->proprietorModel->GetProprietorData($Result->DocID)):?>

			<div id="proprietorship" class="tab-pane fade">
				<div class="container">
					<div class="row">
						<div class="col-md-11">
							<div class="invoice-content">
								<div class="table-responsive">

									<table id="data-table" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>#</th>
												<th>Entry Date</th>
												<th>Registered Proprietor</th>
												<th>Address</th>
												<th>Residence</th>
												<th>ID/Passport</th>
												<th>Telephone</th>
												<th>Email</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
<?php foreach ($propdata as $prop => $propcolumn) : ?>
											<tr class="odd gradeX">
												<td><?php echo $propcolumn->EntryNo; ?></td>
												<td><?php echo $propcolumn->EntryDate; ?></td>
												<td><?php echo $propcolumn->RegisteredProprietor; ?></td>
												<td><?php echo $propcolumn->RegisteredBoxOffice; ?></td>
												<td><?php echo $propcolumn->RegisteredResidence; ?></td>
												<td><?php echo $propcolumn->RegisteredID; ?></td>
												<td><?php echo $propcolumn->RegisteredTel1; ?></td>
												<td><?php echo $propcolumn->RegisteredEmail; ?></td>
												<td>Edit</td>
											</tr>

<?php endforeach; ?>

                            <?php endif; ?>

										</tbody>
									</table>
								</div>


							</div>


						</div>
					</div>









				</div>

			</div>

			
			<div id="ProposedParties" class="tab-pane fade">
				<div class="container">
					<div class="row">
						<div class="col-md-11">
							<div class="invoice-content">
								<div class="table-responsive">

									<table id="data-table" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>Name</th>
												<th>Nationality</th>
												<th>Telephone</th>
												<th>Email</th>
												<th>Postal Address</th>
												<th>Residence</th>
												<th>Contact Person?</th>
												
											</tr>
										</thead>
										<tbody>
										<?php if ($ProposedParties=$this->ConsentModel->GetBuyer($TransactionID)):?>
<?php foreach ($ProposedParties as $Party => $Partycolumn) : ?>
											<tr class="odd gradeX">
												<td> <?php echo $Partycolumn->FirstName; ?> <?php echo $Partycolumn->LastName; ?></td>
												<td><?php echo $Partycolumn->Nationality; ?></td>
												<td><?php echo $Partycolumn->TelephoneNumber; ?></td>
												<td><?php echo $Partycolumn->EmailAddress; ?></td>
												<td><?php echo $Partycolumn->BoxOfficeAddress; ?></td>
												<td><?php echo $Partycolumn->PhysicalAddress; ?></td>
												<td><?php echo $Partycolumn->IsContactPerson; ?></td>
												
											</tr>

<?php endforeach; ?>

                            <?php endif; ?>

										</tbody>
									</table>
								</div>


							</div>


						</div>
					</div>









				</div>

			</div>
			
			<div id="Invoice" class="tab-pane fade">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<table id="data-table" class="table table-striped table-bordered nowrap">
                                <thead>
                                    <tr>
                                         <th>#</th>
										 <th>Invoice ID</th>
                                        
										<th>Bill Date</th>
										<th>Due Date</th>
										<th>Invoice Value</th>
										
										<th>Status</th>
										<th> <i class="fa fa-bars"></th>

										
                                    </tr>
                                </thead>
                                <tbody>
								<?php if ($InvoiceData=$this->ConsentModel->GetInvoice($TransactionID)):?>
								<?php foreach ($InvoiceData as $Invoice => $InvoiceColumn) : ?>
								<tr>
								<td>#</td>
										 <td><?php echo $InvoiceColumn->InvoiceID; ?></td>
                                      
                                        <td><?php echo $InvoiceColumn->BillDate; ?></td>
										<td><?php echo $InvoiceColumn->DueDate; ?></td>
										<td><?php echo $InvoiceColumn->InvoiceValue; ?></td>
										<td><?php echo $InvoiceColumn->Status; ?></td>
										
										<td> <i class="fa fa-bars"></td>
                                    </tr>
									
									
									<?php endforeach; ?>

                            <?php endif; ?>
                                </tbody>
                            </table>
							


						</div>
					</div>



				</div>

			</div>
			
		</div>
	
<div class="modal fade" id="modal-dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											<h4 class="modal-title">Task Details</h4>
										</div>
										<div class="modal-body">
											<div id="panel_edit_account" class="tab-pane in active">
									
									<?php echo form_open('stampduty/EditTask' , array('class'=>'margin-bottom-0', 'id'=>'ConsentTask'));?>
											<div class="row">
												<div class="col-md-12">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">
															Applicant
														</label>
														<input type="text" placeholder="" value = "<?php echo $ApplicantColumn->FirstName; ?> <?php echo $ApplicantColumn->MiddleName; ?> <?php echo $ApplicantColumn->LastName; ?>"class="form-control" id="Applicant" name="Applicant" required readonly>
													</div>
													</div>
													
													<div class="col-md-3">
													<div class="form-group">
														<label class="control-label">
															ID/Passport
														</label>
														<input type="text" placeholder="" class="form-control" id="IDNumber" value="<?php echo $ApplicantColumn->IDNumber; ?>" name="IDNumber"  readonly>
													</div>
													</div>
													
													<div class="col-md-3">
													<div class="form-group">
														<label class="control-label">
															Transaction ID
														</label>
														<input type="text" placeholder="" class="form-control" id="TransactionID" value="<?php echo $ApplicantColumn->TransactionID; ?>" name="TransactionID" readonly>
													</div>
													</div>
													</div>
													<div class="col-md-12">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">
															Rent Clearance Status
														</label>
														<select name="RentClearanceStatus" id="RentClearanceStatus" class="form-control" required readonly>
														<option value=""></option>
																	<option value="Cleared">Cleared</option>
																		<option value="Pending">Pending</option>
																		
																	
																</select>
													</div>
													</div>
													
													<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">
															Rates Clearance Status
														</label>
														<select name="RatesClearanceStatus" id="RatesClearanceStatus" class="form-control" required readonly>
														<option value=""></option>
																	<option value="Cleared">Cleared</option>
																		<option value="Pending">Pending</option>
																		
																	
																</select>
													</div>
													</div>
													</div>
													
													<div class="col-md-12">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">
															Type of Transaction
														</label>
														<select name="TypeOfTransaction" id="TypeOfTransaction" class="form-control" required readonly>
														<option value=""></option>
																	<option value="Sale">Sale</option>
																		<option value="Gift">Gift</option>
																		<option value="Spouse">Spouse Transfer</option>
																		
																		
																	
																</select>
													</div>
													</div>
													
													<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">
															Amount Transacted
														</label>
														<input type="text" placeholder="Transaction Amount" value = "<?php echo $ConsentColumn->TransactionAmount; ?>" class="form-control" id="TransactionAmount" name="TransactionAmount" required readonly>
													</div>
													</div>
													</div>
													
													<div class="col-md-12">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">
															Parties' Relationship
														</label>
														<select name="PartiesRelationship" id="PartiesRelationship" class="form-control" required>
														<option value=""></option>
																	<option value="Married">Married</option>
																		<option value="Siblings">Siblings</option>
																		<option value="ParentChild">Parent <> Child</option>
																		<option value="Other">Other</option>
																		
																	
																</select>
													</div>
													</div>
													
													<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">
															Proof Attached?
														</label>
														<select name="RelationshipProof" id="RelationshipProof" class="form-control" required>
														<option value=""></option>
																	<option value="Yes">Yes</option>
																		<option value="No">No</option>
																		
																		
																	
																</select>
													</div>
													</div>
													</div>
													
													
													<div class="col-md-12">
												<div class="col-md-12">
													<div class="form-group">
													<label class="control-label">
															Approval
														</label>
														<select name="ConsentStatus" id="ConsentStatus" class="form-control" required>
														<option value=""></option>
																	<option value="Exempt">Exempt</option>
																		<option value="Approved">Approve for Valuation</option>
																			
																</select>
													</div>
													</div>

													</div>
													
													<div class="col-md-12">
													<div class="col-md-12">
				
                                                                <div class="form-group">
                                                                    <label class="control-label">
                                                                         Description/Notes
                                                                    </label>
                                                                    <textarea class="form-control" name="Notes" placeholder="Notes" name="description"><?php ?></textarea>
                                                                </div>

														</div></div>
													
													
													
													</div>
													
													
													
												</div>
											</div>
											
										
											<div class="row">
											<div class="col-md-12">
												<div class="col-md-8">
													<p>
														By clicking Create user, you are agreeing to the Policy and Terms &amp; Conditions.
													</p>
												</div>
												<div class="col-md-4">
													<button class="btn btn-sm btn-success" type="submit">
														Update Task <i class="fa fa-arrow-circle-right"></i>
													</button>
													
											
																
												</div>
										
											</div>
											</div>
										<?php echo form_close(); ?>
									</div>
										</div>
										
									</div>
								</div>
							</div>
<?php endforeach; ?>

                            <?php endif; ?>	


  <?php endforeach; ?>

                            <?php endif; ?>

</div>


		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/AutoFill/js/dataTables.autoFill.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/KeyTable/js/dataTables.keyTable.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/RowReorder/js/dataTables.rowReorder.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Select/js/dataTables.select.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/table-manage-combine.demo.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<script>
		$(document).ready(function() {
			App.init();
			TableManageCombine.init();
		});
	</script>

</body>
</html>
				