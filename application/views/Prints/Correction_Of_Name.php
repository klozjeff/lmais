 <?php
//============================================================+


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = K_PATH_IMAGES."logo_example.jpg";
        $this->Image($image_file, 10, 10, 15, "", "JPG", "", "T", false, 300, "", false, false, 0, false, false, false);
        // Set font
        $this->SetFont("helvetica", "B", 20);
        // Title
        $this->Cell(0, 15, "<< TCPDF Example 003 >>", 0, false, "C", 0, "", 0, false, "M", "M");
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont("helvetica", "I", 8);
        // Page number
        $this->Cell(0, 10, "Page ".$this->getAliasNumPage()."/".$this->getAliasNbPages(), 0, false, "C", 0, "", 0, false, "T", "M");
    }
}

// create new PDF document
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, "UTF-8", false);
$pageLayout = array("297", "210"); //  or array($height, $width) 
$pdf = new TCPDF("p", "mm", $pageLayout, true, "UTF-8", true);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor("Nicola Asuni");
$pdf->SetTitle("Title Document");
$pdf->SetSubject("Title Number");
$pdf->SetKeywords("No keywords provided");

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, "", PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, "", PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__)."/lang/eng.php")) {
    require_once(dirname(__FILE__)."/lang/eng.php");
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont("times", "B", 11);

// add a page
$pdf->AddPage();
$pdf->SetMargins(20.6, 0, 20.6, true);
$left_column = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>
		<tr>
			<td align="center">
			<img alt="" src="https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png" style="height:100px;" /><br/>
			__________<br/>
			THE LAND REGISTRATION ACT<br />
			(No. 3 of 2012, Section 108)<br/>
			
			<br/>
			<strong> APPLICATION TO CORRECT NAME IN THE REGISTER </strong><br/>
			<strong> Nairobi/Block 136/7110 </strong>
			<br/>
			</td>
			</tr>
			
			<tr>
		<td align="left">
			I/We, <strong> {Anne Apis Adhiambo} (Name in the Register) </strong>  <br/>
			HEREBY apply to the Land Regisrar, {Registry Name} District, to correct my name in the register of the piece of land comprised in the above-mentioned titleto read {New Name}
			(Name in the Identity Card).
			
			<br/>
My Identity Card Number is {ID Number From Ecitizen}
			
			<br/>
<br/>
			Signature by the applicant
			<br/>
			in the presence of:
			<br/>
			......................................................................<br/>
			
			
			</td>
	</tr>
	</thead>
</table>';

$Signature = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>


<td align="right">

		.............................................<br/>
			             Land Registrar
		</td>
</tr>
	</thead>
</table>';

$Signature2 = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>


<td align="right">

			
		.............................................<br/>
			             Signature or Mark of the Applicant
		</td>
</tr>
	</thead>
</table>';

$html3 = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>

	<tr>
	<td align="LEFT">

<strong> Land Registrar, </strong>
<br/>

<br/>
<strong> {Name Of Registry}</strong> Registry.

<br/>

			<strong> I CERTIFY </strong> that <strong> Anne Apis Adhiambo </strong>  <br/>appeared before me on the ____ day of ____, 20 ____, 
			and being known to me/being identified by <strong> Input the person\'s name </strong>
			of <strong> P.O. Box 13 Mombasa </strong> acknowledge the above signature or marks ro be his [theirs] and that he [they] had freely and voluntarily executed this instrument 
			and understood its contents.
			<br/><br/>
	
			REGISTERED this ........................ day of .................., 20 .................
			<br/>
			</td>
	</tr>
	</thead>
</table>';

$BottomNote = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>

	<tr>
	<td align="LEFT">
<br/><br/>

<br/><br/>
			<strong> Note: </strong> The person attesting the signature must authenticate the coloured passport size photograph, National ID Number and Tax PIN Number".<br/><br/>
			</td>
	</tr>
	</thead>
</table>';


// writeHTMLCell($w, $h, $x, $y, $html="", $border=0, $ln=0, $fill=0, $reseth=true, $align="", $autopadding=true)


// get current vertical position

$y = $pdf->getY();


$pdf->SetFillColor(255, 255, 255);
$pdf->SetFont("times", "", 11);

// set color for text
$pdf->SetTextColor(0, 0, 0);

$pdf->SetFont("times", "", 13);

// write the first column

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

// QRCODE,L : QR-CODE Low error correction
//$pdf->write2DBarcode('Title No. 50600 LR 13/MN/IV Lesee: Not Provided of Box, Nairobi. Area Size: 2.9640 Ha Term: 99 Years From: 01/03/2019' , 'QRCODE,H', 90, 220, 50, 50, $style, 'N');
//$pdf->Image(base_url().'assets/img/sec.jpg', -30, 260, 0, 0, 'JPG', '', '', false, 300, '', false, false, 0, "", false, false);
$pdf->SetFillColor(255, 255, 255);

$pdf->SetTextColor(0, 0, 0);
$pdf->setCellHeightRatio(1.6);
$pdf->writeHTMLCell("", "", "", $y, $left_column, 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", "", $Signature2, 0, 1, 1, true, "J", true);

$pdf->writeHTMLCell("", "", "", "", $html3, 0, 1, 1, true, "J", true);

$pdf->writeHTMLCell("", "", "", "", $Signature, 0, 1, 1, true, "J", true);
$pdf->setXY(20,280);
$pdf->SetFont("times", "BI", 9);
// CODE 39 EXTENDED + CHECKSUM
$pdf->Cell(0, 0, '074001726000003006652985', 0, 1);
$pdf->write1DBarcode("074001726000003006652985", 'C39', '', '', 90, 10, 0.4, '', 'N');
$pdf->lastPage();

ob_clean();
$pdf->Output("Caution Document.pdf", "I");
end_ob_clean();
//============================================================+
// END OF FILE
//============================================================+
