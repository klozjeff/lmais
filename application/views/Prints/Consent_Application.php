 <?php
//============================================================+


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = K_PATH_IMAGES."logo_example.jpg";
        $this->Image($image_file, 10, 10, 15, "", "JPG", "", "T", false, 300, "", false, false, 0, false, false, false);
        // Set font
        $this->SetFont("helvetica", "B", 20);
        // Title
        $this->Cell(0, 15, "<< TCPDF Example 003 >>", 0, false, "C", 0, "", 0, false, "M", "M");
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont("helvetica", "I", 8);
        // Page number
        $this->Cell(0, 10, "Page ".$this->getAliasNumPage()."/".$this->getAliasNbPages(), 0, false, "C", 0, "", 0, false, "T", "M");
    }
}

// create new PDF document
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, "UTF-8", false);
$pageLayout = array("297", "210"); //  or array($height, $width) 
$pdf = new TCPDF("p", "mm", $pageLayout, true, "UTF-8", true);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor("Kenya Land Management System");
$pdf->SetTitle("Consent Document");
$pdf->SetSubject("Consent Document");
$pdf->SetKeywords("No keywords provided");

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, "", PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, "", PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__)."/lang/eng.php")) {
    require_once(dirname(__FILE__)."/lang/eng.php");
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

if ($PropertyData=$this->PropertyModel->Property_details(6484)):

  foreach ($PropertyData as $Data => $PropertyColumn) :
  

// set font
$pdf->SetFont("times", "B", 11);

// add a page
//https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png
$pdf->AddPage();
$pdf->SetMargins(20.6, 20.6, 20.6, true);
$left_column = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>
		<tr>
			<td align="center">
			<img alt="" src="https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png" style="height:100px;" /><br/>
			__________<br/>
			THE LAND CONTROL ACT<br/>
			(CAP. 302) <br/>
			__________<br/>
			<strong> APPLCATION FOR CONSENT OF LAND CONTROL BOARD </strong><br/>
			To be submitted in TRIPLICATE in respect of each transaction and sent to left at the appropriate office of the Commissioner of Lands<br />
			
			To: THE <strong>'.$PropertyColumn->Title.'</strong><br/>
			LAND CONTROL BOARD.
			</td>
		
	</tr>
	</thead>
</table>';


$PropertySection = '
<table cellspacing="0" cellpadding="1" border="1">
    <tr nobr="true" style="background-color:#FFFFD0;color:#0F00FF;">
  <th colspan="2"><strong> PART A: PROPERTY SECTION </strong> </th>
 </tr>
 
 <tr nobr="true">
  <td width="60%">Registration Section<strong> '.$PropertyColumn->RegistrationSection.'</strong></td>
  <td width="40%"> Parcel Number:  <strong>'.$PropertyColumn->ParcelNo.'</strong></td>
 </tr>
	
    <tr nobr="true">
  <td>Area Size: <strong>' .$PropertyColumn->AreaSize. ' '. $PropertyColumn->AreaUnit.' </strong></td>
  <td>Tenure: <strong>'.$PropertyColumn->NatureOfTitle.'</strong></td>
 </tr>
 </table>
';

$Separator = '<br/>';




// get current vertical position

$y = $pdf->getY();


$pdf->SetFillColor(255, 255, 255);
$pdf->SetFont("times", "", 11);

// set color for text
$pdf->SetTextColor(0, 0, 0);

$pdf->SetFont("times", "", 13);

// write the first column

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

// QRCODE,L : QR-CODE Low error correction
//$pdf->write2DBarcode('Title No. 50600 LR 13/MN/IV Lesee: Not Provided of Box, Nairobi. Area Size: 2.9640 Ha Term: 99 Years From: 01/03/2019' , 'QRCODE,H', 90, 220, 50, 50, $style, 'N');
//$pdf->Image(base_url().'assets/img/sec.jpg', -30, 260, 0, 0, 'JPG', '', '', false, 300, '', false, false, 0, "", false, false);
$pdf->SetFillColor(255, 255, 255);

$pdf->SetTextColor(0, 0, 0);
$pdf->setCellHeightRatio(1.4);

//Header
$pdf->writeHTMLCell("", "", "", $y, "REF: AOS-11111111", 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", $y, $left_column, 0, 1, 1, true, "J", true);

//Property Section
$pdf->setCellHeightRatio(1.6);
$pdf->SetFont("times", "", 11);
$pdf->writeHTMLCell("", "", "", "", $PropertySection, 0, 1, 1, true, "J", true);
if ($PropertyColumn->NatureOfTitle =='Leasehold'):
	
	$Tenure = '
<table cellspacing="0" cellpadding="1" border="1">
	<tr nobr="true">
  <td width = "60%">Lease Term: <strong> 99 years </strong> Rent : <strong> 1300.00 (REV) </strong></td>
  <td width = "40%">Lease From: <strong>12-12-2014</strong></td>
 </tr>
 </table>
';
$pdf->writeHTMLCell("", "", "", "", $Tenure, 0, 1, 1, true, "J", true);
endif;
$pdf->writeHTMLCell("", "", "", "", $Separator, 0, 1, 1, true, "J", true);

//Proprietor SECTION

$ProprietorSection = '
<table cellspacing="0" cellpadding="1" border="1">
    <tr nobr="true" style="background-color:#FFFFD0;color:#0F00FF;">
  <th colspan="3"><strong> PART A: PROPRIETORSHIP SECTION </strong> </th>
 </tr>
 
 <tr nobr="true">
  <td width="15%">Entry #</td>
  <td width="15%">Entry Date</td>
  <td width="70%">Particulars</td>
 </tr>

  
</table>
';
$pdf->writeHTMLCell("", "", "", "", $ProprietorSection, 0, 1, 1, true, "J", true);


if ($ProprietorData=$this->proprietorModel->GetProprietorData(6484)):

  foreach ($ProprietorData as $Proprietor): 

  $ProprietorEntry = '
<table cellspacing="0" cellpadding="1" border="1">
    
 <tr nobr="true">
  <td width="15%">'.$Proprietor->EntryNo.'</td>
  <td width="15%">'.$Proprietor->EntryDate.'</td>
  <td width="70%">Name: '.$Proprietor->RegisteredProprietor.'<br />ID : '. $Proprietor->RegisteredID .'<br />Address : '. $Proprietor->RegisteredAddress .'<br />Entry Remarks : '. $Proprietor->RemarksAndConsideration.'</td>
 </tr>
	
  
</table>
';

  $pdf->writeHTMLCell("", "", "", "", $ProprietorEntry, 0, 1, 1, true, "J", true);

 endforeach;
 endif;
 
 
 
$pdf->writeHTMLCell("", "", "", "", $Separator, 0, 1, 1, true, "J", true);


//EncumbranceSection

$EncumbranceSection = '
<table cellspacing="0" cellpadding="1" border="1">
    <tr nobr="true" style="background-color:#FFFFD0;color:#0F00FF;">
  <th colspan="3"><strong> PART A: ENCUMBRANCES SECTION </strong> </th>
 </tr>
 
 <tr nobr="true">
  <td width="15%">Entry #</td>
  <td width="15%">Entry Date</td>
  <td width="70%">Particulars</td>
 </tr>

  
</table>
';
$pdf->writeHTMLCell("", "", "", "", $EncumbranceSection, 0, 1, 1, true, "J", true);


if ($EncumbranceData=$this->encumbranceModel->GetEncumbranceData(6484)):

  foreach ($EncumbranceData as $Encumbrance): 

  $EncumbranceEntry = '
<table cellspacing="0" cellpadding="1" border="1">
    
 <tr nobr="true">
  <td width="15%">'.$Encumbrance->EntryNo.'</td>
  <td width="15%">'.$Encumbrance->EntryDate.'</td>
  <td width="70%">Nature: '.$Encumbrance->NatureOfEncumbrance.'<br />Further Particulars : '. $Encumbrance->FurtherParticulars .'</td>
 </tr>
	
  
</table>
';
  $pdf->writeHTMLCell("", "", "", "", $EncumbranceEntry, 0, 1, 1, true, "J", true);

 endforeach;
 endif;
 
 
 
$pdf->writeHTMLCell("", "", "", "", '<BR/> <BR/>', 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", "", 'NOTE: THIS SEARCH IS ONLINE GENERATED AND IS VALID; TO AVOID FORGERY

PLEASE ENSURE THAT YOU DO THE SEARCH YOURSELF OR IT IS DONE IN YOUR

PRESENCE.', 0, 1, 1, true, "J", true);

$pdf->write1DBarcode($PropertyColumn->Title, 'C39', '', '', 90, 10, 0.4, '', 'N');
$pdf->lastPage();
endforeach;
endif;
ob_clean();

$pdf->Output(FCPATH.'/Temp/'.$TransferColumn->TransactionID .'-ConsentTransfer.pdf', 'FD');
force_download(FCPATH.'/Temp/'.$TransferColumn->TransactionID .'-ConsentTransfer.pdf', NULL);
unlink(FCPATH.'/Temp/'.$TransferColumn->TransactionID .'-ConsentTransfer.pdf'); 
end_ob_clean();
//============================================================+
// END OF FILE
//============================================================+
