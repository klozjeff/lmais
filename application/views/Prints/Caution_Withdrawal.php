 <?php
//============================================================+


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = K_PATH_IMAGES."logo_example.jpg";
        $this->Image($image_file, 10, 10, 15, "", "JPG", "", "T", false, 300, "", false, false, 0, false, false, false);
        // Set font
        $this->SetFont("helvetica", "B", 20);
        // Title
        $this->Cell(0, 15, "<< TCPDF Example 003 >>", 0, false, "C", 0, "", 0, false, "M", "M");
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont("helvetica", "I", 8);
        // Page number
        $this->Cell(0, 10, "Page ".$this->getAliasNumPage()."/".$this->getAliasNbPages(), 0, false, "C", 0, "", 0, false, "T", "M");
    }
}

// create new PDF document
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, "UTF-8", false);
$pageLayout = array("297", "210"); //  or array($height, $width) 
$pdf = new TCPDF("p", "mm", $pageLayout, true, "UTF-8", true);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor("Nicola Asuni");
$pdf->SetTitle("Title Document");
$pdf->SetSubject("Title Number");
$pdf->SetKeywords("No keywords provided");

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, "", PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, "", PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__)."/lang/eng.php")) {
    require_once(dirname(__FILE__)."/lang/eng.php");
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont("times", "B", 11);

// add a page
$pdf->AddPage();
$pdf->SetMargins(20.6, 0, 20.6, true);
$left_column = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>
		<tr>
			<td align="center">
			<img alt="" src="https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png" style="height:100px;" /><br/>
			__________<br/>
			THE REGISTERED LAND ACT<br />
			
			<br/>
			__________<br/>
			<strong> APPLICATION TO WITHDRAW A CAUTION </strong><br/><br/>
			<br/>
			<strong> Nairobi/Block 136/7110 </strong>
			<br/>
			</td>
			</tr>
			
			<tr>
		<td align="left">
			I, <strong> Anne Apis Adhiambo </strong> of <strong> P.O. Box 136, Kisumu</strong> 
			hereby apply to withdraw the Caution lodged by me/us and registered against the land comprised in the above mentioned title.
			
			<br/><br/>
			
			Signed by the cautioner in  presence of :-
			<br/>
			...........................................   	...........................................           
			<br/>
			<br/>
			
			I certify that <strong> Anne Apis Adhiambo </strong>  <br/>appeared before me on the ____ day of ____, 20 ____, 
			and being known to me/being identified by <strong> Input the person\'s name </strong>
			of <strong> P.O. Box 13 Mombasa </strong> acknowledge the above signature or marks ro be his [theirs] and that he [they] had freely and voluntarily executed this instrument 
			and understood its contents.
			<br/>
			
			
			<br/><br/>
			....................................<br/>
			Signature and Designation of <br/>Person Certifying
			<br/><br/><br/>
REGISTERED this ........................ day of .................., 20 .................
			
			</td>
	</tr>
	</thead>
</table>';

$Signature = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>


<td align="right">
			<br/><br/>
		.............................................<br/>
			             Land Registrar
			</td>
</tr>
	</thead>
</table>';
$html3 = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>

	<tr>
	<td align="LEFT">
<strong>SUBJECT </strong> however to the revisable annual rent of shillings <strong> Thirteen Thousand Shillings Only (Ksh 13,000.00/-)</strong> and to the Act(s) special conditions,
 Encumbrances and other matters specified in the memorandum hereunder written.

 <BR/>
 <strong>IN WITNESS </strong> whereof I have hereunto set my hand and seal this  <strong> 21st </strong> day of <strong> March </strong> Two Thousand and <strong> nineteen </strong>. 
	</td>
	</tr>
	</thead>
</table>';

// writeHTMLCell($w, $h, $x, $y, $html="", $border=0, $ln=0, $fill=0, $reseth=true, $align="", $autopadding=true)


// get current vertical position

$y = $pdf->getY();


$pdf->SetFillColor(255, 255, 255);
$pdf->SetFont("times", "", 11);

// set color for text
$pdf->SetTextColor(0, 0, 0);

$pdf->SetFont("times", "", 13);

// write the first column

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);
$pdf->setXY(20,280);
$pdf->write1DBarcode("074001726000003006652985", 'C39', '', '', 90, 10, 0.4, '', 'N');
// QRCODE,L : QR-CODE Low error correction
//$pdf->write2DBarcode('Title No. 50600 LR 13/MN/IV Lesee: Not Provided of Box, Nairobi. Area Size: 2.9640 Ha Term: 99 Years From: 01/03/2019' , 'QRCODE,H', 90, 220, 50, 50, $style, 'N');
//$pdf->Image(base_url().'assets/img/sec.jpg', -30, 260, 0, 0, 'JPG', '', '', false, 300, '', false, false, 0, "", false, false);
$pdf->SetFillColor(255, 255, 255);

$pdf->SetTextColor(0, 0, 0);
$pdf->setCellHeightRatio(1.6);
$pdf->writeHTMLCell("", "", "", $y, $left_column, 0, 1, 1, true, "J", true);
$pdf->setCellHeightRatio(1.2);
$pdf->writeHTMLCell("", "", 100, "", $Signature, 0, 1, 1, true, "J", true);
$pdf->lastPage();

ob_clean();
$pdf->Output("Caution Document.pdf", "I");
end_ob_clean();
//============================================================+
// END OF FILE
//============================================================+
