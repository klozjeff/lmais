 <?php
//============================================================+


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = K_PATH_IMAGES."logo_example.jpg";
        $this->Image($image_file, 10, 10, 15, "", "JPG", "", "T", false, 300, "", false, false, 0, false, false, false);
        // Set font
        $this->SetFont("helvetica", "B", 20);
        // Title
        $this->Cell(0, 15, "<< TCPDF Example 003 >>", 0, false, "C", 0, "", 0, false, "M", "M");
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont("helvetica", "I", 8);
        // Page number
        $this->Cell(0, 10, "Page ".$this->getAliasNumPage()."/".$this->getAliasNbPages(), 0, false, "C", 0, "", 0, false, "T", "M");
    }
}

// create new PDF document
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, "UTF-8", false);
$pageLayout = array("297", "420"); //  or array($height, $width) 
$pdf = new TCPDF("l", "mm", $pageLayout, true, "UTF-8", true);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor("Nicola Asuni");
$pdf->SetTitle("Title Document");
$pdf->SetSubject("Title Number");
$pdf->SetKeywords("No keywords provided");

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, "", PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, "", PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__)."/lang/eng.php")) {
    require_once(dirname(__FILE__)."/lang/eng.php");
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont("times", "B", 13);

// add a page
$pdf->AddPage();
$left_column = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>
		<tr>
			<td align="center"><img alt="" src="https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png" style="height:100px;" />
			<h4><strong></strong></h4>

			
			<strong>REPUBLIC OF KENYA</strong>

			<p style="margin-bottom:0px;">__________</p>
			THE REGISTERED LAND ACT<br />
			(Chapter 300)
			</td>
	
	</tr>
	</thead>
</table>';

$html = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>
		<tr>
			<td align="center"><img alt="" src="https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png" style="height:100px;" />
			<h4><strong></strong></h4>

			
			<strong>REPUBLIC OF KENYA</strong>

			<p style="margin-bottom:0px;">__________</p>
			THE LAND ACT<br />
			(No. 6 of 2012)<br />
			THE REGISTRATION OF TITLES ACT (CAP. 281) (REPEALED)<br />
			THE GOVERNMENT LANDS ACT (CAP. 280) (REPEALED)<br />
			THE LAND TITLES ACT (CAP 282) (REPEALED)<br />
					
			<BR/>
<strong>CERTIFICATE OF TITLE</strong>	</td>
	</tr>
	
	</thead>
</table>';



$html2 = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>

	<tr>
	<td align="LEFT">

Title No. <strong>CR. 50600</strong> Term: <strong>99 Years</strong>  
From: <strong>01/03/1998</strong>  
Anual Rent Kenya Shillings: <strong>Thirteen Thousand Shillings Only (Ksh 13,000.00/-) (Revisable)</strong><BR/>

I hereby Certify that<BR/>
1. ...........................................................<BR/>
2. ...........................................................<BR/>
3. ...........................................................<BR/>
in the Republic of Kenya, pursuant to .............................................is/are now registered proprietors as lesee(s) from The Government of KENYA
for the term of : <strong> Ninety nine </strong> Years from the <strong> 1st </strong> day of <strong> March </strong> Two Thousand and <strong> nineteen </strong>.

<br/>
ALL That Piece of land situated in the <strong> Mombasa Island in Mombasa County </strong> <br/>
containing by measurement nought decimal five nine seven nought (0.05970) Hactares (less road reserve of ........................ Ha/Ac) or thereabouts
and being Land Reference Number:<strong> LR 13/MN/IV </strong> (Original Number ........................) as delineated on Land Survey Plan Number <strong> 47665</strong>
 annexed <strong> to the said Transfer </strong> <br/>
 
 <strong>SUBJECT </strong> however to the revisable annual rent of shillings <strong> Thirteen Thousand Shillings Only (Ksh 13,000.00/-)</strong> and to the Act(s) special conditions,
 Encumbrances and other matters specified in the memorandum hereunder written.

 <BR/>
 
 

	</td>
	</tr>
	</thead>
</table>';
$html3 = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>

	<tr>
	<td align="LEFT">
<strong>SUBJECT </strong> however to the revisable annual rent of shillings <strong> Thirteen Thousand Shillings Only (Ksh 13,000.00/-)</strong> and to the Act(s) special conditions,
 Encumbrances and other matters specified in the memorandum hereunder written.

 <BR/>
 <strong>IN WITNESS </strong> whereof I have hereunto set my hand and seal this  <strong> 21st </strong> day of <strong> March </strong> Two Thousand and <strong> nineteen </strong>. 
 
 

	</td>
	</tr>
	</thead>
</table>';

// writeHTMLCell($w, $h, $x, $y, $html="", $border=0, $ln=0, $fill=0, $reseth=true, $align="", $autopadding=true)


// get current vertical position

$y = $pdf->getY();


$pdf->SetFillColor(255, 255, 255);
$pdf->SetFont("times", "", 11);

// set color for text
$pdf->SetTextColor(0, 0, 0);
$pdf->StartTransform();

$pdf->Rotate(90);
// Output html.
$pdf->writeHTMLCell("", "", -10, 15, "CR. 50600", $border=0, $ln=1, $fill=1, $reseth=true, $align='J', $autopadding=true);

$pdf->Rotate(0);
$pdf->writeHTMLCell("", "", -130, 15, "13  09 03 2016 074001726000003006652985", $border=0, $ln=1, $fill=1, $reseth=true, $align='J', $autopadding=true);

$pdf->Rotate(0);
$pdf->writeHTMLCell("", "", -170, 15, "47665", $border=0, $ln=1, $fill=1, $reseth=true, $align='J', $autopadding=true);

$pdf->Rotate(-90);
$pdf->writeHTMLCell("", "", 20, -5, "&lt;&lt;FOR OFFICIAL USE ONLY&gt;&gt; 50600 13 074001726000003006652985 0", $border=0, $ln=1, $fill=1, $reseth=true, $align='J', $autopadding=true);
$pdf->StopTransform();


$pdf->SetFont("times", "", 13);

// write the first column
$pdf->writeHTMLCell(200, '', 10, $y, $left_column, 0, 0, 1, true, "J", true);
$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);
$pdf->setXY(80,200);
$pdf->write1DBarcode("074001726000003006652985", 'C39', '', '', 90, 10, 0.4, '', 'N');
// QRCODE,L : QR-CODE Low error correction
$pdf->write2DBarcode('Title No. 50600 LR 13/MN/IV Lesee: Not Provided of Box, Nairobi. Area Size: 2.9640 Ha Term: 99 Years From: 01/03/2019' , 'QRCODE,H', 90, 220, 50, 50, $style, 'N');
$pdf->Image(base_url().'assets/img/sec.jpg', -30, 260, 0, 0, 'JPG', '', '', false, 300, '', false, false, 0, "", false, false);
$pdf->SetFillColor(255, 255, 255);

// set color for text
$pdf->SetTextColor(0, 0, 0);

// write the second column
$pdf->writeHTMLCell(190, "", 220, 20, $html, 0, 1, 1, true, "J", true);

$pdf->SetFont("times", "", 13);
$pdf->setCellHeightRatio(1.8);
$pdf->writeHTMLCell(190, "", "220", 130, $html2, 0, 1, 1, true, "J", true);


$pdf->AddPage();
$pdf->SetTextColor(0, 0, 0);

$pdf->SetTextColor(0, 0, 0);
$pdf->Image(base_url().'assets/img/sec.jpg', -30, 260, 0, 0, 'JPG', '', '', false, 300, '', false, false, 0, "", false, false);

$pdf->StartTransform();

$pdf->Rotate(90);
// Output html.
$pdf->writeHTMLCell("", "", -10, 15, "CR. 50600", $border=0, $ln=1, $fill=1, $reseth=true, $align='J', $autopadding=true);

$pdf->Rotate(0);
$pdf->writeHTMLCell("", "", -130, 15, "13  09 03 2016 074001726000003006652985", $border=0, $ln=1, $fill=1, $reseth=true, $align='J', $autopadding=true);

$pdf->Rotate(0);
$pdf->writeHTMLCell("", "", -170, 15, "47665", $border=0, $ln=1, $fill=1, $reseth=true, $align='J', $autopadding=true);

$pdf->Rotate(-90);
$pdf->writeHTMLCell("", "", 5, 285, "&lt;&lt;FOR OFFICIAL USE ONLY&gt;&gt; 50600 13 074001726000003006652985 0", $border=0, $ln=1, $fill=1, $reseth=true, $align='J', $autopadding=true);

$pdf->Rotate(90);
$pdf->writeHTMLCell("", "", 5, 285, "&lt;&lt;FOR OFFICIAL USE ONLY&gt;&gt; 50600 13 074001726000003006652985 0", $border=0, $ln=1, $fill=1, $reseth=true, $align='J', $autopadding=true);

$pdf->StopTransform();
$pdf->writeHTMLCell(190, "", 20, $y, $html3, 0, 1, 1, true, "J", true);

// reset pointer to the last page
$pdf->lastPage();

//Close and output PDF document
ob_clean();
$pdf->Output("Title Document.pdf", "I");
end_ob_clean();
//============================================================+
// END OF FILE
//============================================================+
