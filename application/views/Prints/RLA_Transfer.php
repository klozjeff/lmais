 <?php

class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = K_PATH_IMAGES."logo_example.jpg";
        $this->Image($image_file, 10, 10, 15, "", "JPG", "", "T", false, 300, "", false, false, 0, false, false, false);
        // Set font
        $this->SetFont("helvetica", "B", 20);
        // Title
        $this->Cell(0, 15, "<< TCPDF Example 003 >>", 0, false, "C", 0, "", 0, false, "M", "M");
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont("helvetica", "I", 8);
        // Page number
        $this->Cell(0, 10, "Page ".$this->getAliasNumPage()."/".$this->getAliasNbPages(), 0, false, "C", 0, "", 0, false, "T", "M");
    }
}


// create new PDF document
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, "UTF-8", false);
$pageLayout = array("297", "210"); //  or array($height, $width) 
$pdf = new TCPDF("p", "mm", $pageLayout, true, "UTF-8", true);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor("Nicola Asuni");
$pdf->SetTitle("Title Document");
$pdf->SetSubject("Title Number");
$pdf->SetKeywords("No keywords provided");

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, "", PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, "", PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__)."/lang/eng.php")) {
    require_once(dirname(__FILE__)."/lang/eng.php");
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
if ($Transfer_Data=$this->ConsentModel->consent_details($transfer_id)):

  foreach ($Transfer_Data as $Data => $TransferColumn) :
  if ($SellerData=$this->SellerModel->GetSellers($transfer_id)):
$Sellers;
$Address;
$PinNumbers1;
$Identification1;
  foreach ($SellerData as $single_seller): 
$Address .= 'P.O. Box '.$single_seller->Address. ", ";
$PinNumbers1 .= $single_seller->PinNumber. ", ";
$Identification1 = $single_seller->Identification. ", ";

$Sellers .= $single_seller->FirstName." " .$single_seller->MiddleName." " .$single_seller->LastName. ", ";
 endforeach;
 $Sellers=rtrim($Sellers, ',');
 $CI =& get_instance();
$Amount_in_words = $CI->convertNumberToWord($TransferColumn->TransactionAmount);



if ($BuyerData=$this->BuyerModel->GetBuyers($transfer_id)):
$Buyers;
$Address;
$PinNumbers;
$Identification;

  foreach ($BuyerData as $single_Buyer): 
$Address .= 'P.O. Box '.$single_Buyer->Address. ", ";
$Address .= $single_Buyer->PinNumber. ", ";
$Identification = $single_Buyer->Identification. ", ";
$Buyers .= $single_Buyer->FirstName." " .$single_Buyer->MiddleName." " .$single_Buyer->LastName. ", ";
 endforeach;
 $Buyers=rtrim($Buyers, ',');
 $TempDate = strtotime($TransferColumn->DateCreated);
 $Application_Date = date('l jS M Y', $TempDate);
 
// set font
$pdf->SetFont("times", "B", 11);
//
// add a page
$pdf->AddPage();
$pdf->SetMargins(20.6, 0, 20.6, true);
$left_column = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>
		<tr>
			<td align="center">
			<img alt="" src="https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png" style="height:100px;" /><br/>
			__________<br/>
			THE LAND REGISTRATION ACT<br />
			(No. 3 of 2012, Section 108)<br/>
			(Cap. 300, Repealed)
			
			<br/>
			<strong> TRANSFER OF LAND </strong><br/>
			<strong> '. $TransferColumn->LRNumber .'</strong>
			<br/>
			</td>
			</tr>
			
			<tr>
		<td align="left">
			I/We, <strong>'. $Sellers .' </strong>  
			in consideration of <strong> Kenya Shillings '.$Amount_in_words.'  (Kshs. '.$TransferColumn->TransactionAmount .') </strong>
			
			(the receipt whereof is whereby acknowledged)
			<br/>
			<strong>HEREBY TRANSFER </strong> to 
			
			
			<strong> '. $Buyers .'</strong>  the land comprises in the above mentioned title.
			
			<br/><br/>
			The Transferees declare that they hold the land as joint proprietor/as proprietors in common * in the following undivided shares:-
			<br/><br/>
			
			Dated this <strong> '.$Application_Date.'</strong>
			<br/><br/>
			Signed by the Transferor
			<br/> Transferor(s) : '
			. $Sellers .'<br/> PIN NO(s):' . $PinNumbers1 .'<br/> ID NO(s): ' . $Identification1 .'
			<br/><br/>
			...........................................   	         (Attach Photo here - Attach another page if reguired)
			<br/>
			In the presence of: 
			<br/>
			
			
			...........................................   	                
			<br/>
			
			
			I certify that <strong> '. $Sellers .' </strong>  <br/>appeared before me on the ____ day of ____, 20 ____, 
			Acknowledge the above signature or marks ro be his/her [theirs] and that he/she [they] had freely and voluntarily executed this instrument 
			and understood its contents.
			<br/>
			
			
			<br/>
			..............................................................<br/>
			Signature and Designation of <br/>Person Certifying
			<br/><br/>

			
			</td>
	</tr>
	</thead>
</table>';



$Signature = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>


<td align="right">

			<br/><br/>
			
		.............................................<br/>
			             Land Registrar
						 
			</td>
</tr>
	</thead>
</table>';

$ApplicationID = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>


<td align="right">

			<br/><br/>
			
		<strong> RL1-'.$TransferColumn->TransactionID .'
						 
			</strong></td>
</tr>
	</thead>
</table>';
$html3 = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>

	<tr>
	<td align="LEFT">
<br/><br/>
Signed by the transferee
			<br/><br/>
			In the presence of:-'
			. $Buyers .'<br/> PIN NO(s):' . $PinNumbers .'<br/> ID NO(s): ' . $Identification .'
			<br/><br/>
			<br/>
			
			<br/>
			
			
			.................................................................   	         (Attach Photo here)           
			<br/><br/>
			
			<strong> I CERTIFY </strong> that <strong> '. $Buyers .' </strong>  <br/>appeared before me on the ____ day of ____, 20 ____, 
			 acknowledge the above signature or marks ro be his [theirs] and that he [they] had freely and voluntarily executed this instrument 
			and understood its contents.
			<br/><br/>
			
			..............................................................<br/>
			'.$TransferColumn->AdvocateFirstName." " .$TransferColumn->AdvocateFirstName." " .$TransferColumn->AdvocateFirstName. 
  '
			 <br/> Signature and Designation of <br/>Person Certifying
			
			<br/><br/><br/><br/>
			REGISTERED this ........................ day of .................., 20 .................
			<br/><br/>
			</td>
	</tr>
	</thead>
</table>';

$BottomNote = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>

	<tr>
	<td align="LEFT">
<br/><br/>

<br/><br/>
			<strong> Note: </strong> The person attesting the signature must authenticate the coloured passport size photograph, National ID Number and Tax PIN Number".<br/><br/>
			</td>
	</tr>
	</thead>
</table>';


// writeHTMLCell($w, $h, $x, $y, $html="", $border=0, $ln=0, $fill=0, $reseth=true, $align="", $autopadding=true)


// get current vertical position

$y = $pdf->getY();


$pdf->SetFillColor(255, 255, 255);
$pdf->SetFont("times", "", 11);

// set color for text
$pdf->SetTextColor(0, 0, 0);

$pdf->SetFont("times", "", 12);

// write the first column

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

// QRCODE,L : QR-CODE Low error correction
//$pdf->write2DBarcode('Title No. 50600 LR 13/MN/IV Lesee: Not Provided of Box, Nairobi. Area Size: 2.9640 Ha Term: 99 Years From: 01/03/2019' , 'QRCODE,H', 90, 220, 50, 50, $style, 'N');
//$pdf->Image(base_url().'assets/img/sec.jpg', -30, 260, 0, 0, 'JPG', '', '', false, 300, '', false, false, 0, "", false, false);
$pdf->SetFillColor(255, 255, 255);

$pdf->SetTextColor(0, 0, 0);
$pdf->setCellHeightRatio(1.6);
$pdf->writeHTMLCell("", "", "", $y, $ApplicationID, 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", $y, $left_column, 0, 1, 1, true, "J", true);



$pdf->AddPage();
$pdf->setCellHeightRatio(1.6);
$pdf->writeHTMLCell("", "", "", $y, $html3, 0, 1, 1, true, "J", true);

$pdf->writeHTMLCell("", "", 100, "", $Signature, 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", "", $BottomNote, 0, 1, 1, true, "J", true);
$pdf->setXY(20,280);
$pdf->write1DBarcode($TransferColumn->TransactionID , 'C39', '', '', 90, 10, 0.4, '', 'N');
$QRContent = 'RL1-'.$TransferColumn->TransactionID.'-Date:  '.$Application_Date.'-Buyers:  '.$Buyers;
$pdf->write2DBarcode($QRContent, 'QRCODE,H', 170, 270, 50, 50, $style, 'N');
$pdf->lastPage();

ob_clean();

$pdf->Output(FCPATH.'/Temp/'.$TransferColumn->TransactionID .'-Transfer.pdf', 'FD');
force_download(FCPATH.'/Temp/'.$TransferColumn->TransactionID .'-Transfer.pdf', NULL);
unlink(FCPATH.'/Temp/'.$TransferColumn->TransactionID .'-Transfer.pdf'); 
end_ob_clean();
endif;
endif;
endforeach;
endif;

//============================================================+
// END OF FILE
//============================================================+
