 <?php
//============================================================+


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = K_PATH_IMAGES."logo_example.jpg";
        $this->Image($image_file, 10, 10, 15, "", "JPG", "", "T", false, 300, "", false, false, 0, false, false, false);
        // Set font
        $this->SetFont("helvetica", "B", 20);
        // Title
        $this->Cell(0, 15, "<< TCPDF Example 003 >>", 0, false, "C", 0, "", 0, false, "M", "M");
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont("helvetica", "I", 8);
        // Page number
        $this->Cell(0, 10, "Page ".$this->getAliasNumPage()."/".$this->getAliasNbPages(), 0, false, "C", 0, "", 0, false, "T", "M");
    }
}

// create new PDF document
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, "UTF-8", false);
$pageLayout = array("297", "210"); //  or array($height, $width) 
$pdf = new TCPDF("p", "mm", $pageLayout, true, "UTF-8", true);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor("Nicola Asuni");
$pdf->SetTitle("Title Document");
$pdf->SetSubject("Title Number");
$pdf->SetKeywords("No keywords provided");

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, "", PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, "", PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__)."/lang/eng.php")) {
    require_once(dirname(__FILE__)."/lang/eng.php");
    $pdf->setLanguageArray($l);
}

if ($Transfer_Data=$this->CautionModel->caution_details($transfer_id)):

  foreach ($Transfer_Data as $Data => $TransferColumn) :
  $TempDate = strtotime($TransferColumn->DateCreated);
 $Application_Date = date('l jS M Y', $TempDate);
  if ($SellerData=$this->SellerModel->GetSellers($transfer_id)):
$Sellers;
$Address;
  foreach ($SellerData as $single_seller): 
$Address .= 'P.O. Box '.$single_seller->Address. ", ";
$Sellers .= $single_seller->FirstName." " .$single_seller->MiddleName." " .$single_seller->LastName. ", ";
 endforeach;
 $Sellers=rtrim($Sellers, ',');
  
// ---------------------------------------------------------

// set font
$pdf->SetFont("times", "B", 11);

// add a page
$pdf->AddPage();
$pdf->SetMargins(20.6, 20.6, 20.6, false);
$left_column = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>
		<tr>
			<td align="center">
			<img alt="" src="https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png" style="height:100px;" /><br/>
			__________<br/>
			THE REGISTERED LAND ACT<br />
			(No. 3 of 2012, Section 108) <br/>
			(Chapter 300)
			<br/>
			<strong> CAUTION </strong><br/><br/>
			<strong> '. $TransferColumn->LRNumber .'</strong>
			</td>
			</tr>
			
			<tr>
		<td align="left">
			I/We, <strong>'. $Sellers .' </strong>   claim an interest as <strong> '. $TransferColumn->CautionInterest .' </strong><br />
			(Caution Descritpion : '. $TransferColumn->CautionDescription .') <br/>
			in the above-mentioned title and forbid the registration of dealings and the making of entries in the register relating to the 
			title <strong> '. $TransferColumn->LRNumber .'</strong> without my consent until this caution has been withdrawn by me or removed by theorder of the Court
			or the Registrar.
			
			<br/><br/>
			Dated this <strong> '.$Application_Date.'</strong>
			<br/><br/>
			Signed in the presence of :-
			<br/><br/><br/>
			...........................................   	...........................................           
			<br/>
			<br/>
			I HEREBY CERTIFY that there appeared before me on the ____ day of ____, 20 ____, the above named <strong> '. $Sellers .' </strong> 
			and, being known to me/being identified by * ....................................................
			acknowledge the above signature or marks ro be his [theirs] and that he [they] had freely and voluntarily executed this document 
			and understood its contents.
			<br/>
			*Delete whichever is not applicable.
			<br/><br/>
			....................................<br/>
						<strong> '. $TransferColumn->advoFName . ' '.$TransferColumn->advoMName .' '.$TransferColumn->advoLName .'</strong>
<br/>
			Signature and Designation of Person Certifying<br/>
			<br/><br/><br/>
REGISTERED this ........................ day of .................., 20 .................
			
			</td>
	</tr>
	</thead>
</table>';

$Signature = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>


<td align="right">
			<br/><br/>
		.............................................<br/>
			             Land Registrar
			</td>
</tr>
	</thead>
</table>';
$html3 = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>

	<tr>
	<td align="LEFT">
<strong>SUBJECT </strong> however to the revisable annual rent of shillings <strong> Thirteen Thousand Shillings Only (Ksh 13,000.00/-)</strong> and to the Act(s) special conditions,
 Encumbrances and other matters specified in the memorandum hereunder written.

 <BR/>
 <strong>IN WITNESS </strong> whereof I have hereunto set my hand and seal this  <strong> 21st </strong> day of <strong> March </strong> Two Thousand and <strong> nineteen </strong>. 
	</td>
	</tr>
	</thead>
</table>';

// writeHTMLCell($w, $h, $x, $y, $html="", $border=0, $ln=0, $fill=0, $reseth=true, $align="", $autopadding=true)


// get current vertical position

$y = $pdf->getY();


$pdf->SetFillColor(255, 255, 255);
$pdf->SetFont("times", "", 11);

// set color for text
$pdf->SetTextColor(0, 0, 0);

$pdf->SetFont("times", "", 13);

// write the first column

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);
// QRCODE,L : QR-CODE Low error correction
//$pdf->write2DBarcode('Title No. 50600 LR 13/MN/IV Lesee: Not Provided of Box, Nairobi. Area Size: 2.9640 Ha Term: 99 Years From: 01/03/2019' , 'QRCODE,H', 90, 220, 50, 50, $style, 'N');
//$pdf->Image(base_url().'assets/img/sec.jpg', -30, 260, 0, 0, 'JPG', '', '', false, 300, '', false, false, 0, "", false, false);
$pdf->SetFillColor(255, 255, 255);

$pdf->SetTextColor(0, 0, 0);
$pdf->setCellHeightRatio(1.6);
$pdf->writeHTMLCell("", "", "", $y, $TransferColumn->TransactionID.'/'.$TransferColumn->RowID, 0, 1, 1, true, "J", true);

$pdf->writeHTMLCell("", "", "", $y, $left_column, 0, 1, 1, true, "J", true);
$pdf->setCellHeightRatio(1.2);
$pdf->writeHTMLCell("", "", 100, "", $Signature, 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", "", "<br/> <br />", 0, 1, 1, true, "J", true);

$pdf->writeHTMLCell("", "", "", "", '<BR/> <BR/>', 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", "", 'I/we '.$Sellers.' (in support of this caution) do hereby sincerely declare as follows:-<br/><br/>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
I make this declaration conscientiously believing the same to be true and accordinf to the Oaths and Statutory Declarations Act.<br/><br/>

<br/>Declared before me at <br/><br/>
.............................................. <br/> <br/>
on the ......................day <br/> <br/>
of ...................20................
<br/><br/><br/>
Signature of Land Registrar', 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", "", '<BR/> <BR/>', 0, 1, 1, true, "J", true);

$pdf->write1DBarcode($TransferColumn->TransactionID, 'C39', '', '', 90, 10, 0.4, '', 'N');
$pdf->lastPage();

ob_clean();

$pdf->Output(FCPATH.'/Temp/'.$TransferColumn->TransactionID .'-Caution.pdf', 'FD');
force_download(FCPATH.'/Temp/'.$TransferColumn->TransactionID .'-Caution.pdf', NULL);
unlink(FCPATH.'/Temp/'.$TransferColumn->TransactionID .'-Caution.pdf'); 
end_ob_clean();
//============================================================+
// END OF FILE
//============================================================+
endif;
endforeach;
endif;
