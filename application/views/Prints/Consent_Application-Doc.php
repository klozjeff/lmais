 <?php

class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = K_PATH_IMAGES."logo_example.jpg";
        $this->Image($image_file, 10, 10, 15, "", "JPG", "", "T", false, 300, "", false, false, 0, false, false, false);
        // Set font
        $this->SetFont("helvetica", "B", 20);
        // Title
        $this->Cell(0, 15, "<< TCPDF Example 003 >>", 0, false, "C", 0, "", 0, false, "M", "M");
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont("helvetica", "I", 8);
        // Page number
        $this->Cell(0, 10, "Page ".$this->getAliasNumPage()."/".$this->getAliasNbPages(), 0, false, "C", 0, "", 0, false, "T", "M");
    }
}


// create new PDF document
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, "UTF-8", false);
$pageLayout = array("297", "210"); //  or array($height, $width) 
$pdf = new TCPDF("p", "mm", $pageLayout, true, "UTF-8", true);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor("Nicola Asuni");
$pdf->SetTitle("Title Document");
$pdf->SetSubject("Title Number");
$pdf->SetKeywords("No keywords provided");

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, "", PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, "", PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__)."/lang/eng.php")) {
    require_once(dirname(__FILE__)."/lang/eng.php");
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
if ($Transfer_Data=$this->ConsentModel->consent_details($transfer_id)):

  foreach ($Transfer_Data as $Data => $TransferColumn) :
 
$Sellers;
$Address;
  
 $CI =& get_instance();
$Amount_in_words = $CI->convertNumberToWord($TransferColumn->TransactionAmount);

 $TempDate = strtotime($TransferColumn->DateCreated);
 $Application_Date = date('l jS M Y', $TempDate);
 
// set font
$pdf->SetFont("times", "B", 11);
//https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png
// add a page
$pdf->AddPage();
$pdf->SetMargins(20.6, 20.6, 20.6, false);
$left_column = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>
		<tr>
			<td align="center">
			<img alt="" src="https://lands.ecitizen.go.ke/assets_unified/images/Coat_of_Arms.png" style="height:100px;" /><br/>
			__________<br/>
			THE LAND CONTROL ACT<br/>
			(CAP. 302) <br/>
			__________<br/>
			<strong> APPLCATION FOR CONSENT OF LAND CONTROL BOARD </strong><br/><br/>
			To be submitted in TRIPLICATE in respect of each transaction and sent to left at the appropriate office of the Commissioner of Lands
			<br/>
			</td> 
			</tr>
			
			<tr>
		<td align="left">
		
		
			
			To: THE <strong>'.$TransferColumn->Registry.'</strong><br/>
			    LAND CONTROL BOARD.<br/>
			
			</td>
	</tr>
	</thead>
</table>';

$PropertySection = '
<table cellspacing="0" cellpadding="1" border="1">
    <tr nobr="true" style="background-color:#FFFFD0;color:#0F00FF;">
  <th colspan="2"><strong> PART A: PROPERTY DETAILS </strong> </th>
 </tr>
 
 <tr nobr="true">
  <td width="50%">Registry<strong> '.$TransferColumn->RegistryID.'</strong></td>
  <td width="50%"> Registration Section:  <strong>'.$TransferColumn->RegistrationSectionID.'</strong></td>
 </tr>
    <tr nobr="true">
  <td>Area Size: <strong>' .$TransferColumn->AreaSize. ' '. $TransferColumn->AreaUnit.' </strong></td>
  <td>Tenure: <strong>'.$TransferColumn->NatureOfTitle.'</strong></td>
 </tr>
 <tr nobr="true">
  <td width = "50%">Status of Land: <strong> Developed </strong></td>
  <td width = "50%">Nature of Development: <strong> Blank </strong></td>
 </tr>
 <tr nobr="true">
  <td width = "50%">Interest Passing: <strong> Leasehold </strong></td>
  <td width = "50%">Whole of Part of Interest: <strong>All</strong></td>
 </tr>
 
 <tr nobr="true">
  <td width = "50%">Date of Transfer: <strong> '.$Application_Date.' </strong></td>
  <td width = "50%">Value Submitted: <strong>Ksh. '.$TransferColumn->TransactionAmount . '</strong></td>
 </tr>
 </table>
';




$Signature = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>


<td align="right">

			<br/><br/>
			
		.............................................<br/>
			             Land Registrar
						 
			</td>
</tr>
	</thead>
</table>';

$ApplicationID = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>


<td align="right">

			<br/><br/>
			
		<strong> CST-'.$TransferColumn->TransactionID .'
						 
			</strong></td>
</tr>
	</thead>
</table>';
$html3 = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>

	<tr>
	<td align="LEFT">
<br/><br/>
Signed by the transferee
			<br/><br/>
			In the presence of:-
			<br/>
			
			<br/>
			
			
			.................................................................   	         (Attach Photo here)           
			<br/><br/>
			
			<strong> I CERTIFY </strong> that <strong> '. $Buyers .' </strong>  <br/>appeared before me on the ____ day of ____, 20 ____, 
			 acknowledge the above signature or marks ro be his [theirs] and that he [they] had freely and voluntarily executed this instrument 
			and understood its contents.
			<br/><br/>
			
			..............................................................<br/>
			Signature and Designation of <br/>Person Certifying
			
			<br/><br/><br/><br/>
			REGISTERED this ........................ day of .................., 20 .................
			<br/><br/>
			</td>
	</tr>
	</thead>
</table>';

$BottomNote = '<table border-spacing="0" cellpadding="0" cellspacing="0" style="margin-bottom: 10px; width:100%; font-family: "Roboto", sans-serif; text-align:center;">
	<thead>

	<tr>
	<td align="LEFT">
<br/><br/>

<br/><br/>
			<strong> Note: </strong> The person attesting the signature must authenticate the coloured passport size photograph, National ID Number and Tax PIN Number".<br/><br/>
			</td>
	</tr>
	</thead>
</table>';


// writeHTMLCell($w, $h, $x, $y, $html="", $border=0, $ln=0, $fill=0, $reseth=true, $align="", $autopadding=true)


// get current vertical position

$y = $pdf->getY();


$pdf->SetFillColor(255, 255, 255);
$pdf->SetFont("times", "B", 10);

// set color for text
$pdf->SetTextColor(0, 0, 0);

$pdf->SetFont("times", "B", 10);

// write the first column

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

// QRCODE,L : QR-CODE Low error correction
//$pdf->write2DBarcode('Title No. 50600 LR 13/MN/IV Lesee: Not Provided of Box, Nairobi. Area Size: 2.9640 Ha Term: 99 Years From: 01/03/2019' , 'QRCODE,H', 90, 220, 50, 50, $style, 'N');
//$pdf->Image(base_url().'assets/img/sec.jpg', -30, 260, 0, 0, 'JPG', '', '', false, 300, '', false, false, 0, "", false, false);
$pdf->SetFillColor(255, 255, 255);

$pdf->SetTextColor(0, 0, 0);
$pdf->setCellHeightRatio(1.6);
$pdf->writeHTMLCell("", "", "", $y, $ApplicationID, 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", $y, $left_column, 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", "", $PropertySection, 0, 1, 1, true, "J", true);

$pdf->writeHTMLCell("", "", "", "", $Separator, 0, 1, 1, true, "J", true);

//Sellers section



$pdf->setCellHeightRatio(1.4);

 if ($SellerData=$this->SellerModel->GetSellers($transfer_id)):

$ProprietorSection = '
<table cellspacing="0" cellpadding="1" border="1">
    <tr nobr="true" style="background-color:#FFFFD0;color:#0F00FF;">
  <th colspan="3"><strong> PART B: SELLERS SECTION </strong> </th>
 </tr>
 
</table>
';
$pdf->writeHTMLCell("", "", "", "", $ProprietorSection, 0, 1, 1, true, "J", true);
 $index = 1;
                                                                                        $total_rent = 0;

foreach ($SellerData as $single_seller): 
$Address .= 'P.O. Box '.$single_seller->Address. ", ";


  $EachSeller = '
<table cellspacing="0" cellpadding="1" border="1">
    
 <tr nobr="true">
  <td width="6%">'.$index.'</td>
  <td width="22%"> ID: '.$single_seller->Identification.'</td>
  <td width="72%">Name: '.$single_seller->FirstName." " .$single_seller->MiddleName." " .$single_seller->LastName. 
  ' <br /> Tel :'.$single_seller->MobileNumber.
  ' <br /> Box :'.$single_seller->Address.
  ' <br /> Town :'.$single_seller->Town.
  ' <br /> Box :'.$single_seller->EmailAddress.
    ' <br /><br /> Signature :'.

'</td>

 </tr>
	
  
</table>
';

  $pdf->writeHTMLCell("", "", "", "", $EachSeller, 0, 1, 1, true, "J", true);
                                                                                            $index++;

 endforeach;
 
 endif;

$pdf->writeHTMLCell("", "", "", "", $Separator, 0, 1, 1, true, "J", true);

//Buyers Section
 
  if ($BuyerData=$this->BuyerModel->GetBuyers($transfer_id)):
$ProprietorSection = '
<table cellspacing="0" cellpadding="1" border="1">
    <tr nobr="true" style="background-color:#FFFFD0;color:#0F00FF;">
  <th colspan="3"><strong> PART C: BUYERS SECTION </strong> </th>
 </tr>
 
</table>
';
$pdf->writeHTMLCell("", "", "", "", $ProprietorSection, 0, 1, 1, true, "J", true);
 $index = 1;
                                                                                        $total_rent = 0;

  foreach ($BuyerData as $single_Buyer): 
  $Address .= 'P.O. Box '.$single_Buyer->Address. ", ";

  $EachSeller = '
<table cellspacing="0" cellpadding="1" border="1">
    
 <tr nobr="true">
  <td width="6%">'.$index.'</td>
  <td width="22%"> ID: '.$single_Buyer->Identification.'</td>
  <td width="72%">Name: '.$single_Buyer->FirstName." " .$single_Buyer->MiddleName." " .$single_Buyer->LastName. 
  ' <br /> Tel :'.$single_Buyer->MobileNumber.
  ' <br /> Box :'.$single_Buyer->Address.
  ' <br /> Town :'.$single_Buyer->Town.
  ' <br /> Box :'.$single_Buyer->EmailAddress.
  ' <br /><br /> Signature :'.
'</td>

 </tr>
	
  
</table>
';

  $pdf->writeHTMLCell("", "", "", "", $EachSeller, 0, 1, 1, true, "J", true);
                                                                                            $index++;

 endforeach;
 
 endif;
$pdf->writeHTMLCell("", "", "", "", $Separator, 0, 1, 1, true, "J", true);


//advocates
$AdvovateSection = '
<table cellspacing="0" cellpadding="1" border="1">
    <tr nobr="true" style="background-color:#FFFFD0;color:#0F00FF;">
  <th colspan="2"><strong> PART D: ADVOCATE SECTION </strong> </th>
 </tr>
  <tr>
  
  <td width="28%"> ID: '.$TransferColumn->AdvocateIDNumber.'</td>
  <td width="72%">Name: '.$TransferColumn->FirstName." " .$TransferColumn->MiddleName." " .$TransferColumn->LastName. 
  ' <br /> Tel :'.$TransferColumn->AdvocatePhone.
  ' <br /> PIN :'.$TransferColumn->AdvocatePFNumber.
  ' <br /> Box :'.$TransferColumn->AdvocatePostalAddress.
  ' <br /> Email :'.$TransferColumn->AdvocateEmail.
  ' <br /> Law Firm :'.$TransferColumn->AdvocateLawFirm.
  ' <br /><br /><br /> Signature and Official Stamp:'.

'</td>
  </tr> 
</table>
';



$pdf->writeHTMLCell("", "", "", "", $AdvovateSection, 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", "", $Separator, 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", "", $Separator, 0, 1, 1, true, "J", true);


$pdf->writeHTMLCell("", "", "", "", 'FOR OFFICIAL USE ONLY <BR/><BR/><BR/>
REMARKS OR SPECIAL CONDITIONS<BR/>
a. <BR/>
b. <BR/>
c. <BR/>
d. <BR/>
e. <BR/><BR/>

..................................................<BR/><BR/>

Chairman <BR/><BR/>
.................................... Land Control Board

', 0, 1, 1, true, "J", true);

$pdf->writeHTMLCell("", "", "", "", $Separator, 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", "", $Separator, 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", "", $Separator, 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", "", $Separator, 0, 1, 1, true, "J", true);

$pdf->writeHTMLCell("", "", "", "", 'NOTE: THIS CONSENT IS ONLINE GENERATED AND IS VALID; TO AVOID FORGERY

PLEASE ENSURE THAT YOU DO THE SEARCH YOURSELF OR IT IS DONE IN YOUR PRESENCE.', 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", "", $Separator, 0, 1, 1, true, "J", true);
$pdf->writeHTMLCell("", "", "", "", $Separator, 0, 1, 1, true, "J", true);

$pdf->write1DBarcode($TransferColumn->TransactionID , 'C39', '', '', 90, 10, 0.4, '', 'N');
$QRContent = 'CST-'.$TransferColumn->TransactionID.'-Date:  '.$Application_Date.'-Buyers:  '.$Buyers;
$pdf->write2DBarcode($QRContent, 'QRCODE,H', 170, "", 30, 30, $style, 'N');


$pdf->lastPage();


ob_clean();

$pdf->Output(FCPATH.'/Temp/'.$TransferColumn->TransactionID .'-ConsentTransfer.pdf', 'FD');
force_download(FCPATH.'/Temp/'.$TransferColumn->TransactionID .'-ConsentTransfer.pdf', NULL);
unlink(FCPATH.'/Temp/'.$TransferColumn->TransactionID .'-ConsentTransfer.pdf');end_ob_clean();


endforeach;
endif;

//============================================================+
// END OF FILE
//============================================================+
