<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
<!-- begin #content -->
<script type="text/javascript">
    var step = <?php echo array_key_exists('step', $input) ? $input['step'] : -1; ?>;
    var inlineJs = {
        baseUrl: "<?php echo base_url(); ?>"
    }
</script>
<div id="content" class="content">
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <?php
                    if (isset($sessioned_action) && ($sessioned_action == 'updating')) {
                        $amount = number_format($sessioned_balance);
                    } else {
                        $amount = $input['step'] == 1 ? $convenience_model->ServiceAmount : number_format($sessioned_consentrequest['AmountPayable']);
                    }//E# if else statement
                    ?>
                    <h4 class="panel-title"><span class="pull-left">REGISTRATION OF A CONSENT <?php echo $sessioned_consentrequest ? 'FOR LR NUMBER' : '' ?> <b><?php echo $sessioned_consentrequest ? $sessioned_consentrequest['LRNumber'] : '' ?></b></span><span data-toggle="tooltip" data-placement="top" title="After reviewing your application, kindly pay KES <?php echo $amount; ?>" class="pull-right label label-danger">KES <?php echo $amount; ?></span></h4>
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <div id="wizard">
                        <ol class="bwizard-steps clearfix clickable" role="tablist">
                            <li role="tab" aria-selected="true" class="<?php echo ($input['step'] == 1) ? 'active' : ''; ?>" style="z-index: 4;"><span class="label badge-inverse">1</span><a>
                                    Property details
                                </a></li>
                          
                            <li role="tab" aria-selected="false"class="<?php echo ($input['step'] == 2) ? 'active' : ''; ?>" style="z-index: 3;"><span class="label">2</span><a>
                                    Consent Request Details

                                </a></li>
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 3) ? 'active' : ''; ?>" style="z-index: 2;"><span class="label">3</span><a>
                                    Review Consent Request Application
                                </a></li>
                            <li role="tab" aria-selected="false" class="<?php echo ($input['step'] == 4) ? 'active' : ''; ?>" style="z-index: 1;"><span class="label">4</span><a>
                                    Payment 
                                </a></li>
                        </ol>
                        <?php if ($input['step'] == 1): ?>
                            <!-- begin wizard step-1 -->
                            <div class="wizard-step-1">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the first step of 4 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you have the property details, applicant id number, caution interest and the reason for caution</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-8" style="padding-top: 38px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Check Availability</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open('consentrequest/check_property_details', array('data-parsley-validate' => 'true', 'name' => 'form-wizard',)); ?>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        Registry
                                                                    </label>
                                                                    <select name="RegistryID" id="RegistryID" class="form-control" required>
                                                                        <option value=""></option>

                                                                        <?php
                                                                        $GetRegistries = $this->ParcelsValuationModel->GetRegistries();

                                                                        if (is_array($GetRegistries)) {
                                                                            foreach ($GetRegistries as $Registry) {
                                                                                ?>
                                                                                <option value="<?= $Registry->RowID; ?>"><?= $Registry->Name; ?></option>
                                                                                <?php
                                                                            }
                                                                        } else {
                                                                            ?>
                                                                            <option value="0">No data</option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label>Registration Section</label>
                                                                        <select name="RegistrationSectionID" onchange="selectOption()" id="RegistrationSectionID" class="form-control" data-parsley-group="wizard-step-1" required/>
                                                                        <option></option>
                                                                        <?php
                                                                        $GetRegistrationSections = $this->Registrymodel->GetRegistrationSections();

                                                                        if (is_array($GetRegistrationSections)) {
                                                                            foreach ($GetRegistrationSections as $Section) {
                                                                                ?>
                                                                                <option value="<?= $Section->RowID; ?>"><?= $Section->Name; ?></option>
                                                                                <?php
                                                                            }
                                                                        } else {
                                                                            ?>
                                                                            <option value="0">No data</option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        Parcel Number
                                                                    </label>
                                                                    <input type="text"  placeholder="Parcel Number" class="form-control" id="ParcelNo" name="ParcelNo" value="<?php echo $sessioned_consentrequest ? $sessioned_consentrequest['ParcelNumber'] : ''; ?>" required>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label class="control-label">
                                                                        Tenure
                                                                    </label>
                                                                    <select name="TenureID" id="TenureID" class="form-control" required>
                                                                        <option value=""></option>

                                                                        <?php
                                                                        //$GetRegistries = $this->ParcelsValuationModel->GetRegistries();
                                                                        $Tenure = $this->TenureModel->GetAllTenure();
                                                                        if (is_array($Tenure)) {
                                                                            foreach ($Tenure as $SingleTenure) {
                                                                                ?>
                                                                                <option value="<?= $SingleTenure->RowID; ?>"><?= $SingleTenure->Name; ?></option>
                                                                                <?php
                                                                            }
                                                                        } else {
                                                                            ?>
                                                                            <option value="0">No data</option>
                                                                            <?php
                                                                        }
                                                                        ?>

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-9">
                                                                    <div class="form-group connected-group">

                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <label class="control-label">
                                                                                    Title Number
                                                                                </label>
                                                                                <input type="text"  placeholder="Title Number" class="form-control" id="LRNumber" name="LRNumber" value="<?php echo $sessioned_consentrequest ? $sessioned_consentrequest['LRNumber'] : ''; ?>"  disabled>
                                                                            </div>

                                                                        </div>
                                                                    </div>




                                                                </div>




                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-">
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Check Property and continue <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>
                            </div>
                            <!-- end wizard step-1 -->
                            <!-- begin wizard step-2 -->
                                              
                          <?php elseif ($input['step'] == 2) : ?>
                            <div class="wizard-step-2">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the second step of 4 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-8" style="padding-top: 38px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Consent Request Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open_multipart('consentrequest/rate_clearance', array('data-parsley-validate' => 'true', 'name' => 'form-wizard')); ?>
                                                                                                                 
                                                            <div class="row">

                                                              <div class="col-md-12">
															  <div class="col-md-6">
                                                                    <label class="control-label">
                                                                       Consent Request Type
                                                                    </label>
                                                                    <select name="ConsenttypeID" id="ConsenttypeID" class="form-control" required>
                                                                        <option value=""></option>
																		
																		 <?php
                                                                        //$GetRegistries = $this->ParcelsValuationModel->GetRegistries();
                                                                        $Consenttype = $this->ConsenttypeModel->GetAllConsent();
                                                                        if (is_array($Consenttype)) {
                                                                            foreach ($Consenttype as $SingleConsenttype) {
                                                                                ?>
                                                                                <option value="<?= $SingleConsenttype->RowID; ?>"><?= $SingleConsenttype->Name; ?></option>
                                                                                <?php
                                                                            }
                                                                        } else {
                                                                            ?>
                                                                            <option value="0">No data</option>
                                                                            <?php
                                                                        }
                                                                        ?>

                                                                    </select>
                                                                </div>

                                                                
                                                                 <div class="col-md-6">
                                                                   <label class="control-label">
                                                                         Amount
                                                                        </label>
                                                                     <div class="form-group">
                                                                      <input type="text"  placeholder="Amount" class="form-control" id="Amount" name="Amount" value="<?php echo $sessioned_consentrequest ? $sessioned_consentrequest['Amount'] : ''; ?>" required>
                                                                     </div>
                                                                    </div>
                                                               </div>
                                                            </div>
															

                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <p>
                                                                        *Terms &amp; Conditions.
                                                                    </p>
                                                                </div>
                                                                <!--  <div class="col-md-">
                                                                     <button class="btn btn-sm btn-success" type="submit">
                                                                         Save progress <i class="fa fa-arrow-circle-right"></i>
                                                                     </button>
                                                                 </div> -->

                                                                <div class="col-md-10">
                                                                    <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/consentrequest/apply_consentrequest?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                    <button class="btn btn-sm btn-success" type="submit">
                                                                        Continue <i class="fa fa-arrow-circle-right"></i>
                                                                    </button>
                                                                </div>


                                                            </div>
                                                            <?php echo form_close(); ?>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- end row -->
                                </fieldset>

                            </div>
                    <?php elseif ($input['step'] == 3): ?>
                            <div class="wizard-step-3">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col-md-4">

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">

                                                    <h4 class="panel-title">Notes </h4>
                                                </div>
                                                <div class="panel-body bg-blue text-white">
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>This is the third step of 4 to complete your application. </li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Ensure you're using your account. If not, your application will be rejected.</li><br/>
                                                    <li><i class="fa-li fa fa-spinner fa-spin"></i>Not to forge any details.</li><br/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8" style="padding-top: 38px; overflow-y:scroll;height: 300px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Review your Consent application</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="panel_edit_account" class="tab-pane in active">
                                                        <?php echo form_open_multipart('consentrequest/review_application', array('class' => 'margin-bottom-0', 'id' => 'RegisterUser')); ?>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <table class="table table-bordered table-hover">
                                                                    <tr>
                                                                        <td>Registry</td>
                                                                        <td><?php echo $registry_model ? $registry_model->Name : ''; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Registration Section</td>
                                                                        <td><?php echo $registration_section_model ? $registration_section_model->Name : ''; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Parcel Number</td>
                                                                        <td><?php echo $sessioned_consentrequest['ParcelNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>LR Number</td>
                                                                        <td><?php echo $sessioned_consentrequest['LRNumber']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Tenure</td>
                                                                        <td><?php echo $tenure_model ? $tenure_model->Name : ''; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Consent Interest</td>
                                                                        <td><?php echo $sessioned_consentrequest['ConsentInterest']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Consent description</td>
                                                                        <td><?php echo $sessioned_consentrequest['ConsentDescription']; ?></td>
                                                                    </tr>
                                                                     <tr>
                                                                        <td>Consent Document</td>
                                                                        <td><?php echo $sessioned_consentrequest['ConsentDocument']; ?></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>Consent Amount</td>
                                                                        <td>KES <?php echo $sessioned_consentrequest['AmountPayable']; ?></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <p>
                                                                    *Terms &amp; Conditions.
                                                                </p>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/consentrequest/apply_consentrequest?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                                <button class="btn btn-sm btn-success" type="submit">
                                                                    Make payment <i class="fa fa-arrow-circle-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>
                            </div> 

                        <?php elseif ($input['step'] == 4) : ?>
                            <div class="wizard-step-4">
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="panel panel-default panel-blog panel-checkout">
                                            <div class="panel-body">
                                                <ul class="blog-meta mb5">
                                              
                                                </ul>
                                                <?php if (array_key_exists('step', $input) && $input['step'] == 4): ?>
                                                    <div style="background-color: #FFF;">
                                                        <?php
                                                        $apiClientId = '13';
                                                        $amount = $sessioned_consentrequest['AmountPayable'];
                                                        // $amount = '10';
                                                        $serviceId = '29';
                                                        $idNumber = $sessioned_user['IDNumber'];
                                                        $currency = 'KES';
                                                        $billRef = $sessioned_consentrequest['EcitizenBillRef'];
                                                        $serviceDesc = $sessioned_consentrequest['Description'];
                                                        $clientName = $sessioned_user['FirstName'] . ' ' . $sessioned_user['LastName'];
                                                        $key = 'TYV0FUD0BY';
                                                        $secret = 'BGI0ZTSPL2WX0X6';

                                                        $payload = "{$apiClientId}{$amount}{$serviceId}{$idNumber}{$currency}{$billRef}{$serviceDesc}{$clientName}{$secret}";
                                                        $hash = base64_encode(hash_hmac("sha256", $payload, $key));
                                                        ?>
                                                        <form id="idPaymentForm" method="post" action="https://pesaflow.ecitizen.go.ke/PaymentAPI/iframev2.1.php" target="my_frame">
                                                            <input type="hidden" name="apiClientID" value="13" />
                                                            <input type="hidden" name="secureHash" value="<?php echo $hash; ?>" />
                                                            <input type="hidden" name="billDesc" value="<?php echo $sessioned_consentrequest['Description']; ?>" />
                                                            <input type="hidden" name="billRefNumber" value="<?php echo $billRef; ?>" />
                                                            <input type="hidden" name="currency" value="KES" />
                                                            <input type="hidden" name="serviceID" value="29" />
                                                            <input type="hidden" name="clientMSISDN" value="<?php echo $sessioned_user['Phone']; ?>" />
                                                            <input type="hidden" name="clientName" value="<?php echo $clientName; ?>" />
                                                            <input type="hidden" name="clientIDNumber" value="<?php echo $sessioned_user['IDNumber']; ?>" />
                                                            <input type="hidden" name="clientType" value="citizen" />
                                                            <input type="hidden" name="clientEmail" value="<?php echo $sessioned_user['Email']; ?>" />
                                                            <input type="hidden" name="callBackURLOnSuccess"  value="<?php echo base_url(); ?>/consentrequest/payment_completed?bill_ref=<?php echo $billRef; ?>" />
                                                            <input type="hidden" name="callBackURLOnFail"  value="<?php echo base_url(); ?>/consentrequest/apply_consentrequest?step=4" />
                                                            <input type="hidden" name="notificationURL"  value="<?php echo base_url(); ?>/cconsentrequest/apply_consentrequest?step=4" />
                                                            <input type="hidden" name="amountExpected" value="<?php echo $amount; ?>" />
                                                        </form>
                                                        <iframe width="100%" height="600px" style="border: none;" name="my_frame"></iframe>
                                                        <script type="text/javascript">
                                                            document.getElementById('idPaymentForm').submit();
                                                        </script>
                                                    </div>
                                                    <a class="btn btn-sm btn-warning" href="<?php echo base_url() . '/consentrequest/apply_consentrequest?step=' . ($input['step'] - 1); ?>">Previous</a>
                                                <?php endif; ?>
                                            </div><!-- panel-body -->
                                        </div>
                                    </div>

                                    <!-- end row -->
                                </fieldset>
                            </div>
                        <?php endif; ?>
                        <!-- end wizard step-5 -->


                    </div>

                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<!-- end #content -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/parsley/dist/parsley.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-wizards-validation.demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/apps.min.js?time=<?php echo time(); ?>"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<script></script>
<script>
                                                            $("#County").change(function (evt) {
                                                                var ActID = document.getElementById('Acts').value;
                                                                var CountyID = document.getElementById('County').value;
                                                                var select = $("#County");
                                                                var selectData = select.serialize();
                                                                var ajax = $.ajax({
                                                                    dataType: "json",
                                                                    type: 'POST',
                                                                    url: "<?php echo base_url(); ?>Transfer/GetRegistriesMatch/" + ActID + "/" + CountyID,
                                                                    data: selectData
                                                                });
                                                                ajax.done(function (response) {
                                                                    $("#testurl").html(response);
                                                                    var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                                    for (var key in response) {
                                                                        if (response.hasOwnProperty(key)) {
                                                                            //alert(key);
                                                                            //alert(response[key].RowID);
                                                                            //alert(response[key].NameOfRoad);
                                                                            listItems += "<option value='" + response[key].Registres + "'>" + response[key].Registries + "</option>";
                                                                        }
                                                                    }
                                                                    $("#Registry").html(listItems);
                                                                });
                                                                ajax.fail(function () {

                                                                });
                                                            });

                                                            $("#towntype").change(function (evt) {
                                                                var townTypeID = document.getElementById('towntype').value;
                                                                var select = $("#towntype");
                                                                var selectData = select.serialize();
                                                                var ajax = $.ajax({
                                                                    dataType: "json",
                                                                    type: 'POST',
                                                                    url: "<?php echo base_url(); ?>start/GetTownNamesMatchTownType/" + townTypeID,
                                                                    data: selectData
                                                                });
                                                                ajax.done(function (response) {
                                                                    var listItems = '<option selected="selected" value="0">- Select -</option>';
                                                                    for (var key in response) {
                                                                        if (response.hasOwnProperty(key)) {
                                                                            //alert(key);
                                                                            //alert(response[key].RowID);
                                                                            //alert(response[key].NameOfRoad);
                                                                            listItems += "<option value='" + response[key].NameOfTown + "'>" + response[key].NameOfTown + "</option>";
                                                                        }
                                                                    }
                                                                    $("#townNames").html(listItems);
                                                                });
                                                                ajax.fail(function () {

                                                                });
                                                            });

</script>

<script>
    $(document).ready(function () {
        App.init();
        //FormWizardValidation.init();
    });
</script>


<script>

    // $('#advoNationality option').each(function () {
    //     $(this).attr('value', $(this).text());
    // });

</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>