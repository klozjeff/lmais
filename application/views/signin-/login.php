<div id="page-container" class="fade">
    <!-- begin login -->
    <div class="login login-with-news-feed">
        <!-- begin news-feed -->

        <!-- end news-feed -->
        <?php if (isset($input) && array_key_exists('otp_view', $input) && $input['otp_view'] == 'otp'): ?>
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-5">
                    <div class="">
                        <!-- begin login-header -->
                        <div class="login-header">
                            <div class="brand">
                                <span class="logo"></span> LMAIS
                                <small>Central Login Page</small>
                            </div>
                            <div class="icon">
                                <i class="fa fa-sign-in"></i>
                            </div>
                        </div>
                        <!-- end login-header -->
                        <!-- begin login-content -->
                        <div class="login-content">
                            <?php echo $this->session->flashdata('loginmsg'); ?>
                            <?php echo form_open("start/confirm_otp"); ?>                        
                            <div class="form-group m-b-20">
                                <input type="text" class="form-control input-lg" name="SmsOtp" placeholder="Enter SMS code" required />
                            </div>
                            <div class="login-buttons">
                                <button type="submit" class="btn btn-success btn-block btn-lg">Login</button>
                            </div>
                            </form>
                            <?php if (isset($input) && array_key_exists('user_id', $input)): ?>

                                <?php echo form_open("start/resend_otp"); ?>
                                <div class="login-buttons" style="margin-top: 10px">
                                    <input type="hidden" name="user_id" value="<?php echo $input['user_id']; ?>"/>
                                    <button type="submit" class="btn btn-warning btn-block btn-lg">Resend code</button>
                                </div>
                                </form>
                            <?php endif; ?>
                            <div class="m-t-20">
                                Go back <a href="<?php echo base_url(); ?>">here</a> to go back to login page.
                            </div>

                        </div>
                        <!-- end login-content -->
                    </div>
                </div>
                <div class="col-md-4">
                </div>
            </div>
        <?php else: ?>
            <!-- begin right-content -->
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-5">
                    <div class="">
                        <!-- begin login-header -->

                        <div class="login-header">
                            <div class="brand">
                                <span class="logo"></span> LMAIS
                                <small>Central Login Page</small>
                            </div>
                            <div class="icon">
                                <i class="fa fa-sign-in"></i>
                            </div>
                        </div>
                        <!-- end login-header -->
                        <!-- begin login-content -->
                        <div class="login-content">
                            <?php echo $this->session->flashdata('loginmsg'); ?>
                            <?php echo form_open("start/LoginUsers"); ?>
                            <div class="form-group m-b-20">
                                <input type="text" class="form-control input-lg" name="IDNumber" placeholder="IDNumber" required />
                            </div>
                            <div class="form-group m-b-20">
                                <input type="password" class="form-control input-lg" name="Password" placeholder="Password" required />
                            </div>
                            <div class="checkbox m-b-20">
                                <label>
                                    <input type="checkbox" /> Remember Me
                                </label>
                            </div>
                            <div class="login-buttons">
                                <button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
                            </div>
                            <div class="m-t-20">
                                Forgot Password? Click <a href="<?php echo base_url(); ?>start/reset_password_form">here</a> to reset password.
                            </div>
                            </form>
                        </div>
                        <!-- end login-content -->
                    </div>
                </div>
                <div class="col-md-4">
                </div>
            </div>
            <!-- end right-container -->

        <?php endif; ?>
    </div>
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/js/login-v2.demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function () {
        App.init();
        LoginV2.init();
    });
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
