-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.19-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5144
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table lmais.lmais_consenttype
CREATE TABLE IF NOT EXISTS `lmais_consenttype` (
  `RowID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `DateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `S_ROWID` (`RowID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table lmais.lmais_consenttype: ~4 rows (approximately)
/*!40000 ALTER TABLE `lmais_consenttype` DISABLE KEYS */;
INSERT INTO `lmais_consenttype` (`RowID`, `Name`, `Description`, `CreatedBy`, `DateCreated`) VALUES
	(1, 'To Lease', 'To Lease', 'admin', '0000-00-00 00:00:00'),
	(2, 'Sub Lease', 'Sub Lease', 'admin', '0000-00-00 00:00:00'),
	(3, 'Transfer', 'Transfer', 'admin', '0000-00-00 00:00:00'),
	(4, 'Charge/Further Charge', 'Charge/Further Charge', 'admin', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `lmais_consenttype` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
