-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.19-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5144
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table lmais.lmais_consentrequest
CREATE TABLE IF NOT EXISTS `lmais_consentrequest` (
  `RowID` int(11) NOT NULL AUTO_INCREMENT,
  `TransactionID` varchar(20) NOT NULL,
  `RegistrationSectionID` int(255) NOT NULL,
  `RegistryID` int(255) NOT NULL,
  `ParcelNumber` int(255) NOT NULL,
  `TenureID` int(255) NOT NULL,
  `LRNumber` varchar(255) NOT NULL,
  `DocID` varchar(255) NOT NULL,
  `AmountPayable` int(255) NOT NULL,
  `UserID` int(255) NOT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `ConsentInterest` varchar(255) NOT NULL,
  `ConsentDocument` varchar(255) NOT NULL,
  `ConsentDescription` varchar(255) NOT NULL,
  `Bank` varchar(255) NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ConvenienceFee` float(12,0) NOT NULL,
  `ConsentrequestFee` float(12,0) NOT NULL,
  `Agent` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Ip` varchar(255) CHARACTER SET utf8 NOT NULL,
  `EcitizenBillRef` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Status` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ConsenttypeID` varchar(50) NOT NULL,
  `Amount` varchar(255) NOT NULL,
  PRIMARY KEY (`RowID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- Dumping data for table lmais.lmais_consentrequest: ~7 rows (approximately)
/*!40000 ALTER TABLE `lmais_consentrequest` DISABLE KEYS */;
INSERT INTO `lmais_consentrequest` (`RowID`, `TransactionID`, `RegistrationSectionID`, `RegistryID`, `ParcelNumber`, `TenureID`, `LRNumber`, `DocID`, `AmountPayable`, `UserID`, `Description`, `ConsentInterest`, `ConsentDocument`, `ConsentDescription`, `Bank`, `DateCreated`, `ConvenienceFee`, `ConsentrequestFee`, `Agent`, `Ip`, `EcitizenBillRef`, `Status`, `ConsenttypeID`, `Amount`) VALUES
	(20, 'LS-TRA00D0C2Q', 2, 6, 236, 1, 'KILIFI/JILORE/236', '8444', 10, 39, 'Consent Request', 'rere', 'rails_logo6.png', 'ewewe', '', '2017-04-15 15:55:52', 5, 5, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '154.70.33.83', 'consentrequest_20', 'paid', '1', ''),
	(21, 'LS-TRA00D0C2R', 2, 6, 236, 1, 'KILIFI/JILORE/236', '8444', 10, 39, 'Consent Request', 'safdsafsd', 'rails_logo7.png', 'qwreqrwq', '', '2017-04-15 16:14:12', 5, 5, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '154.70.33.83', 'consentrequest_21', 'unpaid', '3', ''),
	(22, 'LS-TRA00D0C2U', 6, 3, 1048, 1, 'GATAMAIYU/KAGAA/1048', '7102', 10, 29, 'Consent Request', '1234556', 'tda3.jpg', '54515615', '', '2017-04-16 02:32:38', 5, 5, 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', '197.181.56.118', 'consentrequest_22', 'unpaid', '1', ''),
	(23, '', 2, 6, 236, 1, 'KILIFI/JILORE/236', '8444', 10, 39, 'Consent Request', '', '', '', '', '2017-04-16 14:04:51', 5, 5, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '197.183.114.247', 'consentrequest_23', 'unpaid', '', ''),
	(24, '', 2, 6, 236, 1, 'KILIFI/JILORE/236', '8444', 10, 39, 'Consent Request', '', '', '', '', '2017-04-16 14:36:06', 5, 5, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '197.183.114.247', 'consentrequest_24', 'unpaid', '1', 'wq21321'),
	(25, 'LS-TRA00D0C2Z', 2, 6, 236, 1, 'KILIFI/JILORE/236', '8444', 10, 29, 'Consent Request', '', '', '', '', '2017-04-16 17:50:17', 5, 5, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '197.183.114.247', 'consentrequest_25', 'unpaid', '4', '500'),
	(26, '', 6, 3, 1048, 1, 'GATAMAIYU/KAGAA/1048', '7102', 10, 29, 'Consent Request', '', '', '', '', '2017-04-16 17:51:06', 5, 5, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '197.183.114.247', 'consentrequest_26', 'unpaid', '', '');
/*!40000 ALTER TABLE `lmais_consentrequest` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
